<?php 
function webnus_ourcurriculum( $atts ) {
	extract(shortcode_atts(array(
		'type' 					=> '1',
		'step_title' 			=> '',
		'step_number' 			=> '',
		'course_name' 			=> '',
		'course_description' 	=> '',
	), $atts ));


	
	$step_title 		= $step_title ? '<h5>'. $step_title .'</h5>' : '' ;
	$step_number		= $step_number ? '<h3>'. $step_number .'</h3>' : '' ;
	$course_name		= $course_name ? '<h6>'. $course_name .'</h6>' : '' ;
	$course_description	= $course_description ? '<p>'. $course_description .'</p>' : '' ;

	$out= '<div class="our-curriculum curriculum-'. $type .'">
				<img src="' . get_template_directory_uri() . '/images/pin.png">
				<div class="our-curriculum-header"></div>
				<div class="our-curriculum-content-wrap">
					<div class="oc-header"> '. $step_title . $step_number .'</div>
					<div class="oc-content">'. $course_name . $course_description .'</div>
					<div class="our-curriculum-footer"></div>
				</div>
				
			</div>';
	return $out;
}
add_shortcode( 'ourcurriculum','webnus_ourcurriculum' );