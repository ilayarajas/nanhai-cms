<div class="wrap">
	<h1><?php _e('Add/Edit User Badge', 'watuproplay')?></h1>
	
	<form method="post" class="watupro" onsubmit="return validateBadgeForm(this);">
	<div class="wp-admin watupro-padded postbox">
		<fieldset>
			<legend><h2><?php _e('Badge Details', 'watupro')?></h2></legend>
			<p><label><?php _e('Badge name:', 'watuproplay')?></label> <input type="text" name="name" value="<?php echo @$badge->name?>" size="30"></p>					
			<p><label><?php _e('Badge Content or Graphic', 'watuproplay')?></label> <?php wp_editor(stripslashes(@$badge->content), 'content')?></p>
			
			<p class="watupro-help"><?php _e('Feel free to simply upload and place a badge graphic in the box. You can also just design it in the editor or HTML.', 'watuproplay')?></p>
			
				<p><input type="checkbox" name="is_open_badge" value="1" <?php if(!empty($badge->is_open_badge)) echo 'checked'?> onclick="this.checked ? jQuery('#openBadgeSettings').show() : jQuery('#openBadgeSettings').hide();"> <?php _e("Issue as <a href='http://openbadges.org/' target='_blank'>Mozilla Open Badge</a>", 'watuproplay')?></p>
				<div id="openBadgeSettings" style="display: <?php echo empty($badge->is_open_badge) ? 'none' : 'block'?>">
					<p><a href="admin.php?page=watuproplay_open_badge_settings" target="_blank"><?php _e('[Manage Your Issuer Settings]', 'watuproplay');?></a></p>			
					<p><label><?php _e('Badge graphic URL <br>(must be .png file):', 'watuproplay')?></label> <input type="text" name="badge_graphic" value="<?php echo @$badge->badge_graphic?>" size="60"></p>
					<p><label><?php _e('Badge criteria URL:<br>(required by specs.)', 'watuproplay')?></label> <input type="text" name="badge_criteria_url" value="<?php echo @$badge->badge_criteria_url?>" size="60"></p>
					<p><?php _e('When such badge is awarded the user will see an "Add to Mozilla Backpack" link under the badge in their "Badges and rewards" page. Note that you need to enable this page in the <a href="admin.php?page=watupro_play">settings page.</a>', 'watuproplay');?></p>
				</div>
			
			<?php if(function_exists('badgeos_get_achievements')):?>	
				<p><input type="checkbox" name="is_badgeos_badge" value="1" <?php if(!empty($badge->is_badgeos_badge)) echo 'checked'?> onclick="this.checked ? jQuery('#badgeosBadgeSettings').show() : jQuery('#badgeosBadgeSettings').hide();"> <?php printf(__("Issue as <a href='%s' target='_blank'>Badge OS</a> Achievement", 'watuproplay'), 'http://badgeos.org')?></p>	
				
				<div id="badgeosBadgeSettings" style="display: <?php echo empty($badge->is_badgeos_badge) ? 'none' : 'block'?>">
					<?php _e('Enter achievement ID:', 'watuproplay');?> <input type="text" name="badgeos_id" value="<?php echo @$badge->badgeos_id?>" size="4">
				</div>
			<?php endif;?>
			
			<?php if(class_exists('myCRED_Badge_Module')):?>
				<p><input type="checkbox" name="is_mycred_badge" value="1" <?php if(!empty($badge->is_mycred_badge)) echo 'checked'?> onclick="this.checked ? jQuery('#mycredBadgeSettings').show() : jQuery('#mycredBadgeSettings').hide();"> <?php printf(__("Issue as <a href='%s' target='_blank'>myCRED</a> Badge", 'watuproplay'), 'http://mycred.me')?></p>	
				<div id="mycredBadgeSettings" style="display: <?php echo empty($badge->is_mycred_badge) ? 'none' : 'block'?>">
					<select name="mycred_id">
						<?php foreach($mycred_badges as $mycred):
							$selected = (!empty($badge->mycred_id) and $badge->mycred_id == $mycred->ID) ? ' selected': '';?>
							<option value="<?php echo $mycred->ID?>"<?php echo $selected?>><?php echo stripslashes($mycred->post_title);?></option>
						<?php endforeach;?>
					</select>
				</div>	
			<?php endif;?>
		</fieldset>
		<fieldset>
			<legend><h2><?php _e('Badge Requirements', 'watuproplay')?></h2></legend>
			<p class="wtupro-help"><?php _e('You can mix multiple requirements for a single badge in any way you want', 'watuproplay')?></p>
			
			<p><label><?php _e('Required total points:', 'watuproplay')?></label> <input type="text" name="required_points" value="<?php echo @$badge->required_points?>" size="6">
			<?php _e('from question category:', 'watuproplay');?> <select name="points_cat_id">
				<option value="0" <?php if(empty($badge->points_cat_id)) echo 'selected'?>><?php _e('All categories', 'watuproplay');?></option>
				<?php foreach($qcats as $cat):?>
					<option value="<?php echo $cat->ID?>" <?php if(@$badge->points_cat_id == $cat->ID) echo 'selected'?>><?php echo stripslashes(apply_filters('watupro_qtranslate', $cat->name));?></option>
				<?php endforeach;?>
			</select>
			<input type="checkbox" name="use_latest_points" value="1" <?php if(!empty($badge->use_latest_points)) echo 'checked'?>> <?php _e('Consider only latest attempt on each quiz', 'watuproplay');?></p>
			
			<p><label><?php _e('Required avg. % correct answers from all tests:', 'watuproplay')?></label> <input type="text" name="required_percent" value="<?php echo @$badge->required_percent?>" size="4">% 
			<?php _e('from question category:', 'watuproplay');?> <select name="percent_cat_id">
				<option value="0" <?php if(empty($badge->percent_cat_id)) echo 'selected'?>><?php _e('All categories', 'watuproplay');?></option>
				<?php foreach($qcats as $cat):?>
					<option value="<?php echo $cat->ID?>" <?php if(@$badge->percent_cat_id == $cat->ID) echo 'selected'?>><?php echo stripslashes(apply_filters('watupro_qtranslate', $cat->name));?></option>
				<?php endforeach;?>
			</select>			
			<?php _e("(This considers only last user's attempt on each quiz)", 'watuproplay');?></p>
			
			<p><label><?php _e('Required number unique tests submitted:', 'watuproplay')?></label> <input type="text" name="required_num_exams" value="<?php echo @$badge->required_num_exams?>" size="4"></p>
			
			<p><label><?php _e('Must take all exams from:', 'watuproplay')?></label> <select name="required_category">
			<option value="0" <?php if(empty($badge->required_category)) echo "selected"?>><?php _e('- Not applicable -', 'watuproplay')?></option>
			<?php foreach($cats as $cat):?>
				<option value="<?php echo $cat->ID?>" <?php if(!empty($badge->id) and $cat->ID == $badge->required_category) echo 'selected'?>><?php echo $cat->name?></option>
			<?php endforeach;?>			
			</select> <?php _e('category', 'watuproplay')?></p>
		</fieldset>	
		
		<p align="center"><input type="submit" value="<?php _e('Save Badge', 'watuproplay')?>"> 
		<?php if(!empty($badge->id)):?>
		<input type="button" value="<?php _e('Delete Badge', 'watuproplay')?>" onclick="watuproplayDeleteBadge(this.form);">
		<input type="hidden" name="del" value="0">
		<?php endif;?></p>
	
	</div>
	<input type="hidden" name="ok" value="1">
	</form>
</div>

<script type="text/javascript" >
function validateBadgeForm(frm) {
	if(frm.name.value=='') {
		alert("<?php _e('Please enter name for this badge', 'watuproplay')?>");
		frm.name.focus();
		return false;
	}
	
	return true;
}

function watuproplayDeleteBadge(frm) {
	if(confirm("<?php _e('Are you sure?', 'watuproplay')?>")) {
		frm.del.value=1;
		frm.submit();
	}
}
</script>