<?php
	// Build Shortcode
	add_action('wp_ajax_it_shortcode_builder_vc', 'it_shortcode_builder_vc');

	function it_shortcode_builder_vc()
	{
		//Convert String of post date to separate index
		parse_str($_POST['postdata'], $my_array_of_vars);		
		$shortcode = '[it_logo_showcase';
		foreach($my_array_of_vars as $field_name => $value)
		{
			$name = str_ireplace(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ ,'' ,$field_name);
            if($name == 'category')
            {
                $arr = $value;
                $value = implode(',',$arr);
            }
            if($value != 'none' && $value != '' && $value != '0')
    		    $shortcode .= ' '.$name.'="'.$value.'" ';
		}
		$shortcode .= ']';
		echo $shortcode;
        wp_die();
	}

	// quick view
	add_action('wp_ajax_it_quick_view_get_vc', 'it_quick_view_get_vc');
    add_action('wp_ajax_nopriv_it_quick_view_get_vc', 'it_quick_view_get_vc');
	function it_quick_view_get_vc()
    {
        $html='';
		$post_id = $_GET['id'];
        extract($_GET);
		
        /*if(!wp_verify_nonce( $nonce, 'it_logoshowcase_nonce' ) )
        {
            $arr = array(
                'success'=>'no-nonce',
                'products' => array()
            );
            print_r($arr);
            die();
        }*/
		//die(print_r($item_details)."ali");
        $title_str = $item_details['title'];
		$image_url = wp_get_attachment_image_src( $item_details['image'], 'full');
		$image_url = $image_url[0];
        // meta
        $link_url = $item_details['link_url'];
        $link_target = $item_details['link_target'];
        $full_desc_str = $item_details['full_description'];
		
        $website = $item_details['website'];
        $email = $item_details['email'];
        $tell = $item_details['tell'];
        $facebook = $item_details['facebook'];
        $twitter = $item_details['twitter'];
        $instagram = $item_details['instagram'];
        $linked_in = $item_details['linked_in'];
        $google_plus = $item_details['google_plus'];
        $gallery_str = $item_details['gallery_image'];
        $video = $item_details['video'];
        $video_url = ($video != '') ? wp_get_attachment_link($video) : false;

        if($link_url != '')
            $image_box = '<a href="'.$link_url.'" target="'.$link_target.'">
                            <img src="'.$image_url.'" class="ls-zoomin" />
                        </a>';
        else
            $image_box = '<img src="'.$image_url.'" class="ls-zoomin" />';
        // show title
        if ($title != 'true')
            $title_str ='';
        // for inline
        $html .= '<div class="ls-grid-m-12 ls-grid-t-12 ls-grid-d-12 ls-inline-box-' . $rand_id . '" id="show-inline-box"  >
            <div class="ls-showdetail" style="display: none" >
                <div data-remodal-action="close" class="ls-showdetail-close" aria-label="Close">
                    <i class="fa fa-times"></i>
                </div>
                <div class="ls-grid-m-12 ls-grid-d-6">
                    <div class="ls-showdetail-logo">
                        '.$image_box.'
                    </div>
                    <div class="ls-showdetail-title">'.$title_str.'</div>';
        if($contact == 'true' && $website != '')
            $html.= '<div class="ls-showdetail-contact">
                        <i class="fa fa-link"></i>
                        <a href="'.$website.'">'.$website.'</a>
                    </div>';
        if($contact == 'true' && $email != '')
            $html.= '
                    <div class="ls-showdetail-contact">
                        <i class="fa fa-envelope"></i>
                        <a href="mailto:'.$email.'">'.$email.'</a>
                    </div>';
        if($contact == 'true' && $tell != '')
            $html.= '
                    <div class="ls-showdetail-contact">
                        <i class="fa fa-phone"></i>
                        <span>'.$tell.'</span>
                    </div>';
        if($social == 'true') {
            $html .= '<div class="ls-socials">';
            if($facebook != '')
                $html .= '<a href="'.$facebook.'"><i class="fa fa-facebook"></i></a>';
            if($twitter != '')
                $html .= '<a href="'.$twitter.'"><i class="fa fa-twitter"></i></a>';
            if($google_plus != '')
                $html .= '<a href="'.$google_plus.'"><i class="fa fa-google-plus"></i></a>';
            if($instagram != '')
                $html .= '<a href="'.$instagram.'"><i class="fa fa-instagram"></i></a>';
            if($linked_in != '')
                $html .= '<a href="'.$linked_in.'"><i class="fa fa-linkedin"></i></a>';
            $html .= '</div>';
        }
        $html.='</div>
                <div class="ls-grid-m-12 ls-grid-d-6 ls-content-area">';
        if ($full_desc == 'true' && $full_desc_str != '')
            $html .= '<div class="ls-full-content" >
                        '.do_shortcode($full_desc_str).'
                    </div>';
        if ($gallery == 'true') {
            $html .= '<div class="ls-gallery">';
            $gallery_arr = explode(',', $gallery_str);
            foreach ($gallery_arr as $image_id) {
                $image_url_f = wp_get_attachment_image_src($image_id , 'full');
                $image_url_m = wp_get_attachment_image_src($image_id , 'medium');
                $html .= '<div class="ls-grid-m-12 ls-grid-t-4 ls-grid-d-3">
                             <div class="ls-gallery-thumb">
                                <a href="' . $image_url_f[0] . '" data-lightbox="gallery-'.$post_id.'" >
                                    <img src="' . $image_url_m[0] . '" class="ls-zoomin" />
                                </a>
                             </div>
                          </div>';
            }
            $html .= '</div>';
        }
        $html .= '</div>
            </div>
        </div>';
        echo $html;
        wp_die();
    }
    // ajax shortcode preview
    add_action('wp_ajax_it_preview_vc', 'it_preview_vc');
    function it_preview_vc()
    {
        //Convert String of post date to separate index
        parse_str($_POST['postdata'], $my_array_of_vars);
        $atts = array();
        foreach($my_array_of_vars as $field_name => $value)
        {
            $name = str_ireplace(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ ,'' ,$field_name);
            if($name == 'category')
            {
                $value = implode(',',$value);
            }
            $atts[$name] = $value;
        }
        echo it_preview_vc_shortcode($atts);
        die();
    }
