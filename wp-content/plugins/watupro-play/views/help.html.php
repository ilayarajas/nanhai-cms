<div class="wrap">
	<h1>WatuPRO Play Help</h1>
	
	<p>This page will shortly explain how to use the four main functions of the WatuPRO Play plugin.</p>
	
	<ol>
		<li><a href="#shortcodes">Shortcodes</a></li>
		<li><a href="#overview">User Overview Page</a></li>
		<li><a href="#levels">Levels</a></li>
		<li><a href="#badges">Badges</a></li>
		<li><a href="#rewards">Rewards</a></li>
	</ol>
	
	<a name="shortcodes"></a>
	<h2>Shortcodes</h2>
	
	<p>On the <a href="admin.php?page=watupro_play">main page of the plugin</a> there is shortcode generator for various shortcodes - leaderboards, charts, etc. Once you generate a shortcode for anything you need to copy it and place it in a post or page. These shortcodes do not get saved in the shortcode generator. If you make a change to a shortcode in the generator it will not automatically reflect the page where the old shortcode is published. You need to manually replace the old shortcode with the new one.</p>
	
	<a name="overview"></a>
	<h2>User Overview Page</h2>
	
	<p>At the bottom of the <a href="admin.php?page=watupro_play">same page</a> there is a section that lets you enable and configure a user overview page. This page can show the user their current level, points and badges earned, and rewards. If your users don't access the standard WP dashboard you can use the shortcode to publish the user overview page on the front-end (in a post or page). Any changes you do to this configuration will automatically be reflected - you don't need to re-publish the shortcode.</p>
	
	<a name="levels"></a>
	<h2>Levels</h2>
	
	<p>Each user can have only one level at a time. Levels are typically gradual - for example "Newbie", "Advanced user", "Wizard" etc. For each level you can mix various requirments like points, average % correct answers, etc. When the user meets ALL the requirements, he gains the associated level.</p>
	
	<p>You can also configure the contents of the optional emails sent when a level is earned.</p>
	
	<a name="badges"></a>
	<h2>Badges</h2>
	
	<p>Badges are similar to levels in the sense that they can be assigned to your users upon achieving a number of points, % correct answers, completing all tests from a category etc. Unlike the level, one user can own several badges at the same time. For example "Completed all exams", "Collected minimum 100 points", "Gave over 90% correct answers" etc. Badges usually have some graphical presentation.</p>
	
	<h3>Badges as Open Badges</h3>
	<p>Each badge can be issued as <a href="http://openbadges.org/" target="_blank">Mozilla Open Badge</a>. To do this you need to create the badge in WatuPRO Play and to enter your issuer settings, badge graphic URL and the criteria URL per Open Badge's requirements.</p>
	
	<h3>Badges as BadgeOS Achievements</h3>
	
	<p>Each of the already created badges in WatuPRO Play can also automatically issue a BadgeOS Achievement. All you need to do is to enter the BadgeOS Achievement ID. When the user meets the badge criteria, they will earn both the WatuPRO Play badge and the associated BadgeOS Achievement.</p>
	
	<a name="rewards"></a>
	<h2>Rewards</h2>
	
	<p>Using rewards you can encourage the participation of the users in your site. Rewards can be virtual or tangible items and users can earn them by redeeming the points, collected by taking tests. Each reward has a cost in points earned from completing quizzes. You an also configure optional email notices sent to you and / or the user when a reward is earned.</p>
</div>