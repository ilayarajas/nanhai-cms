<?php

function learning_suite_webnus_ourclass( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'type' 				=> '1',
		'title' 			=> '',
		'image' 			=> '',
		'age' 				=> '',
		'class_size' 		=> '',
		'class_content' 	=> '',
		'link_title' 		=> '',
		'link_url' 			=> '',
		'bgcolor' 			=> '',
		'border_img_color'	=> '',
		'btn_color'			=> '',

	), $atts));
	
	if( is_numeric( $image ) )
		$image = wp_get_attachment_url( $image );
	
	$image 			=  $image ? '<div class="img-wrap col-md-6" style=" border-color: ' . $border_img_color . '; "><img src="' . $image . '" alt=""></div>' : '' ;
	$title 			=  $title ? '<h5>' . $title . '</h5>' : '' ;
	$age 			=  $age ? '<span>' . $age . '</span>' : '' ;
	$class_size 	=  $class_size ? '<span>' . $class_size . '</span>' : '' ;
	$class_content 	=  $class_content ? '<p>' . $class_content . '</p>' : '' ;
	$link_title 	=  $link_title ? '<a class="magicmore" href="' . $link_url . '" style=" background: ' . $btn_color . '; ">' . $link_title . '</a>' : '' ;


	$out  = 
		'<div class="our-clss-type' . $type . ' clearfix" style=" background:' . $bgcolor . ';">
			' . $image . '
			<div class="content-wrap col-md-6">
				 ' . $title . '
				 <div class="main-content">
					 <div class="class-desc">
 						 ' . $age . '
 						 ' . $class_size . '
				 	</div>
				 	' . $class_content . '
				 	' . $link_title . '
				 </div>
			</div>
		</div>';
	
	
	return $out;
}
add_shortcode('ourclass', 'learning_suite_webnus_ourclass');