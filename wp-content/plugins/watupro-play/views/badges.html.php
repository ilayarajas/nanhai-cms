<div class="wrap">
	<h1><?php _e('Manage Badges', 'watuproplay')?></h1>
	
	<div class="watupro-alert">
	<p><?php _e('Badges are similar to levels in the sense that they can be assigned to your users upon achieving a number of points, % correct answers, completing all tests from a category etc. Unlike the level, one user can own several badges at the same time. For example "Completed all exams", "Collected minimum 100 points", "Gave over 90% correct answers" etc. Badges usually have some graphical presentation.', 'watuproplay')?></p>
	</div>
	
	<p><a href="admin.php?page=watuproplay_badges&action=add"><?php _e('Click here to create a new badge', 'watuproplay')?></a></p>
	
	<?php if(sizeof($badges)):?>
		<table class="widefat">
			<tr><th><?php _e('Badge', 'watuproplay')?></th><th><?php _e('Shortcode *', 'watuproplay')?></th>
			<th><?php _e('Achieved by', 'watuproplay')?></th><th><?php _e('Edit', 'watuproplay')?></th></tr>
			<?php foreach($badges as $badge):
				$class = ('alternate' == @$class) ? '' : 'alternate';?>
				<tr class="<?php echo $class?>"><td><?php echo $badge->name?></td><td><input type="text" onclick="this.select();" value="[watuproplay-list badge <?php echo $badge->id?>]"></td>
				<td><?php if($badge->num_users):?>
					<a href="admin.php?page=watuproplay_badge_users&id=<?php echo $badge->id?>"><?php printf(__('%d users', 'watuproplay'), $badge->num_users);?></a>				
				<?php else: _e('No one yet', 'watuproplay'); endif;?></td>				
				<td><a href="admin.php?page=watuproplay_badges&action=edit&id=<?php echo $badge->id?>"><?php _e('Edit', 'watuproplay')?></a></td></tr>
			<?php endforeach;?>
		</table>
		
		<p class="watuproplay-help"><?php _e('* The shortcode shown in the table can be used to display all users who have this badge. For the moment it will only list usernames one per line. More advanced configurations are coming soon.', 'watuproplay')?></p>
		
		<p>&nbsp;</p>
		
		<h2><?php _e('Manage Notifications for Badges', 'watuproplay')?></h2>
		<div class="wp-admin watupro-padded postbox">
			<form method="post" class="watupro">
				<h3><?php _e('Email message sent to you', 'watuproplay')?></h3>
				<p><input type="checkbox" name="admin_badge_email" value="1" <?php if(get_option('watuproplay_admin_badge_email')) echo 'checked'?>> <?php _e('Email me when user earned new badge(s)', 'watuproplay')?> </p>
								
				<p><label><?php _e('Email subject:', 'watuproplay')?></label> <input type="text" name="admin_badge_subject" value="<?php echo get_option('watuproplay_admin_badge_subject')?>" size="60"></p>
				
				<p><label><?php _e('Email contents:', 'watuproplay')?></label> <?php wp_editor(stripslashes(get_option('watuproplay_admin_badge_message')), 'admin_badge_message')?></p>
				
				<p class="watupro-help"><?php _e('You can use the following variables:', 'watuproplay')?> {{user-name}}, {{user-email}}, {{user-id}}, {{assigned-badges}}</p>
				
				<p align="center"><input type="submit" value="<?php _e('Save Notification Email', 'watuproplay')?>"></p>
				<input type="hidden" name="save_admin_badge_email" value="1">
			</form>	
			
			<p>&nbsp;</p>
			
			<form method="post" class="watupro">
				<h3><?php _e('Email message sent to user', 'watuproplay')?></h3>
				<p><input type="checkbox" name="user_badge_email" value="1" <?php if(get_option('watuproplay_user_badge_email')) echo 'checked'?>> <?php _e('Email user when they earn new badge(s)', 'watuproplay')?> </p>
								
				<p><label><?php _e('Email subject:', 'watuproplay')?></label> <input type="text" name="user_badge_subject" value="<?php echo get_option('watuproplay_user_badge_subject')?>" size="60"></p>
				
				<p><label><?php _e('Email contents:', 'watuproplay')?></label> <?php wp_editor(stripslashes(get_option('watuproplay_user_badge_message')), 'user_badge_message')?></p>
				
				<p class="watupro-help"><?php _e('You can use the following variables:', 'watuproplay')?> {{user-name}}, {{user-email}}, {{user-id}}, {{assigned-badges}}</p>
				<p class="watupro-help"><?php _e('In case you activate your badges as Mozilla open badges you can also use the:', 'watuproplay')?> {{open-badges-url}} <?php _e('variable inside a HTML link to give a clickable link so user can accept their new badges in their Mozilla backpack.', 'watuproplay');?></p>
				
				<p align="center"><input type="submit" value="<?php _e('Save Notification Email', 'watuproplay')?>"></p>
				<input type="hidden" name="save_user_badge_email" value="1">
			</form>	
		</div>	
	<?php else:?>
		<p><?php _e('You have not yet created any badges', 'watuproplay')?></p>	
	<?php endif;?>
</div>