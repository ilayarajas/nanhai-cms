<?php

class TEAM_AWESOME {
    
    static $loaded_includes = false;
    static $options = false;
    
    public static function load_includes() {
    
           
        if (self::$loaded_includes === true) {
            return;
        }
        
        wp_enqueue_script("jquery");
        wp_enqueue_style("font-awesome", "http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css");
        wp_enqueue_style("team-awesome-scroll", plugins_url("/front/css/scroll.css", TEAM_AWESOME__FILE__));
        wp_enqueue_style("team-awesome-avatars", plugins_url("/front/css/ta-avatars.css", TEAM_AWESOME__FILE__));
        wp_enqueue_style("team-awesome", plugins_url("/front/css/ta.css", TEAM_AWESOME__FILE__));
        
        wp_enqueue_script("team-awesome-mobile-events", plugins_url("/front/js/mobileevents.js", TEAM_AWESOME__FILE__), array("jquery"));
        wp_enqueue_script("team-awesome-easing-js", plugins_url("/front/js/jquery.easing.js", TEAM_AWESOME__FILE__), array("jquery"));
        wp_enqueue_script("team-awesome-scroll-js", plugins_url("/front/js/scroll.js", TEAM_AWESOME__FILE__), array("jquery"));
        wp_enqueue_script("team-awesome-js", plugins_url("/front/js/ta.js", TEAM_AWESOME__FILE__), array("jquery"));
        
        add_action("wp_head", array("TEAM_AWESOME", "enqueue_google_fonts"));
        add_action("wp_head", array("TEAM_AWESOME", "enqueue_custom_style"));
        
        self::$loaded_includes = true;
    }
    
    public static function create_js_instance($settings = array()) {
        if (empty($settings)) {
            return;
        }
        
        $json = json_encode($settings);
        
        ?>
        <script type="text/javascript">
            if (typeof team_awesome_instance === "undefined") {
                var team_awesome_instance = [];
            }
            team_awesome_instance.push(<?php echo $json; ?>);
        </script>
        <?php
        
    }
    
    public static function get_options() {
        
        if (self::$options) {
            return self::$options;
        }
        
        $options = get_option("team-awesome");
        
        $options["id"] = "team-awesome-id-" . md5(microtime());
        
        self::$options = $options;
        
        return $options;
    }
    
    public static function get_categories() {
        
        $cats = get_option("team-awesome-categories", array());
        
        $filter = isset(self::$options["cat"]) ? self::$options["cat"] : false;
        
        if ($filter) {
            $filter = explode(",", $filter);

            foreach ($cats as $k => $cat) {
                if (!in_array($cat->id, $filter)) {
                    unset($cats[$k]);
                }
            }
        }

        usort($cats, array('TEAM_AWESOME_ADMIN','sortByOrder'));
        return stripslashes_deep($cats);
    }
    
    public static function sortByOrder($a, $b) {
        return $a->ordering - $b->ordering;
    }
    
    public static function get_member($memberid = false) {

        $members = get_option("team-awesome-members", array());
        
        foreach ($members as $k => $mem) {
            if ($mem->id == $memberid) {
                return stripslashes_deep($mem);
            }
        }
    }
    
    public static function get_members() {

        $members = get_option("team-awesome-members", array());
        
        $filter = isset(self::$options["cat"]) ? self::$options["cat"] : false;
        
        if ($filter) {
            $filter = explode(",", $filter);
            $fmembers = array();

            foreach ($members as $k => $mem) {
                foreach ($filter as $f) {
                    if (in_array($f, $mem->catid)) {
                        $fmembers[$k] = $mem;
                    }
                }
            }
            
            $members = $fmembers;
        }

        usort($members, array('TEAM_AWESOME_ADMIN','sortByOrder'));
        
        return stripslashes_deep($members);
    }
    
    public static function enqueue_google_fonts() {
        
        $fonts = array(
            "titlefont",
            "catfont",
            "pnamefont",
            "pshortdescfont",
            "skillstitlefont",
            "skillsprctfont"
        );

        $include = array();
        $options = self::get_options();
        
        foreach ($fonts as $font) {
            $fontfamily = $options[$font];

            if (!empty($fontfamily)) {
                if ($fontfamily != "default") {
                    $include[] = $fontfamily;
                }
            }
        }
        
        $include = array_unique($include);

        $fonts = implode("|", $include);

        if (!empty($fonts)) {
        ?>
            <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=<?php echo urlencode($fonts); ?>">
        <?php
        }
    }
    
    public static function enqueue_custom_style() {
        
        $options = self::get_options();
        
        $id = ".team-awesome";
        
        ?>
            <style>
                
                /* Blur slide effect */

                .ta-svg-filters {
                    position: fixed;
                    left: -9999px;
                }

                .ta-svg-blur {
                    -webkit-filter: url("#ta-blur");
                    filter: url("#ta-blur");
                }

                .ta-svg-blur-vert {
                    -webkit-filter: url("#ta-blur-vert");
                    filter: url("#ta-blur-vert");
                }
                
                <?php echo $id; ?> {
                    background-color: <?php echo $options["bgcolor"]; ?>;
                    background-image: url("<?php echo $options["bgimage"]; ?>");
                }
                
                <?php if ($options["cats"] == "disable") : ?>
                    <?php echo $id; ?> .team-awesome-menu {
                        display: none !important;
                    }
                <?php endif; ?>
                
                <?php echo $id; ?> .team-awesome-member-box {
                    height: <?php echo $options["boxheight"]; ?>px;
                    margin: <?php echo $options["boxspacing"]; ?>px;
                }
                
                <?php echo $id; ?> .team-awesome-menu {
                    text-align: <?php echo $options["catsalign"]; ?>
                }
                
                <?php echo $id; ?> .team-awesome-member-circle-stamp-inner,
                <?php echo $id; ?> .team-awesome-member-box-stamp-inner {
                    border-color: <?php echo $options["avatarbc"]; ?>;
                }
                
                <?php echo $id; ?> .team-awesome-member-hex-stamp-inner-video-border {
                    background-color: <?php echo $options["avatarbc"]; ?>;
                }
                <?php echo $id; ?> .team-awesome-member-hex-stamp-inner-video-border::before, 
                <?php echo $id; ?> .team-awesome-member-hex-stamp-inner-video-border::after {
                    border-top-color: <?php echo $options["avatarbc"]; ?>;
                    border-bottom-color: <?php echo $options["avatarbc"]; ?>;
                }
                
                <?php echo $id; ?> .team-awesome-member-circle-stamp-inner-video-border,
                <?php echo $id; ?> .team-awesome-member-box-stamp {
                    background-color: <?php echo $options["avatarbc"]; ?>;
                }
                
                
                
                <?php echo $id; ?> .team-awesome-member-hex-stamp-inner {
                    border-color: <?php echo $options["avatarbc"]; ?>;
                }
                
                <?php if ($options["avatarb"] == "off") : ?>
                    
                    <?php echo $id; ?> .team-awesome-member-box-stamp {
                        background: none;
                    }
                   
                    <?php echo $id; ?> .team-awesome-member-circle-stamp-inner-video-border,
                    <?php echo $id; ?> .team-awesome-member-hex-stamp-inner-video-border {
                        display: none;
                    }
                    
                    <?php echo $id; ?> .team-awesome-member-circle-stamp-inner, 
                    <?php echo $id; ?> .team-awesome-member-box-stamp-inner, 
                    <?php echo $id; ?> .team-awesome-member-hex-stamp-inner {
                        border:none;
                    }   

                    <?php echo $id; ?> .team-awesome-member-hex-stamp-inner {
                        position: relative;
                        width: 193px; 
                        height: 111.43px;
                        margin: 0;
                        background-size: auto 222.8572px;
                        background-position: center;
                    }

                    <?php echo $id; ?> .ta-hexTop,
                    <?php echo $id; ?> .ta-hexBottom {
                        width: 136.47px;
                        height: 136.47px;
                        left: 28.26px;
                        border:none;
                    }
                    
                    <?php echo $id; ?> .ta-hexTop:after,
                    <?php echo $id; ?> .ta-hexBottom:after {
                        width: 193px;
                        height: 111.42860195359778px;
                    }

                    <?php echo $id; ?> .team-awesome-member-hex-stamp-inner:after {
                        top: 0;
                        left: 0;
                        width: 193.0000px;
                        height: 111.4286px;
                    }
                
                <?php endif; ?>
                
                <?php echo $id; ?> .team-awesome-container h4 {
                    <?php echo self::fontfamily_to_css($options["titlefont"]); ?>
                    font-size: <?php echo $options["titlesize"];?>px;                
                    line-height: <?php echo $options["titleline"];?>;                
                    color: <?php echo $options["titlecolor"];?>;                
                }
                
                <?php echo $id; ?> .team-awesome-menu li {
                    <?php echo self::fontfamily_to_css($options["catfont"]); ?>
                    font-size: <?php echo $options["catsize"];?>px;                
                    line-height: <?php echo $options["catline"];?>;                
                    color: <?php echo $options["catcolor"];?>;                
                }
                
                <?php echo $id; ?> .team-awesome-menu li.active {
                    color: <?php echo $options["catactivecolor"];?>;                
                }
                
                <?php echo $id; ?> .team-awesome-member-about-title {
                    <?php echo self::fontfamily_to_css($options["pnamefont"]); ?>
                    font-size: <?php echo $options["pnamesize"];?>px;                
                    line-height: <?php echo $options["pnameline"];?>;                
                    color: <?php echo $options["pnamecolor"];?>;                
                }
                
                <?php echo $id; ?> .team-awesome-member-about-occupation {
                    <?php echo self::fontfamily_to_css($options["pposfont"]); ?>
                    font-size: <?php echo $options["ppossize"];?>px;                
                    line-height: <?php echo $options["pposline"];?>;                
                    color: <?php echo $options["pposcolor"];?>;                
                }
                
                <?php echo $id; ?> .team-awesome-member-about-short {
                    <?php echo self::fontfamily_to_css($options["pshortdescfont"]); ?>
                    font-size: <?php echo $options["pshortdescsize"];?>px;                
                    line-height: <?php echo $options["pshortdescline"];?>;                
                    color: <?php echo $options["pshortdesccolor"];?>;                
                }
                
                <?php echo $id; ?> .team-awesome-slider-control-left,
                <?php echo $id; ?> .team-awesome-slider-control-right {
                    background-color: <?php echo $options["pagibg"];?>;
                    border-color: <?php echo $options["pagibr"];?>;
                }
                
                <?php echo $id; ?> .team-awesome-slider-control-left i,
                <?php echo $id; ?> .team-awesome-slider-control-right i {
                    color: <?php echo $options["pagiico"];?>;
                }
                
                <?php echo $id; ?> .team-awesome-slider-control-left:hover,
                <?php echo $id; ?> .team-awesome-slider-control-right:hover {
                    background-color: <?php echo $options["pagibghover"];?>;
                    border-color: <?php echo $options["pagibrhover"];?>;
                }
                
                <?php echo $id; ?> .team-awesome-slider-control-left:hover i,
                <?php echo $id; ?> .team-awesome-slider-control-right:hover i {
                    color: <?php echo $options["pagiicohover"];?>;
                }
                
                <?php echo $id; ?> .team-awesome-member-pop-percents-box-title {
                    <?php echo self::fontfamily_to_css($options["skillstitlefont"]); ?>
                    font-size: <?php echo $options["skillstitlesize"];?>px;                
                    line-height: <?php echo $options["skillstitleline"];?>;                
                    color: <?php echo $options["skillstitlecolor"];?>;                
                }
                
                <?php echo $id; ?> .team-awesome-member-pop-percents-box-aftertitle {
                    <?php echo self::fontfamily_to_css($options["skillsprctfont"]); ?>
                    font-size: <?php echo $options["skillsprctsize"];?>px;                
                    line-height: <?php echo $options["skillsprctline"];?>;                
                    color: <?php echo $options["skillsprctcolor"];?>;                
                }
                
                <?php echo $id; ?> .team-awesome-member-pop-percents-outer {
                    background-color: <?php echo $options["skillsbarbg"];?>;  
                }
                
                <?php echo $id; ?> .team-awesome-member-pop-percents-inner {
                    background-color: <?php echo $options["skillsbarfill"];?>;  
                }
                
                <?php echo $id; ?> .team-awesome-member-about-dash {
                    height: <?php echo (int) $options["dividert"];?>px;  
                    width: <?php echo (int) $options["dividerw"];?>px;  
                    background-color: <?php echo $options["dividerc"];?>;  
                }
            </style>
        <?php
    }
    
    public static function fontfamily_to_css($font) {
        $font = explode(":", $font);
        if ($font[0] == "default") return;
        
        ob_start();
        
        echo "font-family: $font[0];";
        
        if (isset($font[1])) {
            $weight = preg_replace('/[a-zA-Z]/', '', $font[1] );
            $style  = preg_replace('/[0-9]/', '', $font[1] );
            if ($weight != "" ) echo "font-weight:$weight;";
            if ($style != ""  ) echo "font-style:$style;";
        }
        
        echo "\n\r";
        
        return ob_get_clean();
    }
    

}