<table class="watupro-barchart" cellpadding="5">
	<tr><?php foreach($cats as $cat):?>
		<td style="position:relative;height:<?php echo $height?>px;width:<?php echo $width?>px;">		
			<div style="width:<?php echo $width?>px;height:<?php echo $step*$cat['percent']?>px;background-color:<?php echo $color?>;position:absolute;bottom:0;"></div>			
		</td>
	<?php endforeach;?>
	</tr>
	<tr>
		<?php foreach($cats as $cat):?>
		<td><p><?php switch($from):
			case 'percent_max_points': echo sprintf(__('<b>%s</b> - %d%% (%d of %d points)', 'watupro'), stripslashes($cat['name']), $cat['percent'], $cat['points'], $cat['max_points']); break;
			case 'points': echo sprintf(__('<b>%s</b> - %d points', 'watupro'), stripslashes($cat['name']), $cat['percent']); break;
			case 'answered': case 'correct': default: echo sprintf(__('<b>%s</b> - %d%%', 'watupro'), stripslashes($cat['name']), $cat['percent']); break;
		endswitch;?></p></td>
		<?php endforeach;?>
	</tr>
</table>