<?php
function michigan_webnus_icon ($atts, $content = null) {
	extract(shortcode_atts(array(
	'name'			=>	'',
	'link'      	=>	'',
	'link_class'    =>	'',
	'icon_class'    =>	'',
	'size'			=>	'',
	'color'			=>	'',
	'bgcolor'			=>	'',
	'icon_center'		=>	'',
	), $atts));

	$style = 'style="';
	if(!empty($size))  $style .= ' font-size:' . $size. ';';
	if(!empty($color)) $style .= ' color:' . $color. ';';
	if(!empty($bgcolor)) $style .= ' background-color:' . $bgcolor. ';';
	if(!empty($icon_center)) $style .= 'text-align:center; display:block;';
	
	$style .= '"';			
				
	if(!empty($link)){
	 $out = '<a href="'. esc_url($link) .'" class="'. $link_class .'"><i class="'. $name . $icon_class.'" '.$style.'></i></a>';
	}
	else{
	 $out = '<i class="'. $name .' '. $icon_class.'" '.$style.'></i>';
	}
	return $out;
}
add_shortcode('icon','michigan_webnus_icon');
?>