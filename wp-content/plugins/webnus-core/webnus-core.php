<?php
/*
Plugin Name: Webnus Core
Description: Add Webnus Shortcodes & Post types to your WordPress website.
Version: 1.0
Author: Webnus
Author URI: http://webnus.net
License: GPL2
*/


add_action( 'plugins_loaded', 'shortcodes_init');
add_action('init', 'w_course_register');
add_filter('manage_edit-excursion_columns', 'w_add_new_excursion_columns');
add_action('manage_excursion_posts_custom_column', 'w_manage_excursion_columns', 5, 2);
add_action('init', 'w_excursion_register');
add_filter('manage_edit-faq_columns', 'w_add_new_faq_columns');
add_action('manage_faq_posts_custom_column', 'w_manage_faq_columns', 5, 2);
add_action('init', 'w_faq_register');
add_filter('manage_edit-goal_columns', 'w_add_new_goal_columns');
add_action('manage_goal_posts_custom_column', 'w_manage_goal_columns', 5, 2);
add_action('init', 'w_goal_register');


// Shortcodes init
function shortcodes_init() {
	foreach( glob( plugin_dir_path( __FILE__ ) . '/shortcodes/*.php' ) as $filename ) {
		require_once $filename;
	}
}


// Register Course Post Type
function w_course_register() {
    $labels = array(
        'name' => __('Courses', 'michigan-webnus'),
        'all_items' => __('All Courses', 'michigan-webnus'),
        'singular_name' => __('Course', 'michigan-webnus'),
        'add_new' => __('Add Course', 'michigan-webnus'),
        'add_new_item' => __('Add New Course', 'michigan-webnus'),
        'edit_item' => __('Edit Course', 'michigan-webnus'),
        'new_item' => __('New Course', 'michigan-webnus'),
        'view_item' => __('View Course', 'michigan-webnus'),
        'search_items' => __('Search Course', 'michigan-webnus'),
        'not_found' => __('No Course found', 'michigan-webnus'),
        'not_found_in_trash' => __('No Item found in Trash', 'michigan-webnus'),
        'parent_item_colon' => '',
        'menu_name' => __('Courses', 'michigan-webnus')
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'rewrite' => array('slug' => 'course'),
        'supports' => array(
            'title',
            'thumbnail',
			'page-attributes',
			'excerpt',
			'comments',
        ),
		'has_archive' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-money',
        'taxonomies' => array('course_cat', 'michigan-webnus')
    );
    register_post_type('course', $args);
	w_course_register_taxonomies();
}

// Register Course Category Taxonomy
function w_course_register_taxonomies() {
	register_taxonomy( 'course_cat','course', array(
				'hierarchical' 			=> true,
				'label' 				=> __( 'Course Categories', 'michigan-webnus' ),
				'labels' => array(
						'name' 				=> __( 'Course Categories', 'michigan-webnus' ),
						'singular_name' 	=> __( 'Course Category', 'michigan-webnus' ),
						'menu_name'			=> _x( 'Categories', 'Admin menu name', 'michigan-webnus' ),
						'search_items' 		=> __( 'Search Course Categories', 'michigan-webnus' ),
						'all_items' 		=> __( 'All Course Categories', 'michigan-webnus' ),
						'parent_item' 		=> __( 'Parent Course Category', 'michigan-webnus' ),
						'parent_item_colon' => __( 'Parent Course Category:', 'michigan-webnus' ),
						'edit_item' 		=> __( 'Edit Course Category', 'michigan-webnus' ),
						'update_item' 		=> __( 'Update Course Category', 'michigan-webnus' ),
						'add_new_item' 		=> __( 'Add New Course Category', 'michigan-webnus' ),
						'new_item_name' 	=> __( 'New Course Category Name', 'michigan-webnus' ),
					),
				'show_ui' 				=> true,
				'query_var' 			=> true,
				'rewrite' 				=> array(
				'slug'         => empty( $permalinks['category_base'] ) ? _x( 'course-category', 'slug', 'michigan-webnus' ) : $permalinks['category_base'],
				'with_front'   => false,
				'hierarchical' => true,
			),
		)
	);
}



//Register Excursion Post Type
function w_excursion_register() {
    $labels = array(
        'name' => __('Excursions', 'michigan-webnus'),
        'all_items' => __('All Excursions', 'michigan-webnus'),
        'singular_name' => __('Excursion', 'michigan-webnus'),
        'add_new' => __('Add Excursion', 'michigan-webnus'),
        'add_new_item' => __('Add New Excursion', 'michigan-webnus'),
        'edit_item' => __('Edit Excursion', 'michigan-webnus'),
        'new_item' => __('New Excursion', 'michigan-webnus'),
        'view_item' => __('View Excursion', 'michigan-webnus'),
        'search_items' => __('Search Excursion', 'michigan-webnus'),
        'not_found' => __('No Excursion found', 'michigan-webnus'),
        'not_found_in_trash' => __('No Item found in Trash', 'michigan-webnus'),
        'parent_item_colon' => '',
        'menu_name' => __('Excursions', 'michigan-webnus')
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'rewrite' => array('slug' => 'excursion'),
        'supports' => array(
            'title',
            'thumbnail',
            'editor',
            'page-attributes',
            'excerpt',
            'comments',
        ),
        'menu_position' => 21,
        'menu_icon' => 'dashicons-palmtree',
    );
    register_post_type('excursion', $args);
}

//Admin Dashboard Listing Excursion Columns Title
function w_add_new_excursion_columns() {
    $columns['cb'] = '<input type="checkbox" />';
    $columns['title'] = __('Title', 'michigan-webnus');
    $columns['thumbnail'] = __('Thumbnail', 'michigan-webnus' );
    $columns['author'] = __('Author', 'michigan-webnus' );
    $columns['date'] = __('Date', 'michigan-webnus');
    return $columns;  
}


//Admin Dashboard Listing Excursion Columns Manage
function w_manage_excursion_columns($columns) {
    global $post;
    switch ($columns) {
        case 'thumbnail':
            if(get_the_post_thumbnail( $post->ID, 'thumbnail' )){
                    echo get_the_post_thumbnail( $post->ID, 'thumbnail' );
                }else{ 
                    echo '<img width="150" height="150" src="'.EXCURSION_URL.'/images/no_image.jpg" class="attachment-thumbnail wp-post-image" alt="no image">';
             }
        break;
        case 'excursion_category':
            $terms = wp_get_post_terms($post->ID, 'excursion_category');  
            foreach ($terms as $term) {  
                echo $term->name .'&nbsp;&nbsp; ';  
            }  
        break;
    }
}

//Register FAQ Post Type
function w_faq_register() {
        $labels = array(
            'name'                => _x( 'FAQs', 'Post Type General Name', 'michigan-webnus' ),
            'singular_name'       => _x( 'FAQ', 'Post Type Singular Name', 'michigan-webnus' ),
            'menu_name'           => __( 'FAQ', 'michigan-webnus' ),
            'parent_item_colon'   => __( 'Parent Item:', 'michigan-webnus' ),
            'all_items'           => __( 'All Items', 'michigan-webnus' ),
            'view_item'           => __( 'View Item', 'michigan-webnus' ),
            'add_new_item'        => __( 'Add New FAQ Item', 'michigan-webnus' ),
            'add_new'             => __( 'Add New', 'michigan-webnus' ),
            'edit_item'           => __( 'Edit Item', 'michigan-webnus' ),
            'update_item'         => __( 'Update Item', 'michigan-webnus' ),
            'search_items'        => __( 'Search Item', 'michigan-webnus' ),
            'not_found'           => __( 'Not found', 'michigan-webnus' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'michigan-webnus' ),
        );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'rewrite' => array('slug' => 'faq'),
        'supports' => array(
            'title',
            'thumbnail',
            'editor',
			'page-attributes',
			'excerpt',
			'comments',
        ),
        'menu_position' => 22,
        'menu_icon' => 'dashicons-editor-help',
    );
    register_post_type('faq', $args);
	w_faq_register_taxonomies();
}
//Register FAQ Category Taxonomy
function w_faq_register_taxonomies() {
	$labels = array(
			'name'					=> __('FAQ Categories', 'michigan-webnus'),
			'singular_name'			=> __('FAQ Category',  'michigan-webnus'),
			'all_items'				=> __('All FAQ Categories', 'michigan-webnus'),
	);
	register_taxonomy('faq_category', 'faq', 	
	array(
	'hierarchical' => true,
	'labels' => $labels,
	'query_var' => true,
	'rewrite' => array('slug' => 'faq-category')
	));
	
}
	
//Admin Dashboard Listing FAQ Columns Title
function w_add_new_faq_columns() {
	$columns['cb'] = '<input type="checkbox" />';
 	$columns['title'] = __('Title', 'michigan-webnus');
	$columns['faq_category'] = __('Categories', 'michigan-webnus' );
	$columns['date'] = __('Date', 'michigan-webnus');
	return $columns; 
}

//Admin Dashboard Listing FAQ Columns Manage
function w_manage_faq_columns($columns) {
	global $post;
	switch ($columns) {
 	case 'faq_category':
		$terms = wp_get_post_terms($post->ID, 'faq_category');  
		foreach ($terms as $term) {  
			echo $term->name .'&nbsp;&nbsp; ';  
		}  
	break;
	}
}


//Register Goal Post Type
function w_goal_register() {
    $labels = array(
        'name' => __('Goals', 'michigan-webnus'),
        'all_items' => __('All Goals', 'michigan-webnus'),
        'singular_name' => __('Goal', 'michigan-webnus'),
        'add_new' => __('Add Goal', 'michigan-webnus'),
        'add_new_item' => __('Add New Goal', 'michigan-webnus'),
        'edit_item' => __('Edit Goal', 'michigan-webnus'),
        'new_item' => __('New Goal', 'michigan-webnus'),
        'view_item' => __('View Goal', 'michigan-webnus'),
        'search_items' => __('Search Goal', 'michigan-webnus'),
        'not_found' => __('No Goal found', 'michigan-webnus'),
        'not_found_in_trash' => __('No Item found in Trash', 'michigan-webnus'),
        'parent_item_colon' => '',
        'menu_name' => __('Goals', 'michigan-webnus')
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'rewrite' => array('slug' => 'goal'),
        'supports' => array(
            'title',
            'thumbnail',
            'editor',
			'page-attributes',
			'excerpt',
			'comments',
        ),
        'menu_position' => 23,
        'menu_icon' => 'dashicons-heart',
    );
    register_post_type('goal', $args);
	w_goal_register_taxonomies();
}
//Register Goal Category Taxonomy
function w_goal_register_taxonomies() {
	$labels = array(
			'name'					=> __('Goals Categories', 'michigan-webnus'),
			'singular_name'			=> __('Goal Category',  'michigan-webnus'),
			'all_items'				=> __('All Goals Categories', 'michigan-webnus'),
	);
	register_taxonomy('goal_category', 'goal', 	
	array(
	'hierarchical' => true,
	'labels' => $labels,
	'query_var' => true,
	'rewrite' => array('slug' => 'goal-category')
	));
	
}
	
//Admin Dashboard Listing Goal Columns Title
function w_add_new_goal_columns() {
	$columns['cb'] = '<input type="checkbox" />';
 	$columns['title'] = __('Title', 'michigan-webnus');
	$columns['goal_category'] = __('Categories', 'michigan-webnus' );
	$columns['date'] = __('Date', 'michigan-webnus');
	$columns['received'] = __('Amount Received', 'michigan-webnus');
	$columns['amount'] = __('Amount Needed', 'michigan-webnus');
	$columns['end'] = __('End Date', 'michigan-webnus');
	return $columns; 
}

// Admin Dashboard Listing Goal Columns Manage
function w_manage_goal_columns($columns) {
	global $post;
	switch ($columns) {
 	case 'goal_category':
		$terms = wp_get_post_terms($post->ID, 'goal_category');  
		foreach ($terms as $term) {  
			echo $term->name .'&nbsp;&nbsp; ';  
		}  
	break;
	case 'received':
	echo rwmb_meta( 'michigan_goal_amount_received_meta' );
	break;
	case 'amount':
	echo rwmb_meta( 'michigan_goal_amount_meta' );
	break;
	case 'end':
	echo rwmb_meta( 'michigan_goal_end_meta' );
	break;
	}
}

// Login
function michigan_webnus_login() {
	global $user_ID, $user_identity;
	if ($user_ID) : ?>
		<div id="user-logged">
			<span class="author-avatar"><?php echo get_avatar( $user_ID, $size = '100'); ?></span>
			<div class="user-welcome"><?php esc_html_e('Welcome','michigan'); ?> <strong><?php echo esc_html($user_identity) ?></strong></div>
			<ul class="logged-links">
			<?php if(current_user_can('publish_posts')) { ?>
				<li><a href="<?php echo esc_url(home_url('/wp-admin/')); ?>"><?php esc_html_e('Dashboard','michigan'); ?> </a></li>
			<?php } else { ?>
				<li><a href="<?php echo esc_url(home_url('/my-courses/')); ?>"><?php esc_html_e('My Courses','michigan'); ?> </a></li>
			<?php } ?>
				<li><a href="<?php echo esc_url(home_url('/wp-admin/profile.php')); ?>"><?php esc_html_e('Edit Profile','michigan'); ?> </a></li>
				<li><a href="<?php echo esc_url(wp_logout_url(get_permalink())); ?>"><?php esc_html_e('Logout','michigan'); ?> </a></li>    
			</ul>
			<div class="clear"></div>
		</div>
	<?php else: ?>
		<div id="user-login">
			<?php wp_login_form(array('label_username' => esc_html__( 'Username','michigan' ),'label_password' => esc_html__( 'Password','michigan' ),'label_remember' => esc_html__( 'Remember Me','michigan' ),
				'label_log_in'   => esc_html__( 'Log In','michigan' ),));?> 
			<ul class="login-links">
				<?php if ( get_option('users_can_register') ) : ?><?php echo wp_register() ?><?php endif; ?>
				<li><a href="<?php
					$siteURL = get_option('siteurl');
					echo"{$siteURL}/wp-login.php?action=lostpassword";?>"><?php esc_html_e('Lost your password?','michigan'); ?></a></li>
			</ul>
		</div>
	<?php endif;
}

