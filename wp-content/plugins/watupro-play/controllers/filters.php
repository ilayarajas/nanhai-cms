<?php
// performs various filters
// http://wordpress.org/support/topic/passing-variables-using-the-permalink-structure?replies=4
class WatuPROPLayFilters {
	static function query_vars($vars) {
		$vars[] = 'wtpplay_list';		
		return $vars;
	}
	
	static function the_content($content) {
		global $wp_query;
		if(isset($wp_query->query_vars['wtpplay_list'])) {
			// list users by given criteria
			$by = !empty($_GET['by']) ? $_GET['by'] : '';
			switch($by) {
				case 'level':
				break;

				case 'badge':
				break;				
				
				default:
					// unrecognized criteria, don't filter
					return $content;
				break;
			}
		}
		
		return $content;
	}
}