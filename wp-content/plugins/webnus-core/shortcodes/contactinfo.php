<?php 
function webnus_contact_info( $atts, $content = null ) {
	extract(shortcode_atts(array(
			'heading_text'	=> '',
			'phone_number'	=> '',
			'email'			=> '',
			'email_url'		=> '',
	), $atts ));

	$heading_text	= $heading_text		? '<h4>' . $heading_text . '</h4>' : '' ;
	$phone_number	= $phone_number	? '<p>' . $phone_number . '</p>' : '' ;
	$email			= $email		? '<a href="mailto:'.$email_url.'">' . $email . '</a>' : '' ;

	$out = 
		'<div class="contac-info">
			' . $heading_text . $phone_number . $email . '
		</div>';

	return $out;
}
add_shortcode( 'contact_info','webnus_contact_info' );