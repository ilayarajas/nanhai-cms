Please See documentation for details.

Install and setup
1. Login to your WordPress Admin. Click on Plugins | Add New from the left hand menu.

2. Click on the �Upload� option, then click �Choose File� to select the zip file from your computer. Once selected, press �OK� and press the "Install Now" button.

3. Activate the plugin.


4. Configure plugin settings
/wp-admin/edit.php?post_type=interactive_table&page=it-settings

5. Add Category and Item You Needed.

6. Add shortcode to your page or post or widget
[interactive_table category="2, 3, 4, 8"]


7. You can overide default settings from shortcode.
[interactive_table category="2, 3, 4, 8" block_heading_position="Top" block_associates_color="#D54E21" cell_title_color="#C2FFFF" cell_hover_title_color="#FFFE9B" cell_font_color="#FFFFFF" cell_hover_font_color="#FFFFFF" read_more_font_color="#64D863" cell_read_more="Read More" vertical_posts_per_row="2" fixed_highlight=""]

8. Pricing Table Shortocde.
[interactive_pricing_table category="19, 20, 21, 22" block_heading_position="top"]
[interactive_pricing_table category="19, 20, 21, 22" block_heading_position="left"]


9. Contact with us if have any question. We're grad to hear and help.
