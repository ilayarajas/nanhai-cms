<?php
class WatuPROPlayReward {
	function add($vars) {
		global $wpdb;
		
		$result = $wpdb -> query( $wpdb->prepare("INSERT INTO ".WATUPROPLAY_REWARDS." SET 
			title=%s, description=%s, points=%s, qty=%d, tangible=%d, in_store=%d", $vars['title'], $vars['description'], 
			$vars['points'], $vars['qty'], @$vars['tangible'], @$vars['in_store']) );
			
		if($result === false) return false;
		return true;	
	}
	
	function edit($vars, $id) {
		global $wpdb;
		
		$result = $wpdb -> query( $wpdb->prepare("UPDATE ".WATUPROPLAY_REWARDS." SET 
			title=%s, description=%s, points=%s, qty=%d, tangible=%d, in_store=%d WHERE id=%d", 
			$vars['title'], $vars['description'], $vars['points'], $vars['qty'], @$vars['tangible'], @$vars['in_store'], $id) );
			
		if($result === false) return false;
		return true;	
	}
	
	function delete($id) {
		global $wpdb;
		
		$result = $wpdb -> query($wpdb->prepare("DELETE FROM ".WATUPROPLAY_REWARDS." WHERE id=%d", $id));
		
		if($result === false) return false;
		return true;		
	}
	
	// displays the available reward items
	function display_listing($format) {
		global $user_ID, $wpdb;
		
		// select user points
		$points = get_user_meta($user_ID, 'watuproplay-points', true);
		
		$is_logged_in = is_user_logged_in();
		
		// select rewards
		$rewards = $wpdb -> get_results("SELECT * FROM ".WATUPROPLAY_REWARDS." WHERE qty!=0 ORDER BY points");
		
		// output
		$output = '';
		if($format == 'table') {
			$output = "<table class='watuproplay-rewards'>";
			foreach($rewards as $item) {
				$output .= "<tr class='watuproplay-rewarditem'><td><h3>".$item->title."</h3>".apply_filters('watupro_content',stripslashes($item->description))."</td>";
				$output .= "<td><h3>".$item->points." ".__('points', 'watuproplay')."</h3>";
				
				if($is_logged_in) {
					if($item->points > $points) $output .= __('You need more points', 'watuproplay');
					else $output .="<form method='post'><input type='submit' name='watuproplay_redeem' value='".__('Redeem', 'watuproplay')."'>
					<input type='hidden' name='watuproplay_item_id' value='".$item->id."'></form>";
				} else {
					$output .= __('Login to redeem', 'watuproplay');
				}						
				
				$output .= "</td></tr>";
			}
			$output .= "</table>"; 
		}
		else {
			$output = "<div class='watuproplay-rewards'>";
			foreach($rewards as $item) {
				$output .= "<div class='watuproplay-rewarditem'><td><h3>".$item->title."</h3>".apply_filters('watupro_content',stripslashes($item->description))."</td>";
				$output .= "<h3>".$item->points." ".__('points', 'watuproplay')."</h3>";
				
				if($is_logged_in) {
					if($item->points > $points) $output .= __('You need more points', 'watuproplay');
					else $output .="<form method='post'><input type='submit' name='watuproplay_redeem' value='".__('Redeem', 'watuproplay')."'>
					<input type='hidden' name='watuproplay_item_id' value='".$item->id."'></form>";
				} else {
					$output .= __('Login to redeem', 'watuproplay');
				}						
				
				$output .= "</div>";
			}
			$output .= "</div>"; 
		}
		
		return $output;
	} // end display_listing
	
	// tries to redeem item
	function redeem($id) {
		global $wpdb, $user_ID;
		
		if(!is_user_logged_in()) return __('You have to be logged in to redeem rewards', 'watuproplay');

		// select item	
		$item = $wpdb->get_row( $wpdb->prepare("SELECT * FROM ".WATUPROPLAY_REWARDS." WHERE id=%d", $id) );
		
		// is it really available?
		if(empty($item->id) or $item->qty==0) return __('Sorry, this item is not available at the moment', 'watuproplay');
		
		// do I have the points to redeem it?
		$points = get_user_meta($user_ID, 'watuproplay-points', true);
		if($points < $item->points) return __('Sorry, looks like your points balance is not enough to redeem this item', 'watuproplay');
		
		// now insert in redeemed rewards
		$wpdb->query( $wpdb->prepare("INSERT INTO ".WATUPROPLAY_REDEEMED." SET
			user_id=%d, item_id=%d, date=CURDATE(), points=%s", $user_ID, $id, $item->points));
				
		// reduce my points
		$points -= $item->points;
		update_user_meta($user_ID, 'watuproplay-points', $points);
		
		// reduce item quantity unless it's intangible
		if($item->qty != -1) {
			$wpdb->query( $wpdb->prepare("UPDATE ".WATUPROPLAY_REWARDS." SET qty=qty-1 WHERE id=%d", $item->id));
		}
		
		// send notification email if any
		if(get_option('watuproplay_redeem_email')) {
			$sender = $receiver = get_option('admin_email');
			$subject = get_option('watuproplay_redeem_subject');
			$message = stripslashes(get_option('watuproplay_redeem_message'));
			
			$user_info = get_userdata($user_ID);
			
			$message = str_replace('{{user-name}}', $user_info->user_login, $message);
			$message = str_replace('{{user-email}}', $user_info->user_email, $message);
			$message = str_replace('{{item-name}}', $item->title, $message);
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
			wp_mail($receiver, $subject, $message, $headers);	
		}
		
		// get the success message and return it
		$output = get_option('watuproplay_redeem_flash');
		$output = apply_filters('watupro_content', stripslashes($output));
		
		return $output;
	}
}