<?php
if(isset($_POST['it_settings_options_submit'])){	
	update_option( 'it_block_heading_position', $_POST['block_heading_position'] );
	update_option( 'it_cell_title_color', $_POST['cell_title_color'] );
	update_option( 'it_cell_font_color', $_POST['cell_font_color'] );
	update_option( 'it_cell_hover_title_color', $_POST['cell_hover_title_color'] );
	update_option( 'it_cell_hover_font_color', $_POST['cell_hover_font_color'] );	
	update_option( 'it_read_more_font_color', $_POST['read_more_font_color'] );
	update_option( 'it_display_mode', $_POST['display_mode'] );
	update_option( 'it_highlight_mode', $_POST['highlight_mode'] );
	update_option( 'cell_highlight_color', $_POST['cell_highlight_color'] );	
	update_option( 'it_single_cell_slug_url', strtolower(str_replace(" ", "-", esc_attr($_POST['single_cell_slug_url']))) );
	update_option( 'it_read_more', $_POST['read_more'] );
	update_option( 'readmore_label', $_POST['readmore_label'] );	
	update_option( 'it_load_default_css', $_POST['load_default_css'] );
	update_option( 'it_vertical_posts_per_row', $_POST['vertical_posts_per_row'] );
	if( isset($_POST['cell_hover']) ) {
		update_option( 'it_cell_hover', $_POST['cell_hover'] );
	} else {
		update_option( 'it_cell_hover', '' );
	}

	if( isset($_POST['title_hover']) ) {
		update_option( 'it_title_hover', $_POST['title_hover'] );
	} else {
		update_option( 'it_title_hover', '' );
	}
	
	if( isset($_POST['fixed_highlight']) ) {
		update_option( 'fixed_highlight', $_POST['fixed_highlight'] );
	} else {
		update_option( 'fixed_highlight', '' );
	}
	
	
	
	echo "<div class='updated'><p>Successfully Updated</p></div>";
}
?>
<div class="wrap">
<h2 style="margin-bottom:15px;"><?php echo __('Settings'); ?></h2>
<form name="it_settings" method="post" action="edit.php?post_type=interactive_table&page=it-settings">
	<table class="form-table">
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Block Heading'); ?></th>
			<td style="padding-top:0;">
                <select name="block_heading_position">
                	<option value="Top" <?php if( get_option('it_block_heading_position') == 'Top' ) echo 'selected="selected"'; ?>>Top</option>
                	<option value="Left" <?php if( get_option('it_block_heading_position') == 'Left' ) echo 'selected="selected"'; ?>>Left</option>
                	<option value="Right" <?php if( get_option('it_block_heading_position') == 'Right' ) echo 'selected="selected"'; ?>>Right</option>
                	<option value="Bottom" <?php if( get_option('it_block_heading_position') == 'Bottom' ) echo 'selected="selected"'; ?>>Bottom</option>
                </select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Posts per row'); ?></th>
			<td style="padding-top:0;">
            	<input type="text" name="vertical_posts_per_row" value="<?php echo get_option('it_vertical_posts_per_row'); ?>" />
                <br />
                <small>Applicable if "Block Heading" set to Left/Right</small>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Cell Title Color'); ?></th>
			<td style="padding-top:0;">
				<input type="text" name="cell_title_color" class="color-picker" value="<?php echo get_option('it_cell_title_color'); ?>" />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Cell Hover Title Color'); ?></th>
			<td style="padding-top:0;">
				<input type="text" name="cell_hover_title_color" class="color-picker" value="<?php echo get_option('it_cell_hover_title_color'); ?>" />
			</td>
		</tr>		
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Cell Font Color'); ?></th>
			<td style="padding-top:0;">
				<input type="text" name="cell_font_color" class="color-picker" value="<?php echo get_option('it_cell_font_color'); ?>" />
			</td>
		</tr>		
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Cell Hover Font Color'); ?></th>
			<td style="padding-top:0;">
				<input type="text" name="cell_hover_font_color" class="color-picker" value="<?php echo get_option('it_cell_hover_font_color'); ?>" />
			</td>
		</tr>

		
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Read More Font Color'); ?></th>
			<td style="padding-top:0;">
				<input type="text" name="read_more_font_color" class="color-picker" value="<?php echo get_option('it_read_more_font_color'); ?>" />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Display Mode'); ?></th>
			<td style="padding-top:0;">
                <select name="display_mode">
                	<option value="Highlight" <?php if( get_option('it_display_mode') == 'Highlight' ) echo 'selected="selected"'; ?>>Highlight</option>
                	<option value="Filter" <?php if( get_option('it_display_mode') == 'Filter' ) echo 'selected="selected"'; ?>>Filter</option>
                </select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Highlight Mode'); ?></th>
			<td style="padding-top:0;">
                <select name="highlight_mode">
                	<option value="On Hover" <?php if( get_option('it_highlight_mode') == 'On Hover' ) echo 'selected="selected"'; ?>>On Hover</option>
                	<option value="On Click" <?php if( get_option('it_highlight_mode') == 'On Click' ) echo 'selected="selected"'; ?>>On Click</option>
                </select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Highlight Color'); ?></th>
			<td style="padding-top:0;">
				<input type="text" name="cell_highlight_color" class="color-picker" value="<?php echo get_option('cell_highlight_color'); ?>" />
			</td>
		</tr>		
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Cell Hover'); ?></th>
			<td style="padding-top:0;">
				<input type="checkbox" name="cell_hover" id="cell_hover" value="enable" <?php if(get_option('it_cell_hover')) echo 'checked'; ?> /> <label for="cell_hover">Enable</label>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Single cell slug URL'); ?></th>
			<td style="padding-top:0;">
            	<input type="text" name="single_cell_slug_url" value="<?php echo get_option('it_single_cell_slug_url'); ?>" />
                <br />
                <small>Re-save permalink again if you update this slug URL</small>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Title Hover'); ?></th>
			<td style="padding-top:0;">
				<input type="checkbox" name="title_hover" id="title_hover" value="enable" <?php if(get_option('it_title_hover')) echo 'checked'; ?> /> <label for="title_hover">Enable</label>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Read More'); ?></th>
			<td style="padding-top:0;">
                <select name="read_more">
                	<option value="Yes" <?php if( get_option('it_read_more') == 'Yes' ) echo 'selected="selected"'; ?>>Yes</option>
                	<option value="No" <?php if( get_option('it_read_more') == 'No' ) echo 'selected="selected"'; ?>>No</option>
                </select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Read More Label'); ?></th>
			<td style="padding-top:0;">
            	<input type="text" name="readmore_label" value="<?php echo get_option('readmore_label'); ?>" />
                <br />
                <small>Read More Link Text</small>
			</td>
		</tr>		
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Load Default CSS'); ?></th>
			<td style="padding-top:0;">
                <select name="load_default_css">
                	<option value="Yes" <?php if( get_option('it_load_default_css') == 'Yes' ) echo 'selected="selected"'; ?>>Yes</option>
                	<option value="No" <?php if( get_option('it_load_default_css') == 'No' ) echo 'selected="selected"'; ?>>No</option>
                </select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row" style="padding-top:0;"><?php echo __('Fixed Highlight'); ?></th>
			<td style="padding-top:0;">
				<input type="checkbox" name="fixed_highlight" id="fixed_highlight" value="enable" <?php if(get_option('fixed_highlight')) echo 'checked'; ?> /> <label for="fixed_highlight">Enable</label>
			</td>
		</tr>		
	</table>			
	<p class="submit">
		<input type="submit" name="it_settings_options_submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
	</p>
</form>	
</div>	