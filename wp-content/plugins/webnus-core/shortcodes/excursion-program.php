<?php
function michigan_excursion_program( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'img'		=> '',
		'title'		=> '',
		'date'		=> esc_html( '04/07/2015 - 10:30 A.M' ),
		'programs'	=> '',
	), $atts) );

	if( is_numeric( $img ) )
		$img = wp_get_attachment_url( $img );

	// programs loop
	$programs_out	= '';
	$programs_data	= array();
	$programs		= (array) vc_param_group_parse_atts( $programs );

	foreach ( $programs as $data ) :

		$new_line 					= $data;
		$new_line['program_title']	= isset( $data['program_title'] ) ? $data['program_title'] : '';
		$new_line['program_text']	= isset( $data['program_text'] ) ? $data['program_text'] : '';
		$programs_data[] 			= $new_line;
	endforeach;

	foreach ( $programs_data as $line ) :
		$programs_out .= '
			<div class="program">
				<h3>' . esc_html( $line['program_title'] ) . '</h3>
				<p>' . esc_html( $line['program_text'] ) . '</p>
			</div>';
	endforeach;

	$out = '
		<div class="excursion-program">
			<div class="excursion-item">
				<header class="excursion-header">
					<img src="' . esc_url( $img ) . '" alt="' . esc_attr( $title ) . '">
					<div class="title-meta-sec">
						<h2 class="entry-title">' . esc_html( $title ) . '</h2>
						<span class="entry-time">' . esc_html( $date ) . '</span>
					</div>
				</header>
				<div class="excursion-content">
					' . $programs_out . '
				</div>
			</div>
		</div>';

	return $out;
}

add_shortcode( 'excursion-program', 'michigan_excursion_program' );