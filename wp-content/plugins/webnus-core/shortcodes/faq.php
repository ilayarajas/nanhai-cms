<?php
 function michigan_webnus_faq( $attributes, $content = null ) {
extract(shortcode_atts(	array(
	'type'=>'minimal',
	'category'=>'',
	'count'=>'6',
	'page'=>'',
	'icon'=>'',
), $attributes));
	ob_start();		
	$paged = ( is_front_page() ) ? 'page' : 'paged' ;
	$pages = ($page)?'&paged='.get_query_var($paged):'&paged=1';
	$query = new WP_Query('post_type=faq&posts_per_page='.$count.'&category_name='.$category.$pages);
		if(empty($count)){
		$count=1;
	}
	$rcount= 1;
?>
<div class="container faq-<?php echo $type ?>">
<?php
	while ($query -> have_posts()) : $query -> the_post();
		$content = get_the_content();
		$title = get_the_title();
		if ($type=='toggle'){	
			echo '[accordion title="'.$title.'"]' . $content . '[/accordion]';
		}else if ($type=='minimal'){
			$icon=($icon)?$icon:'fa-question-circle';
			echo '<article><i class="colorf '.$icon.'"></i><h4>'.$title.'</h4><p>'.$content.'</p></article>';  
		}
	endwhile;
	echo "</div>";
if($page){ ?>
	<section class="container aligncenter">
	<?php 
		if(function_exists('wp_pagenavi')) {
			wp_pagenavi( array( 'query' => $query ) );
		}
	?>
        <hr class="vertical-space2">
    </section>  
	<?php }
		$out = ob_get_contents();
		ob_end_clean();	
		wp_reset_postdata();
		return $out;
	}
 add_shortcode('faq', 'michigan_webnus_faq');
?>