<?php
// functions that manage the users.php page in admin and maybe more
class WTPReportsUser {
	static function add_status_column($columns) {	
		$columns['exam_reports'] = sprintf(__('%s Reports', 'watupro'), __('Quiz', 'watupro'));
	 	return $columns;	
	}
	
	// chart by question category performance on a single quiz
	// atts: width (of the bar), height (max-height of the chart), from=answered, correct; color
	static function chart_by_category($atts) {
		global $wpdb;
		
		$taking_id = empty($atts['taking_id']) ? $_POST['watupro_current_taking_id'] : intval($taking_id);
		
		// select taking, if not found return empty string
		$taking = $wpdb->get_row($wpdb->prepare("SELECT * FROM " . WATUPRO_TAKEN_EXAMS . " WHERE ID=%d", $taking_id));
		
		if(empty($taking->ID)) return "";
		
		$width = empty($atts['width']) ? 100 : intval($atts['width']);
		if($width < 10) $width = 10;
		$height = empty($atts['height']) ? 300 : intval($atts['height']);
		if($height < 100) $height = 100;
		$step = round($height / 100, 2);
		
		$color = empty($atts['color']) ? '#CCC' : $atts['color'];
		
		$from = empty($atts['from']) ? 'correct' : $atts['from'];
		if(!in_array($from, array('correct', 'answered', 'points', 'percent_max_points'))) $from = 'correct';
		
		$survey_sql = empty($atts['include_survey_questions']) ? 'AND tQ.is_survey=0' : '';
		
		// select answers join by category
		$answers = $wpdb->get_results($wpdb->prepare("SELECT tA.answer, tA.is_correct, tC.ID as cat_id, tC.name as cat,
			tQ.is_survey as is_survey, tA.points as points, tA.question_id as question_id
			FROM ".WATUPRO_STUDENT_ANSWERS." tA JOIN ".WATUPRO_QUESTIONS." tQ ON tQ.ID = tA.question_id $survey_sql
			LEFT JOIN ".WATUPRO_QCATS." tC ON tC.ID = tQ.cat_id
			WHERE tA.taking_id=%d order by tA.ID", $taking_id));
			
		// when calculating % of max points we need to get max points for each question
		// we also need this when doing the chart on absolute points to properly identify the step 
		if($from == 'percent_max_points' or $from == 'points') {
			$exam = $wpdb->get_row($wpdb->prepare("SELECT * FROM " . WATUPRO_EXAMS . " WHERE ID=%d", $taking->exam_id));
			$max_of_all = 0;
			foreach($answers as $answer) $qids[] = $answer->question_id;
	 		$questions = $wpdb->get_results("SELECT * FROM ".WATUPRO_QUESTIONS." WHERE ID IN (".implode(',', $qids).")");
	 		$_watu = new WatuPRO();
	 		$_watu->match_answers($questions, $exam);	 	
	 		foreach($questions as $question) {
	 			foreach($answers as $cnt=>$answer) {
	 				if($answer->question_id == $question->ID) {
	 					$answers[$cnt]->max_points = WTPQuestion::max_points($question);
	 				}
	 			}	 			
	 		}
		}	// end percent_max_points
		
		// fill categories array		
		$cats = array();
		foreach($answers as $answer) {			
			if(!isset($cats[$answer->cat_id])) { 
				// create the cat element			
				if(empty($answer->cat)) $answer->cat = __('Uncategorized', 'watupro');
				$cats[$answer->cat_id] = array("id"=>$answer->cat_id, "name"=>$answer->cat, 
					"num_questions"=>0, "num_answered"=>0, "num_correct"=>0, "points"=>0, "max_points"=>0);
			}
			
			$cats[$answer->cat_id]['num_questions']++;
			if(!empty($answer->answer)) $cats[$answer->cat_id]['num_answered']++;
			if(!empty($answer->is_correct)) $cats[$answer->cat_id]['num_correct']++;
			if(!empty($answer->points)) $cats[$answer->cat_id]['points'] += $answer->points;
			if(!empty($answer->max_points)) $cats[$answer->cat_id]['max_points'] += $answer->max_points;
		}	
		
		// now accordingly to "from" calculate the % in each category
		foreach($cats as $cnt=>$cat) {
			switch($from) {				
				case 'points': 
					$percent = $cat['points'];
					if($max_of_all < $cat['max_points']) $max_of_all = $cat['max_points']; 
				break;
				case 'percent_max_points': 
					$percent = ($cat['max_points'] and $cat['points'] > 0) ? round(100 * $cat['points'] / $cat['max_points']) : 0; 
				break;
				case 'answered': $percent = round(100 * $cat['num_answered'] / $cat['num_questions']); break;
				case 'correct': default: $percent = round(100 * $cat['num_correct'] / $cat['num_questions']); break;
			}			
			$cats[$cnt]['percent'] = $percent;
		}
			
		// when working with absolute points (from = "points") we have to redefine the $step
		if($from == 'points') $step = round($height / $max_of_all);
			
		include(WATUPRO_PATH . "/modules/reports/views/barchart-by-category.html.php");
	} // end chart_by_category 
}