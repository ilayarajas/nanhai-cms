<?php
// level model - will handle badges too because they share the same table
class WatuPROPlayLevel {
	function add($vars) {
		global $wpdb;
		$vars['name'] = esc_attr($vars['name']);
		$is_mycred_badge = empty($vars['is_mycred_badge']) ? 0 : 1;
		$mycred_id = empty($vars['mycred_id']) ? 0 : intval($vars['mycred_id']);		
		
		$result = $wpdb->query( $wpdb->prepare("INSERT INTO ".WATUPROPLAY_LEVELS." SET
			name=%s, content=%s, required_points=%d, required_percent=%d, required_num_exams=%d,
			required_exam_details=%s, category_id=%d, required_category=%d, atype=%s, rank=%d,
			is_open_badge=%d, badge_graphic=%s, badge_criteria_url=%s, is_badgeos_badge=%d, 
			badgeos_id=%d, points_cat_id=%d, percent_cat_id=%d, use_latest_points=%d, 
			is_mycred_badge=%d, mycred_id=%d", 
			$vars['name'], $vars['content'], $vars['required_points'], $vars['required_percent'],
			$vars['required_num_exams'], @$vars['required_exam_details'], @$vars['category_id'], 
			@$vars['required_category'], $vars['atype'], @$vars['rank'], @$vars['is_open_badge'],
			@$vars['badge_graphic'], @$vars['badge_criteria_url'], @$vars['is_badgeos_badge'], 
			@$vars['badgeos_id'], $vars['points_cat_id'], $vars['percent_cat_id'], 
			@$vars['use_latest_points'], $is_mycred_badge, $mycred_id) );
			
		if($result === false) return false;
		return true;	
	}
	
	function save($vars, $id) {
		global $wpdb;
		$vars['name'] = esc_attr($vars['name']);
		$is_mycred_badge = empty($vars['is_mycred_badge']) ? 0 : 1;
		$mycred_id = empty($vars['mycred_id']) ? 0 : intval($vars['mycred_id']);
		
		$result = $wpdb->query( $wpdb->prepare("UPDATE ".WATUPROPLAY_LEVELS." SET
			name=%s, content=%s, required_points=%d, required_percent=%d, required_num_exams=%d,
			required_exam_details=%s, category_id=%d, required_category=%d, atype=%s, rank=%d,
			is_open_badge=%d, badge_graphic=%s, badge_criteria_url=%s, is_badgeos_badge=%d, 
			badgeos_id=%d, points_cat_id=%d, percent_cat_id=%d, use_latest_points=%d,
			is_mycred_badge=%d, mycred_id=%d WHERE id=%d", 
			$vars['name'], $vars['content'], $vars['required_points'], $vars['required_percent'],
			$vars['required_num_exams'], @$vars['required_exam_details'], @$vars['category_id'], 
			@$vars['required_category'], $vars['atype'], @$vars['rank'], @$vars['is_open_badge'],
			@$vars['badge_graphic'], @$vars['badge_criteria_url'], @$vars['is_badgeos_badge'], 
			@$vars['badgeos_id'], $vars['points_cat_id'], $vars['percent_cat_id'], @$vars['use_latest_points'], 
			$is_mycred_badge, $mycred_id, $id) );
			
		if($result === false) return false;
		return true;	
	}
	
	function delete($id) {
		global $wpdb;
		
		$result = $wpdb -> query( $wpdb->prepare("DELETE FROM ".WATUPROPLAY_LEVELS." WHERE id=%d", $id));
		
		if($result === false) return false;
		return true;	
	}
	
	// figures out whether the user has to be assigned a new level
	// and assigns it
	// this method is query-heavy and should only be called when user has submitted a test
	// $userstats comes pre-calculated from the user module in this plugin.
	// Both assign_level and assign_badges will be called by 1 method in that class upon completing exam by the user
	// or editing exam results by admin
	function assign_level($uid, $userstats, $exams, $taking_id) {
		global $wpdb;
			
		// find all levels
		$levels = $wpdb->get_results("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE atype='level' ORDER BY rank", ARRAY_A);
		
		// for each level we need to check:
		// 1) that it's not already assigned
		// 2 if requirements are met
		// 3 when matching level is found, stop searching
		foreach($levels as $level) {
			if($this->check_requirements($userstats, $level, $exams)) {			
				// notify user/admin here if required
				$this->notify($uid, $level);	
				update_user_meta($uid, 'watuproplay_user_level', $level['id']);
				return true;
			}			
		} // end foreach level
	}
	
	// figures out whether the user has to be assigned a new badge
	// and assigns it
	// this method is query-heavy and should only be called when user has submitted a test
	// $userstats comes pre-calculated from the WatuPROPlayUser model in this plugin.
	// Both assign_level and assign_badges will be called by 1 method in that class upon completing exam by the user
	// or editing exam results by admin
	function assign_badges($uid, $userstats, $exams, $taking_id) {
		global $wpdb;

		// find all badges
		$badges = $wpdb->get_results("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE atype='badge' ORDER BY id", ARRAY_A);
		$user_badges = get_user_meta($uid, 'watuproplay_user_badges', true);
		$user_badges = explode("|", $user_badges);
	
		$badges_to_assign = array();
		foreach($badges as $badge) {
			
			if($this->check_requirements($userstats, $badge, $exams)) {			
				$badges_to_assign[] = $badge['id'];
				
				// insert in history
				// don't re-insert history item for a badge that I already have
				if(!@in_array($badge['id'], $user_badges)) {		
					$wpdb->query( $wpdb->prepare("INSERT INTO ".WATUPROPLAY_HISTORY." SET
					user_id=%d, item_id=%d, action='assigned badge', timestamp=%d, datetime=NOW(), taking_id=%d",
					$uid, $badge['id'], time(), $taking_id));
				} // end if badge doesn't already exist
				
				// award as badgeos badge?				
				if(!empty($badge['is_badgeos_badge']) and is_numeric($badge['badgeos_id']) and function_exists('badgeos_award_achievement_to_user')) {					
					badgeos_award_achievement_to_user($badge['badgeos_id'], $uid);
				}		
				
				// award as mycred badge?
				if(!empty($badge['is_mycred_badge']) and is_numeric($badge['mycred_id'])) {
					update_user_meta( $uid, 'mycred_badge' . $badge['mycred_id'], 0, true );
				}		
				
			} // end if requirements satisfied
		} // end foreach badge
		
		$this->notify_badges($uid, $badges_to_assign);
		update_user_meta($uid, 'watuproplay_user_badges', "|".implode("|",$badges_to_assign)."|");
	}
	
	// gets and returns the user level
	function get_user_level($uid) {
		global $wpdb;
		$level_id = get_user_meta($uid, 'watuproplay_user_level', true);

		$level = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE id=%d", $level_id));		
				
		return $level;
	}
	
	// gets current badges
	function get_user_badges($uid) {
		$badges = get_user_meta($uid, 'watuproplay_user_badges', true);
		$badges = explode("|", $badges);
		$badges = array_filter($badges);
		return $badges;
	}
	
	// check requirements for a level/badge
	// $level is selected as array
	function check_requirements($userstats, $level, $exams) {
		// here in the future we will have a call to $_user->recalc_stats($level->category_id) 
		// in case the level is limited by cat. For now NYI!
		
		// which points to use - all or from latest attempt?
		$compare_points = empty($level['use_latest_points']) ? 'points' : 'latest_points';
					
		if(empty($level['points_cat_id']) and $level['required_points'] > $userstats[$compare_points]) return false;
		if(!empty($level['points_cat_id'])) {
			// requirement is points from given category
			if(empty($userstats['qcats'][$level['points_cat_id']]) or $userstats['qcats'][$level['points_cat_id']][$compare_points] < $level['required_points']) return false;
		}		
		
		if(empty($level['percent_cat_id']) and $level['required_percent'] > $userstats['percent']) return false;
			
		if(!empty($level['percent_cat_id'])) {
			// requirement is points from given category			
			if(empty($userstats['qcats'][$level['percent_cat_id']]) or $userstats['qcats'][$level['percent_cat_id']]['percent'] < $level['required_percent']) return false;
		}		
			
		if($level['required_num_exams'] > $userstats['num_exams']) return false;

		// required all exams from a category?
		if(!empty($level['required_category'])) {			
			foreach($exams as $exam) {
				if($exam->cat_id != $level['required_category']) continue;
				
				// even one non-completed exam from this cat means failed requirement
				if(!@in_array($exam->ID, $userstats['completed_exam_ids'])) return false;
			} // end foreach exam
		} // end required category check
		
		// specific exam(s) required (with specific grade)
		// this will be stored as [[exam_id, grade_id],[exam_id, grade_id]]
		$required_exam_details = unserialize($level['required_exam_details']);
		if(!empty($required_exam_details) and is_array($required_exam_details) and sizeof($required_exam_details)) {
			foreach($required_exam_details as $required_exam) {
				list($exam_id, $grade_id) = $required_exam;
				$exam_satisfied = false;
				foreach($userstats['takings'] as $taking) {
					if($taking->exam_id==$exam_id and ($taking->grade_id==$grade_id or empty($grade_id))) $exam_satisfied = true;
				}
				
				if(!$exam_satisfied) return false;
			} // end foreach required exam
		} // end required exam details check		
		
		// if we reached this point, means all the requirements were met and the level/badge can be assigned	
		return true;
	}
	
	// send notice to user and/or admin when new level is reached
	function notify($uid, $level) {
		global $wpdb;
		
		$do_notify_admin = get_option('watuproplay_admin_level_email');
		$do_notify_user = get_option('watuproplay_user_level_email');
		
		if(empty($do_notify_admin) and empty($do_notify_user)) return true;
		
		// now let's get the old level. We can do this because this function is called 
		// before actually applying the new level to the user
		$old_level_id = get_user_meta($uid, 'watuproplay_user_level', true);
		
		// go further only if the level is changed
		if($old_level_id == $level['id']) return true;
		
		if($old_level_id) {
			$old_level_name = $wpdb->get_var( $wpdb->prepare("SELECT name FROM ".WATUPROPLAY_LEVELS." WHERE id=%d", $old_level_id));
		} 
		else $old_level_name = __('n/a', 'watuproplay');
		
		$user_info = get_userdata($uid);
		
		// now send the emails
		if($do_notify_admin) {
			$sender = watupro_admin_email(); 
			$receiver = get_option('admin_email');
			$subject = get_option('watuproplay_admin_level_subject');
			$message = stripslashes(get_option('watuproplay_admin_level_message'));
			
			$message = str_replace('{{user-name}}', $user_info->user_login, $message);
			$message = str_replace('{{user-email}}', $user_info->user_email, $message);
			$message = str_replace('{{user-id}}', $uid, $message);
			$message = str_replace('{{old-level}}', $old_level_name, $message);
			$message = str_replace('{{new-level}}', $level['name'], $message);			
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
			wp_mail($receiver, $subject, $message, $headers);	
		}
		
		if($do_notify_user) {
			$sender = watupro_admin_email();
			$receiver = $user_info->user_email;
			$subject = get_option('watuproplay_user_level_subject');
			$message = stripslashes(get_option('watuproplay_user_level_message'));
			
			$message = str_replace('{{user-name}}', $user_info->user_login, $message);
			$message = str_replace('{{user-email}}', $user_info->user_email, $message);
			$message = str_replace('{{user-id}}', $uid, $message);
			$message = str_replace('{{old-level}}', $old_level_name, $message);
			$message = str_replace('{{new-level}}', $level['name'], $message);
						
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";			
			wp_mail($receiver, $subject, $message, $headers);	
		}
		
		return true;
	}
	
	// similar to notify() but about badges. The difference is that we'll notify once for all new badges.
	function notify_badges($uid, $badges_to_assign) {
		global $wpdb;
		
		if(!is_array($badges_to_assign)  or !sizeof($badges_to_assign)) return true;
		
		$do_notify_admin = get_option('watuproplay_admin_badge_email');
		$do_notify_user = get_option('watuproplay_user_badge_email');
		
		if(empty($do_notify_admin) and empty($do_notify_user)) return true;
		
		// figure out if there is really a change in badges
		// we'll first take the current badges - this is possible because notify_badges
		// is called before the new badges are assigned.
		$old_badges = $this->get_user_badges($uid);
		$diff = array_diff($badges_to_assign, $old_badges);
		if(!sizeof($diff)) return true;
		
		// construct the string
		$badges = $wpdb->get_results("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE id IN (".implode(",", $diff).") ORDER BY name");
		$new_badges_str = "";
		foreach($badges as $cnt=>$badge) {
			if($cnt>0) $new_badges_str .= ", ";
			$new_badges_str .= $badge->name;
		}		
		
		$user_info = get_userdata($uid);
		
		// now send the emails
		if($do_notify_admin) {
			$sender = watupro_admin_email(); 
			$receiver = get_option('admin_email');
			$subject = get_option('watuproplay_admin_badge_subject');
			$message = stripslashes(get_option('watuproplay_admin_badge_message'));
			
			$message = str_replace('{{user-name}}', $user_info->user_login, $message);
			$message = str_replace('{{user-email}}', $user_info->user_email, $message);
			$message = str_replace('{{user-id}}', $uid, $message);		
			$message = str_replace('{{assigned-badges}}', $new_badges_str, $message);
			$message = str_replace('{{open-badges-url}}', site_url("?wtp_accept_badges=1&uid=".$uid."&bids=".implode(".", $diff)), $message);
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
			$headers .= 'From: '. $sender . "\r\n";
			$message = wpautop($message, false);
			wp_mail($receiver, $subject, $message, $headers);	
		}
		
		if($do_notify_user) {
			$sender = watupro_admin_email();
			$receiver = $user_info->user_email;
			$subject = get_option('watuproplay_user_badge_subject');
			$message = stripslashes(get_option('watuproplay_user_badge_message'));
			
			$message = str_replace('{{user-name}}', $user_info->user_login, $message);
			$message = str_replace('{{user-email}}', $user_info->user_email, $message);
			$message = str_replace('{{user-id}}', $uid, $message);
			$message = str_replace('{{assigned-badges}}', $new_badges_str, $message);
			$message = str_replace('{{open-badges-url}}', site_url("?wtp_accept_badges=1&uid=".$uid."&bids=".implode(".", $diff)), $message);
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
			$headers .= 'From: '. $sender . "\r\n";
			$message = wpautop($message, false);
			wp_mail($receiver, $subject, $message, $headers);	
		}
	}
}