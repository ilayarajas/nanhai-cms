<div class="team-awesome-member-avatar-box">

    <div class="team-awesome-member-box-stamp">
        <div class="team-awesome-member-box-stamp-inner" style="background-image: url('<?php echo $member->avatar; ?>')"></div>
        
         <?php if ($member->vidavatar != ""): ?>
        
        <div class="team-awesome-member-box-stamp-inner-video">
            <video loop muted src="<?php echo $member->vidavatar; ?>"></video>
            <canvas width="225" height="225"></canvas>
        </div>
        
        <?php endif; ?>
    </div>

    <div class="team-awesome-social-box">
        <?php
        $socials = $member->social;
        $letters = array("a", "b", "c", "d", "e", "f", "g", "h");
        ?>

        <?php foreach ($socials as $k => $social) : ?>
            <?php
            $letter = $letters[$k];
            ?>
            <?php if ($social["link"] != "") : ?>
            <div class="team-awesome-social-button <?php echo $letter; ?>"><div class="ta-color-<?php echo $social["icon"] ?> team-awesome-social-button-inner"><a href="<?php echo $social["link"] ?>"><i class="fa <?php echo $social["icon"] ?>"></i></a></div></div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>

</div>