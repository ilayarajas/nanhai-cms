<?php
// levels controller
class WatuPROPlayRewards {
	static function manage() {
		global $wpdb;
		$_reward = new WatuPROPlayReward();
			
		$action = empty($_GET['action']) ? 'list' : $_GET['action'];
		switch($action) {
			case 'add':
				if(!empty($_POST['ok'])) {
					$_reward -> add($_POST);
					watuproplay_redirect('admin.php?page=watuproplay_rewards');
				}
				
				require(WATUPROPLAY_PATH."/views/reward.html.php");
			break;
			
			case 'edit':
				if(!empty($_POST['del'])) {
					$_reward -> delete($_GET['id']);
					watuproplay_redirect('admin.php?page=watuproplay_rewards');
				}			
			
				if(!empty($_POST['ok'])) {
					$_reward->edit($_POST, $_GET['id']);
					watuproplay_redirect('admin.php?page=watuproplay_rewards');
				}
				
				// select this level
				$reward = $wpdb -> get_row( $wpdb->prepare("SELECT * FROM ".WATUPROPLAY_REWARDS." WHERE id=%d", $_GET['id']));
				require(WATUPROPLAY_PATH."/views/reward.html.php");
			break;			
			
			case 'list':
				// save the email notice settings
				if(!empty($_POST['save_notice_email'])) {
					update_option('watuproplay_redeem_email', $_POST['redeem_email']);
					update_option('watuproplay_redeem_subject', $_POST['redeem_subject']);
					update_option('watuproplay_redeem_message', $_POST['redeem_message']);
				}			
			
				// save on-screen message
				if(!empty($_POST['save_notice_flash'])) {
					update_option('watuproplay_redeem_flash', $_POST['redeem_flash']);
				}			
			
				// select currently available badges
				$rewards = $wpdb -> get_results("SELECT * FROM ".WATUPROPLAY_REWARDS." ORDER BY title");		
				
				require(WATUPROPLAY_PATH."/views/rewards.html.php");
			default:
			break;
		}
	}
	
	// view/mark redeemed rewards
	static function history() {
		// select all redeemed rewards by delivered|not delivered|all and limit 50 per page
		global $wpdb;
		
		// change delivered status of item
		if(!empty($_POST['ok'])) {			
			$all_ids_sql = implode(",", $_POST['ids']);			
			$shipped_ids = empty($_POST['shipped_ids']) ? array(0) : $_POST['shipped_ids'];	
			$shipped_ids_sql = implode(",", $shipped_ids);		
			
			$wpdb->query("UPDATE ".WATUPROPLAY_REDEEMED." SET is_shipped=1 WHERE id IN ($shipped_ids_sql)");
			$wpdb->query("UPDATE ".WATUPROPLAY_REDEEMED." SET is_shipped=0 WHERE id NOT IN ($shipped_ids_sql) AND id IN ($all_ids_sql)");
		}
		
		$offset = empty($_GET['offset']) ? 0 : $_GET['offset'];

		$status_sql = "";		
		if(isset($_GET['status']) and $_GET['status']!='all') {
			$status_sql = $wpdb->prepare("WHERE tH.is_shipped = %d", $_GET['status']); 	
		}
		
		$items = $wpdb -> get_results($wpdb->prepare("SELECT SQL_CALC_FOUND_ROWS tH.*, tR.title as item_title, tU.user_login as user_login
			FROM ".WATUPROPLAY_REDEEMED." tH JOIN ".WATUPROPLAY_REWARDS." tR ON tR.id = tH.item_id 
			JOIN {$wpdb->users} tU ON tU.id = tH.user_id
			$status_sql LIMIT %d, 50", $offset));		
			
		// get count
		$cnt_items = $wpdb->get_var("SELECT FOUND_ROWS()");
		
		require(WATUPROPLAY_PATH."/views/rewards-history.html.php");		
	}
	
	// to display extra payment option from the play plugin
	static function payment_options() {
		$currency = get_option('watupro_currency');
		$accept_paypoints = get_option('watupro_accept_paypoints');
		$paypoints_button = get_option('watupro_paypoints_button');
		if(empty($paypoints_button)) $paypoints_button = "<p align='center'>".__('You can also buy access to this exam with {{{points}}} points from your balance. You currently have [watuproplay-points] points total.', 'watuproplay')."</p><p align='center'>{{{button}}}</p>";
		include(WATUPROPLAY_PATH."/views/payment-options.html.php");
	}
}