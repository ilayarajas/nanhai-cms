<?php
            $o = '<style>
            label{
            width: 30%;
            display: inline-block;
            }
            select ,textarea{
                width: 60%;
            }
            .half{
                width: 45%;
                padding:0 10px;
                float: left;
            }
            #' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'cat_box{
                margin-left: 32%;
            }
            .wp-picker-container, .wp-picker-container:active{
                vertical-align: middle;
            }
            @media (max-width: 720px){
                label{
                    width: 100%;
                    display: inline-block;
                }
                select,textarea{
                    width: 100%;
                }
                #' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'cat_box{
                    margin-left: 0;
                }
                .half{
                    width: 90%;
                    float: none;
                }
            }
            </style>
            <h1>' . __('ShortCode Generator', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</h1>
					<div class="half"><h3>' . __('Shortcode Options', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</h3>

									<div id="tabsholder">';
            // tabs
            $o .= '<ul class="tabs">
                    <li id="tab1">'.__("General" ,__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</li>
                    <li id="tab2">'.__("Layout" ,__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</li>
                    <li id="tab3">'.__("Styles" ,__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</li>
                    <li id="tab4">'.__("Hover Styles" ,__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</li>
                    <li id="tab5">'.__("View Details" ,__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</li>
                    </ul><div class="postbox">
									<form id="it_shortcode_generator_form" style="padding:5px 20px;">';
            // end tabs
            $o .='<div class="contents marginbot">';
            // General Tab
            $o .= '<div id="content1" class="tabscontent">';
            // query builder
            $o .= '<p><label for="query_hr" >' . __('Query Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="query_hr"></p>';
            // category select

            $o .= '<p>
                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'category">' . __('Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
                <select id="it_custom_cat" class="it_inputs" >
                    <option value="true">' . __('All Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                    <option value="false">' . __('Custom Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                </select>
            </p>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'cat_box', array(
                'parent_id' => array('it_custom_cat'),
                'it_custom_cat' => array('select', 'false')
            ));
            $categories = get_terms(__Custom_Post_Taxonomy__);
            if ($categories) {
                $o .= '<p id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'cat_box">' . $dep;
                foreach ($categories as $category) {
                    $o .= '<input type="checkbox" name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'category[]" value="' . $category->term_id . '" class="it_inputs" >' . $category->name . '<br>';
                }
                $o .= '</p>';
            }
            $o .= '<p>
                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'count">' . __('Logo Count', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
                     <input
                     name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'count"
                     id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'count"
                     class="it_inputs"
                     type="number" min="0" size="1" value="-1">
            </p>';
            // OrderBy

            $o .= '<p>
            <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'orderby" >' . __('Order By', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
            <select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'orderby" name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'orderby" class="it_inputs">
                <option value="order">' . __('Order Field', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="title">' . __('Title', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="id">' . __('ID', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="date">' . __('Date', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="modified">' . __('Modified', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
            </p>';
                // Order
                $o .= '<p>
                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'order">' . __('Order', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
                <select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'order" name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'order" class="it_inputs">
                    <option value="ASC">' . __('Ascending', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                    <option value="DESC">' . __('Descending', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                </select>
            </p></div>';

            // layout Tab
            $o .= '<div id="content2" class="tabscontent">';
            // layout setting
            $o .= '<p><label for="layout_hr" >' . __('Layout Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="layout_hr"></p>';
            // Layout Select
            $o .= '<p>
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout">' . __('Layout', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout" name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout" class="it_inputs">
							<option value="list">list</option>
							<option value="grid">grid</option>
							<option value="carousel">carousel</option>
							<option value="isotope">isotope</option>
						</select>
					</p>';

            // fields for grid layout

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_desktop', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'grid', 'isotope')
            ));

            $o .= '<p>' . $dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_desktop">' . __('Desktop Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_desktop"
							   name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_desktop"
							   class="it_inputs">
							<option vlaue="1">1</option>
							<option vlaue="2">2</option>
							<option vlaue="3">3</option>
							<option vlaue="4" selected>4</option>
							<option vlaue="6">6</option>
							<option vlaue="12">12</option>
						</select>
					</p>';

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_tablet', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'grid', 'isotope')
            ));

            $o .= '<p>' . $dep . '  	<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_tablet">' . __('Tablet Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_tablet"
							   name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_tablet"
							   class="it_inputs">
							<option vlaue="1">1</option>
							<option vlaue="2">2</option>
							<option vlaue="3">3</option>
							<option vlaue="4" selected>4</option>
							<option vlaue="6">6</option>
							<option vlaue="12">12</option>
						</select>
					</p>';

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_mobile', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'grid', 'isotope')
            ));

            $o .= '<p>' . $dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_mobile">' . __('Mobile Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_mobile"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'col_mobile"
						       class="it_inputs">
						    <option vlaue="1">1</option>
							<option vlaue="2" selected>2</option>
							<option vlaue="3">3</option>
							<option vlaue="4">4</option>
							<option vlaue="6">6</option>
							<option vlaue="12">12</option>
						</select>
					</p>';
            // end grid fields

            // fields for carousel layout
            $dep = it_logo_showcase_dependency('carousel_hr', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel')
            ));
            $o .= '<p>' . $dep . '<label for="carousel_hr" >' . __('Carousel Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="carousel_hr"></p>';
            // skin
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'skin', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel')
            ));

            $o .= '<p>' . $dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'skin">' . __('Carousel Skin', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'skin"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'skin"
						       class="it_inputs">
		                    <option value="light" >'.__("Light",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</option>
		                    <option value="dark" >'.__("Dark",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</option>
		                    <option value="red" >'.__("Red",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</option>
		                    <option value="orange" >'.__("Orange",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</option>
		                    <option value="blue" >'.__("Blue",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</option>
		                    <option value="green" >'.__("Green",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</option>
						</select>
					</p>';
            //auto play
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel')
            ));

            $o .= '<p>' . $dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay">' . __('Auto Play', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay"
						       class="it_inputs">
						    <option value="true">true</option>
							<option value="false">false</option>
						</select>
					</p>';

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay_speed', array(
                'parent_id' => array(
                    __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout',
                    __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay'
                ),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay' => array('select', 'true')
            ));
            $o .='<p>'. $dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay_speed">' . __('Auto Play Speed', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<input type="text" size="4" value="3000" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay_speed"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay_speed"
						       class="it_inputs">
					</p>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'pause', array(
                'parent_id' => array(
                    __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout',
                    __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay'
                ),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'autoplay' => array('select', 'true')
            ));
            $o .= '<p>'.$dep . '<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'pause">' . __('Stop On Hover', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
                                    <select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'pause"
                                           name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'pause"
                                           class="it_inputs">
                                        <option value="true" >True</option>
                                        <option value="false">False</option>
                                    </select>
                                </p>';

            //items per view
            $dep = it_logo_showcase_dependency('items_hr', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel')
            ));
            $o .= '<p>' . $dep . '<label for="items_hr" >' . __('Carousel Items per view', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><br id="items_hr"></p>';

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'laptop_item', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel')
            ));

            $o .= '<p>' . $dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'laptop_item">' . __('Laptop', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<input type="text" size="3" value="4" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'laptop_item"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'laptop_item"
						       class="it_inputs">
					</p>';

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'desktop_item', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel')
            ));

            $o .= '<p>'.$dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'desktop_item">' . __('Desktop', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<input type="text" size="3" value="4" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'desktop_item"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'desktop_item"
						       class="it_inputs">
					</p>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tablet_item', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel')
            ));

            $o .= '<p>'.$dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tablet_item">' . __('Tablet', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<input type="text" size="3" value="3" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tablet_item"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tablet_item"
						       class="it_inputs">
					</p>';

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'mobile_item', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel')
            ));

            $o .= '<p>'.$dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'mobile_item">' . __('Mobile', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<input type="text" size="3" value="2" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'mobile_item"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'mobile_item"
						       class="it_inputs">
					</p>';

            // paginetion & controls

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'pagination', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel')
            ));

            $o .= '<p>' . $dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'pagination">' . __('Show Pagination', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'pagination"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'pagination"
						       class="it_inputs">
		                    <option value="true" >true</option>
		                    <option value="false" >false</option>
						</select>
					</p>';


            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'navigation', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'carousel')
            ));

            $o .= '<p>' . $dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'navigation">' . __('Show Controls', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'navigation"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'navigation" class="it_inputs" >
		                    <option value="true" >true</option>
		                    <option value="false" >false</option>
						</select>
					</p>';
            // end carousel fields
            // fields for isotope layout
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'filter_button_skin', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'isotope')
            ));

            $o .= '<p>' . $dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'filter_button_skin">' . __('Filter Button Theme', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'filter_button_skin"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'filter_button_skin" class="it_inputs" >
		                    <option value="light" >'.__("Light",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</option>
		                    <option value="dark" >'.__("Dark",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</option>
						</select>
					</p>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'filter_button_position', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'isotope')
            ));

            $o .= '<p>' . $dep . '
						<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'filter_button_position">' . __('Filter Button Position', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'filter_button_position"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'filter_button_position" class="it_inputs" >
		                    <option value="left" >' . __("Left", __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
		                    <option value="right" >' . __("Right", __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
		                    <option value="center" >' . __("Center", __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
						</select>
					</p></div>';
            // Style Tab
            $o .= '<div id="content3" class="tabscontent">';
            // Style setting
            $o .= '<p><label for="border_hr" >' . __('Style Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="border_hr"></p>';
            // background Item color
            $o .= '<p>
					<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'background_color" >' . __('Item Background Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'background_color"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'background_color"
					        type="text" class="wp_ad_picker_color it_inputs" value="" data-default-color="">';
            // borders
            $o .= '<p><label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style">' . __('Border Style', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style"
						       name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style" class="it_inputs" >
						    <option value="none" >None</option>
		                    <option value="solid" >Solid</option>
		                    <option value="dashed" >Dashed</option>
		                    <option value="dotted" >Dotted</option>
						</select>
					</p>';

            $dep = it_logo_showcase_dependency('border_width_title', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));
            $o .= $dep.'<p><label id="border_width_title" >' . __('Border Width', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_top_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_top', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_top_box">
                        <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_top" >' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                                <input type="number" name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_top"
                                                    id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_top"
                                                    value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                    title="' . __('Only Digits!', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '" class="input-text qty text it_inputs" />
                                                <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                        </div>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_right_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_right', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_right_box">
                                    <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_right" >' . __('Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                                            <input type="number"
                                                                name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_right"
                                                                id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_right"
                                                                value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                                class="input-text qty text it_inputs" />
                                                            <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                    </div>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_bottom_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_bottom', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_bottom_box">
                                    <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_bottom" >' . __('Bottom', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                                            <input type="number"
                                                                name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_bottom"
                                                                id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_bottom"
                                                                value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                                class="input-text qty text it_inputs" />
                                                            <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                    </div>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_left_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_left', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_left_box">
                                    <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_left" >' . __('Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                                            <input type="number"
                                                                name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_left"
                                                                id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_width_left"
                                                                value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                                class="input-text qty text it_inputs" />
                                                            <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                    </div>';

            // end border width
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_color_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted', 'double')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_color', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_style' => array('select', 'solid', 'dashed', 'dotted', 'double')
            ));

            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_color_box">
					<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_color" >' . __('Border Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_color"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_color"
					        type="text" class="wp_ad_picker_color it_inputs" value="#eee" data-default-color="#eee">
			</div>
            </p>';
            // border radius

            $o .= '<p><label id="border_radius_title" >' . __('Border Radius', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';

            $o .= '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_top_right_box">
                    <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_top_right" >' . __('Top-Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                        <input type="number" name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_top_right"
                            id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_top_right"
                            value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                            class="input-text qty text it_inputs" />
                        <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                    </div>';
            $o .= '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_top_left_box">
                        <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_top_left" >' . __('Tpo-left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                            <input type="number"
                                name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_top_left"
                                id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_top_left"
                                value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                class="input-text qty text it_inputs" />
                            <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                    </div>';
            $o .= '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_bottom_right_box">
                        <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_bottom_right" >' . __('Bottom-Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                            <input type="number"
                                name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_bottom_right"
                                id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_bottom_right"
                                value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                class="input-text qty text it_inputs" />
                            <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                    </div>';

            $o .= '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_bottom_left_box">
                                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_bottom_left" >' . __('Bottom-Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                    <input type="number"
                                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_bottom_left"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'border_radius_bottom_left"
                                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text it_inputs" />
                                    <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                </div><hr>';

            // end border radius

            // padding
            $o .= '<p><label >' . __('Padding', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';
            $o .= '<div class="small-lbl-cnt">
									<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_top" class="small-label">'.__('Top',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</label>
									<input type="number"
                                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_top"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_top"
                                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text it_inputs" />
									<span class="input-unit">'.__('px',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_right" class="small-label">'.__('Right',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</label>
									<input class="it_inputs"  type="number"
                                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_right"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_right"
                                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text it_inputs" />
									<span class="input-unit">'.__('px',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_bottom" class="small-label">'.__('Bottom',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</label>
									<input class="it_inputs"  type="number"
                                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_bottom"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_bottom"
                                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text it_inputs" />
									<span class="input-unit">'.__('px',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</span>
                                </div>
								<div class="small-lbl-cnt">
									<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_left" class="small-label">'.__('Left',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</label>
									<input class="it_inputs"  type="number"
                                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_left"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'padding_left"
                                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text it_inputs" />
									<span class="input-unit">'.__('px',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</span>
								</div>';

            // margin
            $o .= '<p><label >' . __('margin', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';
            // dep for remove margin top and bottom when carousel is selected
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_top_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'grid' ,'list' ,'isotope')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_top', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'grid' ,'list' ,'isotope')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_top_box">
                            <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_top" >' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                <input type="number"
                                    name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_top"
                                    id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_top"
                                    value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                    class="input-text qty text it_inputs" />
                                <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                        </div>';

            $o .= '<div class="small-lbl-cnt">
									<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_right" class="small-label">'.__('Right',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</label>
									<input class="it_inputs"  type="number"
                                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_right"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_right"
                                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text it_inputs" />
									<span class="input-unit">'.__('px',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</span>
								</div>';

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_bottom_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'grid' ,'list' ,'isotope')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_bottom', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout' => array('select', 'grid' ,'list' ,'isotope')
            ));
            $o .= $dep .'<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_bottom_box">
									<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_bottom" class="small-label">'.__('Bottom',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</label>
									<input class="it_inputs"  type="number"
                                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_bottom"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_bottom"
                                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text it_inputs" />
									<span class="input-unit">'.__('px',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</span>
                                </div>
								<div class="small-lbl-cnt">
									<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_left" class="small-label">'.__('Left',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</label>
									<input class="it_inputs"  type="number"
                                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_left"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'margin_left"
                                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text it_inputs" />
									<span class="input-unit">'.__('px',__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</span>
								</div>'.$dep;
            // shadow options
            $o .= '<p><label for="shadow_hr" >' . __(' Shadow Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="shadow_hr"></p>';
            $o .= '<p><div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_box">
                                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow" >' . __('Box Shadow', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
                                <select  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow" class="it_inputs">
                                    <option value="none" >None</option>
                                    <option value="outset" >Outset Shadow</option>
                                    <option value="inset" >Inset Shadow</option>
                                </select>
                        </div>
                        </p>';
            // horizontal position
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_h_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow' => array('select', 'outset', 'inset')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_h', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow' => array('select', 'outset', 'inset')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_h_box">
                            <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_h" >' . __('Horizontal', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                <input type="number"
                                    name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_h"
                                    id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_h"
                                    value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                    class="input-text qty text it_inputs" />
                                <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                        </div>';

            // vertical position
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_v_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow' => array('select', 'outset', 'inset')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_v', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow' => array('select', 'outset', 'inset')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_v_box">
                            <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_v" >' . __('Vertical', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                <input type="number"
                                    name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_v"
                                    id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_v"
                                    value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                    class="input-text qty text it_inputs" />
                                <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                        </div>';
            // blur
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_blur_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow' => array('select', 'outset', 'inset')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_blur', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow' => array('select', 'outset', 'inset')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_blur_box">
                            <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_blur" >' . __('Blur', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                <input type="number"
                                    name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_blur"
                                    id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_blur"
                                    value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                    class="input-text qty text it_inputs" />
                                <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                        </div>';
            // shadow size
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_spread_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow' => array('select', 'outset', 'inset')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_spread', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow' => array('select', 'outset', 'inset')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_spread_box">
                            <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_spread" >' . __('Spread', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                <input type="number"
                                    name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_spread"
                                    id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_spread"
                                    value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                    class="input-text qty text it_inputs" />
                                <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                        </div>';
            // color
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_color_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow' => array('select', 'outset', 'inset')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_color', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow' => array('select', 'outset', 'inset')
            ));

            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_color_box">
                                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_color">' . __('Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                <input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_color"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shadow_color"
                                        type="text" class="wp_ad_picker_color it_inputs" value="#999" data-default-color="#999">
                        </div>
                        </p>';
			
			$o .='
	<p><label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'item_animation" >' . __('Item Animation', __IT_LOGO_SHOWCASE_FIELDS_PERFIX__) . ' :</label>
                <select  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'item_animation"
                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'item_animation" class="it_inputs">
                    <option value="it-no-animation">None</option>
					<option value="bounce">bounce</option>
					<option value="flash">flash</option>
					<option value="pulse">pulse</option>
					<option value="rubberBand">rubberBand</option>
					<option value="shake">shake</option>
					<option value="swing">swing</option>
					<option value="tada">tada</option>
					<option value="wobble">wobble</option>
					<option value="jello">jello</option>
					<option value="bounceIn">bounceIn</option>
					<option value="bounceInDown">bounceInDown</option>
					<option value="bounceInLeft">bounceInLeft</option>
					<option value="bounceInRight">bounceInRight</option>
					<option value="bounceInUp">bounceInUp</option>
					<option value="fade">fade</option>
					<option value="fadeInDown">fadeInDown</option>
					<option value="fadeInDownBig">fadeInDownBig</option>
					<option value="fadeInLeft">fadeInLeft</option>
					<option value="fadeInLeftBig">fadeInLeftBig</option>
					<option value="fadeInRight">fadeInRight</option>
					<option value="fadeInRightBig">fadeInRightBig</option>
					<option value="fadeInUp">fadeInUp</option>
					<option value="fadeInUpBig">fadeInUpBig</option>
					<option value="flipInX">flipInX</option>
					<option value="flipInY"selected="selected">flipInY</option>
					<option value="lightSpeedIn">lightSpeedIn</option>
					<option value="rotateIn">rotateIn</option>
					<option value="rotateInDownLeft">rotateInDownLeft</option>
					<option value="rotateInDownRight">rotateInDownRight</option>
					<option value="rotateInUpLeft">rotateInUpLeft</option>
					<option value="rotateInUpRight">rotateInUpRight</option>
					<option value="slideInDown">slideInDown</option>
					<option value="slideInLeft">slideInLeft</option>
					<option value="slideInRight">slideInRight</option>
					<option value="slideInUp">slideInUp</option>
					<option value="zoomIn">zoomIn</option>
					<option value="zoomInDown">zoomInDown</option>
					<option value="zoomInLeft">zoomInLeft</option>
					<option value="zoomInRight">zoomInRight</option>
					<option value="zoomInUp">zoomInUp</option>
					<option value="rollIn">rollIn</option>
                </select>
        </p>
';		

            // custom css class name
            $o .= '<hr><p><div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'custom_class">
                                            <label style="vertical-align: top;" for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'custom_class">' . __('Custom Class', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                            <input type="text" style="width:60%"
                                                    name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'custom_class"
                                                    id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'custom_class"
                                                    class="it_inputs">
                                    </div>
                                    </p></div>';
            //
            $o .= '<div id="content4" class="tabscontent">';
            // hover setting
            $o .= '<p><label for="hover_hr" >' . __('Hover Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="hover_hr"></p>';
            // Hover background Item color
            $o .= '<p>
					<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_background_color" >' . __('Item Background Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_background_color"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_background_color"
					        type="text" class="wp_ad_picker_color it_inputs" value="" data-default-color="">';
            $o .= '<p><label for="it_effect" >' . __('Effect on hover', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'effect"
					        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'effect"
					        class="it_inputs">
					    <option value="none" >None</option>
					    <option value="ls-opacity">Opacity</option>
					    <option value="ls-greyscale">Grey Scale</option>
					    <option value="ls-zoomin">Zoom In</option>
					    <option value="ls-zoomout">Zoom Out</option>
					</select>
            </p>';
            // hover border setting
            $o .= '<p><label for="hover_border_hr" >' . __('Hover Border Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="hover_border_hr"></p>';
            $o .= '<p><label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style">' . __('Border Style', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
                                    <select id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style"
                                           name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style" class="it_inputs" >
                                        <option value="none" >None</option>
                                        <option value="solid" >Solid</option>
                                        <option value="dashed" >Dashed</option>
                                        <option value="dotted" >Dotted</option>
                                    </select>
                                </p>';

            $dep = it_logo_showcase_dependency('hover_border_width_title', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));
            $o .= $dep.'<p><label id="hover_border_width_title" >' . __('Hover Border Width', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';

            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_top_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_top', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_top_box">
                                    <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_top" >' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                                            <input type="number" name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_top"
                                                                id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_top"
                                                                value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                                title="' . __('Only Digits!', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '" class="input-text qty text it_inputs" />
                                                            <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                    </div>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_right_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_right', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_right_box">
                                                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_right" >' . __('Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                                                        <input type="number"
                                                                            name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_right"
                                                                            id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_right"
                                                                            value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                                            class="input-text qty text it_inputs" />
                                                                        <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                                </div>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_bottom_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_bottom', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_bottom_box">
                                                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_bottom" >' . __('Bottom', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                                                        <input type="number"
                                                                            name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_bottom"
                                                                            id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_bottom"
                                                                            value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                                            class="input-text qty text it_inputs" />
                                                                        <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                                </div>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_left_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_left', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_left_box">
                                                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_left" >' . __('Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                                                        <input type="number"
                                                                            name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_left"
                                                                            id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_width_left"
                                                                            value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                                            class="input-text qty text it_inputs" />
                                                                        <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                                </div>';

            // end border width
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_color_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted', 'double')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_color', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_style' => array('select', 'solid', 'dashed', 'dotted', 'double')
            ));

            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_color_box">
                                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_color" >' . __('Hover Border Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                <input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_color"
                                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_color"
                                        type="text" class="wp_ad_picker_color it_inputs" value="#eee" data-default-color="#eee">
                        </div>
                        </p>';
            // hover border radius

            $o .= '<p><label id="hover_border_radius_title" >' . __('Hover Border Radius', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';

            $o .='<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_top_right_box">
                    <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_top_right" >' . __('Top-Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                        <input type="number" name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_top_right"
                            id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_top_right"
                            value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                            class="input-text qty text it_inputs" />
                        <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                    </div>';

            $o .= '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_top_left_box">
                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_top_left" >' . __('Tpo-left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                    <input type="number"
                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_top_left"
                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_top_left"
                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                        class="input-text qty text it_inputs" />
                    <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
            </div>';

            $o .= '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_bottom_right_box">
                        <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_bottom_right" >' . __('Bottom-Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                            <input type="number"
                                name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_bottom_right"
                                id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_bottom_right"
                                value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                class="input-text qty text it_inputs" />
                            <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                    </div>';

            $o .= '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_bottom_left_box">
                                            <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_bottom_left" >' . __('Bottom-Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                                <input type="number"
                                                    name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_bottom_left"
                                                    id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_border_radius_bottom_left"
                                                    value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                                    class="input-text qty text it_inputs" />
                                                <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                            </div>';

            // end border radius
            // end hover border setting
            // hover shadow options
            $o .= '<p><label for="hover_shadow_hr" >' . __('Hover Shadow Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="hover_shadow_hr"></p>';
            $o .= '<p><div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_box">
					<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow" >' . __('Box Shadow', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow" class="it_inputs">
					    <option value="none" >None</option>
                        <option value="outset" >Outset Shadow</option>
                        <option value="inset" >Inset Shadow</option>
					</select>
			</div>
            </p>';
            // horizontal position
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_h_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow' => array('select', 'outset', 'inset')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_h', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow' => array('select', 'outset', 'inset')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_h_box">
                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_h" >' . __('Horizontal', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                    <input type="number"
                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_h"
                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_h"
                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                        class="input-text qty text it_inputs" />
                    <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
            </div>';

            // vertical position
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_v_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow' => array('select', 'outset', 'inset')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_v', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow' => array('select', 'outset', 'inset')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_v_box">
                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_v" >' . __('Vertical', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                    <input type="number"
                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_v"
                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_v"
                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                        class="input-text qty text it_inputs" />
                    <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
            </div>';
            // blur
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_blur_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow' => array('select', 'outset', 'inset')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_blur', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow' => array('select', 'outset', 'inset')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_blur_box">
                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_blur" >' . __('Blur', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                    <input type="number"
                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_blur"
                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_blur"
                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                        class="input-text qty text it_inputs" />
                    <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
            </div>';
            // shadow size
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_spread_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow' => array('select', 'outset', 'inset')
            ));

            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_spread', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow' => array('select', 'outset', 'inset')
            ));

            $o .= $dep . '<div class="small-lbl-cnt" id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_spread_box">
                <label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_spread" >' . __('Spread', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                    <input type="number"
                        name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_spread"
                        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_spread"
                        value="0" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                        class="input-text qty text it_inputs" />
                    <span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
            </div>';
            // color
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_color_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow' => array('select', 'outset', 'inset')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_color', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow' => array('select', 'outset', 'inset')
            ));

            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_color_box">
					<label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_color">' . __('Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_color"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'hover_shadow_color"
					        type="text" class="wp_ad_picker_color it_inputs" value="#999" data-default-color="#999">
			</div>
            </p></div>';

            // View Details Tab
            $o .='<div id="content5" class="tabscontent">';
            // logo quick view or tooltip
            $o .= '<p><label for="quick_view_hr" >' . __('Quick View Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="quick_view_hr"></p>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'=> array('select' , 'grid' ,'isotope' ,'carousel')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'=> array('select' , 'grid' ,'isotope' ,'carousel')
            ));
            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip_box">
                         <input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip"
					        type="checkbox" class="it_inputs" value="true" >
					        <span>' . __('Tooltip', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div>
            </p>';

$o .= '<p><label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type" >' . __('Quick View Type', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type" class="it_inputs">
					    <option value="none" >None</option>
                        <option value="full" >Full View</option>
					</select>
            </p>';
            // customize show details for full view
            // hide title
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'view_type', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout',__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'layout'=> array('select' , 'grid' ,'list' ,'carousel'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $o .= $dep.'<p><label for="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'view_type" >' . __('View Type', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'view_type"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'view_type" class="it_inputs">
                        <option value="popup" >'.__("Pop Up",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</option>
					    <option id="it_inline_opt" value="inline">'.__("Inline",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</option>
					</select>
            </p>';
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'title', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'title_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'title_box">
                         <input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'title"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'title"
					        type="checkbox" class="it_inputs" value="false" >
					        <span>' . __('Hide Title', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div>
            </p>';
            // hide contact Info
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'contact', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'contact_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'contact_box">
                         <input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'contact"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'contact"
					        type="checkbox" class="it_inputs" value="false">
					        <span>' . __('Hide Contact Info', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div>
            </p>';
            //
            // hide social
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'social', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'social_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'social_box">
                         <input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'social"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'social"
					        type="checkbox" class="it_inputs" value="false">
					        <span>' . __('Hide Social Links', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div>
            </p>';
            //
            // hide full description
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'full_desc', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'full_desc_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'full_desc_box">
                         <input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'full_desc"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'full_desc"
					        type="checkbox" class="it_inputs" value="false">
					        <span>' . __('Hide Full Description', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div>
            </p>';
            //

            // hide gallery
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'gallery', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'gallery_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'gallery_box">
                         <input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'gallery"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'gallery"
					        type="checkbox" class="it_inputs" value="false">
					        <span>' . __('Hide Gallery Images', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div>
            </p>';
            //

            // hide Video
            $dep = it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'video', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $dep .= it_logo_showcase_dependency(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'video_box', array(
                'parent_id' => array(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type'),
                __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_type' => array('select', 'full')
            ));
            $o .= '<p>' . $dep . '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'video_box">
                         <input  name="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'video"
					        id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'video"
					        type="checkbox" class="it_inputs" value="false">
					        <span>' . __('Hide Video', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div>
            </p></div>';
            // end Content Tabs
            $o .= '</div></div>';
            //
            $o .= '</form>
					</div></div>
		            <div class="half" ><h3>' . __('Shortcode Text', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</h3>
						     <div id="shortcode_div" style="padding:10px; background-color:#fff;border-left:4px solid #7ad03a;-webkit-box-shadow:0 1px 1px 0 rgba(0,0,0,.1);box-shadow:0 1px 1px 0 rgba(0,0,0,.1); display:block;">
						       <textarea id="it_shortcode_box" rows="8" style="min-width:100%; height:300px;"></textarea>
						       <span class="description">' . __("Use this shortcode to display the list of logos in your posts or pages! Just copy this piece of text and place it where you want it to display.", __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
						    </div>
                    </div>
                    <div class="clearfix" style="width:70%;margin:20px 0;">
                        <h3>'.__("Preview Box",__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</h3>
					    <div id="it_preview" class="postbox" >
                            <div></div>
                        </div>
					</div>	 ';
            echo $o;
?>