<?php

function michigan_webnus_ourteam ($atts, $content = null) {
	extract(shortcode_atts(array(
	'type'  => '1',
	'img'=>'',
	'name' => '',
	'link' => '',
	'title' =>'',
	'text'=>'',
	'attachment'=>'',
	'audio'=>'',
	'download'=>'',
	'background_color'=>'',
	'social'=>'',
	'first_social'=>'twitter',
	'first_url'=>'',
	'second_social'=>'facebook',
	'second_url'=>'',
	'third_social'=>'google-plus',
	'third_url'=>'',
	'fourth_social'=>'linkedin',
	'fourth_url'=>'',
	), $atts));
	
	if(is_numeric($img)) $img = wp_get_attachment_url( $img );

	switch ($type) {
		case '1':
			$out  = '<article class="our-team">';
			if(!empty($link)){
				$out .= '<figure><a href="'. esc_url($link) .'"><img src="'. $img .'" alt=""></a></figure>';
				$out .= '<h2><a href="'. esc_url($link) .'">'. $name .'</a></h2>';
			}
			else{
				$out .= '<figure><img src="'. $img .'" alt=""></figure>';
				$out .= '<figcaption><h2>'. $name .'</h2>';
			}
			$out .= '<h5>'. $title .'</h5>';
			$out .= '<p>'. $text .'</p></figcaption>';
			if($social=='enable'){
				$out .= '<div class="social-team">';
				if(!empty($first_url))
					$out .= '<a href="'. esc_url($first_url) .'"><i class="fa-'. $first_social .'"></i></a>';
				if(!empty($second_url))
					$out .= '<a href="'. esc_url($second_url) .'"><i class="fa-'. $second_social .'"></i></a>';
				if(!empty($third_url))
					$out .= '<a href="'. esc_url($third_url) .'"><i class="fa-'. $third_social .'"></i></a>';
				if(!empty($fourth_url))
					$out .= '<a href="'. esc_url($fourth_url) .'"><i class="fa-'. $fourth_social .'"></i></a>';
				$out .= '</div>';
			}
			$out .= '</article>';

		break;

		case '2':
			$out = '<article class="our-team2">';
			if(!empty($link)){
				$out .= '<h4><a href="'. esc_url($link) .'">'. esc_html__('View Profile','michigan') .'</a></h4>';
			}
			$out .= '<figure><img src="'. $img .'" alt=""></figure>';
			$out .= "<div class=\"content-team\">";
			$out .= '<h2>';
			$out .= ($link)?'<a href="'. esc_url($link) .'">'. $name .'</a>':$name;
			$out .= '</h2>';
			$out .= '<h5>'. $title .'</h5>';
			$out .= '<p>'. $text .'</p>';
			$out .= '</div>';
			if($social=='enable') {
				$out .= '<div class="social-team"><ul>';
				if(!empty($first_url))
					$out .= '<li class="first-social"><a href="'. esc_url($first_url) .'"><i class="fa-'. $first_social .'"></i></a></li>';
				if(!empty($second_url))
					$out .= '<li class="second-social"><a href="'. esc_url($second_url) .'"><i class="fa-'. $second_social .'"></i></a></li>';
				if(!empty($third_url))
					$out .= '<li class="third-social"><a href="'. esc_url($third_url) .'"><i class="fa-'. $third_social .'"></i></a></li>';
				if(!empty($fourth_url))
					$out .= '<li class="fourth-social"><a href="'. esc_url($fourth_url) .'"><i class="fa-'. $fourth_social .'"></i></a></li>';
				$out .= '</ul></div>';
			}
			$out .= '</article>';	
		break;
		
		case 3:	
			$out = "<article class=\"our-team3 clearfix\">";
			$out .= '<figure><img src="'. $img .'" alt=""></figure>';
			$out .= "<div class=\"tdetail\">";
			$out .= '<h2>'. $name .'</h2>';
			$out .= '<h5>'. $title .'</h5>';
			if($social=='enable') {
				$out .= '<div class="social-team">';
				if(!empty($first_url))
					$out .= '<a href="'. esc_url($first_url) .'"><i class="fa-'. $first_social .'"></i></a>';
				if(!empty($second_url))
					$out .= '<a href="'. esc_url($second_url) .'"><i class="fa-'. $second_social .'"></i></a>';
				if(!empty($third_url))
					$out .= '<a href="'. esc_url($third_url) .'"><i class="fa-'. $third_social .'"></i></a>';
				if(!empty($fourth_url))
					$out .= '<a href="'. esc_url($fourth_url) .'"><i class="fa-'. $fourth_social .'"></i></a>';
				$out .= '</div>';
			}
			$out .= '</div>';

			$out .= '</article>';
		break;

		case 4:	
			$out = "<article class=\"our-team4 clearfix\">";
			$out .= '<figure><img src="'. $img .'" alt=""></figure>';
			$out .= "<div class=\"tdetail\">";
			$out .= '<h2>'. $name .'</h2>';
			$out .= '<h5>'. $title .'</h5>';
			$out .= '<p>'. $text .'</p>';
			if($social=='enable') {
				$out .= '<div class="social-team">';
				if(!empty($first_url))
					$out .= '<a href="'. esc_url($first_url) .'"><i class="fa-'. $first_social .'"></i></a>';
				if(!empty($second_url))
					$out .= '<a href="'. esc_url($second_url) .'"><i class="fa-'. $second_social .'"></i></a>';
				if(!empty($third_url))
					$out .= '<a href="'. esc_url($third_url) .'"><i class="fa-'. $third_social .'"></i></a>';
				if(!empty($fourth_url))
					$out .= '<a href="'. esc_url($fourth_url) .'"><i class="fa-'. $fourth_social .'"></i></a>';
				$out .= '</div>';
			}
			$out .= '</div>';

			$out .= '</article>';
		break;
		
		case 5:
			$download_icon = ($download)? '<a style="color:'.$background_color.';" href="'.$download.'"><i class="fa-download"></i></a>' : '';
			$audio_icon = ($audio)? '<a style="color:'.$background_color.';" href="'.$audio.'"><i class="fa-microphone"></i></a>' : '';
			$icons = ($download OR $audio)? '<div class="attachment">'.$download_icon.' '.$audio_icon.'</div>' : '';
			$out = '<article style="background-color: '.$background_color.'; " class="our-team' . $type . ' clearfix">';
			$out .= '<div class="content-o clearfix"><figure><img src="'. $img .'" alt=""></figure>';
			$out .= '<div class="tdetail">';
			$out .= '<h2>'. $name .'</h2>';
			$out .= '<h5>'. $title .'</h5>';
			$out .= ($download OR $audio)? '<div class="attachment">'.$audio_icon.' '.$download_icon.'</div>' : '';
			$out .= '<p>'. $text .'</p>';
			if($social=='enable') {
				$out .= '<div  class="social-team">';
				if(!empty($first_url))
					$out .= '<a style="color:'.$background_color.';" href="'. esc_url($first_url) .'"><i   class="fa-'. $first_social .'"></i></a>';
				if(!empty($second_url))
					$out .= '<a style="color:'.$background_color.';" href="'. esc_url($second_url) .'"><i class="fa-'. $second_social .'"></i></a>';
				if(!empty($third_url))
					$out .= '<a style="color:'.$background_color.';" href="'. esc_url($third_url) .'"><i class="fa-'. $third_social .'"></i></a>';
				if(!empty($fourth_url))
					$out .= '<a style="color:'.$background_color.';" href="'. esc_url($fourth_url) .'"><i class="fa-'. $fourth_social .'"></i></a>';
				$out .= '</div>';
			}
			$out .= '</div></div>';

			$out .= '</article>';
		break;	

		case 6:	
			$download_icon = ($download)? '<a style="color:'.$background_color.';" href="'.$download.'"><i class="fa-download"></i></a>' : '';
			$audio_icon = ($audio)? '<a style="color:'.$background_color.';" href="'.$audio.'"><i class="fa-microphone"></i></a>' : '';
			$icons = ($download OR $audio)? '<div class="attachment">'.$download_icon.' '.$audio_icon.'</div>' : '';
			$out = '<article style="background-color: '.$background_color.'; " class="our-team' . $type . ' clearfix">';
			$out .= '<div class="content-o clearfix">
					<img src="'. $img .'" alt="">
					<div class="tdetail">
					<h2>'. $name .'</h2>
					<h5>'. $title .'</h5>
					<p>'. $text .'</p>';

			$out .= '</div>';
			if($social=='enable') {
				$out .= '<div  class="social-team">';
				if(!empty($first_url))
					$out .= '<a style="color:'.$background_color.';" href="'. esc_url($first_url) .'"><i   class="fa-'. $first_social .'"></i></a>';
				if(!empty($second_url))
					$out .= '<a style="color:'.$background_color.';" href="'. esc_url($second_url) .'"><i class="fa-'. $second_social .'"></i></a>';
				if(!empty($third_url))
					$out .= '<a style="color:'.$background_color.';" href="'. esc_url($third_url) .'"><i class="fa-'. $third_social .'"></i></a>';
				if(!empty($fourth_url))
					$out .= '<a style="color:'.$background_color.';" href="'. esc_url($fourth_url) .'"><i class="fa-'. $fourth_social .'"></i></a>';
				$out .= '</div>';
			}
			$out .= '</div>';

			$out .= '</article>';
		break;	
	}

return $out;
}
add_shortcode('ourteam','michigan_webnus_ourteam');