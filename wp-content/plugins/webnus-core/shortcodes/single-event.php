<?php
function michigan_webnus_event( $attributes, $content = null ) {
extract(shortcode_atts(	array(
	'event_tag'		=>'featured event',
	'post'			=>'',
	'type'			=>'latest',
	'eventDisplay'	=>'custom',
), $attributes));
	ob_start();	
	$w_post = ( $type=='custom' || $type=='custom-t2' )?'&p='.$post:'&posts_per_page=1';
	$query = new WP_Query('post_type=tribe_events'.$w_post.'&eventDisplay=custom');
	if ($query -> have_posts()) : $query -> the_post();
	//terms		
		$post_id				= get_the_ID();
		$place					= tribe_get_venue($post_id);
		$address				= tribe_get_address($post_id);
		$content				='<p>'.michigan_webnus_excerpt(28).'</p>';
		$title					= get_the_title();
		$permalink				= get_the_permalink();
		$background_image_url	= wp_get_attachment_url( get_post_thumbnail_id() );
		$date = ( $type=='custom' ) ? tribe_get_start_date($post_id,false,'D, F d Y H:i') : tribe_get_start_date($post_id,false,' M d,Y') ;
		$time = tribe_get_start_date($post_id,false,'H:ia,');

		$background_style = !empty($background_image_url)?"background: url('{$background_image_url}')":'';
		if ( $type=='custom' ) {
		echo '<article class="' . $type . ' single-event" style="'.$background_style.';height: 678px;background-size: cover;">';
		echo '<a href="'.$permalink.'" class="event-cover">
		<div class="event-overlay colorb"></div>
		<div class="event-detail">
		<div class="event-tag">'.$event_tag.'</div>
		<div class="event-date">'.$date.'</div>
		<h4 class="event-title">'.$title.'</h4>
		<div class="event-place">'.$place.'</div>
		</div>
		</a></article>';
		} elseif ( $type=='custom-t2' ) {
			$out='
			<article class="event-' . $type . '">
				<div class="col-sm-8 l-side"><img src="' . $background_image_url . '" width="772" height="405"></div>
				<div class="col-sm-4 r-side">
					<div class="event-tagline"><span>' . esc_html__( 'events', 'michigan' ) . '</span><i class="fa fa-calendar-o" aria-hidden="true"></i></div>
					<h4 class="event-title">'.$title.'</h4>
					<div class="event-date">'.$date.'</div>
					<div class="event-date-place">'. $time . $place .','. $address .'</div>
					<a class="readmore" href="'.$permalink.'" class="event-cover">' . esc_html__( 'read more', 'michigan' ) . '</a>
				</div>
			</article>';
			echo $out;
		}
	endif;


	$out = ob_get_contents();
	ob_end_clean();	
	wp_reset_postdata();
	return $out;
 }
 add_shortcode('anevent', 'michigan_webnus_event');
?>