<div class="wrap">
	<h1><?php _e('Manage Rewards', 'watuproplay')?></h1>
	
	<div class="watupro-alert">
	<p><?php _e('Using rewards you can encourage the participation of the users in your site. Rewards can be virtual or tangible items and users can earn them by redeeming the points, collected by taking tests.', 'watuproplay')?></p>
	</div>
	
	<p><a href="admin.php?page=watuproplay_rewards&action=add"><?php _e('Click here to create a new reward in the store', 'watupro')?></a>
	| <a href="admin.php?page=watuproplay_rewards_history"><?php _e('View and ship redeemed rewards', 'watuproplay')?></a></p>
	
	<?php if(sizeof($rewards)):?>
		<table class="widefat">
			<tr><th><?php _e('Reward', 'watuproplay')?></th><th><?php _e('Price', 'watuproplay')?></th>
			<th><?php _e('Quantity', 'watuproplay')?></th><th><?php _e('Tangible?', 'watuproplay')?></th>
			<th><?php _e('Edit', 'watuproplay')?></th></tr>
			<?php foreach($rewards as $reward):?>
				<tr><td><?php echo $reward->title?></td><td><?php echo $reward->points.' '.__('points', 'watuproplay')?></td>
				<td><?php echo ($reward->qty==-1) ? __('Unlimited','watuproplay') : $reward->qty?></td><td><?php echo $reward->tangible?__('Yes','watuproplay'):__('No', 'watuproplay')?></td>
				<td><a href="admin.php?page=watuproplay_rewards&action=edit&id=<?php echo $reward->id?>"><?php _e('Edit', 'watuproplay')?></a></td></tr>
			<?php endforeach;?>
		</table>
		
		<p>&nbsp;</p>
		<h2><?php _e('Manage Notifications for Reward Items', 'watuproplay')?></h2>
		<div class="wp-admin watupro-padded postbox">
			<form method="post" class="watupro">
				<h3><?php _e('Email message sent to you', 'watuproplay')?></h3>
				<p><input type="checkbox" name="redeem_email" value="1" <?php if(get_option('watuproplay_redeem_email')) echo 'checked'?>> <?php _e('Email me when user redeems an item')?> </p>
				<p class="watupro-help"><?php _e('It is recommended to select the checkbox above so you get notified when you have to ship or deliver a reward item to the user.', 'watuproplay')?></p>
				
				<p><label><?php _e('Email subject:', 'watupro')?></label> <input type="text" name="redeem_subject" value="<?php echo get_option('watuproplay_redeem_subject')?>" size="60"></p>
				
				<p><label><?php _e('Email contents:', 'watupro')?></label> <?php wp_editor(stripslashes(get_option('watuproplay_redeem_message')), 'redeem_message')?></p>
				
				<p class="watupro-help"><?php _e('You can use the following variables: {{item-name}}, {{user-email}}, {{user-name}}, {{shipping-address}} (shipping address coming soon)', 'watuproplay')?></p>
				
				<p align="center"><input type="submit" value="<?php _e('Save Notification Email', 'watuproplay')?>"></p>
				<input type="hidden" name="save_notice_email" value="1">
			</form>	
			
			<p>&nbsp;</p>
			
			<form method="post" class="watupro">
				<h3><?php _e('On-screen message shown to user', 'watuproplay')?></h3>
				
				<p><?php _e('This message will be shown to the user after they have redeemed the reward. Use the variable {{back-url}} in a hyperlink to give the user path back to the rewards listing', 'watuproplay')?></p>
				
				<?php wp_editor(stripslashes(get_option('watuproplay_redeem_flash')), 'redeem_flash')?>
				
				<p align="center"><input type="submit" value="<?php _e('Save On-Screen Notice', 'watuproplay')?>"></p>
				<input type="hidden" name="save_notice_flash" value="1"> 
			</form>	
		</div>
	<?php else:?>
		<p><?php _e('You have not yet created any rewards', 'watuproplay')?></p>	
	<?php endif;?>
</div>