<?php

// grid class number
$desk_num = intval(12 / $col_desktop);
$tablet_num = intval(12 / $col_tablet);
$mobile_num = intval(12 / $col_mobile);

// get logos query
include ("it_build_query.php");
global $script_outputs;
$script_outputs='<script type="text/javascript">';
// output
$css_classes = '';
$modal ='';
$tooltip_class = '';
$html='';
$show_class ='';
$logo_effect = 'pl-animate'.$rand_id.' '.$item_animation;
if ($query->have_posts()) {
    // assign value to variables
    if($layout != 'list')
        $css_classes .= 'ls-item-wrap-'.$rand_id.' ls-grid-d-' . $desk_num . ' ls-grid-tl-' . $desk_num . ' ls-grid-m-' . $mobile_num . ' ls-grid-t-' . $tablet_num . ' ls-grid-' . $tablet_num . ' ';

    // begin html tags
    $html ='<div class="ls-logo-cnt ls-gred-layout '.$custom_class.'" data-rand-id="'.$rand_id.'" data-desktop="'.$col_desktop.'" data-tablet="'.$col_tablet.'" data-mobile="'.$col_mobile.'">
            <div class="ls-ajax-loading" style="display:none;">
                <div class="ls-ajax-loading-back"></div>
                <img src="'.__IT_LOGO_SHOWCASE_CSS_URL__.'ajax-loader.gif" />
            </div>';

    switch ($layout) {
        case 'grid':
            if ($quick_view_type == 'full' && $view_type == 'inline') {
                $css_classes .= 'show-inline ';
            }
            break;
        case 'list':
            $html ='<div class="ls-logo-cnt ls-list-layout  '.$custom_class.'" data-rand-id="'.$rand_id.'" data-desktop="1" data-tablet="1" data-mobile="1" >';
            if ($quick_view_type == 'full' && $view_type == 'inline') {
                $show_class = 'show-inline';
            }
            break;
        case 'carousel':
            /////OWL CAROUSEL//////
            wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel-css');
            wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel-th');
            wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel');
            
			$html .= '<div id="' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-box_'.$rand_id.'" class="owl-carousel owl-theme '.$skin.'">';
            if ($quick_view_type == 'full' && $view_type == 'inline') {
                $show_class = 'show-inline-car ';
            }
            break;

        case 'isotope':
            /////Isotope//////
            wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'isotope');
            // get categories
            $taxonomies = __Custom_Post_Taxonomy__;
            $tax_args = array(
                'orderby' => 'name',
                'order' => 'ASC',
                'hide_empty' => true
            );

            $terms = get_terms($taxonomies, $tax_args);
            
			// Echo Category buttons
            if (!empty($terms) && !is_wp_error($terms)) {
				
                $term_list = '<div class="ls-filtering ls-align-' . $filter_button_position . ' ls-' . $filter_button_skin . '-filter ">
                                        <div class="ls-filter-item ls-active-filter" data-filter="*">'.__('show all','it_logo_showcase_textdomain').'</div>';
                foreach ($terms as $term) {
					
                    if (is_array($tax_terms)) {
						
                        if (in_array($term->term_id, $tax_terms))
                            $term_list .= '<div class="ls-filter-item" data-filter=".' . $term->term_id . '">' . $term->name . '</div>';
                    }else
						                        
						$term_list .= '<div class="ls-filter-item" data-filter=".' . $term->term_id . '">' . $term->name . '</div>';
                }
                $term_list .= '</div>';
                $html .= $term_list;
            }

            if ($quick_view_type == 'full' && $view_type == 'inline') {
                $css_classes .= 'show-inline-iso ';
            }

            $html .= '<div class= "isotope-grid" id="isotope-grid-'.$rand_id.'" >';
            break;
        default:
            break;
    }
    // quick view
    if($quick_view_type == 'full') {
        // Full View
        wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'lightbox-me-css');
        wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'scroller-css');
		
        wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'lightbox-me');
        wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'showHide');
        wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'scroller');
        // ajax post args
        $args = array(
            'rand_id' => $rand_id,
            'title' => $title,
            'contact' => $contact,
            'social' => $social,
            'full_desc' => $full_desc,
            'gallery' => $gallery
        );
        $script_outputs .= 'if(!args)   var args = [];
                        args["'.$rand_id.'"] = ' . json_encode($args) . ';';
        //
        if ($view_type == 'popup' || $layout == 'isotope') {
            wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal-css');
            wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal-theme-css');
            wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal');
            $html .= '<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
                        <div class="ls-ajax-loading" style="display:none;">
                            <div class="ls-ajax-loading-back"></div>
                            <img src="'.__IT_LOGO_SHOWCASE_CSS_URL__.'ajax-loader.gif" />
                        </div>
                        <div class="inner-modal">
                        </div>
                </div>';
            $modal = 'data-remodal-target="modal"';
            $css_classes .= 'it-modal ';
        }
    }
    if($tooltip == 'true') {
        $tooltip_class = 'tooltip ';
    }
    // logo loop
    $i=0;
    while ($query->have_posts()) {
        $i++;
        $query->the_post();
        $post_id = $query->post->ID;
        if (has_post_thumbnail($post_id)) {
            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'full');
            $image_url = $image_url[0];
        }
        $shortDesc = get_post_meta($post_id, __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shortDesc', true);
        $link_url = get_post_meta($post_id, __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'link_url', true);
        $link_target = get_post_meta($post_id, __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'link_target', true);
        $tooltip_str='';
		if($tooltip == 'true') {
            $tooltip_str = 'title = "'.get_the_title().'"';
        }
        // link event
        $link_detail = '';
        $more_btn ='';
        if ($quick_view_type == 'none' && $link_url!='') {
            $link_detail = '<a href="' . $link_url . '" target="' . $link_target . '" >
                                <img  src="' . $image_url . '" class="' . $effect . '" >
                            </a>';
        }
        else{
            if ($quick_view_type != 'none' ) {
                $more_btn = '<div class="ls-more-btn">'.__('More Details','it_logo_showcase_textdomain').'</div>';
            }
            $link_detail = '<img  src="' . $image_url . '" class="' . $effect . '" >';
        }
        //
        switch ($layout) {
            case 'grid':
                $html .= '<div class="' . $css_classes . ' " data-item-number="'.$i.'" '.$modal.' data-id="' . $post_id . '">
                            <div class="ls-gred-item ls-gred-item-'.$rand_id.' '.$logo_effect.' '.$tooltip_class.'" ' . $tooltip_str . ' >
                                '.$link_detail.'
                            </div>
                          </div>';
                break;
            case 'list':
                    $html .= '<div class="it-modal ls-item-wrap-'.$rand_id.' ls-grid-m-12 ls-grid-t-12 ls-grid-d-12 '.$show_class.'" data-item-number="'.$i.'" data-id="' . $post_id . '" '.$modal.'>
                                <div class="ls-list-item ls-gred-item-'.$rand_id.'  '.$logo_effect.' ">
                                    <div class="ls-grid-m-12 ls-grid-t-4 ls-grid-m-3 ls-list-img">
                                        '.$link_detail.'
                                    </div>
                                    <div class="ls-grid-m-12 ls-grid-t-8 ls-grid-m-9">
                                        <div class="ls-list-title">'.get_the_title().'</div>
                                        <div class="ls-list-desc">'.$shortDesc.'</div>
                                        '.$more_btn.'
                                    </div>
                                </div>
                            </div>';
                break;
            case 'carousel':
                // Echo Logos
                $html .= '<div class="item it-modal ls-item-wrap-'.$rand_id.' '.$show_class.'" '.$modal.' data-id="' . $post_id . '">
                            <div class="ls-gred-item ls-gred-item-'.$rand_id.' '.$tooltip_class.'"' . $tooltip_str . ' >
                                '.$link_detail.'
                            </div>
                          </div>';
                break;
            case 'isotope':
                // get category of post for isotope filter
                $terms = wp_get_post_terms($post_id, __Custom_Post_Taxonomy__, array("fields" => "all"));
                $term_classes = '';
                foreach ($terms as $term) {
                    $term_classes .= $term->term_id . ' ';
                }
                //
                $html .= '<div class="grid-item  ' . $css_classes . ' '.$term_classes.'" '.$modal.' data-item-number="'.$i.'" data-id="' . $post_id . '">
                            <div class="ls-gred-item ls-gred-item-'.$rand_id.' '.$logo_effect.' '.$tooltip_class.'" ' . $tooltip_str . ' >
                                '.$link_detail.'
                            </div>
                          </div>';

                break;

            default:
                break;
        }
    }
// end html tags
    switch ($layout) {
        case 'carousel':
            if ($autoplay == 'true')
                $autoplay = $autoplay_speed;
            $script_outputs .= '
                            jQuery(function($) {
                                $("#' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-box_'.$rand_id.'").owlCarousel({
                                    autoPlay: ' . $autoplay . ', //Set AutoPlay to 3 seconds
                                    items : ' . $laptop_item . ',
                                    itemsDesktop : [1199,' . $desktop_item . '],
                                    itemsTablet: [768,' . $tablet_item . '],
                                    itemsMobile : [479,' . $mobile_item . '],
                                    navigation : ' . $navigation . ',
                                    pagination : ' . $pagination . ',
                                    rewindSpeed : ' . $rewindSpeed . ',
                                    paginationSpeed : ' . $paginationSpeed . ',
                                    slideSpeed : ' . $slideSpeed . ',
                                    stopOnHover : '.$pause.',
                                    navigationText: [
                                        "<i class=\'fa fa-angle-left\'></i>",
                                        "<i class=\'fa fa-angle-right\' ></i>"
                                    ]
                                });
                            });
                        ';
            break;
        case 'isotope':
            $script_outputs.= '
				jQuery(function ($) {
					$("#isotope-grid-'.$rand_id.'").isotope({
						itemSelector: ".grid-item",
						layoutMode: "fitRows",
						filter: "*"
					});
					$(".ls-filter-item").on("click", function () {
					    $(this).closest(".ls-logo-cnt").find(".ls-filtering").find(".ls-active-filter").removeClass("ls-active-filter");
					    $(this).addClass("ls-active-filter");
						var filterValue = $(this).attr("data-filter");
						$(this).closest(".ls-logo-cnt").find("#isotope-grid-'.$rand_id.'").isotope({filter: filterValue});
					});
				});';
            $html .= '</div>';
            break;
        default:
            break;
    }
  	if($tooltip == 'true') {
        $script_outputs .= '
                jQuery(function($){
                    $(".tooltip").tipsy({
                        gravity : $.fn.tipsy.autoNS ,
                        fade : true
                    });
                });';
    }
	if ($logo_effect!= 'it-no-animation')	{
		$script_outputs .='
			jQuery(document).ready(function() {
				wow'.$rand_id.' = new WOW(
				  {
					boxClass:     "pl-animate'.$rand_id.'",
					animateClass: "visible animated",
					offset:       100,
				  }
				);
				wow'.$rand_id.'.init();
			});';
	}
    $script_outputs .= '</script>';
    $html .= '</div>';
}
?>