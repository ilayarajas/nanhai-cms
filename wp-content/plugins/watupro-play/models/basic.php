<?php
// main model containing general config and UI functions
class WatuPROPlay {
   static function install($update = false) {
   	global $wpdb;	
   	$wpdb -> show_errors();
   	
		if(!function_exists('watupro_add_db_fields')) {
			 wp_die("<p align='center'>".
				 sprintf(__("You need to install and activate WatuPRO before you can use the Play Plugin.<br><a href='%s'><b>Go Back to the Plugins Page</b></a>", 'watuproplay'), 'plugins.php')
				 ."</p>");
		}   	
   	
   	if(!$update) self::init();
   	
   	// is WatuPRO activated?
   	/*$current_plugins = get_option('active_plugins');		
		if(!in_array('watupro/watupro.php', $current_plugins)) die("This plugin only works when WatuPRO is installed and active.");*/
		
		// extra tables
		// levels and badges
		if($wpdb->get_var("SHOW TABLES LIKE '".WATUPROPLAY_LEVELS."'") != WATUPROPLAY_LEVELS) {  
            $sql = "CREATE TABLE `".WATUPROPLAY_LEVELS."` (
							id int(11) unsigned NOT NULL auto_increment,							
							name VARCHAR(255) NOT NULL DEFAULT '',
							content TEXT,
							required_points INT UNSIGNED NOT NULL DEFAULT 0,
							required_percent TINYINT UNSIGNED NOT NULL DEFAULT 0,
							required_num_exams INT UNSIGNED NOT NULL DEFAULT 0,
							required_exam_details TEXT, /* serialized exams with grades */
							required_category INT UNSIGNED NOT NULL DEFAULT 0, /* rerquires all exams from this category to be taken*/
							atype VARCHAR(100) NOT NULL DEFAULT 'badge', /* level or badge */
							rank TINYINT UNSIGNED NOT NULL DEFAULT 1,
							PRIMARY KEY  (id)							
						) ENGINE=INNODB CHARACTER SET utf8;";
            $wpdb->query($sql);    
        }    
        
      // rewards that can be redeemed for points 
      // user_id - when user got the reward we'll make a record with their ID so we know it
       // qty = -1 means it's unlimited quantity (digital reward)
      if($wpdb->get_var("SHOW TABLES LIKE '".WATUPROPLAY_REWARDS."'") != WATUPROPLAY_REWARDS) {  
            $sql = "CREATE TABLE `".WATUPROPLAY_REWARDS."` (
							id int(11) unsigned NOT NULL auto_increment,							
							title VARCHAR(255) NOT NULL DEFAULT '',
							description TEXT,
							points INT UNSIGNED NOT NULL DEFAULT 0,
							qty INT NOT NULL DEFAULT 0,	
							tangible TINYINT NOT NULL DEFAULT 0,
							PRIMARY KEY (id)							
						) ENGINE=INNODB CHARACTER SET utf8;";
            $wpdb->query($sql);    
        }     
        
       // redeemed items 
        if($wpdb->get_var("SHOW TABLES LIKE '".WATUPROPLAY_REDEEMED."'") != WATUPROPLAY_REDEEMED) {  
            $sql = "CREATE TABLE `".WATUPROPLAY_REDEEMED."` (
							id int(11) unsigned NOT NULL auto_increment,							
							user_id INT UNSIGNED NOT NULL,
							item_id INT UNSIGNED NOT NULL,
							date DATE NOT NULL DEFAULT '2000-01-01',
							is_shipped TINYINT NOT NULL DEFAULT 0,
							points INT UNSIGNED NOT NULL DEFAULT 0,
							PRIMARY KEY (id)							
						) ENGINE=INNODB CHARACTER SET utf8;";
            $wpdb->query($sql);    
        }     
        
       // badge and levels history. For now used only for open badges but later we may want to display it 
       if($wpdb->get_var("SHOW TABLES LIKE '".WATUPROPLAY_HISTORY."'") != WATUPROPLAY_HISTORY) {  
            $sql = "CREATE TABLE `".WATUPROPLAY_HISTORY."` (
							id int(11) unsigned NOT NULL auto_increment,							
							user_id INT UNSIGNED NOT NULL,
							item_id INT UNSIGNED NOT NULL,
							action VARCHAR(255) NOT NULL DEFAULT 'assigned baddge',
							`timestamp` INT UNSIGNED NOT NULL,
							`datetime` DATETIME,
							PRIMARY KEY (id)							
						) ENGINE=INNODB CHARACTER SET utf8;";
            $wpdb->query($sql);    
        }     
        
		
		// extra fields to existing tables
		watupro_add_db_fields(array(
			array("name"=>"category_id", "type"=>"INT UNSIGNED NOT NULL DEFAULT 0"), /* limits the requirements to this cat ID, not used at this time*/
			array("name"=>"is_open_badge", "type"=>"TINYINT UNSIGNED NOT NULL DEFAULT 0"),
			array("name"=>"badge_graphic", "type"=>"VARCHAR(255) NOT NULL DEFAULT ''"),
			array("name"=>"badge_criteria_url", "type"=>"VARCHAR(255) NOT NULL DEFAULT ''"),
			array("name"=>"is_badgeos_badge", "type"=>"TINYINT UNSIGNED NOT NULL DEFAULT 0"),			
			array("name"=>"badgeos_id", "type"=>"INT UNSIGNED NOT NULL DEFAULT 0"),	
			array("name"=>"points_cat_id", "type"=>"INT UNSIGNED NOT NULL DEFAULT 0"), /* when total points should be from given quesiton cat */
			array("name"=>"percent_cat_id", "type"=>"INT UNSIGNED NOT NULL DEFAULT 0"), /* when % correct should be from given quesiton cat */
			array("name"=>"use_latest_points", "type"=>"TINYINT UNSIGNED NOT NULL DEFAULT 0"), /* whether to use all points or from latest attempt */
			array("name"=>"is_mycred_badge", "type"=>"TINYINT UNSIGNED NOT NULL DEFAULT 0"),			
			array("name"=>"mycred_id", "type"=>"INT UNSIGNED NOT NULL DEFAULT 0"),	
		), WATUPROPLAY_LEVELS);
		
		watupro_add_db_fields(array(
			array("name"=>"taking_id", "type"=>"INT UNSIGNED NOT NULL DEFAULT 0"), /* the taking ID which caused the event (if any)*/			
		), WATUPROPLAY_HISTORY);
		
		watupro_add_db_fields(array(
			array("name"=>"in_store", "type"=>"INT UNSIGNED NOT NULL DEFAULT 0"), /* the taking ID which caused the event (if any)*/			
		), WATUPROPLAY_REWARDS);
		// exit;
		
		update_option('watuproplay_db_version', 1.85);
  }
   
   // main menu
   static function menu() {   	
		add_menu_page(__('WatuPRO Play Plugin', 'watuproplay'), __('Watu PRO Play', 'watuproplay'), 
			WATUPROPLAY_MANAGE_CAPS, "watupro_play", array('WatuPROPlay', 'main'));		
		add_submenu_page('watupro_play', __('Manage Levels', 'watuproplay'), __('Manage Levels', 'watuproplay'), WATUPROPLAY_MANAGE_CAPS,
			'watuproplay_levels', array('WatuPROPlayLevels', 'manage'));
		add_submenu_page('watupro_play', __('Manage Badges', 'watuproplay'), __('Manage Badges', 'watuproplay'), WATUPROPLAY_MANAGE_CAPS,
			'watuproplay_badges', array('WatuPROPlayBadges', 'manage'));
		add_submenu_page('watupro_play', __('Manage Rewards', 'watuproplay'), __('Manage Rewards', 'watuproplay'), WATUPROPLAY_MANAGE_CAPS,
			'watuproplay_rewards', array('WatuPROPlayRewards', 'manage'));
		add_submenu_page('watupro_play', __('Help', 'watuproplay'), __('Help', 'watuproplay'), WATUPROPLAY_MANAGE_CAPS,
			'watuproplay_help', array(__CLASS__, 'help'));	
			
		add_submenu_page(NULL, __('Redeemed Reward Items', 'watuproplay'), __('Redeemed Reward Items', 'watuproplay'), WATUPROPLAY_MANAGE_CAPS,
			'watuproplay_rewards_history', array('WatuPROPlayRewards', 'history'));	
		add_submenu_page(NULL, __('Open Badge Settings', 'watuproplay'), __('Open Badge Settings', 'watuproplay'), WATUPROPLAY_MANAGE_CAPS,
			'watuproplay_open_badge_settings', array('WatuPROPlayOpenBadges', 'settings'));		
		add_submenu_page(NULL, __('Users who earned a badge', 'watuproplay'), __('Users who earned a badge', 'watuproplay'), WATUPROPLAY_MANAGE_CAPS,
			'watuproplay_badge_users', array('WatuPROPlayBadges', 'users_earned'));			
	}
	
	// CSS and JS
	static function scripts() {
		// 
	}
	
	static function main() {
		global $wpdb;		
		
		// select all exams	
		$exams = $wpdb -> get_results( "SELECT * FROM ".WATUPRO_EXAMS." ORDER BY name" );
		
		// select exam categories
		$cats = $wpdb -> get_results( "SELECT * FROM ".WATUPRO_CATS." ORDER BY name" );
		
		// select question categories
		$qcats = $wpdb -> get_results( "SELECT * FROM ".WATUPRO_QCATS." ORDER BY name" );
		
		if(!empty($_POST['save_overview'])) {
			update_option('watuproplay_enable_overview', $_POST['enable_overview']);
			update_option('watuproplay_overview_title', $_POST['overview_title']);
			update_option('watuproplay_overview_design', $_POST['overview_design']);
		}
		
		$enable_overview = get_option('watuproplay_enable_overview');
		$overview_title = get_option('watuproplay_overview_title');
		$overview_design = get_option('watuproplay_overview_design');
		
		if(empty($overview_title)) $overview_title = "My Badges and Rewards";
		if(empty($overview_design)) $overview_design = "<p>Current points: [watuproplay-points]<br>
Current level: [watuproplay-userlevel].</p>
<h3>My Badges:</h3> 
[watuproplay-userbadges]		
<h3>Redeemed Rewards:</h3>
[watuproplay-userrewards]";	
							
		require(WATUPROPLAY_PATH."/views/main.html.php");
	}
	
	// initialization
	static function init() {
		global $wpdb;
		load_plugin_textdomain( 'watuproplay', false, WATUPROPLAY_RELATIVE_PATH."/languages/" );
		if (!session_id()) @session_start();

		$manage_caps = current_user_can('manage_options')?'manage_options':'watupro_manage_exams';
    	define('WATUPROPLAY_MANAGE_CAPS', $manage_caps);
		
		// define table names	
		define('WATUPROPLAY_LEVELS', $wpdb->prefix.'watuproplay_levels');
		define('WATUPROPLAY_REWARDS', $wpdb->prefix.'watuproplay_rewards');
		define('WATUPROPLAY_REDEEMED', $wpdb->prefix.'watuproplay_redeemed_rewards');
		define('WATUPROPLAY_HISTORY', $wpdb->prefix.'watuproplay_history');
				
		// add actions
		WatuPROPlayActions::init();
		
		// add shortcodes
		WatuPROPlayShortcodes::init();
		
		// template redirect for the open badges
		add_action('template_redirect', array('WatuPROPlayOpenBadges', 'template_redirect'));
		
		$db_version=get_option("watuproplay_db_version");
		if($db_version < 1.85) self :: install(true);
	}
	
	static function help() {
		include(WATUPROPLAY_PATH . "/views/help.html.php");
	}
}