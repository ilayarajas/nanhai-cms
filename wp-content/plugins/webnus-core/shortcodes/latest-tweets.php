<?php
function michigan_webnus_twitterfeed ( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'title'					=> '',
		'username'				=> '',
		'count' 				=> '1',
		'access_token'			=> '',
		'access_token_secret'	=> '',	
		'consumer_key'			=> '',	
		'consumer_secret'		=> '',	
		'background_image'		=> '',	
	), $atts));
	ob_start(); ?>
	<section class="wrap-tweets-carousel">
		<div class="container">
			<div class="col-md-12">
				<hr class="vertical-space4">
				<div class="tweets-carousel">
					<?php
					require_once dirname( __FILE__ ) . '/twitter-api.php';
				
					/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
					$settings = array(
						'oauth_access_token' => $access_token,
						'oauth_access_token_secret' => $access_token_secret,
						'consumer_key' => $consumer_key,
						'consumer_secret' => $consumer_secret
					);
				
					$url			= "https://api.twitter.com/1.1/statuses/user_timeline.json";
					$requestMethod	= "GET";
					$getfield		= "?screen_name=$username&count=$count";
					$twitter		= new TwitterAPIExchange($settings);
					$tweets			= json_decode($twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest(),$assoc = TRUE);
				
					if( isset( $tweets['errors'][0]['message'] ) && $tweets['errors'][0]['message'] != '' ) :
						echo '
							<h3>' . esc_html__( 'Sorry, there was a problem.', 'michigan' ) . '</h3>
							<p>' . esc_html__( 'Twitter returned the following error message:', 'michigan' ) . '</p>
							<p><em>' . $tweets['errors'][0]['message'] . '</em></p>';
					else :
						echo '<div class="tweets-owl-carousel owl-carousel owl-theme">';
							foreach( $tweets as $tweet ) :
								// Convert attags to twitter profiles in <a> links
								$tweet['text'] = preg_replace("/@([A-Za-z0-9\/\.]*)/", "<a href=\"http://www.twitter.com/$1\">@$1</a>", $tweet['text']);
								// Formatting Twitter’s Date/Time
								$tweet['created_at'] = date("l M j \- g:ia",strtotime($tweet['created_at']));
				
						        echo '<div class="tweet-item"><p>' . $tweet['text'] . '</p></div>';
						    endforeach;
						echo '</div>';
					endif; ?>
				
					<div class="tweets-navigation">
						<a class="btn prev"><i class="sl-arrow-left"></i></a>
						<a class="btn next"><i class="sl-arrow-right"></i></a>
					</div>
				</div>
				<hr class="vertical-space4">
			</div>
		</div>
	</section>

	<?php
	$out = ob_get_contents();
	ob_end_clean();
	$out = str_replace('<p></p>','',$out);
	return $out;
}

add_shortcode( 'twitterfeed', 'michigan_webnus_twitterfeed' );