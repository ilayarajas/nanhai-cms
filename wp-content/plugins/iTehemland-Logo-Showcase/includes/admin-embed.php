<?php
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'custom-css', __IT_LOGO_SHOWCASE_CSS_URL__ . 'custom_css.css', array() , null);
	wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'public-css', __IT_LOGO_SHOWCASE_CSS_URL__. 'public.css', array() , null);
	wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'owlCarousel/owl-carousel/owl.carousel.css');
	wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel-th', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'owlCarousel/owl-carousel/owl.theme.css');
	
	///////// grid responsive ///////////
    wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'responsive-grid', __IT_LOGO_SHOWCASE_CSS_URL__ . 'responsive-grid.css', true);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'responsive-grid');
	
	/////tooltip//////
	wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'tooltip/src/stylesheets/tipsy.css');
	
	/////TyTabs////////
	wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tytabs-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'tytabs/styles.css');
	
	//FONTAWESOME STYLE //font-awesome-css
    wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'font-awesome-ccc', __IT_LOGO_SHOWCASE_CSS_URL__ . 'font-awesome.css', true);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'font-awesome-ccc');
	
	/////////ADMIN STYLE/////////////////
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'admin-style', __IT_LOGO_SHOWCASE_CSS_URL__ . 'back-end/admin-style.css', true);

    wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'css-custom', __IT_LOGO_SHOWCASE_CSS_URL__ . 'style.css', true);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'css-custom');
	
	/////////////////////////CSS CHOSEN///////////////////////
    wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'chosen_css_1', __IT_LOGO_SHOWCASE_CSS_URL__ . 'back-end/chosen/chosen.css', false, '1.0.0');
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'chosen_css_1');

    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'style-css', __IT_LOGO_SHOWCASE_CSS_URL__ . 'back-end/style.css', false, '1.0.0');

	///////////////////Animation.css//////////////////////
	wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'animate', __IT_LOGO_SHOWCASE_CSS_URL__ . 'animate.css', true);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'animate');
	
	
	/////COLOR PICKKER//////
    wp_enqueue_style('wp-color-picker');	
	
	
	//JS
	/////JS ENQUEUE////////////
    wp_enqueue_script('jquery');
	
	//////Carousel////////
	wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'owlCarousel/owl-carousel/owl.carousel.min.js');
	
	//////Isotop///////
	wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'isotope', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'isotope/isotope.pkgd.min.js');
	

	/////tooltip//////
    wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'tooltip/src/javascripts/jquery.tipsy.js');
    
    // TyTabs
    wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tytabs', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'tytabs/tytabs.jquery.min.js');
    
	//////WOW/////////////
	wp_register_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'wow', __IT_LOGO_SHOWCASE_JS_URL__ . 'front-end/wow.js', array('jquery'), false, true );
	wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'wow');
	
    ///////PICKKER//////
    wp_enqueue_script('wp-color-picker');

    //FOR UPLOAD FILE IN TAXONOMY
    if (function_exists('wp_enqueue_media')) {
        wp_enqueue_media();
    } else {
        wp_enqueue_style('thickbox');
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
    }


    //////////////////CHOSEN//////////////////////////
    wp_register_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'chosen_js1', __IT_LOGO_SHOWCASE_JS_URL__ . 'back-end/chosen/chosen.jquery.min.js', false, '1.0.0');
    wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'chosen_js1');


    //////////////////DEPENDENCY//////////////////////////
    global $post_type;
    if (__Custom_Post__ == $post_type || (isset($_GET['post_type']) && __Custom_Post__ == $_GET['post_type'])) {
        wp_register_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'dependency', __IT_LOGO_SHOWCASE_JS_URL__ . 'back-end/dependency/dependsOn-1.0.1.min.js', false, '1.0.0');
        wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'dependency');
    }

    //////////////////CUSTOM JS//////////////////////////
    wp_register_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'js-custom', __IT_LOGO_SHOWCASE_JS_URL__ . 'back-end/custom-js.js', true);
    wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'js-custom');

	/////////Ajax Object ////////////////////////////////
	wp_localize_script( __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'js-custom', 'ajax_object', array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
        'nonce' => wp_create_nonce( 'it_logoshowcase_nonce' ) ) );

    $output = '
			<script type="text/javascript">
				jQuery(document).ready(function(jQuery){
					jQuery(".chosen-select-public").chosen();
			
					if(jQuery("#' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'fetch_type").val()=="build_query"){
						setTimeout(function(){
							if(jQuery(".chosen-select-build").is(":visible")) {
								jQuery(".chosen-select-build").chosen();
							}	
						},100);	
					}
					
					jQuery("#' . __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'fetch_type").change(function(){
						if(jQuery(this).val()=="build_query"){
							setTimeout(function(){
								if(jQuery(".chosen-select-build").is(":visible")) {
									jQuery(".chosen-select-build").chosen();
								}	
							},100);	
						}
					});	
				});
			</script>	
	';
    echo $output;

    if (!function_exists('it_logo_showcase_dependency')) {
        function it_logo_showcase_dependency($element_id, $args)
        {
            $output = '';
            $output .= '
			<script type="text/javascript">
				jQuery(document).ready(function(jQuery){
				
				jQuery("#"+"' . $element_id . '").dependsOn({';
            foreach ($args['parent_id'] as $parent) {
                $element_type = $args[$parent][0];
                unset($args[$parent][0]);
                switch ($element_type) {
                    case "select": {
                        $output .= '
								"#' . $parent . '": {
										values: [\'' . (is_array($args[$parent]) ? implode("','", $args[$parent]) : $args[$parent]) . '\']
								},';
                    }
                        break;

                    case "checkbox" :
	                    {
                        if ($args[$parent] == 'true')
                            $output .= '
									"#' . $parent . '": {
										checked: true
									},';
                        else
                            $output .= '
									"#' . $parent . '": {
										checked: false
									},';

                    }
                        break;
                }
            }
            $output .= '
					});
				});
			 </script>';
            return $output;
        }
    }
?>