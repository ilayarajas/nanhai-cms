<div class="wrap">
	<h1><?php _e('Redeemed Reward Items', 'watuproplay')?></h1>
	
	<p><?php _e('Here you can find all the existing reward items that are redeemed by your users. When you deliver a digital or physical item you can mark this here for your records', 'watuproplay');?></p>
	
	<p><?php _e('Show:', 'watuproplay')?> <a href="admin.php?page=watuproplay_rewards_history" <?php if(!isset($_GET['status']) or $_GET['status']=='all') echo 'style="font-weight:bold"'?>><?php _e('All', 'watuproplay')?></a>
	| <a href="admin.php?page=watuproplay_rewards_history&status=1" <?php if(isset($_GET['status']) and $_GET['status']=='1') echo 'style="font-weight:bold"'?>><?php _e('Delivered', 'watuproplay')?></a>
	| <a href="admin.php?page=watuproplay_rewards_history&status=0" <?php if(isset($_GET['status']) and $_GET['status']=='0') echo 'style="font-weight:bold"'?>><?php _e('Not delivered', 'watuproplay')?></a></p>
	
	<form method="post">
	<?php foreach($items as $item):?><input type="hidden" name="ids[]" value="<?php echo $item->id?>"><?php endforeach;?>
	
	<table class="widefat">
		<tr><th><?php _e('Item title','watuproplay')?></th><th><?php _e('User redeemed','watuproplay')?></th><th><?php _e('Date redeemed','watuproplay')?></th>
		<th><?php _e('Points spent','watuproplay')?></th><th><?php _e('Delivered?','watuproplay')?></th></tr>
		<?php foreach($items as $item):?>
			<tr><td><?php echo $item->item_title?></td><td><?php echo $item->user_login?></td>
			<td><?php echo date(get_option('date_format'), strtotime($item->date))?></td><td><?php echo $item->points?></td>
			<td align="center"><input type="checkbox" name="shipped_ids[]" value="<?php echo $item->id?>" <?php if($item->is_shipped) echo 'checked'?>></td></tr>
		<?php endforeach;?>
	</table>
	
	<p align="center"><input type="submit" value="<?php _e('Update delivered rewards', 'watuproplay')?>"></p>
	<input type="hidden" name="ok" value="1">
	</form>
	
	<p align="center"><?php if($offset>0):?><a href="admin.php?page=watuproplay_rewards_history&status=<?php echo $_GET['status']?>&offset=<?php echo ($offset-50)?>"><?php _e('previous page', 'watuproplay')?></a><?php endif;?>
	&nbsp;	
	<?php if($cnt_items > $offset+50):?><a href="admin.php?page=watuproplay_rewards_history&status=<?php echo $_GET['status']?>&offset=<?php echo ($offset+50)?>"><?php _e('next page', 'watuproplay')?></a><?php endif;?></p>
</div>	