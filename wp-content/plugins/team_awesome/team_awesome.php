<?php

/*
    Plugin Name: Team Awesome
    Version: 1.3.4
    Description: Team Awesome plugin by Stachethemes
    Author: Stachethemes
    Author URI: http://www.stachethemes.com/
    License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/

defined("DS") ? null : define("DS", DIRECTORY_SEPARATOR);
defined("TEAM_AWESOME__FILE__") ? null : define("TEAM_AWESOME__FILE__", __FILE__);

    
/* 
 * Load classes 
 */
require_once dirname(TEAM_AWESOME__FILE__) . DS . "admin" . DS . "class" . DS . "ta.admin.class.php";
require_once dirname(TEAM_AWESOME__FILE__) . DS . "front" . DS . "class" . DS . "ta.class.php";



/* 
 * Load language 
 */
load_plugin_textdomain('team-awesome', false, basename( dirname(TEAM_AWESOME__FILE__)) . DS . 'languages' );



/* 
 * Activation Hook 
 */
register_activation_hook(TEAM_AWESOME__FILE__, "team_awesome_activate");

function team_awesome_activate() {
    require_once dirname(TEAM_AWESOME__FILE__) . DS . "activate.php";
}


/* 
 * Un-install hook 
 */
register_uninstall_hook(TEAM_AWESOME__FILE__, "team_awesome_uninstall");

function team_awesome_uninstall() {
    require_once dirname(TEAM_AWESOME__FILE__) . DS . "uninstall.php";
}


/* 
 * Register admin menu 
 */
function team_awesome_menus() {
    add_menu_page("Team Awesome", "Team Awesome", "update_plugins", "team_awesome", "team_awesome_menu_home", "dashicons-format-image");

    add_submenu_page("team_awesome", "General", "General", "update_plugins", "team_awesome_general", "team_awesome_menu_general");
   
    add_submenu_page("team_awesome", "Categories", "Categories", "update_plugins", "team_awesome_categories", "team_awesome_menu_categories");
    
    add_submenu_page("team_awesome", "Members", "Members", "update_plugins", "team_awesome_members", "team_awesome_menu_members");
}
  
function team_awesome_menu_home() {
    require dirname(TEAM_AWESOME__FILE__) . DS . "admin" . DS . "view" . DS . "home" . DS . "index.php";
}

function team_awesome_menu_general() {
    require dirname(TEAM_AWESOME__FILE__) . DS . "admin" . DS . "view" . DS . "general" . DS . "index.php";
}

function team_awesome_menu_categories() {
    require dirname(TEAM_AWESOME__FILE__) . DS . "admin" . DS . "view" . DS . "categories" . DS . "index.php";
}

function team_awesome_menu_members() {
    require dirname(TEAM_AWESOME__FILE__) . DS . "admin" . DS . "view" . DS . "members" . DS . "index.php";
}

add_action("admin_menu", "team_awesome_menus");



/* Admin menu includes */
function team_awesome_admin_includes($hook) {
    if (
            $hook == 'toplevel_page_team_awesome' ||
            $hook == 'team-awesome_page_team_awesome_general' ||
            $hook == 'team-awesome_page_team_awesome_categories' ||
            $hook == 'team-awesome_page_team_awesome_members'
    ) 
    {
        TEAM_AWESOME_ADMIN::load_includes();
    }
}

add_action('admin_enqueue_scripts', 'team_awesome_admin_includes');


/* Shortcode */


function team_awesome_check_for_shortcode() {
    global $post;
    if (!$post) { return; }
    
    if (has_shortcode($post->post_content, 'team_awesome')) {
        TEAM_AWESOME::load_includes();
    }
}

add_action('wp_enqueue_scripts', 'team_awesome_check_for_shortcode');

function team_awesome_display($atts = array()) {

    ob_start();
    
    // double-check css/scripts
    TEAM_AWESOME::load_includes();
    
    // get options
    $options = TEAM_AWESOME::get_options();
    
    // override options from shortcode attributes
    if (!empty($atts)) {
        foreach ($atts as $key => $value) {
            $options[$key] = $value;
        }
    }
    
    // update options 
    TEAM_AWESOME::$options = $options;
    
    // display html
    
    if (isset($options["member"])) {
        $memberid = (int)$options["member"];
        require dirname(TEAM_AWESOME__FILE__) . DS . "front" . DS . "view" . DS . "default-single.php" ;
    } else {
        require dirname(TEAM_AWESOME__FILE__) . DS . "front" . DS . "view" . DS . "default.php" ;
    }
    
    // $json are members html object from default.php
    if ($json) {
        $options["members"] = $json;
    }
    
    // create js instance
    TEAM_AWESOME::create_js_instance($options);
    
    TEAM_AWESOME::$options = "";
    
    return ob_get_clean();

}

add_shortcode("team_awesome", "team_awesome_display");
