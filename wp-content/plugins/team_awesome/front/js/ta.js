(function ($) {

    "use strict";

    var ta_helper = {
        
        canvas_polygon: function (ctx, x, y, radius, sides, startAngle, anticlockwise) {

            if (sides < 3) {
                return;
            }

            var a = (Math.PI * 2) / sides;
            a = anticlockwise ? -a : a;
            ctx.save();
            ctx.translate(x, y);
            ctx.rotate(startAngle);
            ctx.moveTo(radius, 0);
            for (var i = 1; i < sides; i++) {
                ctx.lineTo(radius * Math.cos(a * i), radius * Math.sin(a * i));
            }
            ctx.closePath();
            ctx.restore();
        },
        
        onResizeEnd: function (callback, time) {
            var id;
            time = time ? time : 10;
            
            $(window).on("resize", function () {
                
                clearTimeout(id);
                id = setTimeout(function () {
                    if (typeof callback === "function") {
                        callback.call();
                    }
                }, time);
            });

        },
        
        onScrollEnd: function (callback, time, relelem) {
            var id;
            time = time ? time : 50;
            $(document).on("scroll", relelem, function () {
                clearTimeout(id);
                id = setTimeout(function () {
                    if (typeof callback === "function") {
                        callback.call();
                    }
                }, time);
            });

        },
        
        isScrolledIntoView: function (elem, relelem) {

            if (!relelem) {
                relelem = window;
            }

            // Check elem on front page
            if ($(elem).length <= 0) {
                return false;
            }

            var docViewTop = relelem === window ? $(relelem).scrollTop() : $(relelem).offset().top;
            var docViewBottom = docViewTop + $(relelem).outerHeight(true);

            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).outerHeight(true);
            
            return (docViewBottom >= elemTop && docViewTop <= elemBottom);
        },
        
        isMobile: function() {
            return ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) ? true : false;
        },
        
        clickHandle: function(){
            return this.isMobile() ? "touchstart" : "click";
        }
    };

    function team_awesome() {
        
        var instance = "";
        var $instance = "";

        var glob = {
            settings: {}
        };
        
        this.init = function (settings) {
            glob.settings = settings;

            instance = "#" + glob.settings.id;
            $instance = $(instance);
            
            if (!ta_helper.isMobile()) {
                $(window).on("scroll", function () {
                    /* background parallax */
                    $instance.filter(".ta-parallax").each(function () {
                        if (ta_helper.isScrolledIntoView(this)) {
                            var scrolledY = -1 * ($(window).scrollTop() - $(this).offset().top + $(this).height());
                            $(this).css('background-position', 'center ' + ((scrolledY * 0.2)) + 'px');
                        }
                    });
                });
            }
            
            if (ta_helper.isMobile()) {
                $instance.addClass("ta-no-blur");
            }

            categories.init();
            memberBox.init();
            slider.init();
        };
        
        var categories = {
            
            $blur: "",
            maxblur: 100,
            easing: "easeOutQuint", /* blur easing */
            speed: 500, /* should match css anim speed */
            
            init: function () {
                this.$blur = $("#ta-blur-vert").children().get(0);
                this.filter();
                this.bindControls();
            },
            
            swapOld: function(){
                
                var parent = this;
                
                $instance.find('.team-awesome-container').removeClass("ta-anim-swap-new");
                $instance.find('.team-awesome-container').addClass("ta-anim-swap-old");
               
                // STOP BLUR
                if (ta_helper.isMobile()) {
                    return;
                }
                
                // blur
                $instance.find('.team-awesome-container').css({
                    tablurvert: 0
                }).stop().animate({
                    tablurvert: 1
                }, {
                    start: function () {
                        $instance.find('.team-awesome-member-box').addClass("ta-svg-blur-vert");
                        parent.setBlur(0, 0);
                    },
                    step: function (x) {
                        var p = parent.maxblur - Math.round(parent.maxblur * x);
                        parent.setBlur(0, p);
                    },
                    complete: function () {
                        $instance.find('.team-awesome-member-box').removeClass("ta-svg-blur-vert");
                    },
                    easing: parent.easing,
                    duration: parent.speed
                });
            },
            
            swapNew: function(){
                
                var parent = this;
                slider.runVideos();
                $instance.find('.team-awesome-container').removeClass("ta-anim-swap-old");
                $instance.find('.team-awesome-container').addClass("ta-anim-swap-new");
                
                // STOP BLUR
                if (ta_helper.isMobile()) {
                    return;
                }
                
                // blur
                $instance.find('.team-awesome-container').css({
                    tablurvert: 0
                }).stop().animate({
                    tablurvert: 1
                }, {
                    start: function () {
                        $instance.find('.team-awesome-member-box').addClass("ta-svg-blur-vert");
                        parent.setBlur(0, 0);
                    },
                    step: function (x) {
                        var p = parent.maxblur - Math.round(parent.maxblur * x);
                        parent.setBlur(0, p);
                    },
                    complete: function () {
                        $instance.find('.team-awesome-member-box').removeClass("ta-svg-blur-vert");
                        $instance.find('.team-awesome-container').removeClass("ta-anim-swap-new");
                    },
                    easing: parent.easing,
                    duration: parent.speed
                });
            },
            
            setBlur: function (x, y) {
                this.$blur.setAttribute("stdDeviation", x + "," + y);
            },
            
            bindControls: function () {

                var parent = this;

                $instance.find(".team-awesome-menu li").on(ta_helper.clickHandle(), function (e) {
                    
                    e.preventDefault();
                    
                    if ($(this).hasClass("active")) {
                        return;
                    }

                    $(this).addClass("active");
                    $(this).siblings().removeClass("active");
                    
                    var swap = function () {
                        parent.swapOld();
                        setTimeout(function () {
                            parent.filter();
                            if (slider.$slider) {
                                slider.reset();
                            }
                            parent.swapNew();
                            
                        }, 500);
                    };
                    
                    
                    // Check for opened popup

                    if ($(instance).find(".ta-open").length > 0) {
                        $(instance).find(".ta-open").removeClass("ta-open");

                        setTimeout(function () {
                            slider.$slider.css({
                                overflow: "hidden"
                            });
                            swap();
                        }, 350);

                    } else {
                        swap();
                    }
                    
                });
            },
            filter: function () {
                // Filter members by category

                var active = $(instance).find(".team-awesome-menu .active").attr("data-cat");
                
                if (!active) {
                    active = 0;
                }
                
                var all = glob.settings.members;
                var filter = "";
                
                if (active == 0) {
                    filter = all;
                } else {
                    filter = $(all).filter(function (index, obj) {
                        var catarr = obj.cat.split("-");
                        return $.inArray(active, catarr) >= 0;
                    });
                }

                // Clear old
                $instance.find('.team-awesome-slide').remove();
                $instance.find('.team-awesome-member-box').remove();
                
                // Add New 
                $(filter).each(function () {
                    
                    var html = this.html;
                    
                    $(html).appendTo($instance.find(".team-awesome-slider"));
                    
                    memberBox.dimensions();
                    memberBox.popDimensions();
                });
            }
        };

        var memberBox = {
            
            mobileAvatarHover: false,
            
            init: function () {
                
                var parent = this;
                
                parent.dimensions();
                parent.popDimensions();
                parent.bindControls();

            },
            
            mediaQuery: function(){
                memberBox.scrolls();
            },
            popDimensions: function () {
                var $box   = $instance.find(".team-awesome-member-box");
                
                $box.each(function(){
                    
                    var $pop = $(this).find(".team-awesome-member-pop");
                    var $inner = $pop.find(".team-awesome-member-pop-inner");

                    $pop.css({
                        maxHeight: $pop.attr("data-maxheight"),
                        width: $pop.attr("data-width"),
                        height: $inner.outerHeight() + 20
                    });

                    $pop.css({
                        left: $(this).outerWidth() / 2 - $pop.outerWidth() / 2,
                        top: $(this).outerHeight() / 2 - $pop.outerHeight() / 2
                    });
                    
                });
                
            },
            dimensions: function () {

                var $boxes = $(instance).find(".team-awesome-member-box");

                $boxes.each(function () {

                    var boxWidth = $(this).attr("data-width");

                    // Member box width
                    $(this).css({
                        width: boxWidth
                    });

                });

            },
            scrolls: function () {
                $instance.find(".team-awesome-member-pop-scroll").jScrollPane({
                    verticalGutter: 0
                });
            },
            emptyStats: function (el) {
                var $box = $(instance).find(".ta-open");

                if (el) {
                    $box = $(el);
                }

                setTimeout(function () {
                    $box.find(".team-awesome-member-pop-percents-inner").stop().css({
                        width: 0
                    });
                    $box.find(".team-awesome-member-pop-percents-box-aftertitle").stop().text("0%");
                }, 1000);
                $box.find(".team-awesome-member-pop-percents-inner").removeAttr("data-filled");
            },
            fillStats: function (delay, speed) {
                if (!delay) {
                    delay = 0;
                }
                
                if (!speed) {
                    speed = 2000;
                }
                
                setTimeout(function () {

                    var $box = $instance.find(".ta-open");

                    var a = $box.find(".team-awesome-member-pop-stats");
                    var b = $box;

                    if (ta_helper.isScrolledIntoView(a, b)) {

                        // Fill stats bars
                        $box.find(".team-awesome-member-pop-percents-inner:not([data-filled])").each(function (i) {
                            var th = this;
                            var fill = $(th).attr("data-fill");

                            $(th).attr("data-filled", 1);

                            $(th).stop().delay(i * 100).animate({
                                width: fill
                            }, {
                                duration: speed,
                                easing: "easeInOutExpo",
                                step: function (x) {
                                    x = Math.round(x) + "%";
                                    $(th).parents(".team-awesome-member-pop-percents-box").find(".team-awesome-member-pop-percents-box-aftertitle").text(x);
                                }
                            });

                        });
                    }

                }, delay);


            },
            getRandomAnim: function () {
                // Random social hover animation

                var animationArray = new Array(
                        "ta-anim-grow",
                        "ta-anim-pulse",
                        "ta-anim-wobble-hor",
                        "ta-anim-wobble-ver",
                        "ta-anim-wobble-br",
                        "ta-anim-wobble-tr",
                        "ta-anim-wobble-t",
                        "ta-anim-wobble-b",
                        "ta-anim-wobble-c",
                        "ta-anim-buzz"
                        );


                var random = animationArray[Math.floor(Math.random() * animationArray.length)];

                return random;
            },
            cleanRandomAnim: function (el) {
                // Remove social hover animation

                var classes = "ta-anim-grow ta-anim-pulse ta-anim-wobble-hor ta-anim-wobble-ver ta-anim-wobble-br ta-anim-wobble-tr ta-anim-wobble-t ta-anim-wobble-b ta-anim-wobble-c ta-anim-buzz";

                $(el).removeClass(classes);
            },
            bindControls: function () {

                var parent = this;
                
                /* UNIVERSAL */
                
                ta_helper.onScrollEnd(function () {
                    parent.fillStats();
                }, 100, instance + " .ta-open .team-awesome-member-pop-scroll");
                
                ta_helper.onResizeEnd(function(){
                    memberBox.scrolls();
                }, 100);
                
                /* /UNIVERSAL */
                
                /* MOBILE BINDS */
                
                if (ta_helper.isMobile()) {
                    // a link fix
                    $(document).on(ta_helper.clickHandle(), instance + " a", function (e) {
                        e.stopPropagation();
                        return;
                    });
                    
                    // ta-hover
                    $(document).on(ta_helper.clickHandle(), instance + " .team-awesome-member-avatar-circle, .team-awesome-member-avatar-box, .team-awesome-member-avatar-hex", function (e) {
                        e.preventDefault();
                        if (parent.mobileAvatarHover === false) {
                            $(this).addClass("ta-hover");
                            parent.mobileAvatarHover = true;
                        }
                    });
                    
                    // ta-open
                    $(document).on(ta_helper.clickHandle(), instance + " .team-awesome-member-circle-stamp, .team-awesome-member-box-stamp, .team-awesome-member-hex-stamp", function (e) {

                        e.preventDefault();

                        if (parent.mobileAvatarHover === false) {
                            return;
                        }

                        if (glob.settings.pop == "disable") {
                            return;
                        }

                        if ($instance.find(".team-awesome-slide").is(":animated")) {
                            return;
                        }

                        $instance.find(".team-awesome-member-box").removeClass("ta-open");
                        $(this).parents(".team-awesome-member-box").addClass("ta-open");

                        parent.fillStats(1000);

                        // hide <> cslide slides
                        if (slider.$slider) {
                            slider.$slider.find(".team-awesome-slide").eq(slider.cslide).siblings().hide();
                        }

                        // visible to stop cutting the popup
                        $instance.find(".team-awesome-slider").css({
                            overflow: "visible"
                        });

                        parent.scrolls();

                    });
                    
                    // pop close
                    $(document).on("tap", instance + " .team-awesome-member-pop", function (e) {

                        e.preventDefault();

                        parent.mobileAvatarHover = false;

                        parent.emptyStats(this);
                        $(this).parents(".team-awesome-member-box").removeClass("ta-open");

                        $instance.find(".team-awesome-member-avatar-circle, .team-awesome-member-avatar-box, .team-awesome-member-avatar-hex").removeClass("ta-hover");

                        setTimeout(function () {
                            $instance.find(".team-awesome-slider").css({
                                overflow: "hidden"
                            });
                        }, 500);

                    });
                }
                
                /* /MOBILE BINDS */
                
                /* NORMAL BINDS */
                if (!ta_helper.isMobile()) {
                    
                    // ta-hover
                    $(document).on("hover", instance + " .team-awesome-member-avatar-circle, .team-awesome-member-avatar-box, .team-awesome-member-avatar-hex", function () {
                        $(this).addClass("ta-hover");
                    });

                    $(document).on("mouseleave", instance + " .team-awesome-member-avatar-circle, .team-awesome-member-avatar-box, .team-awesome-member-avatar-hex", function () {
                        $(this).removeClass("ta-hover");
                    });
                    
                    $(document).on("hover", instance + " .team-awesome-social-button-inner", function () {
                        $(this).siblings().addClass("ta-nofocus");
                        $(this).removeClass("ta-nofocus").addClass("ta-focus");

                        var klass = glob.settings.socanim;

                        if (klass == "random") {
                            klass = parent.getRandomAnim();
                        }

                        $(this).addClass(klass);
                    });
                    
                    $(document).on("mouseleave", instance + " .team-awesome-social-button-inner", function () {
                        $(this).removeClass("ta-focus");

                        parent.cleanRandomAnim(this);
                    });
                    
                    // ta-open
                    $(document).on(ta_helper.clickHandle(), instance + " .team-awesome-member-circle-stamp, .team-awesome-member-box-stamp, .team-awesome-member-hex-stamp", function (e) {

                        e.preventDefault();

                        if (glob.settings.pop == "disable") {
                            return;
                        }

                        if ($(instance).find(".team-awesome-slide").is(":animated")) {
                            return;
                        }

                        $instance.find(".team-awesome-member-box").removeClass("ta-open");
                        $(this).parents(".team-awesome-member-box").addClass("ta-open");
                        
                        parent.fillStats(1000);

                        // hide <> cslide slides
                        if (slider.$slider) {
                            slider.$slider.find(".team-awesome-slide").eq(slider.cslide).siblings().hide();
                        }

                        // visible to stop cutting the popup
                        $instance.find(".team-awesome-slider").css({
                            overflow: "visible"
                        });

                        parent.scrolls();

                    });
                    
                    // pop close
                    $(document).on(ta_helper.clickHandle(), instance + " .team-awesome-member-pop", function (e) {

                        e.preventDefault();

                        parent.emptyStats(this);
                        $(this).parents(".team-awesome-member-box").removeClass("ta-open");

                        setTimeout(function () {
                            $instance.find(".team-awesome-slider").css({
                                overflow: "hidden"
                            });
                        }, 500);
                    });
                    
                };
                
                /* /NORMAL BINDS */
                
                
                
                

            }
        };

        var slider = {
            $slider: "",
            perpage: 3,
            cslide: 0,
            total: "",
            $blur: "",
            maxblur: 100,
            easing: "easeOutQuint", /* blur easing */
            speed: 1000, /* should match css anim speed */
            
            init: function () {
                
                this.$slider = $(instance).find(".team-awesome-slider");
                
                if (glob.settings.slider == "enable") {
                    this.perpage = this.$slider.attr("data-perpage");
                    this.total = Math.ceil(this.$slider.find(".team-awesome-member-box").length / this.perpage);
                    this.$blur = $("#ta-blur").children().get(0);

                    this.runVideos();
                    this.mediaQuery(true);
                    this.bindControls();
                } else {
                    this.runVideos();
                    // disables slider reset function
                    this.$slider = false; 
                }

            },
            
            runVideos: function(){
                
                // check compatability here
                if (ta_helper.isMobile()) {
                    $instance.find(".team-awesome-member-hex-stamp-inner-video").hide();
                    $instance.find(".team-awesome-member-circle-stamp-inner-video").hide();
                    $instance.find(".team-awesome-member-box-stamp-inner-video").hide();
                    return;
                }
                
                // if video, hide image
                $instance.find(".team-awesome-member-circle-stamp-inner-video, .team-awesome-member-box-stamp-inner-video, .team-awesome-member-hex-stamp-inner-video").each(function(){
                   $(this).prev().hide();
                });
                
                // run box
                $instance.find(".team-awesome-member-box-stamp-inner-video").each(function(){
                    
                    var canvas = $(this).find("canvas").get(0);
                    var ctx    = canvas.getContext('2d');
                    var video  =  $(this).find("video").get(0);
                    
                    var r = glob.settings.avatarb == "off" ? 159 : 150;
                    var ar = $(video).width() / $(video).height();
                    
                    
                    ta_helper.canvas_polygon(ctx, 112.5, 112.5, r, 4, Math.PI / 4, true);

                    var offset_x = -1 * $(video).width() / 2;
                    
                    ctx.imageSmoothingEnabled = true;
                    
                    $(video).on('canplaythrough', function () {
                        this.play();
                    });
                    
                    video.addEventListener('play', function () {
                        var th = this;
                        
                        try {
                            (function loop() {
                                if (!th.paused && !th.ended) {
                                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                                    ctx.translate(offset_x, 0);
                                    ctx.save();
                                    ctx.clip();
                                    ctx.drawImage(th, 0, 0, canvas.width * ar, canvas.height);
                                    ctx.restore();
                                    ctx.translate(-offset_x, 0);
                                    setTimeout(loop, 1000 / 30); // drawing at 30fps
                                }
                            })();
                        } 
                        catch (err) {
                            loop();
                        }
                    });
                    
                });
                
                
                // run circles
                $instance.find(".team-awesome-member-circle-stamp-inner-video").each(function(){
                    
                    var canvas = $(this).find("canvas").get(0);
                    var ctx    = canvas.getContext('2d');
                    var video  =  $(this).find("video").get(0);
                    var r = glob.settings.avatarb == "off" ? 112.5 : 106.5;
                    var ar = $(video).width() / $(video).height();
                    
                    ctx.beginPath();
                    ctx.arc(112.5, 112.5, r, 0, Math.PI * 2, true);
                    ctx.closePath();
                    
                    var offset_x = -1 * $(video).width() / 2 ;
                    
                    ctx.imageSmoothingEnabled = true;
                    
                    $(video).on('canplaythrough', function () {
                        this.play();
                    });
                    
                    video.addEventListener('play', function () {
                        var th = this;
                        
                        try {
                            (function loop() {
                                if (!th.paused && !th.ended) {
                                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                                    ctx.translate(offset_x, 0);
                                    ctx.save();
                                    ctx.clip();
                                    ctx.drawImage(th, 0, 0, canvas.width * ar, canvas.height);
                                    ctx.restore();
                                    ctx.translate(-offset_x, 0);
                                    setTimeout(loop, 1000 / 30); // drawing at 30fps
                                }
                            })();
                        }
                        catch (err) {
                            loop();
                        }
                        
                    });
                    
                });
                
                // run hexagons
                $instance.find(".team-awesome-member-hex-stamp-inner-video").each(function(){
                    
                    var canvas = $(this).find("canvas").get(0);
                    var ctx    = canvas.getContext('2d');
                    var video  =  $(this).find("video").get(0);
                    
                    var r  = glob.settings.avatarb == "off" ? 112 : 105;
                    var ar = $(video).width() / $(video).height();
                    
                    
                    ta_helper.canvas_polygon(ctx, 112, 112, r, 6, Math.PI / 2, true);

                    var offset_x = -1 * $(video).width() / 2 ;
                    
                    ctx.imageSmoothingEnabled = true;
                    
                    
                    $(video).on('canplaythrough', function () {
                        this.play();
                    });
                    
                    video.addEventListener('play', function () {
                        var th = this;
                        
                        try {
                            (function loop() {
                                if (!th.paused && !th.ended) {
                                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                                    ctx.translate(offset_x, 0);
                                    ctx.save();
                                    ctx.clip();
                                    ctx.drawImage(th, 0, 0, canvas.width * ar, canvas.height);
                                    ctx.restore();
                                    ctx.translate(-offset_x, 0);
                                    setTimeout(loop, 1000 / 30); // drawing at 30fps
                                }
                            })();
                        }
                        
                        catch(err) {
                            loop();
                        };
                        
                    });

                });
          
            },
            
            reset: function () {
                this.cslide = 0;
                this.total = Math.ceil(this.$slider.find(".team-awesome-member-box").length / this.perpage);
                this.dimensions();
                this.clearPages();
                this.createPages();
                memberBox.mobileAvatarHover = false;
            },
            
            setBlur: function (x, y) {
                this.$blur.setAttribute("stdDeviation", x + "," + y);
            },
            
            dimensions: function () {
                this.$slider.css({
                    height: this.$slider.find(".team-awesome-member-box").outerHeight(true)
                });
            },
            
            clearPages: function(){
                var parent = this;
                parent.$slider.find(".team-awesome-member-box").appendTo(parent.$slider);
                parent.$slider.find(".team-awesome-slide").remove();
                
            },
            
            createPages: function () {

                var parent = this;

                if (parent.total > 1) {
                    var i = parent.total;

                    while (i > 0) {
                        var html = "<div class=\"team-awesome-slide\"></div>";
                        $(html).appendTo(parent.$slider);
                        i--;
                    }

                    //var n = parent.$slider.find(".team-awesome-slide").length;
                    var n = -1;
                    
                    parent.$slider.find(".team-awesome-member-box").each(function (i) {

                        if (i % parent.perpage === 0) {
                            n++;
                        }
                        
                        $(this).appendTo(parent.$slider.find(".team-awesome-slide").eq(n));

                    });
                }

                // enable/disable pagination
                if (this.total <= 1) {
                    $instance.find(".team-awesome-slider-control").hide();
                } else {
                    $instance.find(".team-awesome-slider-control").show();
                }
            },
            
            mediaQuery: function (force) {

                var parent = this;

                var maxSize = parent.$slider.width();
                var cur     = parent.perpage;
                var limit   = parent.$slider.attr("data-perpage");
                
                parent.perpage = Math.floor(maxSize / $instance.find(".team-awesome-member-box").outerWidth(true));
                
                if (parent.perpage > limit) {
                    parent.perpage = limit;
                }
                
                if (parent.perpage <= 0) {
                    parent.perpage = 1;
                }
                
                if (parent.perpage != cur) {
                    parent.reset();
                }
                
                if (force === true) {
                    parent.reset();
                }
                
            },
            
            bindControls: function () {

                var parent = this;
                
                ta_helper.onResizeEnd(function(){
                    parent.mediaQuery();
                }, 100);
                
                
                $(instance).find(".team-awesome-slider-control-right").on(ta_helper.clickHandle(), function(e) {
                
                    e.preventDefault();

                    var doslide = function () {
                        
                        memberBox.mobileAvatarHover = false;

                        var $old = parent.$slider.find(".team-awesome-slide").eq(parent.cslide);

                        // clear old animation classes 

                        $old.removeClass("ta-slide-ctl ta-slide-rtc ta-slide-ctr ta-slide-ltc");
                        $old.addClass("ta-slide-ctl");
                        $old.show();


                        var nextslide = parent.cslide + 1;

                        if (nextslide >= parent.total) {
                            nextslide = 0;
                        }

                        var $new = parent.$slider.find(".team-awesome-slide").eq(nextslide);

                        $new.removeClass("ta-slide-ctl ta-slide-rtc ta-slide-ctr ta-slide-ltc");
                        $new.show();
                        $new.addClass("ta-slide-rtc");

                        parent.cslide = nextslide;
                        
                        // STOP BLUR
                        if (ta_helper.isMobile()) {
                            return;
                        }
                        
                        // simple blur 
                        parent.$slider.css({
                            tablur: 0
                        }).stop().animate({
                            tablur: 1
                        }, {
                            start: function () {
                                parent.$slider.addClass("ta-svg-blur");
                                parent.setBlur(0, 0);
                            },
                            step: function (x) {
                                var p = parent.maxblur - Math.round(parent.maxblur * x);
                                parent.setBlur(p, 0);
                            },
                            complete: function () {
                                parent.$slider.removeClass("ta-svg-blur");
                            },
                            easing: parent.easing,
                            duration: parent.speed
                        });
                    };

                    // Check for opened popup

                    if ($instance.find(".ta-open").length > 0) {
                        $instance.find(".ta-open").removeClass("ta-open");

                        setTimeout(function () {
                            parent.$slider.css({
                                overflow: "hidden"
                            });
                            doslide();
                        }, 350);

                    } else {
                        doslide();
                    }

                });

                $instance.find(".team-awesome-slider-control-left").on(ta_helper.clickHandle(), function (e) {
                    
                    e.preventDefault();
                    
                    var doslide = function () {
                        
                        memberBox.mobileAvatarHover = false;

                        var $old = parent.$slider.find(".team-awesome-slide").eq(parent.cslide);

                        // clear old animatino classes 

                        $old.removeClass("ta-slide-ctl ta-slide-rtc ta-slide-ctr ta-slide-ltc");
                        $old.addClass("ta-slide-ctr");
                        $old.show();

                        var nextslide = parent.cslide - 1;
                        if (nextslide < 0) {
                            nextslide = parent.total - 1;
                        }

                        var $new = parent.$slider.find(".team-awesome-slide").eq(nextslide);

                        $new.removeClass("ta-slide-ctl ta-slide-rtc ta-slide-ctr ta-slide-ltc");
                        $new.show();
                        $new.addClass("ta-slide-ltc");

                        parent.cslide = nextslide;

                        // STOP BLUR
                        if (ta_helper.isMobile()) {
                            return;
                        }

                        // simple blur 
                        parent.$slider.css({
                            tablur: 0
                        }).stop().animate({
                            tablur: 1
                        }, {
                            start: function () {
                                parent.$slider.addClass("ta-svg-blur");
                                parent.setBlur(0, 0);
                            },
                            step: function (x) {
                                var p = parent.maxblur - Math.round(parent.maxblur * x);
                                parent.setBlur(p, 0);
                            },
                            complete: function () {
                                parent.$slider.removeClass("ta-svg-blur");
                            },
                            easing: parent.easing,
                            duration: parent.speed
                        });

                    };

                    // Check for opened popup

                    if ($instance.find(".ta-open").length > 0) {
                        $instance.find(".ta-open").removeClass("ta-open");

                        setTimeout(function () {
                            parent.$slider.css({
                                overflow: "hidden"
                            });
                            doslide();
                        }, 350);

                    } else {
                        doslide();
                    }
                });
            }
        };
    }

    $(document).ready(function () {

        if (typeof window.team_awesome_instance !== "undefined") {
            $(window.team_awesome_instance).each(function () {
                new team_awesome().init(this);
            });
        }

    });

})(jQuery);