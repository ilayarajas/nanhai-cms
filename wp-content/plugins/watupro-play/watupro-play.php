<?php
/*
Plugin Name: Watu PRO Play
Plugin URI: http://calendarscripts/watupro/modules.html
Description: Gamification and Competition Plugin for WatuPRO
Author: Kiboko Labs
Version: 2.0.2
Author URI: http://calendarscripts.info/
License: GPLv2 or later
Text Domain: watuproplay
*/

define( 'WATUPROPLAY_PATH', dirname( __FILE__ ) );
define( 'WATUPROPLAY_PARENT_DIR', dirname( dirname(__FILE__) ));
define( 'WATUPROPLAY_RELATIVE_PATH', dirname( plugin_basename( __FILE__ )));
define( 'WATUPROPLAY_URL', plugin_dir_url( __FILE__ ));

// require controllers and models
include(WATUPROPLAY_PATH."/lib/functions.php");
include(WATUPROPLAY_PATH."/models/basic.php");
include(WATUPROPLAY_PATH.'/models/level.php');
include(WATUPROPLAY_PATH.'/models/reward.php');
include(WATUPROPLAY_PATH.'/models/user.php');
include(WATUPROPLAY_PATH."/controllers/shortcodes.php");
include(WATUPROPLAY_PATH."/controllers/actions.php");
include(WATUPROPLAY_PATH."/controllers/leaderboards.php");
include(WATUPROPLAY_PATH."/controllers/levels.php");
include(WATUPROPLAY_PATH."/controllers/badges.php");
include(WATUPROPLAY_PATH."/controllers/rewards.php");
include(WATUPROPLAY_PATH."/controllers/open-badges.php");
include(WATUPROPLAY_PATH."/controllers/charts.php");

add_action('init', array("WatuPROPlay", "init"));

register_activation_hook(__FILE__, array("WatuPROPlay", "install"));
add_action('admin_menu', array("WatuPROPlay", "menu"));
add_action('admin_enqueue_scripts', array("WatuPROPlay", "scripts"));

// show the things on the front-end
add_action( 'wp_enqueue_scripts', array("WatuPROPlay", "scripts"));

// other actions
add_action('wp_ajax_watuproplay_ajax', 'watuproplay_ajax');
add_action('wp_ajax_nopriv_watuproplay_ajax', 'watuproplay_ajax');