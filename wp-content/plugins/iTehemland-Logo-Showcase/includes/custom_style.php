<?php
if(!function_exists('it_logo_showcase_custom_css')) {
    function it_logo_showcase_custom_css($style_arr, $rand_id = '')
    {

        //
        //$style = '<style>';
        $r = (int)$style_arr['margin-right'];
        $l = (int)$style_arr['margin-left'];
        $style = '
        .ls-list-layout #show-inline-box.ls-inline-box-' . $rand_id . '{
            padding:0 ' . $r . 'px 0 ' . $l . 'px !important;
        }
        .ls-list-layout #show-inline-box.ls-inline-box-' . $rand_id . ' .ls-showdetail{
            border-color :' . $style_arr['border-color'] . ' !important;
        }

        .ls-gred-layout #show-inline-box.ls-inline-box-' . $rand_id . '{
            padding:0 ' . $r . 'px 0 ' . $l . 'px !important;
        }
        .ls-gred-layout #show-inline-box.ls-inline-box-' . $rand_id . ' .ls-showdetail{
            border-color :' . $style_arr['border-color'] . ' !important;
        }

        .ls-item-wrap-' . $rand_id . '{
            padding-right:' . $r . 'px !important;
            padding-left :' . $l . 'px !important;
        }
		.ls-gred-item-' . $rand_id . '{
            background-color: ' . $style_arr['background-color'] . ' !important;
		    padding:' . $style_arr['padding-top'] . 'px ' . $style_arr['padding-right'] . 'px ' . $style_arr['padding-bottom'] . 'px ' . $style_arr['padding-left'] . 'px !important;
		    margin:' . $style_arr['margin-top'] . 'px 0 ' . $style_arr['margin-bottom'] . 'px 0 !important;
			border-style :' . $style_arr['border-style'] . ' !important;
			border-color :' . $style_arr['border-color'] . ' !important;
			border-width :' . $style_arr['border-width-top'] . 'px ' . $style_arr['border-width-right'] . 'px ' . $style_arr['border-width-bottom'] . 'px ' . $style_arr['border-width-left'] . 'px !important;
			-webkit-border-radius: ' . $style_arr['border-radius-top-left'] . 'px ' . $style_arr['border-radius-top-right'] . 'px ' . $style_arr['border-radius-bottom-right'] . 'px ' . $style_arr['border-radius-bottom-left'] . 'px !important;
			-moz-border-radius: ' . $style_arr['border-radius-top-left'] . 'px ' . $style_arr['border-radius-top-right'] . 'px ' . $style_arr['border-radius-bottom-right'] . 'px ' . $style_arr['border-radius-bottom-left'] . 'px !important;
			border-radius: ' . $style_arr['border-radius-top-left'] . 'px ' . $style_arr['border-radius-top-right'] . 'px ' . $style_arr['border-radius-bottom-right'] . 'px ' . $style_arr['border-radius-bottom-left'] . 'px !important;';
        if ($style_arr['shadow'] == 'inset')
            $style .= 'box-shadow: ' . $style_arr['shadow-h'] . 'px ' . $style_arr['shadow-v'] . 'px ' . $style_arr['shadow-blur'] . 'px ' . $style_arr['shadow-spread'] . 'px ' . $style_arr['shadow-color'] . ' ' . $style_arr['shadow'] . ' !important;';
        else if ($style_arr['shadow'] == 'outset')
            $style .= 'box-shadow: ' . $style_arr['shadow-h'] . 'px ' . $style_arr['shadow-v'] . 'px ' . $style_arr['shadow-blur'] . 'px ' . $style_arr['shadow-spread'] . 'px ' . $style_arr['shadow-color'] . ' !important;';
        if ($style_arr['effect'] == 'opacity')
            $style .= 'opacity:0.6 !important;';
        $style .= '
		}
		.ls-gred-item-' . $rand_id . ':hover{
		    background-color: ' . $style_arr['hover-background-color'] . ' !important;
		    border-style :' . $style_arr['hover-border-style'] . ' !important;
			border-color :' . $style_arr['hover-border-color'] . ' !important;
			border-width :' . $style_arr['hover-border-width-top'] . 'px ' . $style_arr['hover-border-width-right'] . 'px ' . $style_arr['hover-border-width-bottom'] . 'px ' . $style_arr['hover-border-width-left'] . 'px !important;
			-webkit-border-radius: ' . $style_arr['hover-border-radius-top-left'] . 'px ' . $style_arr['hover-border-radius-top-right'] . 'px ' . $style_arr['hover-border-radius-bottom-right'] . 'px ' . $style_arr['hover-border-radius-bottom-left'] . 'px !important;
			-moz-border-radius: ' . $style_arr['hover-border-radius-top-left'] . 'px ' . $style_arr['hover-border-radius-top-right'] . 'px ' . $style_arr['hover-border-radius-bottom-right'] . 'px ' . $style_arr['hover-border-radius-bottom-left'] . 'px !important;
			border-radius: ' . $style_arr['hover-border-radius-top-left'] . 'px ' . $style_arr['hover-border-radius-top-right'] . 'px ' . $style_arr['hover-border-radius-bottom-right'] . 'px ' . $style_arr['hover-border-radius-bottom-left'] . 'px !important;';
        if ($style_arr['hover-shadow'] == 'inset')
            $style .= 'box-shadow: ' . $style_arr['hover-shadow-h'] . 'px ' . $style_arr['hover-shadow-v'] . 'px ' . $style_arr['hover-shadow-blur'] . 'px ' . $style_arr['hover-shadow-spread'] . 'px ' . $style_arr['hover-shadow-color'] . ' ' . $style_arr['hover-shadow'] . ' !important;';
        else if ($style_arr['hover-shadow'] == 'outset')
            $style .= 'box-shadow: ' . $style_arr['hover-shadow-h'] . 'px ' . $style_arr['hover-shadow-v'] . 'px ' . $style_arr['hover-shadow-blur'] . 'px ' . $style_arr['hover-shadow-spread'] . 'px ' . $style_arr['hover-shadow-color'] . ' !important;';
        $style .= '}';
        //$style .= '</style>';
        //echo $style;
        if (is_admin()) {
            echo '<style>' . $style . '</style>';
        } else {
            wp_add_inline_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'custom-css', $style);
        }
    }
}