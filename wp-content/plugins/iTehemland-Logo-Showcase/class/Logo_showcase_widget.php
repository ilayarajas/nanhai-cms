<?php
/**
 * Widget
 */
function register_it_logo_showcase_grid_widget() {
    //register_widget( 'it_logo_showcase_grid_widget' );
    //register_widget( 'it_logo_showcase_list_widget' );
    //register_widget( 'it_logo_showcase_carousel_widget' );
    //register_widget( 'it_logo_showcase_isotope_widget' );
}
add_action( 'widgets_init', 'register_it_logo_showcase_grid_widget' );

class it_logo_showcase_grid_widget extends WP_Widget
{

    /**
     * @var array
     */
    private $params = array(
        'category' => 'all',
        'orderby' => 'menu_order',
        'order' => 'DESC',
        'layout' => 'grid',
        'col_desktop' => 4,
        'col_tablet' => 3,
        'col_mobile' => 2,
        // style
        'margin_top' => 0,
        'margin_right' => 0,
        'margin_bottom' => 0,
        'margin_left' => 0,
        'padding_top' => 0,
        'padding_right' => 0,
        'padding_bottom' => 0,
        'padding_left' => 0,
        'background_color' => 'transparent',
        'border_style' => 'none',
        'border_width' => 1,
        'border_radius' => 0,
        'border_color' => '#eee',
        'custom_class' => '',
        // hover effect
        'effect' => 'none',
        // shadow on hover
        'shadow' => 'none',
        'shadow_color' => '#999',
        // quick view setting
        'quick_view_type' => 'tooltip',

        // customize show details for full view
        'title' => 'true',
        'contact' => 'true',
        'social' => 'true',
        'full_desc' => 'true',
        'gallery' => 'true',
        'video' => 'true',

        // Carousel Setting
        'laptop_item' => 4,
        'desktop_item' => 6,
        'tablet_item' => 3,
        'mobile_item' => 2,
        'theme' => 'owl-theme',
        'autoplay' => 'true',
        'autoplay_speed' => 3000,
        'slideSpeed' => 1000,
        'paginationSpeed' => 3000,
        'rewindSpeed' => 1000,
        'navigation' => 'true',
        'pagination' => 'true'
    );

    /**
     * Register widget with WordPress.
     */
    public function __construct()
    {
        parent::__construct(
            'it_logo_showcase_grid_widget', // Base ID
            __('Grid Logo Show Case', __IT_LOGO_SHOWCASE_TEXTDOMAIN__), // Name
            array('description' => __('Display grid Logo images on your website', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */

    public function widget($args, $instance)
    {
        $args['title'] = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
        echo $args['before_widget'];
        if (!empty($args['title'])) echo $args['before_title'] . $args['title'] . $args['after_title'];
        echo it_preview_shortcode($instance);
        echo $args['after_widget'];
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        foreach ($this->params as $param_key => $param_val) {
            $instance[$param_key] = $new_instance[$param_key];
        }
        return $instance;
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     *
     * @return html tags
     */
    public function form($instance)
    {
        $instance = wp_parse_args($this->params ,(array)$instance);
        $title = !empty($instance['title']) ? $instance['title'] : __('New title', __IT_LOGO_SHOWCASE_TEXTDOMAIN__);
        $o = '';
        ?>
        <p><label for="<?php
            echo $this->get_field_id('title'); ?>"><?= __('Tilte', __IT_LOGO_SHOWCASE_TEXTDOMAIN__); ?> :</label>
            <input class="widefat" id="<?php
            echo $this->get_field_id('title'); ?>" name="<?php
            echo $this->get_field_name('title'); ?>" type="text" value="<?php
            echo esc_attr($title); ?>"/></p>
        <?php
        // query builder
        $o .= '<p><label for="query_hr" >' . __('Query Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="query_hr"></p>';
        // OrderBy

        $o .= '<p>
            <label for="' . $this->get_field_id('orderby') . '" >' . __('Order By', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
            <select id="' . $this->get_field_id('orderby') . '" name="' . $this->get_field_name('orderby') . '" >
                <option value="order" ' .
            selected($orderby, 'order') . ' >' . __('Order Field', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="title" ' .
            selected($orderby, 'title') . '>' . __('Title', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="id" ' .
            selected($orderby, 'id') . '>' . __('ID', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="date" ' .
            selected($orderby, 'date') . '>' . __('Date', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="modified" ' .
            selected($orderby, 'modified') . '>' . __('Modified', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';
        // Order
        $o .= '<p>
            <label for="' . $this->get_field_id('order') . '">' . __('Order', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
            <select id="' . $this->get_field_id('order') . '" name="' . $this->get_field_name('order') . '">
                <option value="ASC" ' .
            selected($order, 'ASC') . '>' . __('Ascending', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="DESC" ' .
            selected($order, 'DESC') . '>' . __('Descending', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';
        // category select

        $o .= '<p>
            <label for="' . $this->get_field_id('category') . '">' . __('Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
            <select id="it_custom_cat" >
                <option value="true">' . __('All Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="false">' . __('Custom Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';

        $categories = get_terms(__Custom_Post_Taxonomy__);
        if ($categories) {
            $o .= '<p id="' . $this->get_field_id('cat_box') . '">';
            foreach ($categories as $category) {
                $o .= '<input type="checkbox" name="' . $this->get_field_name('category') . '[]" value="' . $category->slug . '" >' . $category->name . '<br>';
            }
            $o .= '</p>';
        }
        // layout setting
        $o .= '<p><label for="layout_hr" >' . __('Layout Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="layout_hr"></p>';
        // fields for grid layout
        $o .= '<p>
						<label for="' . $this->get_field_id('col_desktop') . '">' . __('Desktop Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('col_desktop') . '"
							   name="' . $this->get_field_name('col_desktop') . '">
							<option vlaue="1" ' .
            selected($col_desktop, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_desktop, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_desktop, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_desktop, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_desktop, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_desktop, 12) . ' >12</option>
						</select>
					</p>';
        $o .= '<p><label for="' . $this->get_field_id('col_tablet') . '">' . __('Tablet Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('col_tablet') . '"
							   name="' . $this->get_field_name('col_tablet') . '">
							<option vlaue="1" ' .
            selected($col_tablet, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_tablet, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_tablet, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_tablet, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_tablet, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_tablet, 12) . ' >12</option>
						</select>
					</p>';
        $o .= '<p><label for="' . $this->get_field_id('col_mobile') . '">' . __('Mobile Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
						<select id="' . $this->get_field_id('col_mobile') . '"
						       name="' . $this->get_field_name('col_mobile') . '">
						    <option vlaue="1" ' .
            selected($col_mobile, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_mobile, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_mobile, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_mobile, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_mobile, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_mobile, 12) . ' >12</option>
						</select>
					</p>';
        // end grid fields

        // Style setting
        $o .= '<p><label for="border_hr" >' . __('Style Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="border_hr"></p>';
        $o .= '<p><label for="' . $this->get_field_id('border_style') . '">' . __('Border Style', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('border_style') . '"
						       name="' . $this->get_field_name('border_style') . '" >
						    <option value="none" ' .
            selected($border_style, 'none') . ' >None</option>
		                    <option value="solid" ' .
            selected($border_style, 'solid') . ' >Solid</option>
		                    <option value="dashed" ' .
            selected($border_style, 'dashed') . ' >Dashed</option>
		                    <option value="dotted" ' .
            selected($border_style, 'dotted') . ' >Dotted</option>
						</select>
					</p>';

        $o .= '<p><div>
            <label for="' . $this->get_field_id('border_width') . '" >' . __('Border Width', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number" name="' . $this->get_field_name('border_width') . '"
									    id="' . $this->get_field_id('border_width') . '"
									    value="' . esc_attr($border_width) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
									     />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div></p>';

        $o .= '<p><div >
            <label for="' . $this->get_field_id('border_radius') . '" >' . __('Border Radius', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number" name="' . $this->get_field_name('border_radius') . '"
									    id="' . $this->get_field_id('border_radius') . '"
									    value="' . esc_attr($border_radius) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
									    />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div></p>';


        $o .= '<p><div >
					<label for="' . $this->get_field_id('border_color') . '" >' . __('Border Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . $this->get_field_name('border_color') . '"
					        id="' . $this->get_field_id('border_color') . '"
					        type="text" class="wp_ad_picker_color" value="' . esc_attr($border_color) . '"
					        data-default-color="' . esc_attr($border_color) . '">
			</div>
            </p>';
        // padding
        $o .= '<p><label >' . __('Padding', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';
        $o .= '<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_top') . '" class="small-label">' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_top') . '"
                                        id="' . $this->get_field_id('padding_top') . '"
                                        value="' . esc_attr($padding_top) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_right') . '" class="small-label">' . __('Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_right') . '"
                                        id="' . $this->get_field_id('padding_right') . '"
                                        value="' . esc_attr($padding_right) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text " />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_bottom') . '" class="small-label">' . __('Bottom', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_bottom') . '"
                                        id="' . $this->get_field_id('padding_bottom') . '"
                                        value="' . esc_attr($padding_bottom) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text " />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                </div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_left') . '" class="small-label">' . __('Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_left') . '"
                                        id="' . $this->get_field_id('padding_left') . '"
                                        value="' . esc_attr($padding_left) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>';

        // margin
        $o .= '<p><label >' . __('margin', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';
        $o .= '<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_top') . '" class="small-label">' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('margin_top') . '"
                                        id="' . $this->get_field_id('margin_top') . '"
                                        value="' . esc_attr($margin_top) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_right') . '" class="small-label">' . __('Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input class="it_inputs"  type="number"
                                        name="' . $this->get_field_id('margin_right') . '"
                                        id="' . $this->get_field_name('margin_right') . '"
                                        value="' . esc_attr($margin_right) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_bottom') . '" class="small-label">' . __('Bottom', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('margin_bottom') . '"
                                        id="' . $this->get_field_id('margin_bottom') . '"
                                        value="' . esc_attr($margin_bottom) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                </div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_left') . '" class="small-label">' . __('Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_id('margin_left') . '"
                                        id="' . $this->get_field_id('margin_left') . '"
                                        value="' . esc_attr($margin_left) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>';

        // custom css class name
        $o .= '<p><div id="' . $this->get_field_id('custom_class') . '">
                                            <label style="vertical-align: top;" for="' . $this->get_field_id('custom_class') . '">' . __('Custom Class', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                            <textarea  name="' . $this->get_field_name('custom_class') . '"
                                                    id="' . $this->get_field_id('custom_class') . '"></textarea>
                                    </div>
                                    </p>';
        //

        // hover setting
        $o .= '<p><label for="hover_hr" >' . __('Hover Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="hover_hr"></p>';

        $o .= '<p><label for="' . $this->get_field_id('effect') . '" >' . __('Effect on hover', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . $this->get_field_name('effect') . '"
					        id="' . $this->get_field_id('effect') . '">
					    <option value="none" ' .
            selected($effect, 'none') . ' >None</option>
                        <option value="opacity" ' .
            selected($effect, 'opacity') . ' >Opacity</option>
					</select>
            </p>';

        $o .= '<p><div>
					<label for="' . $this->get_field_id('shadow') . '" >' . __('Box Shadow', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . $this->get_field_name('shadow') . '"
					        id="' . $this->get_field_id('shadow') . '" >
					    <option value="none" ' .
            selected($shadow, 'none') . ' >None</option>
                        <option value="outset" ' .
            selected($shadow, 'outset') . ' >Outset Shadow</option>
                        <option value="inset" ' .
            selected($shadow, 'inset') . ' >Inset Shadow</option>
					</select>
			</div>
            </p>';

        $o .= '<p><div >
					<label for="' . $this->get_field_id('shadow_color') . '">' . __('Shadow Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . $this->get_field_name('shadow_color') . '"
					        id="' . $this->get_field_id('shadow_color') . '"
					        type="text" class="wp_ad_picker_color" value="' . esc_attr($shadow_color) . '" data-default-color="' . esc_attr($shadow_color) . '">
			</div>
            </p>';
        //
        echo $o;
    }
}
class it_logo_showcase_list_widget extends WP_Widget
{

    /**
     * @var array
     */
    private $params = array(
        'category' => 'all',
        'orderby' => 'menu_order',
        'order' => 'DESC',
        'layout' => 'grid',
        'col_desktop' => 4,
        'col_tablet' => 3,
        'col_mobile' => 2,
        // style
        'margin_top' => 0,
        'margin_right' => 0,
        'margin_bottom' => 0,
        'margin_left' => 0,
        'padding_top' => 0,
        'padding_right' => 0,
        'padding_bottom' => 0,
        'padding_left' => 0,
        'background_color' => 'transparent',
        'border_style' => 'none',
        'border_width' => 1,
        'border_radius' => 0,
        'border_color' => '#eee',
        'custom_class' => '',
        // hover effect
        'effect' => 'none',
        // shadow on hover
        'shadow' => 'none',
        'shadow_color' => '#999',
        // quick view setting
        'quick_view_type' => 'tooltip',

        // customize show details for full view
        'title' => 'true',
        'contact' => 'true',
        'social' => 'true',
        'full_desc' => 'true',
        'gallery' => 'true',
        'video' => 'true',

        // Carousel Setting
        'laptop_item' => 4,
        'desktop_item' => 6,
        'tablet_item' => 3,
        'mobile_item' => 2,
        'theme' => 'owl-theme',
        'autoplay' => 'true',
        'autoplay_speed' => 3000,
        'slideSpeed' => 1000,
        'paginationSpeed' => 3000,
        'rewindSpeed' => 1000,
        'navigation' => 'true',
        'pagination' => 'true'
    );

    /**
     * Register widget with WordPress.
     */
    public function __construct()
    {
        parent::__construct(
            'it_logo_showcase_grid_widget', // Base ID
            __('Grid Logo Show Case', __IT_LOGO_SHOWCASE_TEXTDOMAIN__), // Name
            array('description' => __('Display grid Logo images on your website', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */

    public function widget($args, $instance)
    {
        $args['title'] = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
        echo $args['before_widget'];
        if (!empty($args['title'])) echo $args['before_title'] . $args['title'] . $args['after_title'];
        echo it_preview_shortcode($instance);
        echo $args['after_widget'];
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        foreach ($this->params as $param_key => $param_val) {
            $instance[$param_key] = $new_instance[$param_key];
        }
        return $instance;
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     *
     * @return html tags
     */
    public function form($instance)
    {
        $instance = wp_parse_args((array)$instance, $this->params);
        $instance['title'] = '';
        $title = !empty($instance['title']) ? $instance['title'] : __('New title', __IT_LOGO_SHOWCASE_TEXTDOMAIN__);
        $o = '';
        ?>
        <p><label for="<?php
            echo $this->get_field_id('title'); ?>"><?= __('Tilte', __IT_LOGO_SHOWCASE_TEXTDOMAIN__); ?> :</label>
            <input class="widefat" id="<?php
            echo $this->get_field_id('title'); ?>" name="<?php
            echo $this->get_field_name('title'); ?>" type="text" value="<?php
            echo esc_attr($title); ?>"/></p>
        <?php
        // query builder
        $o .= '<p><label for="query_hr" >' . __('Query Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="query_hr"></p>';
        // OrderBy

        $o .= '<p>
            <label for="' . $this->get_field_id('orderby') . '" >' . __('Order By', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
            <select id="' . $this->get_field_id('orderby') . '" name="' . $this->get_field_name('orderby') . '" >
                <option value="order" ' .
            selected($orderby, 'order') . ' >' . __('Order Field', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="title" ' .
            selected($orderby, 'title') . '>' . __('Title', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="id" ' .
            selected($orderby, 'id') . '>' . __('ID', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="date" ' .
            selected($orderby, 'date') . '>' . __('Date', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="modified" ' .
            selected($orderby, 'modified') . '>' . __('Modified', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';
        // Order
        $o .= '<p>
            <label for="' . $this->get_field_id('order') . '">' . __('Order', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
            <select id="' . $this->get_field_id('order') . '" name="' . $this->get_field_name('order') . '">
                <option value="ASC" ' .
            selected($order, 'ASC') . '>' . __('Ascending', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="DESC" ' .
            selected($order, 'DESC') . '>' . __('Descending', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';
        // category select

        $o .= '<p>
            <label for="' . $this->get_field_id('category') . '">' . __('Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
            <select id="it_custom_cat" >
                <option value="true">' . __('All Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="false">' . __('Custom Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';

        $categories = get_terms(__Custom_Post_Taxonomy__);
        if ($categories) {
            $o .= '<p id="' . $this->get_field_id('cat_box') . '">';
            foreach ($categories as $category) {
                $o .= '<input type="checkbox" name="' . $this->get_field_name('category') . '[]" value="' . $category->slug . '" >' . $category->name . '<br>';
            }
            $o .= '</p>';
        }
        // layout setting
        $o .= '<p><label for="layout_hr" >' . __('Layout Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="layout_hr"></p>';
        // fields for grid layout
        $o .= '<p>
						<label for="' . $this->get_field_id('col_desktop') . '">' . __('Desktop Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('col_desktop') . '"
							   name="' . $this->get_field_name('col_desktop') . '">
							<option vlaue="1" ' .
            selected($col_desktop, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_desktop, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_desktop, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_desktop, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_desktop, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_desktop, 12) . ' >12</option>
						</select>
					</p>';
        $o .= '<p><label for="' . $this->get_field_id('col_tablet') . '">' . __('Tablet Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('col_tablet') . '"
							   name="' . $this->get_field_name('col_tablet') . '">
							<option vlaue="1" ' .
            selected($col_tablet, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_tablet, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_tablet, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_tablet, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_tablet, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_tablet, 12) . ' >12</option>
						</select>
					</p>';
        $o .= '<p><label for="' . $this->get_field_id('col_mobile') . '">' . __('Mobile Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
						<select id="' . $this->get_field_id('col_mobile') . '"
						       name="' . $this->get_field_name('col_mobile') . '">
						    <option vlaue="1" ' .
            selected($col_mobile, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_mobile, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_mobile, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_mobile, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_mobile, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_mobile, 12) . ' >12</option>
						</select>
					</p>';
        // end grid fields

        // Style setting
        $o .= '<p><label for="border_hr" >' . __('Style Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="border_hr"></p>';
        $o .= '<p><label for="' . $this->get_field_id('border_style') . '">' . __('Border Style', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('border_style') . '"
						       name="' . $this->get_field_name('border_style') . '" >
						    <option value="none" ' .
            selected($border_style, 'none') . ' >None</option>
		                    <option value="solid" ' .
            selected($border_style, 'solid') . ' >Solid</option>
		                    <option value="dashed" ' .
            selected($border_style, 'dashed') . ' >Dashed</option>
		                    <option value="dotted" ' .
            selected($border_style, 'dotted') . ' >Dotted</option>
						</select>
					</p>';

        $o .= '<p><div>
            <label for="' . $this->get_field_id('border_width') . '" >' . __('Border Width', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number" name="' . $this->get_field_name('border_width') . '"
									    id="' . $this->get_field_id('border_width') . '"
									    value="' . esc_attr($border_width) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
									     />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div></p>';

        $o .= '<p><div >
            <label for="' . $this->get_field_id('border_radius') . '" >' . __('Border Radius', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number" name="' . $this->get_field_name('border_radius') . '"
									    id="' . $this->get_field_id('border_radius') . '"
									    value="' . esc_attr($border_radius) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
									    />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div></p>';


        $o .= '<p><div >
					<label for="' . $this->get_field_id('border_color') . '" >' . __('Border Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . $this->get_field_name('border_color') . '"
					        id="' . $this->get_field_id('border_color') . '"
					        type="text" class="wp_ad_picker_color" value="' . esc_attr($border_color) . '"
					        data-default-color="' . esc_attr($border_color) . '">
			</div>
            </p>';
        // padding
        $o .= '<p><label >' . __('Padding', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';
        $o .= '<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_top') . '" class="small-label">' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_top') . '"
                                        id="' . $this->get_field_id('padding_top') . '"
                                        value="' . esc_attr($padding_top) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_right') . '" class="small-label">' . __('Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_right') . '"
                                        id="' . $this->get_field_id('padding_right') . '"
                                        value="' . esc_attr($padding_right) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text " />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_bottom') . '" class="small-label">' . __('Bottom', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_bottom') . '"
                                        id="' . $this->get_field_id('padding_bottom') . '"
                                        value="' . esc_attr($padding_bottom) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text " />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                </div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_left') . '" class="small-label">' . __('Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_left') . '"
                                        id="' . $this->get_field_id('padding_left') . '"
                                        value="' . esc_attr($padding_left) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>';

        // margin
        $o .= '<p><label >' . __('margin', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';
        $o .= '<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_top') . '" class="small-label">' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('margin_top') . '"
                                        id="' . $this->get_field_id('margin_top') . '"
                                        value="' . esc_attr($margin_top) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_right') . '" class="small-label">' . __('Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input class="it_inputs"  type="number"
                                        name="' . $this->get_field_id('margin_right') . '"
                                        id="' . $this->get_field_name('margin_right') . '"
                                        value="' . esc_attr($margin_right) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_bottom') . '" class="small-label">' . __('Bottom', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('margin_bottom') . '"
                                        id="' . $this->get_field_id('margin_bottom') . '"
                                        value="' . esc_attr($margin_bottom) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                </div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_left') . '" class="small-label">' . __('Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_id('margin_left') . '"
                                        id="' . $this->get_field_id('margin_left') . '"
                                        value="' . esc_attr($margin_left) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>';

        // custom css class name
        $o .= '<p><div id="' . $this->get_field_id('custom_class') . '">
                                            <label style="vertical-align: top;" for="' . $this->get_field_id('custom_class') . '">' . __('Custom Class', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                            <textarea  name="' . $this->get_field_name('custom_class') . '"
                                                    id="' . $this->get_field_id('custom_class') . '"></textarea>
                                    </div>
                                    </p>';
        //

        // hover setting
        $o .= '<p><label for="hover_hr" >' . __('Hover Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="hover_hr"></p>';

        $o .= '<p><label for="' . $this->get_field_id('effect') . '" >' . __('Effect on hover', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . $this->get_field_name('effect') . '"
					        id="' . $this->get_field_id('effect') . '">
					    <option value="none" ' .
            selected($effect, 'none') . ' >None</option>
                        <option value="opacity" ' .
            selected($effect, 'opacity') . ' >Opacity</option>
					</select>
            </p>';

        $o .= '<p><div>
					<label for="' . $this->get_field_id('shadow') . '" >' . __('Box Shadow', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . $this->get_field_name('shadow') . '"
					        id="' . $this->get_field_id('shadow') . '" >
					    <option value="none" ' .
            selected($shadow, 'none') . ' >None</option>
                        <option value="outset" ' .
            selected($shadow, 'outset') . ' >Outset Shadow</option>
                        <option value="inset" ' .
            selected($shadow, 'inset') . ' >Inset Shadow</option>
					</select>
			</div>
            </p>';

        $o .= '<p><div >
					<label for="' . $this->get_field_id('shadow_color') . '">' . __('Shadow Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . $this->get_field_name('shadow_color') . '"
					        id="' . $this->get_field_id('shadow_color') . '"
					        type="text" class="wp_ad_picker_color" value="' . esc_attr($shadow_color) . '" data-default-color="' . esc_attr($shadow_color) . '">
			</div>
            </p>';
        //
        echo $o;
    }
}
class it_logo_showcase_carousel_widget extends WP_Widget
{

    /**
     * @var array
     */
    private $params = array(
        'category' => 'all',
        'orderby' => 'menu_order',
        'order' => 'DESC',
        'layout' => 'grid',
        'col_desktop' => 4,
        'col_tablet' => 3,
        'col_mobile' => 2,
        // style
        'margin_top' => 0,
        'margin_right' => 0,
        'margin_bottom' => 0,
        'margin_left' => 0,
        'padding_top' => 0,
        'padding_right' => 0,
        'padding_bottom' => 0,
        'padding_left' => 0,
        'background_color' => 'transparent',
        'border_style' => 'none',
        'border_width' => 1,
        'border_radius' => 0,
        'border_color' => '#eee',
        'custom_class' => '',
        // hover effect
        'effect' => 'none',
        // shadow on hover
        'shadow' => 'none',
        'shadow_color' => '#999',
        // quick view setting
        'quick_view_type' => 'tooltip',

        // customize show details for full view
        'title' => 'true',
        'contact' => 'true',
        'social' => 'true',
        'full_desc' => 'true',
        'gallery' => 'true',
        'video' => 'true',

        // Carousel Setting
        'laptop_item' => 4,
        'desktop_item' => 6,
        'tablet_item' => 3,
        'mobile_item' => 2,
        'theme' => 'owl-theme',
        'autoplay' => 'true',
        'autoplay_speed' => 3000,
        'slideSpeed' => 1000,
        'paginationSpeed' => 3000,
        'rewindSpeed' => 1000,
        'navigation' => 'true',
        'pagination' => 'true'
    );

    /**
     * Register widget with WordPress.
     */
    public function __construct()
    {
        parent::__construct(
            'it_logo_showcase_grid_widget', // Base ID
            __('Grid Logo Show Case', __IT_LOGO_SHOWCASE_TEXTDOMAIN__), // Name
            array('description' => __('Display grid Logo images on your website', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */

    public function widget($args, $instance)
    {
        $args['title'] = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
        echo $args['before_widget'];
        if (!empty($args['title'])) echo $args['before_title'] . $args['title'] . $args['after_title'];
        echo it_preview_shortcode($instance);
        echo $args['after_widget'];
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        foreach ($this->params as $param_key => $param_val) {
            $instance[$param_key] = $new_instance[$param_key];
        }
        return $instance;
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     *
     * @return html tags
     */
    public function form($instance)
    {
        $instance = wp_parse_args((array)$instance, $this->params);
        $instance['title'] = '';
        $title = !empty($instance['title']) ? $instance['title'] : __('New title', __IT_LOGO_SHOWCASE_TEXTDOMAIN__);
        $o = '';
        ?>
        <p><label for="<?php
            echo $this->get_field_id('title'); ?>"><?= __('Tilte', __IT_LOGO_SHOWCASE_TEXTDOMAIN__); ?> :</label>
            <input class="widefat" id="<?php
            echo $this->get_field_id('title'); ?>" name="<?php
            echo $this->get_field_name('title'); ?>" type="text" value="<?php
            echo esc_attr($title); ?>"/></p>
        <?php
        // query builder
        $o .= '<p><label for="query_hr" >' . __('Query Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="query_hr"></p>';
        // OrderBy

        $o .= '<p>
            <label for="' . $this->get_field_id('orderby') . '" >' . __('Order By', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
            <select id="' . $this->get_field_id('orderby') . '" name="' . $this->get_field_name('orderby') . '" >
                <option value="order" ' .
            selected($orderby, 'order') . ' >' . __('Order Field', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="title" ' .
            selected($orderby, 'title') . '>' . __('Title', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="id" ' .
            selected($orderby, 'id') . '>' . __('ID', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="date" ' .
            selected($orderby, 'date') . '>' . __('Date', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="modified" ' .
            selected($orderby, 'modified') . '>' . __('Modified', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';
        // Order
        $o .= '<p>
            <label for="' . $this->get_field_id('order') . '">' . __('Order', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
            <select id="' . $this->get_field_id('order') . '" name="' . $this->get_field_name('order') . '">
                <option value="ASC" ' .
            selected($order, 'ASC') . '>' . __('Ascending', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="DESC" ' .
            selected($order, 'DESC') . '>' . __('Descending', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';
        // category select

        $o .= '<p>
            <label for="' . $this->get_field_id('category') . '">' . __('Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
            <select id="it_custom_cat" >
                <option value="true">' . __('All Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="false">' . __('Custom Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';

        $categories = get_terms(__Custom_Post_Taxonomy__);
        if ($categories) {
            $o .= '<p id="' . $this->get_field_id('cat_box') . '">';
            foreach ($categories as $category) {
                $o .= '<input type="checkbox" name="' . $this->get_field_name('category') . '[]" value="' . $category->slug . '" >' . $category->name . '<br>';
            }
            $o .= '</p>';
        }
        // layout setting
        $o .= '<p><label for="layout_hr" >' . __('Layout Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="layout_hr"></p>';
        // fields for grid layout
        $o .= '<p>
						<label for="' . $this->get_field_id('col_desktop') . '">' . __('Desktop Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('col_desktop') . '"
							   name="' . $this->get_field_name('col_desktop') . '">
							<option vlaue="1" ' .
            selected($col_desktop, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_desktop, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_desktop, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_desktop, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_desktop, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_desktop, 12) . ' >12</option>
						</select>
					</p>';
        $o .= '<p><label for="' . $this->get_field_id('col_tablet') . '">' . __('Tablet Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('col_tablet') . '"
							   name="' . $this->get_field_name('col_tablet') . '">
							<option vlaue="1" ' .
            selected($col_tablet, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_tablet, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_tablet, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_tablet, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_tablet, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_tablet, 12) . ' >12</option>
						</select>
					</p>';
        $o .= '<p><label for="' . $this->get_field_id('col_mobile') . '">' . __('Mobile Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
						<select id="' . $this->get_field_id('col_mobile') . '"
						       name="' . $this->get_field_name('col_mobile') . '">
						    <option vlaue="1" ' .
            selected($col_mobile, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_mobile, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_mobile, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_mobile, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_mobile, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_mobile, 12) . ' >12</option>
						</select>
					</p>';
        // end grid fields

        // Style setting
        $o .= '<p><label for="border_hr" >' . __('Style Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="border_hr"></p>';
        $o .= '<p><label for="' . $this->get_field_id('border_style') . '">' . __('Border Style', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('border_style') . '"
						       name="' . $this->get_field_name('border_style') . '" >
						    <option value="none" ' .
            selected($border_style, 'none') . ' >None</option>
		                    <option value="solid" ' .
            selected($border_style, 'solid') . ' >Solid</option>
		                    <option value="dashed" ' .
            selected($border_style, 'dashed') . ' >Dashed</option>
		                    <option value="dotted" ' .
            selected($border_style, 'dotted') . ' >Dotted</option>
						</select>
					</p>';

        $o .= '<p><div>
            <label for="' . $this->get_field_id('border_width') . '" >' . __('Border Width', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number" name="' . $this->get_field_name('border_width') . '"
									    id="' . $this->get_field_id('border_width') . '"
									    value="' . esc_attr($border_width) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
									     />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div></p>';

        $o .= '<p><div >
            <label for="' . $this->get_field_id('border_radius') . '" >' . __('Border Radius', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number" name="' . $this->get_field_name('border_radius') . '"
									    id="' . $this->get_field_id('border_radius') . '"
									    value="' . esc_attr($border_radius) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
									    />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div></p>';


        $o .= '<p><div >
					<label for="' . $this->get_field_id('border_color') . '" >' . __('Border Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . $this->get_field_name('border_color') . '"
					        id="' . $this->get_field_id('border_color') . '"
					        type="text" class="wp_ad_picker_color" value="' . esc_attr($border_color) . '"
					        data-default-color="' . esc_attr($border_color) . '">
			</div>
            </p>';
        // padding
        $o .= '<p><label >' . __('Padding', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';
        $o .= '<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_top') . '" class="small-label">' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_top') . '"
                                        id="' . $this->get_field_id('padding_top') . '"
                                        value="' . esc_attr($padding_top) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_right') . '" class="small-label">' . __('Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_right') . '"
                                        id="' . $this->get_field_id('padding_right') . '"
                                        value="' . esc_attr($padding_right) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text " />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_bottom') . '" class="small-label">' . __('Bottom', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_bottom') . '"
                                        id="' . $this->get_field_id('padding_bottom') . '"
                                        value="' . esc_attr($padding_bottom) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text " />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                </div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_left') . '" class="small-label">' . __('Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_left') . '"
                                        id="' . $this->get_field_id('padding_left') . '"
                                        value="' . esc_attr($padding_left) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>';

        // margin
        $o .= '<p><label >' . __('margin', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';
        $o .= '<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_top') . '" class="small-label">' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('margin_top') . '"
                                        id="' . $this->get_field_id('margin_top') . '"
                                        value="' . esc_attr($margin_top) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_right') . '" class="small-label">' . __('Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input class="it_inputs"  type="number"
                                        name="' . $this->get_field_id('margin_right') . '"
                                        id="' . $this->get_field_name('margin_right') . '"
                                        value="' . esc_attr($margin_right) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_bottom') . '" class="small-label">' . __('Bottom', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('margin_bottom') . '"
                                        id="' . $this->get_field_id('margin_bottom') . '"
                                        value="' . esc_attr($margin_bottom) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                </div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_left') . '" class="small-label">' . __('Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_id('margin_left') . '"
                                        id="' . $this->get_field_id('margin_left') . '"
                                        value="' . esc_attr($margin_left) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>';

        // custom css class name
        $o .= '<p><div id="' . $this->get_field_id('custom_class') . '">
                                            <label style="vertical-align: top;" for="' . $this->get_field_id('custom_class') . '">' . __('Custom Class', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                            <textarea  name="' . $this->get_field_name('custom_class') . '"
                                                    id="' . $this->get_field_id('custom_class') . '"></textarea>
                                    </div>
                                    </p>';
        //

        // hover setting
        $o .= '<p><label for="hover_hr" >' . __('Hover Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="hover_hr"></p>';

        $o .= '<p><label for="' . $this->get_field_id('effect') . '" >' . __('Effect on hover', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . $this->get_field_name('effect') . '"
					        id="' . $this->get_field_id('effect') . '">
					    <option value="none" ' .
            selected($effect, 'none') . ' >None</option>
                        <option value="opacity" ' .
            selected($effect, 'opacity') . ' >Opacity</option>
					</select>
            </p>';

        $o .= '<p><div>
					<label for="' . $this->get_field_id('shadow') . '" >' . __('Box Shadow', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . $this->get_field_name('shadow') . '"
					        id="' . $this->get_field_id('shadow') . '" >
					    <option value="none" ' .
            selected($shadow, 'none') . ' >None</option>
                        <option value="outset" ' .
            selected($shadow, 'outset') . ' >Outset Shadow</option>
                        <option value="inset" ' .
            selected($shadow, 'inset') . ' >Inset Shadow</option>
					</select>
			</div>
            </p>';

        $o .= '<p><div >
					<label for="' . $this->get_field_id('shadow_color') . '">' . __('Shadow Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . $this->get_field_name('shadow_color') . '"
					        id="' . $this->get_field_id('shadow_color') . '"
					        type="text" class="wp_ad_picker_color" value="' . esc_attr($shadow_color) . '" data-default-color="' . esc_attr($shadow_color) . '">
			</div>
            </p>';
        //
        echo $o;
    }
}
class it_logo_showcase_isotope_widget extends WP_Widget
{

    /**
     * @var array
     */
    private $params = array(
        'category' => 'all',
        'orderby' => 'menu_order',
        'order' => 'DESC',
        'layout' => 'grid',
        'col_desktop' => 4,
        'col_tablet' => 3,
        'col_mobile' => 2,
        // style
        'margin_top' => 0,
        'margin_right' => 0,
        'margin_bottom' => 0,
        'margin_left' => 0,
        'padding_top' => 0,
        'padding_right' => 0,
        'padding_bottom' => 0,
        'padding_left' => 0,
        'background_color' => 'transparent',
        'border_style' => 'none',
        'border_width' => 1,
        'border_radius' => 0,
        'border_color' => '#eee',
        'custom_class' => '',
        // hover effect
        'effect' => 'none',
        // shadow on hover
        'shadow' => 'none',
        'shadow_color' => '#999',
        // quick view setting
        'quick_view_type' => 'tooltip',

        // customize show details for full view
        'title' => 'true',
        'contact' => 'true',
        'social' => 'true',
        'full_desc' => 'true',
        'gallery' => 'true',
        'video' => 'true',

        // Carousel Setting
        'laptop_item' => 4,
        'desktop_item' => 6,
        'tablet_item' => 3,
        'mobile_item' => 2,
        'theme' => 'owl-theme',
        'autoplay' => 'true',
        'autoplay_speed' => 3000,
        'slideSpeed' => 1000,
        'paginationSpeed' => 3000,
        'rewindSpeed' => 1000,
        'navigation' => 'true',
        'pagination' => 'true'
    );

    /**
     * Register widget with WordPress.
     */
    public function __construct()
    {
        parent::__construct(
            'it_logo_showcase_grid_widget', // Base ID
            __('Grid Logo Show Case', __IT_LOGO_SHOWCASE_TEXTDOMAIN__), // Name
            array('description' => __('Display grid Logo images on your website', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */

    public function widget($args, $instance)
    {
        $args['title'] = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
        echo $args['before_widget'];
        if (!empty($args['title'])) echo $args['before_title'] . $args['title'] . $args['after_title'];
        echo it_preview_shortcode($instance);
        echo $args['after_widget'];
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        foreach ($this->params as $param_key => $param_val) {
            $instance[$param_key] = $new_instance[$param_key];
        }
        return $instance;
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     *
     * @return html tags
     */
    public function form($instance)
    {
        $instance = wp_parse_args((array)$instance, $this->params);
        $instance['title'] = '';
        $title = !empty($instance['title']) ? $instance['title'] : __('New title', __IT_LOGO_SHOWCASE_TEXTDOMAIN__);
        $o = '';
        ?>
        <p><label for="<?php
            echo $this->get_field_id('title'); ?>"><?= __('Tilte', __IT_LOGO_SHOWCASE_TEXTDOMAIN__); ?> :</label>
            <input class="widefat" id="<?php
            echo $this->get_field_id('title'); ?>" name="<?php
            echo $this->get_field_name('title'); ?>" type="text" value="<?php
            echo esc_attr($title); ?>"/></p>
        <?php
        // query builder
        $o .= '<p><label for="query_hr" >' . __('Query Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="query_hr"></p>';
        // OrderBy

        $o .= '<p>
            <label for="' . $this->get_field_id('orderby') . '" >' . __('Order By', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
            <select id="' . $this->get_field_id('orderby') . '" name="' . $this->get_field_name('orderby') . '" >
                <option value="order" ' .
            selected($orderby, 'order') . ' >' . __('Order Field', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="title" ' .
            selected($orderby, 'title') . '>' . __('Title', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="id" ' .
            selected($orderby, 'id') . '>' . __('ID', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="date" ' .
            selected($orderby, 'date') . '>' . __('Date', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="modified" ' .
            selected($orderby, 'modified') . '>' . __('Modified', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';
        // Order
        $o .= '<p>
            <label for="' . $this->get_field_id('order') . '">' . __('Order', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
            <select id="' . $this->get_field_id('order') . '" name="' . $this->get_field_name('order') . '">
                <option value="ASC" ' .
            selected($order, 'ASC') . '>' . __('Ascending', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="DESC" ' .
            selected($order, 'DESC') . '>' . __('Descending', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';
        // category select

        $o .= '<p>
            <label for="' . $this->get_field_id('category') . '">' . __('Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
            <select id="it_custom_cat" >
                <option value="true">' . __('All Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
                <option value="false">' . __('Custom Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</option>
            </select>
        </p>';

        $categories = get_terms(__Custom_Post_Taxonomy__);
        if ($categories) {
            $o .= '<p id="' . $this->get_field_id('cat_box') . '">';
            foreach ($categories as $category) {
                $o .= '<input type="checkbox" name="' . $this->get_field_name('category') . '[]" value="' . $category->slug . '" >' . $category->name . '<br>';
            }
            $o .= '</p>';
        }
        // layout setting
        $o .= '<p><label for="layout_hr" >' . __('Layout Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="layout_hr"></p>';
        // fields for grid layout
        $o .= '<p>
						<label for="' . $this->get_field_id('col_desktop') . '">' . __('Desktop Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('col_desktop') . '"
							   name="' . $this->get_field_name('col_desktop') . '">
							<option vlaue="1" ' .
            selected($col_desktop, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_desktop, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_desktop, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_desktop, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_desktop, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_desktop, 12) . ' >12</option>
						</select>
					</p>';
        $o .= '<p><label for="' . $this->get_field_id('col_tablet') . '">' . __('Tablet Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('col_tablet') . '"
							   name="' . $this->get_field_name('col_tablet') . '">
							<option vlaue="1" ' .
            selected($col_tablet, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_tablet, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_tablet, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_tablet, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_tablet, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_tablet, 12) . ' >12</option>
						</select>
					</p>';
        $o .= '<p><label for="' . $this->get_field_id('col_mobile') . '">' . __('Mobile Columns', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
						<select id="' . $this->get_field_id('col_mobile') . '"
						       name="' . $this->get_field_name('col_mobile') . '">
						    <option vlaue="1" ' .
            selected($col_mobile, 1) . ' >1</option>
							<option vlaue="2" ' .
            selected($col_mobile, 2) . ' >2</option>
							<option vlaue="3" ' .
            selected($col_mobile, 3) . ' >3</option>
							<option vlaue="4" ' .
            selected($col_mobile, 4) . ' >4</option>
							<option vlaue="6" ' .
            selected($col_mobile, 6) . ' >6</option>
							<option vlaue="12" ' .
            selected($col_mobile, 12) . ' >12</option>
						</select>
					</p>';
        // end grid fields

        // Style setting
        $o .= '<p><label for="border_hr" >' . __('Style Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="border_hr"></p>';
        $o .= '<p><label for="' . $this->get_field_id('border_style') . '">' . __('Border Style', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label>
						<select id="' . $this->get_field_id('border_style') . '"
						       name="' . $this->get_field_name('border_style') . '" >
						    <option value="none" ' .
            selected($border_style, 'none') . ' >None</option>
		                    <option value="solid" ' .
            selected($border_style, 'solid') . ' >Solid</option>
		                    <option value="dashed" ' .
            selected($border_style, 'dashed') . ' >Dashed</option>
		                    <option value="dotted" ' .
            selected($border_style, 'dotted') . ' >Dotted</option>
						</select>
					</p>';

        $o .= '<p><div>
            <label for="' . $this->get_field_id('border_width') . '" >' . __('Border Width', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number" name="' . $this->get_field_name('border_width') . '"
									    id="' . $this->get_field_id('border_width') . '"
									    value="' . esc_attr($border_width) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
									     />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div></p>';

        $o .= '<p><div >
            <label for="' . $this->get_field_id('border_radius') . '" >' . __('Border Radius', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number" name="' . $this->get_field_name('border_radius') . '"
									    id="' . $this->get_field_id('border_radius') . '"
									    value="' . esc_attr($border_radius) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
									    />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
			</div></p>';


        $o .= '<p><div >
					<label for="' . $this->get_field_id('border_color') . '" >' . __('Border Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . $this->get_field_name('border_color') . '"
					        id="' . $this->get_field_id('border_color') . '"
					        type="text" class="wp_ad_picker_color" value="' . esc_attr($border_color) . '"
					        data-default-color="' . esc_attr($border_color) . '">
			</div>
            </p>';
        // padding
        $o .= '<p><label >' . __('Padding', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';
        $o .= '<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_top') . '" class="small-label">' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_top') . '"
                                        id="' . $this->get_field_id('padding_top') . '"
                                        value="' . esc_attr($padding_top) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_right') . '" class="small-label">' . __('Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_right') . '"
                                        id="' . $this->get_field_id('padding_right') . '"
                                        value="' . esc_attr($padding_right) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text " />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_bottom') . '" class="small-label">' . __('Bottom', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_bottom') . '"
                                        id="' . $this->get_field_id('padding_bottom') . '"
                                        value="' . esc_attr($padding_bottom) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text " />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                </div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('padding_left') . '" class="small-label">' . __('Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('padding_left') . '"
                                        id="' . $this->get_field_id('padding_left') . '"
                                        value="' . esc_attr($padding_left) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>';

        // margin
        $o .= '<p><label >' . __('margin', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' : </label></p>';
        $o .= '<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_top') . '" class="small-label">' . __('Top', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('margin_top') . '"
                                        id="' . $this->get_field_id('margin_top') . '"
                                        value="' . esc_attr($margin_top) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_right') . '" class="small-label">' . __('Right', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input class="it_inputs"  type="number"
                                        name="' . $this->get_field_id('margin_right') . '"
                                        id="' . $this->get_field_name('margin_right') . '"
                                        value="' . esc_attr($margin_right) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_bottom') . '" class="small-label">' . __('Bottom', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_name('margin_bottom') . '"
                                        id="' . $this->get_field_id('margin_bottom') . '"
                                        value="' . esc_attr($margin_bottom) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
                                </div>
								<div class="small-lbl-cnt">
									<label for="' . $this->get_field_id('margin_left') . '" class="small-label">' . __('Left', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
									<input type="number"
                                        name="' . $this->get_field_id('margin_left') . '"
                                        id="' . $this->get_field_id('margin_left') . '"
                                        value="' . esc_attr($margin_left) . '" size="1" style="width:50px" min="0" pattern="[-+]?[0-9]*[.,]?[0-9]+"
                                        class="input-text qty text" />
									<span class="input-unit">' . __('px', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</span>
								</div>';

        // custom css class name
        $o .= '<p><div id="' . $this->get_field_id('custom_class') . '">
                                            <label style="vertical-align: top;" for="' . $this->get_field_id('custom_class') . '">' . __('Custom Class', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
                                            <textarea  name="' . $this->get_field_name('custom_class') . '"
                                                    id="' . $this->get_field_id('custom_class') . '"></textarea>
                                    </div>
                                    </p>';
        //

        // hover setting
        $o .= '<p><label for="hover_hr" >' . __('Hover Setting', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label><hr id="hover_hr"></p>';

        $o .= '<p><label for="' . $this->get_field_id('effect') . '" >' . __('Effect on hover', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . $this->get_field_name('effect') . '"
					        id="' . $this->get_field_id('effect') . '">
					    <option value="none" ' .
            selected($effect, 'none') . ' >None</option>
                        <option value="opacity" ' .
            selected($effect, 'opacity') . ' >Opacity</option>
					</select>
            </p>';

        $o .= '<p><div>
					<label for="' . $this->get_field_id('shadow') . '" >' . __('Box Shadow', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . ' :</label>
					<select  name="' . $this->get_field_name('shadow') . '"
					        id="' . $this->get_field_id('shadow') . '" >
					    <option value="none" ' .
            selected($shadow, 'none') . ' >None</option>
                        <option value="outset" ' .
            selected($shadow, 'outset') . ' >Outset Shadow</option>
                        <option value="inset" ' .
            selected($shadow, 'inset') . ' >Inset Shadow</option>
					</select>
			</div>
            </p>';

        $o .= '<p><div >
					<label for="' . $this->get_field_id('shadow_color') . '">' . __('Shadow Color', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</label>
					<input  name="' . $this->get_field_name('shadow_color') . '"
					        id="' . $this->get_field_id('shadow_color') . '"
					        type="text" class="wp_ad_picker_color" value="' . esc_attr($shadow_color) . '" data-default-color="' . esc_attr($shadow_color) . '">
			</div>
            </p>';
        //
        echo $o;
    }
}
