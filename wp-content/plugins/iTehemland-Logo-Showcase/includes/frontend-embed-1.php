<?php
	//CSS
		//Enqueue You Css Here
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'custom-css', __IT_LOGO_SHOWCASE_CSS_URL__ . 'custom_css.css', array() , null);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'public-css', __IT_LOGO_SHOWCASE_CSS_URL__. 'public.css', array() , null);

	//JS
	/////JS ENQUEUE////////////
    wp_enqueue_script('jquery');


/////tooltip//////
wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'tooltip/src/javascripts/jquery.tipsy.js');
wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'tooltip/src/stylesheets/tipsy.css');
    //FONTAWESOME STYLE //font-awesome-css
    wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'font-awesome-ccc', __IT_LOGO_SHOWCASE_CSS_URL__ . 'font-awesome.css', true);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'font-awesome-ccc');
    ///////// grid responsive ///////////
    wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'responsive-grid', __IT_LOGO_SHOWCASE_CSS_URL__ . 'responsive-grid.css', true);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'responsive-grid');


    /////Perspective CAROUSEL//////
    //wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'perspective', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'jQuery-Waterwheel-Carousel/js/jquery.waterwheelCarousel.min.js');
    //wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'perspective-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'jQuery-Waterwheel-Carousel/style.css');

	//////////////////CUSTOM JS//////////////////////////
    wp_register_script( __IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'ajaxHandle', __IT_LOGO_SHOWCASE_JS_URL__.'front-end/custom-js.js', array('jquery'), false, true );
    wp_enqueue_script( __IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'ajaxHandle' );
    wp_localize_script( __IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'ajaxHandle', 'ajax_object',
        array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce( 'it_logoshowcase_nonce' ) ) );

    /////COLOR PICKKER//////
    wp_enqueue_style('wp-color-picker');

    /////JS ENQUEUE////////////
    wp_enqueue_script('jquery');
    wp_enqueue_script('wp-color-picker');
?>