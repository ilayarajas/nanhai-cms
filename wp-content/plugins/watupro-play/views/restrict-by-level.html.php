<p><?php _e('Access to this quiz will be granted only to users with the following user levels:', 'watuproplay');?> 
	<?php foreach($levels as $level):?>
		<input type="checkbox" name="play_levels[]" value="<?php echo $level->id?>" <?php if(!empty($play_levels) and in_array($level->id, $play_levels)) echo 'checked'?>> <?php echo stripslashes($level->name);?> &nbsp;
	<?php endforeach;?>
	<br /> <?php _e("If you don't select any levels, the quiz will be accessible to users with any level", 'watuproplay');?>
</p>