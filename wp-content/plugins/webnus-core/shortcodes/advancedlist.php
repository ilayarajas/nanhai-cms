<?php 
function webnus_advancedlist( $atts ) {
	extract(shortcode_atts(array(
		'numlist'	=> '',
		'textlist'	=> '',
		'bgcolor'	=> '',
	), $atts ));

	$numlist 	=  '<span style="background: '. $bgcolor .';" >'. $numlist .'</span>' ? '<span style="background: '. $bgcolor .' ;" >'. $numlist .'</span>' : '' ;
	$textlist 	=  '<p>'. $textlist .'</p>' ? '<p>'. $textlist .'</p>' : '' ;
	// do shortcode actions here
	$out ='<div class="advancedlist"> '. $numlist . $textlist .' </div>';

	return $out;
}
add_shortcode( 'advancedlist','webnus_advancedlist' );