<?php global $plugin_page; ?>
<div class="admin-wrapper">
    <div class="admin-section">
        <div class="admin-section-header">
            <h2><?php _e("Team Members List", "team-awesome"); ?></h2>
            <p><?php _e("Create / Edit existing Member", "team-awesome"); ?></p>
        </div>
        <div class="admin-section-body fullwidth">
            <button onclick="window.location.href = '?page=<?php echo $plugin_page; ?>&view=addedit';"><?php _e("Add New Member", "team-awesome"); ?></button>
            <form method="POST">
                <button class="members-reorder"><?php _e("Update Members Order", "team-awesome"); ?></button>
            </form>
            <ul class="list no-touch">
                <?php $members = TEAM_AWESOME_ADMIN::get_members(); ?>
              
                <?php foreach ($members as $member): ?>
                    <li id="member-<?php echo $member->id; ?>">
                        <p><?php echo $member->title; ?><span>(ID#<?php echo $member->id; ?> <?php _e("Order", "team-awesome"); ?>)</span></span></p>
                        <button onclick="window.location.href = '?page=<?php echo $plugin_page; ?>&view=list&task=delete&id=<?php echo $member->id; ?>'"><?php _e("Delete", "team-awesome"); ?></button>
                        <button onclick="window.location.href = '?page=<?php echo $plugin_page; ?>&view=addedit&id=<?php echo $member->id; ?>';"><?php _e("Edit", "team-awesome"); ?></button>
                        <input autocomplete="off" type="text" name="order" value="<?php echo $member->ordering; ?>" />
                    </li>
                <?php endforeach; ?>
            </ul>

            <ul class="pagination">
                <?php
                TEAM_AWESOME_ADMIN::members_pagination();
                ?>
            </ul>

        </div>
    </div>
</div>