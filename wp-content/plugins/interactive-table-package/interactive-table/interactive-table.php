<?php
/*
Plugin Name: Interactive Table
Plugin URI: http://plugins.rmweblab.com/interactive-table/
Description: A dynamic, clean & responsive table for your contents.
Author: RM Web Lab
Version: 1.0
Author URI: http://rmweblab.com
*****/ 

// Define contants
define('INTERACTIVE_TABLE_ROOT', dirname(__FILE__));
define('INTERACTIVE_TABLE_URL', plugins_url( 'interactive-table/' ));	

include_once(INTERACTIVE_TABLE_ROOT . '/include/it-functions.php');
include_once(INTERACTIVE_TABLE_ROOT . '/include/it-shortcodess.php');
include_once(INTERACTIVE_TABLE_ROOT . '/include/it-metaboxes-config.php');


class Interactive_Table {

    /* Constructor for the class */
    function __construct() {
		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ) );
		add_action( 'plugins_loaded', array( $this, 'init' ), 0 );
        add_action('init', array(&$this, 'register_it_custom_post_type'), 10);

		/* Add admin menu */
		add_action('admin_menu', array(&$this, 'it_settings_page'));

		add_action('wp_enqueue_scripts', array(&$this, 'it_print_scripts'), 10);
		add_action( 'wp_head', array(&$this, 'it_dynamic_styles') );
		/*interactive table post details page*/
		add_filter('single_template', array(&$this, 'get_interactive_table_post_type_template'));

    }
	
	/**
	 * Init localisations and files
	 */
	public function init() {

		// Localisation
		load_plugin_textdomain( 'IT', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}
	
	
	/**
	 * Add relevant links to plugins page
	 * @param  array $links
	 * @return array
	 */
	public function plugin_action_links( $links ) {
		$plugin_links = array(
			'<a href="' . admin_url( 'edit.php?post_type=interactive_table&page=it-settings' ) . '">' . __( 'Settings', 'IT' ) . '</a>',
			'<a href="http://rmweblab.com/support">' . __( 'Support', 'IT' ) . '</a>',
			'<a href="http://rmweblab.com/documentation/plugins/interactive-table/">' . __( 'Docs', 'ID' ) . '</a>',
		);
		return array_merge( $plugin_links, $links );
	}
	

    /**
     * Plugins settings page
     */
    public function it_settings_page() {
		add_submenu_page( 'edit.php?post_type=interactive_table', 'Settings', 'Settings', 'manage_options', 'it-settings', array(&$this, 'it_settings_plug_page'));
	}
	
    /**
     * Plugins settings page
     */
    public function it_settings_plug_page() {
		require_once( INTERACTIVE_TABLE_ROOT . '/include/it-settings.php');
	}			


    /**
     * Register Interactive Table CPT
     *
     */
	public function register_it_custom_post_type() {
        $include_path = INTERACTIVE_TABLE_ROOT . '/include/';		
		include_once($include_path . 'it-custom-post-type.php');
	}
	
    /**
     * InteractiveTable ajax script load.
     */	
	public function it_print_scripts() {
		wp_enqueue_script('jquery');
		
		$ajaxurl = admin_url('admin-ajax.php');
		$ajax_nonce = wp_create_nonce('InteractiveTable');
		$display_mode = get_option('it_display_mode');
		$highlight_mode = get_option('it_highlight_mode');
		$cell_highlight_color = get_option('cell_highlight_color');
		$heading_position = get_option('it_block_heading_position');
		$cell_hover = get_option('it_cell_hover');
		$title_hover = get_option('it_title_hover');
		$args = array(
			'post_type' => 'interactive_table',
			'posts_per_page' => -1,
			'orderby'   => 'menu_order',
			'order'     => 'ASC',
		);
		
		$query = new WP_Query( $args );
		$posts_per_row = 1;
		if($query->have_posts()) {
			$total_posts = $query->post_count;
			$terms = get_terms( 'it_cell_category' );
			$posts_per_row = ceil($total_posts/count($terms));
		}
		wp_localize_script( 'jquery', 'ajaxObj', array( 'ajaxurl' => $ajaxurl, 'ajax_nonce' => $ajax_nonce, 'display_mode' => $display_mode, 'heading_position' => $heading_position, 'v_posts_per_row' => $posts_per_row, 'highlight_mode' => $highlight_mode, 'cell_hover' => $cell_hover, 'title_hover' => $title_hover ) );

		$categories = get_terms( 'it_cell_category', array('hide_empty' => 0) );
		$term_colors = array();
	    foreach ( $categories as $category ) {
	        $t_id = $category->term_id;
	        $term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check
	        if (isset($term_meta['cell_cat_color']) && ($term_meta['cell_cat_color'] != '')) {
	        	$term_color = $term_meta['cell_cat_color'];
	        } else {
	        	$term_color = $cell_highlight_color;
	        }
	        $term_colors[$category->slug] = $term_color;
	    }		
		wp_localize_script( 'jquery', 'termObj', $term_colors );
		
		wp_enqueue_style( 'table-style', plugins_url('/css/itv-style.css', __FILE__ ) );
		if( get_option('it_load_default_css') != 'No' ) {
			wp_enqueue_style( 'default-table-style', plugins_url('/css/itv-default.css', __FILE__ ) );
		}
		wp_enqueue_script( 'table-script', plugins_url('/js/it-script.js', __FILE__ ) );
	}
	
    /**
     * InteractiveTable load dynamic styles.
     */	
	public function it_dynamic_styles() {
	?>
		<style type="text/css">
			
		</style>
    <?php
	
	}

    /**
     * InteractiveTable load single post template.
     */	
	public function get_interactive_table_post_type_template($single_template) {
		 global $post;
	
		 if ($post->post_type == 'interactive_table') {
			$single_template_from_theme = get_stylesheet_directory() . '/single-interactive_table.php';
			if (file_exists($single_template_from_theme)) {
				$single_template = $single_template_from_theme;
			}else{		 
			  	$single_template = dirname( __FILE__ ) . '/templates/single-interactive_table.php';
			  }
		 }
		 return $single_template;
	}	
}


function color_picker_assets($hook_suffix) {
	// $hook_suffix to apply a check for admin page.
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'field-color-picker', plugins_url('/js/Field_Color.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'color_picker_assets' );


global $Interactive_Table;
$Interactive_Table = new Interactive_Table();

/**
 * Add default settings when plugin activate
 * @param  null
 * @return null
 */
function interactive_table_default_data(){
	update_option( 'it_block_heading_position', 'Top' );
	update_option( 'it_cell_title_color', '#000000' );
	update_option( 'it_cell_hover_title_color', '#ffffff' );
	update_option( 'it_cell_font_color', '#000000' );	
	update_option( 'it_cell_hover_font_color', '#ffffff' );	
	update_option( 'it_read_more_font_color', '#ffffff' );
	update_option( 'it_display_mode', 'Highlight' );
	update_option( 'it_highlight_mode', 'On Hover' );
	update_option( 'cell_highlight_color', '#9bbb58' );	
	update_option( 'it_single_cell_slug_url', 'box-cell' );
	update_option( 'it_cell_hover', 'enable' );
	update_option( 'it_title_hover', 'enable' );
	update_option( 'it_read_more', 'Yes' );
	update_option( 'readmore_label', 'Read More' );	
	update_option( 'it_load_default_css', 'Yes' );
	update_option( 'it_vertical_posts_per_row', '2' );
	update_option( 'fixed_highlight', '' );
}
register_activation_hook( __FILE__, 'interactive_table_default_data' );