<?php
 function michigan_webnus_latestnews( $attributes, $content = null ) {
extract(shortcode_atts(	array(
	'type'=>'1',
	'category'=>'',
	'author'=>'',
	'page'=>'',
	'scount'=>'3',
), $attributes));
	ob_start();
	$col = ($scount<5)? 12/$scount:3;
	$row = 12/$col;
	$rcount= 1 ;
echo '<div class="latestnews'.$type.'">';
	$paged = ( is_front_page() ) ? 'page' : 'paged' ;
	$pages = ($page)?'&paged='.get_query_var($paged):'&paged=1';
	$query = new WP_Query('posts_per_page='.$scount.'&category_name='.$category.'&author_name='.$author.$pages);
	if ( $type == 3) { echo '<div class="c-main-title"><i class="fa-newspaper-o"></i> '.esc_html__('News','michigan').'</div><div id="w-h-carusel" class="w-crsl">'; }
while ($query -> have_posts()) : $query -> the_post();
	$my_post = get_post( get_the_ID());
	$author_id = $my_post->post_author;		
	$image = get_the_image( array( 'meta_key' => array( 'Thumbnail', 'Thumbnail' ), 'size' => 'michigan_webnus_cover_img' ,'echo'=>false) );
	if($type==1){
		echo ($rcount == 1)?'<div class="row">':'';
		echo '<div class="col-md-'.$col.' col-sm-'.$col.'"><article class="ln-item"><figure class="ln-image">'.$image.'</figure>
		<p class="ln-date">'.get_the_date('m M Y').'</p>
		<div class="ln-content">
			<h3><a class="hcolorf ln-title" href="'.get_the_permalink().'">'.get_the_title().'</a></h3>
			<p class="latest-excerpt">'.michigan_webnus_excerpt(24).'</p>
		</div></article></div>';
		if($rcount == $row){
			echo '</div>';
			$rcount = 0;
		}
		$rcount++;
	}elseif($type==2){
		echo '<article class="ln-item">
		<div class="ln-date"><div class="ln-day">'.get_the_date('d').'</div><div class="ln-month colorb">'.get_the_date('M').'</div></div>
		<div class="ln-content"><h5 class="ln-cat">';
		the_category(', ');
		echo '</h5><h3 class="ln-title"><a class="hcolorf" href="'.get_the_permalink().'">'.get_the_title().'</a></h3></div>
		</article>';
	}elseif($type==3){
		echo '<article><h3 class="c-h-title"><a class="hcolorf" href="'.get_the_permalink().'">'.get_the_title().'</a></h3>
		<div class="c-metadata">'.esc_html__('by','michigan').' <a href="'.get_author_posts_url( $author_id ).'">' .get_the_author_meta( 'display_name',$author_id ).'</a> <span>|</span> '.get_the_date().' </div>
		<div class="c-read-more"><a class="colorb" href="'.get_the_permalink().'">'.esc_html__('READ MORE','michigan').'</a></div>
		</article>';
	}
endwhile;
	if ($type==3){ echo '</div>'; }
echo((($type=='1'))&&($rcount !=1))?'</div>':'';
echo '</div>';
if($page){ ?>
	<section class="container aligncenter"><?php if(function_exists('wp_pagenavi')) {wp_pagenavi( array( 'query' => $query ) );} ?><hr class="vertical-space2"></section>  
<?php }
	$out = ob_get_contents();
	ob_end_clean();	
	wp_reset_postdata();
	return $out;
 }
 add_shortcode('latestnews', 'michigan_webnus_latestnews');
?>