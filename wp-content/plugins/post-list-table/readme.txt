=== Wordpress Post List Table Plugin ===
Contributors: gz@
Website link: http://gza.dev.ge/post-list-grid-table
Tags: wordpress post list table, filtering, searching, sorting, categories, tags, images
Requires at least: 3.0
Tested up to: 4.2.2
Stable tag: 1.1

== Description ==

The Wordpress Post List Table gives You easiest, efficient and user-friendly way to present all your work/posts on one page via flexible table.


== Features ==

<strong>Wordpress Post List Table features:</strong>
		<ul>
			<li>FILTER posts by Categories & Tags</li>
			<li>SORT posts by Title, Date, etc.</li>
			<li>CATEGORIES to show</li>
			<li>SEARCH and FILTERING on-the-fly for all columns</li>
			<li>PAGING with custom number of items per page</li>
			<li>Custom NAMES for columns</li>
			<li>CHOOSE columns to show (frontend also)</li>
			<li>REORDER columns to show (frontend)</li>
			<li>DEFAULT sorting column and direction</li>
			<li>Cosmic (superfast) SPEED</li>
			<li>RESPONSIVE layout (tablets & phones)</li>
			<li>FLEXITHEMED - inherites CSS</li>
			<li>COMPACT, slender, user-friendly</li>
			<li>UNLIMITED number of posts</li>
			<li>LIGHTWEIGHT code</li>
			<li>plenty of NEW options planned ...</li>
		</ul>


== Installation ==
1. Upload `post-list-table.zip` onto your local computer.
2. Go to your WordPress Dashboard and select <strong>Plugins >> Add New</strong>.
3. Click on the <strong>Upload</strong> option at the top and select the `post-list-table.zip file` you just downloaded.
4. Click on <strong>Install</strong>.
5. Activate the plugin through the 'Plugins' menu in WordPress
6. There should be an additional Post List Table menu on your Dashboard sidebar.
7. Clicking on the Post List Table redirects You on the settings page.

!!! UPDATING !!!
DELETE old version before installing the new one


== Frequently Asked Questions ==
1. Question: How to show/add/edit excerpts?
Answer: while editing the posts activate <strong>Excerpt</strong> from the <strong>Screen Options</strong> (upper right corner).


== Screenshots ==
Full width table with filtering to blog
Full width table with filtering within table
Post List Table Table with sidebar
Plugin options - admin


== Changelog ==
Initial release version 1.0  /June 24, 2015/

Verison: 1.5.0 /November 29, 2015/
+ new: New Admin Panel and options
+ new: Order by Field - You can choose default column for initial ordering
+ new: Order Direction -  Ascending/Descending (for the default column to order)
+ new: Page Length - Choose initial number of records per page (10, 25, 50, 100)
+ new: Choose Categories - select categories to show/hide in the post-list-table
+ new: Subcategories Indentation in the drop-down filter

Verison: 1.2.0 /October 29, 2015/
+ add: responsiveness for the mobiles and tablets
+ add: additional (+) icon to show possible hidden columns for mobile devices
+ add: choosing columns to show from the front end
+ add: column reordering by drag-and-drop from the front end

Verison: 1.1.1 /October 6, 2015/
! fix: bug - only first row affected while hiding featured images
! fix: bug - category drop-down filter disappears when clicking on category within table

Verison: 1.1 /September 29, 2015/
+ new: New Admin Panel and options
+ add: drop-down filtering by Categories & Tags
+ add: instant search/filter by Title, Description & Date
+ add: show/hide filter/search bar for individual fields
+ add: choose top/bottom position for filter/search bar
= upd: now rows initially sorted by Date
= upd: table default style changed to Compact
= upd: Datatables version updated to 1.10.9
! fix: bug for posiible 'short_open_tag' incompatibility fixed
! fix: permalinks behaviour fixed
