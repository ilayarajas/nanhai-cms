<div class="wrap">
	<h1><?php _e('Add/Edit User Level', 'watuproplay')?></h1>
	
	<form method="post" class="watupro" onsubmit="return validateLevelForm(this);">
	<div class="wp-admin watupro-padded postbox">
		<fieldset>
			<legend><h2><?php _e('Level Details', 'watupro')?></h2></legend>
			<p><label><?php _e('Level name:', 'watuproplay')?></label> <input type="text" name="name" value="<?php echo @$level->name?>" size="30"></p>
			<p><label><?php _e('Level rank:', 'watuproplay')?></label> <input type="text" name="rank" value="<?php echo @$level->rank?>" size="4">
			<span class="watupro-help"><?php _e('Please enter whole number. The topmost level rank is 1. It is your responsibility to avoid entering duplicate level ranks.', 'watuproplay')?> </span></p>		
			<p><label><?php _e('Optional description', 'watuproplay')?></label> <?php wp_editor(stripslashes(@$level->content), 'content')?></p>
		</fieldset>
		<fieldset>
			<legend><h2><?php _e('Level Requirements', 'watuproplay')?></h2></legend>
			<p class="wtupro-help"><?php _e('You can mix multiple requirements for a single level in any way you want', 'watuproplay')?></p>
			
			<p><label><?php _e('Required total points:', 'watuproplay')?></label> <input type="text" name="required_points" value="<?php echo @$level->required_points?>" size="6">
			<?php _e('from question category:', 'watuproplay');?> <select name="points_cat_id">
				<option value="0" <?php if(empty($level->points_cat_id)) echo 'selected'?>><?php _e('All categories', 'watuproplay');?></option>
				<?php foreach($qcats as $cat):?>
					<option value="<?php echo $cat->ID?>" <?php if(@$level->points_cat_id == $cat->ID) echo 'selected'?>><?php echo stripslashes(apply_filters('watupro_qtranslate', $cat->name));?></option>
				<?php endforeach;?>
			</select>
			<input type="checkbox" name="use_latest_points" value="1" <?php if(!empty($level->use_latest_points)) echo 'checked'?>> <?php _e('Consider only latest attempt on each quiz', 'watuproplay');?></p></p>
			
			<p><label><?php _e('Required avg. % correct answers from all tests:', 'watuproplay')?></label> <input type="text" name="required_percent" value="<?php echo @$level->required_percent?>" size="4">% 
			<?php _e('from question category:', 'watuproplay');?> <select name="percent_cat_id">
				<option value="0" <?php if(empty($level->percent_cat_id)) echo 'selected'?>><?php _e('All categories', 'watuproplay');?></option>
				<?php foreach($qcats as $cat):?>
					<option value="<?php echo $cat->ID?>" <?php if(@$level->percent_cat_id == $cat->ID) echo 'selected'?>><?php echo stripslashes(apply_filters('watupro_qtranslate', $cat->name));?></option>
				<?php endforeach;?>
			</select>			
			<?php _e("(This considers only last user's attempt on each quiz)", 'watuproplay');?></p>
			
			<p><label><?php _e('Required number unique tests submitted:', 'watuproplay')?></label> <input type="text" name="required_num_exams" value="<?php echo @$level->required_num_exams?>" size="4"></p>
			
			<p><label><?php _e('Must take all exams from:', 'watuproplay')?></label> <select name="required_category">
			<option value="0" <?php if(empty($level->required_category)) echo "selected"?>><?php _e('- Not applicable -', 'watuproplay')?></option>
			<?php foreach($cats as $cat):?>
				<option value="<?php echo $cat->ID?>" <?php if(!empty($level->id) and $cat->ID == $level->required_category) echo 'selected'?>><?php echo $cat->name?></option>
			<?php endforeach;?>			
			</select> <?php _e('category', 'watuproplay')?></p>
		</fieldset>	
		
		<p align="center"><input type="submit" value="<?php _e('Save Level', 'watuproplay')?>"> 
		<?php if(!empty($level->id)):?>
		<input type="button" value="<?php _e('Delete Level', 'watuproplay')?>" onclick="watuproplayDeleteLevel(this.form);">
		<input type="hidden" name="del" value="0">
		<?php endif;?></p>
	</div>
	<input type="hidden" name="ok" value="1">
	</form>
</div>

<script type="text/javascript" >
function validateLevelForm(frm) {
	if(frm.name.value=='') {
		alert("<?php _e('Please enter name for this level', 'watuproplay')?>");
		frm.name.focus();
		return false;
	}
	
	if(frm.rank.value=='' || isNaN(frm.rank.value)) {
		alert("<?php _e('Please enter level rank (order), numbers only', 'watuproplay')?>");
		frm.rank.focus();
		return false;
	}
	
	return true;
}

function watuproplayDeleteLevel(frm) {
	if(confirm("<?php _e('Are you sure?', 'watuproplay')?>")) {
		frm.del.value=1;
		frm.submit();
	}
}
</script>