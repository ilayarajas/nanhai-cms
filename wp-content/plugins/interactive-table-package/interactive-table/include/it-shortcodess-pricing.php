<?php
function it_dynamic_pricing_table_shortcode( $atts, $content = null ) {
    extract(shortcode_atts(array(
        'block_heading_position' => 'Top',
        'block_associates_color' => get_option('cell_highlight_color'),
        'cell_title_color' => get_option('it_cell_title_color'),
        'cell_hover_title_color' => get_option('it_cell_hover_title_color'),		
        'cell_font_color' => get_option('it_cell_font_color'),
        'cell_hover_font_color' => get_option('it_cell_hover_font_color'),		
        'read_more_font_color' => get_option('it_read_more_font_color'),
        'cell_read_more' => get_option('it_read_more'),
        'vertical_posts_per_row' => get_option('it_vertical_posts_per_row'),
		'fixed_highlight' => get_option('fixed_highlight'),
        'category' => null
    ), $atts));
	
	$table_output = '';
	$total_category = 0;
	$category_arr = array();
	$category_arr = explode(",", $category);
	$display_mode = get_option('it_display_mode');	
	if( ($category !== null) && (sizeof($category_arr) > 0) ) {
		$total_category = sizeof($category_arr);
	}
		
	$table_output .= '<div class="dynamic_table_wrap interactive_pricing_table display_heading_'.$block_heading_position.'">';

	$terms = get_terms( 'it_cell_category', array(
		'include'    => $category_arr,
	 ) );

	$count = 0;
	$heading_pos = strtolower($block_heading_position);
	if( $heading_pos == 'top' ) {
		$class = 'horizontal pos-top';
		$col_class = 'horizontal';
	} elseif( $heading_pos == 'left' ) {
		$class = 'vertical pos-left';
		$col_class = 'vertical';
	}else {
		$class = 'horizontal pos-top';
		$col_class = 'horizontal';
	}
	
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		$termOrderArr = array();
		$empOrderArr = array();
		$term_colors = array();
		foreach ( $terms as $term ) {
			// Get the custom fields based on the $presenter term ID  
			$cell_cat_custom_fields = get_option( "taxonomy_term_$term->term_id" );
			//$table_output .= $cell_cat_custom_fields['cell_cat_order'];
			if( $cell_cat_custom_fields['cell_cat_order'] != '' ) {
				$termOrderArr[$term->term_id] = $cell_cat_custom_fields['cell_cat_order'];
			} else {
				$empOrderArr[$term->term_id] = '';
				//continue;
			}
							
			$t_id = $term->term_id;
			$term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check
			if (isset($term_meta['cell_cat_color']) && ($term_meta['cell_cat_color'] != '')) {
				$term_color = $term_meta['cell_cat_color'];
			} else {
				$term_color = $block_associates_color;
			}
			$term_colors[$term->slug] = $term_color;				
			
		}
		asort($termOrderArr);
		
		// merge the two arrays
		$termsArr = $termOrderArr + $empOrderArr;
		$serializedTermIds = array_keys($termsArr);

	}
	
	
	//Enable css for fixed heighlight mode
	if(isset($term_colors)){
		$table_output .= '<style type="text/css">';
		foreach ( $term_colors as $term_color_slug => $term_color_code ) {
			if( $heading_pos == 'top' ) {
				$table_output .= '.interactive_pricing_table .pricing-table-style-top .cell.cell-heading.'.$term_color_slug.'{ background:'.$term_color_code.';}';
				$table_output .= '.interactive_pricing_table .pricing-table-style-top .section-category-'.$term_color_slug.' .pricing-button a.button{ background:'.$term_color_code.';}';
			}	
			if( $heading_pos == 'left' ) {
				$table_output .= '.interactive_pricing_table .pricing-table-style-left .cell.cell-heading.'.$term_color_slug.'{ background:'.$term_color_code.';}';
				$table_output .= '.interactive_pricing_table .pricing-table-style-left .section-category-'.$term_color_slug.' .pricing-button a.button{ background:'.$term_color_code.';}';
			}
		}
		$table_output .= '</style>';
	}
	
	//Enable css for fixed heighlight mode
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		$table_output .= '<div class="pricing-table-style-'.$heading_pos.' pricing-col-'.$total_category.'  '.$class.' terms-'.count($terms).'" data-terms="'.count($terms).'">';
		foreach ( $termsArr as $termId => $termOrder ) {
			$theTerm = get_term_by('id', $termId, 'it_cell_category');
			$section_count = 1;
			$section_count += $theTerm->count;
			$table_output .= '<div class="pricing-col-wrap section-category-'.$theTerm->slug.' section-item-'.$section_count.'">';
			$table_output .= '<div data-tax="'.$theTerm->slug.'" data-taxposts="'.$theTerm->count.'" class="cell cell-heading '.$theTerm->slug.'"><div class="cellwrap"><span>' . $theTerm->name . '</span>'.term_description($termId, 'it_cell_category').'</div></div>';
			
			$args_pricing_item = array(
				'post_type' => 'interactive_table',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'it_cell_category',
						'field'    => 'id',
						'terms'    => $termId,
					),
				),
				'orderby'   => 'menu_order',
				'order'     => 'ASC',
			);				
			$it_pricing_query = new WP_Query( $args_pricing_item );				
			if($it_pricing_query->have_posts()) {				
				while ( $it_pricing_query->have_posts() ) {
					$it_pricing_query->the_post();
					$content = get_the_content();
					$content = apply_filters('the_content', $content);					
					$cell_edit_link = '';
					$post_edit_url = get_edit_post_link( get_the_ID() );
					if($post_edit_url){
						$cell_edit_link = '<a class="edit_cell" href="'.$post_edit_url.'">Edit Cell</a>';
					}					
					$it_cell_class = get_post_meta(get_the_ID(), '_it_cell_class', true);					
					$table_output .= '<div class="cell '.$it_cell_class.'"><div class="cellwrap">'.$content . $cell_edit_link. '</div></div>';
					
				}	
			}			
			wp_reset_query();
			
		$table_output .= '</div><!--pricing-col-wrap-->';	
			
		}
		$table_output .= '</div>';
	}		
	$table_output .= '</div><!--interactive_pricing_table-->';
	
	return $table_output;

}
add_shortcode('interactive_pricing_table', 'it_dynamic_pricing_table_shortcode');

