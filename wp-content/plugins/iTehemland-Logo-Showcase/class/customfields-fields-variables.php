<?php
	// custom variables
    global $it_logo_showcase_metaboxname_fields;
    $it_logo_showcase_metaboxname_fields = array(
        array(
            'label' => __('General', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'desc' => '',
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'general',
            'type' => 'notype',
        ),

        array(
            'label' => '<strong>' . __('Short Description', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</strong>',
            'desc' => __('Enter Short Desc here ... ', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'shortDesc',
            'type' => 'textarea'
        ),
        array(
            'label' => '<strong>' . __('Link Url', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</strong>',
            'desc' => __('http://www.example.com/', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'link_url',
            'type' => 'url'
        ),
        array(
            'label' => '<strong>' . __('Link Target', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</strong>',
            'desc' => __('Choose Target', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'link_target',
            'type' => 'select',
            'options' => array(

                'one' => array(

                    'label' => __('_blank', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
                    'value' => '_blank'
                ),
                'two' => array(

                    'label' => __('_self', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
                    'value' => '_self'
                )
            )
        ),
        array(
            'label' => __('Quick view details', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'desc' => '',
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'quick_view_details',
            'type' => 'notype',
        ),
        array(
            'label' => '<strong>' . __('Full Description', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</strong>',
            'desc' => __('', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'full_desc',
            'type' => 'html_editor'
        ),
        array(
            'label' => '<strong>' . __('Website', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</strong>',
            'desc' => __('http://www.example.com/', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'website',
            'type' => 'url'
        ),
        array(
            'label' => '<strong>' . __('Email', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</strong>',
            'desc' => __('example@domain.com', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'email',
            'type' => 'email'
        ),
        array(
            'label' => '<strong>' . __('Tell', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</strong>',
            'desc' => __('+1-2112345678', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tell',
            'type' => 'text'
        ),

        array(
            'label' => __('Gallery options', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'desc' => '',
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'gallery_option',
            'type' => 'notype',
        ),
        array(
            'label' => '<strong>' . __('Images', __IT_LOGO_SHOWCASE_TEXTDOMAIN__) . '</strong>',
            'desc' => __('Choose you images for gallery', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'gallery_image',
            'type' => 'gallery'
        ),
        array(
            'label' => __('Social Links', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'desc' => '',
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'social_links',
            'type' => 'notype',
        ),
        array(
            'label' => __('Facebook', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'desc' => __('http://www.facebook.com/', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'facebook',
            'type' => 'url',
        ),
        array(
            'label' => __('Twitter', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'desc' => __('http://www.twitter.com/', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'twitter',
            'type' => 'url',
        ),
        array(
            'label' => __('Instagram', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'desc' => __('http://www.instagram.com/', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'instagram',
            'type' => 'url',
        ),
        array(
            'label' => __('Google+', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'desc' => __('http://www.google.com/', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'google_plus',
            'type' => 'url',
        ),
        array(
            'label' => __('Linked In', __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
            'desc' => '',
            'id' => __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'linked_in',
            'type' => 'url',
        )
    );

?>
