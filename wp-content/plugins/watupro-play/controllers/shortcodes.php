<?php
class WatuPROPlayShortcodes {
	// initialize all shortcodes
	static function init() {
		add_shortcode('watuproplay-userlevel', array(__CLASS__,'userlevel'));
		add_shortcode('watuproplay-userbadges', array(__CLASS__,'userbadges'));
		add_shortcode('watuproplay-userrewards', array(__CLASS__,'userrewards'));
		add_shortcode('watuproplay-points', array(__CLASS__,'points'));
		add_shortcode('watuproplay-overview', array(__CLASS__,'user_overview'));
		add_shortcode('watuproplay-avgpercent', array(__CLASS__,'avg_percent'));
		add_shortcode('watuproplay-avgpercent-required', array(__CLASS__,'avg_percent_required'));
		
		// leaderboard shortcodes
		add_shortcode('watuproplay-leaderboard', array(__CLASS__,'leaderboard'));
		
		// rewards list
		add_shortcode('watuproplay-rewards', array(__CLASS__,'rewards'));
		
		// users by level or badge
		add_shortcode('watuproplay-list', array(__CLASS__,'list_users'));
		
		// exam piechart
		add_shortcode('watuproplay-barchart', array(__CLASS__, 'barchart'));
	}
	
	// get the user level shortcode
	// [watuproplay-userlevel] & [watuproplay-userlevel x]
	static function userlevel($atts) {
		global $wpdb, $user_ID;	
		$_level = new WatuPROPlayLevel();
		
		$uid = (!empty($atts[0]) and is_numeric($atts[0])) ? $atts[0] : $user_ID;
		
		if(empty($uid)) return '';
		
		$level = $_level->get_user_level($uid);	
		
		if(empty($level->name)) return __('N/a', 'watuproplay');	
		
		if(empty($atts['show_desc'])) return stripslashes($level->name);
		else {
			 if(empty($tag)) $tag = 'h3';
			 return '<'.$tag.'>' . stripslashes(@$level->name) . '</'.$tag.'>' . stripslashes(@$level->content);
		}  
	}
	
	// get the user badges 
	static function userbadges($atts) {
		global $wpdb, $user_ID;	
		$_level = new WatuPROPlayLevel();
		
		$uid = (!empty($atts[0]) and is_numeric($atts[0])) ? $atts[0] : $user_ID;
		
		if(empty($uid)) return '';
		
		$id_sql = empty($atts['badge_id']) ? '' : $wpdb->prepare(" AND id=%d ", intval($atts['badge_id']));
		
		// select all badges
		$badges = $wpdb -> get_results("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE atype='badge' $id_sql ORDER BY name");
		
		$badge_ids = $_level->get_user_badges($uid);
				
		// we'll display the badges in divs 
		$output = '';
		foreach($badges as $badge) {		
			if(!@in_array($badge->id, $badge_ids)) continue;	
			$output .= "<div class='watuproplay-badge'>".apply_filters('watupro_content', stripslashes($badge->content));
			
			// add open badge link
			if(!empty($user_ID) and $user_ID == $uid and $badge->is_open_badge) {
				$output .= "<p><a href='".site_url("?wtp_accept_badges=1&uid=".$user_ID."&bids=".$badge->id)."' target='_blank'>".__('Add to my Mozilla Backpack', 'watuproplay')."</a></p>";
			}			
			
			$output .= "</div>\n";
		} 
		
		if(empty($output) and empty($atts['badge_id'])) $output = __('There are no badges assigned yet.', 'watuproplay');
		
		return $output;
	}
	
	// get user redeemed rewards
	static function userrewards($atts) {
		global $wpdb, $user_ID;	
		$uid = (!empty($atts[0]) and is_numeric($atts[0])) ? $atts[0] : $user_ID;
		
		$items = $wpdb -> get_results($wpdb->prepare("SELECT tH.*, tR.title as item_title
			FROM ".WATUPROPLAY_REDEEMED." tH JOIN ".WATUPROPLAY_REWARDS." tR ON tR.id = tH.item_id 
			WHERE tH.user_id=%d ORDER BY tH.id DESC", $uid));
			
		if(!sizeof($items)) return __('No rewards have been redeemed yet.', 'watuproplay');			
			
		$output = "<table class='watuproplay-user-rewards widefat'>";
		$output .= "<tr><th>".__('Item title', 'watuproplay')."</th><th>".__('Date Redeemed', 'watuproplay')."</th>
		<th>".__('Points spent', 'watuproplay')."</th><th>Status</th></tr>";
		$dateformat = get_option('date_format');
		foreach($items as $item) {
			$output .= "<tr><td>".$item->item_title."</td><td>".date($dateformat, strtotime($item->date))."</td>
			<td>".$item->points."</td><td>".($item->is_shipped ? __('Delivered','watuproplay') : __('Pending', 'watuproplay'))."</td></tr>";
		}
		$output .= "</table>";
		
		return $output;	
	}
	
	// points balance
	static function points($atts) {
		global $user_ID;
		
		$uid = (!empty($atts[0]) and is_numeric($atts[0])) ? $atts[0] : $user_ID;		
		$points = get_user_meta($uid, 'watuproplay-points', true);
		$points = apply_filters('watuproplay-adjust-points', $points);
		return $points;
	}
	
	// displays user overview page	
	static function user_overview($atts) {
		global $user_ID;		
		//$uid = (!empty($atts[0]) and is_numeric($atts[0])) ? $atts[0] : $user_ID;
		$uid = $user_ID; // for now let's keep this only as current user overview
		// if we are to allow passing user ID, we'll have to require entering the shortcodes with "x"
		// and then in $_user->overview replace these shortcodes so "x" becomes the user ID
		// prior to parsing		
		
		$_user = new WatuPROPlayUser();
		
		return $_user->overview($uid, true);
	}
	
	// various leaderboards
	static function leaderboard($atts) {
		$ltype = $atts[0]; // leaderboard type
		array_shift($atts);
		switch($ltype) {
			case 'qcat':
				return WatuPROPlayLeaderBoards::qcat_leaderboard($atts);
			break;
			case 'cat':
				return WatuPROPlayLeaderBoards::cat_leaderboard($atts);
			break;
			case 'quiz':
				return WatuPROPlayLeaderBoards::quiz_leaderboard($atts);
			break;
			case 'global':
			default:
				return WatuPROPlayLeaderBoards::global_leaderboard($atts);
			break;
		}
	}
	
	// displays the rewards listing or the message after reward is redeemed
	static function rewards($atts) {		
		$_reward = new WatuPROPlayReward();
		if(!empty($_POST['watuproplay_redeem'])) {			
			$output = $_reward->redeem($_POST['watuproplay_item_id']);
		}
		else {
			// display the listing
			$output = $_reward->display_listing($atts[0]);
		}
		
		return $output;
	}
	
	static function list_users($atts) {
		$listby = $atts[0];
		$id = $atts[1];
		
		// select the users
		switch($listby) {
			case 'level':
				$users = get_users(array("meta_key"=>"watuproplay_user_level", "meta_value"=>$id, "orderby"=>"display_name"));				
			break;
			case 'badge':				
				$args = array(
					'orderby' => 'display_name',
					'meta_query' => array(
						array(
							'key' => 'watuproplay_user_badges',
							'value' => "|".$id."|",
							'compare' => "LIKE"							
						)
					) 
				);
				$users = get_users($args);				
			break;
		}
		
		// now prepare the output
		$output = "";
		foreach($users as $user) $output .= $user->display_name."<br>";
		return $output;
	}	
	
	// bar chart of how users answered quiz
	// show % by points or by achieved grade
	static function barchart($atts) {		
		$chart_type = empty($atts[0]) ? 'points' : $atts[0];
		$default_text = ($chart_type == 'points') ? __("%d%% collected %d points", 'watuproplay') : __("%d%% were graded '%s'", 'watuproplay');
		$text = empty($atts[1]) ? $default_text : $atts[1];
		$color = empty($atts[2]) ? 'gray' : $atts[2];
		$height = (empty($atts[3]) or !is_numeric($atts[3])) ? 20 : $atts[3];
		$maxwidth = (empty($atts[4]) or !is_numeric($atts[4])) ? 500 : $atts[4];
		$step = round($maxwidth / 100);
		
		// chart type can contain also "orderby" info.
		if($chart_type != 'points') {
			switch($chart_type) {
				case 'grades': $ob = 'gfrom DESC'; break; // the default option is by best grade
				case 'grades_alpha':
				  $ob = 'grade_title';
				  $chart_type = 'grades';
				break;
			}
		}
		
		// overwrite quiz id
		$quiz_id = empty($atts[5]) ? @$_POST['quiz_id'] : intval($atts[5]);
		
		ob_start();
		if($chart_type == 'points') WatuPROPlayCharts :: exam_by_points($quiz_id, $text, $color, $height, $step);
		if($chart_type == 'grades') WatuPROPlayCharts :: exam_by_grades($quiz_id, $text, $color, $height, $step, $ob);
		if($chart_type == 'avg_score') WatuPROPlayCharts :: exam_by_avg_score($quiz_id, $text, $color, $height, $step);
		$content = ob_get_clean();
		return $content;
	}
	
	// average % correct answer - overall, for a given user
	// considers the latest taking on each quiz - same that is used for levels/badges requirements
	static function avg_percent_required($atts) {
		global $user_ID;
		
		$uid = (!empty($atts[0]) and is_numeric($atts[0])) ? $atts[0] : $user_ID;		
		$_user = new WatuPROPlayUser();
		$stats = $_user->stats($uid);
		
		return $stats['percent'];
	}
	
	static function avg_percent($atts) {
		global $user_ID, $wpdb;
		
		$uid = (!empty($atts[0]) and is_numeric($atts[0])) ? $atts[0] : $user_ID;		
		$percent = $wpdb->get_var($wpdb->prepare("SELECT AVG(percent_correct) as avg_percent FROM ".WATUPRO_TAKEN_EXAMS."
			WHERE user_id=%d AND in_progress=0", $uid));
		return round($percent,2);	 
	}
}