<?php
function  webnus_enrolment_shortcode( $attributes, $content ) {
	extract( shortcode_atts( array(
		'process_item'	 	=> '',
	), $attributes ) );

	// process_item loop
	$process_item		= (array) vc_param_group_parse_atts( $process_item );
	$process_item_data	= array();
	foreach ( $process_item as $data ) :
		$new_line 						= $data;
		$new_line['process_title']		= isset( $data['process_title'] ) ? $data['process_title'] : '';
		$new_line['process_content']	= isset( $data['process_content'] ) ? $data['process_content'] : '';
		$new_line['line_flag']			= isset( $data['line_flag'] ) ? $data['line_flag'] : '';
		$process_item_data[]			= $new_line;
	endforeach;

	// render
	$out  = '
	<div class="enrolment-wrap">';
			foreach ( $process_item_data as $line ) :
				$out .= '
				<div class="enrolment-item">
						<h4>' . $line['process_title'] . '</h4>
						<p>' . $line['process_content'] . '</p>
						<span>' . $line['line_flag'] . '</span>
				</div>';
			endforeach;
	$out .= '
	</div>';

	return $out;
}

add_shortcode( 'enrolment', 'webnus_enrolment_shortcode' );