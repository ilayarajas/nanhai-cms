<div id="wrap">
	<h1><?php _e('Play plugin for WatuPRO', 'watuproplay')?></h1>
	
	<p><?php _e('The Play Plugin adds gamification and competition elements to WatuPRO. You can use it to assign badges, user levels, to allow collected points to be redeemed for rewards, etc. <br> On this page you will learn about the shortcodes that this plugin adds and you can generate shortcodes for various leaderboards', 'watuproplay')?></p>
	
	<hr>
	
	<h2><?php _e('Generate Leaderboard Shortcodes', 'watuproplay')?></h2>
	
	<p><?php _e('The <b>"display by"</b> attribute will define whether the leaderboard will arrange and show results based on logged in user name or non-logged in visitor name. The second is available only for quizzes which have "Ask for user contact details" enabled and "name" field included.', 'watuproplay');?><br>
	<?php _e('Leaderboards for specific quiz will figure out this setting automatically.', 'watuprpoplay');?></p>
		
	<h3><?php _e('Global leaderboards based on all tests', 'watuproplay')?></h3>
	
	<form method="post">
	<p><?php _e('Show leaderboard of')?> <select name="segment">
	<option value="top" <?php if(!empty($_POST['segment']) and $_POST['segment']=='top') echo 'selected'?>><?php _e('Top', 'watuproplay')?></option>	
	<option value="bottom" <?php if(!empty($_POST['segment']) and $_POST['segment']=='bottom') echo 'selected'?>><?php _e('Bottom', 'watuproplay')?></option>
	</select> <input type="text" value="<?php echo empty($_POST['num'])?10:$_POST['num']?>" name="num" size="6"> <?php _e('users based on','watuproplay')?> 
	<select name="basedon">
		<option value="points" <?php if(!empty($_POST['basedon']) and $_POST['basedon']=='points') echo 'selected'?>><?php _e('total points collected', 'watuproplay')?></option>	
		<option value="percent" <?php if(!empty($_POST['basedon']) and $_POST['basedon']=='percent') echo 'selected'?>><?php _e('% correct answers', 'watuproplay')?></option>
		<option  value="tests" <?php if(!empty($_POST['basedon']) and $_POST['basedon']=='tests') echo 'selected'?>><?php _e('number of unique quizzes completed', 'watuproplay')?></option>
	</select>
	<?php _e('display by', 'watuproplay');?>
	<select name="user_criteria">
		<option value="user_login" <?php if(empty($_POST['user_criteria']) or $_POST['user_criteria']=='user_login') echo 'selected'?>><?php _e('user login', 'watuproplay')?></option>
		<option value="name" <?php if(!empty($_POST['user_criteria']) and $_POST['user_criteria']=='name') echo 'selected'?>><?php _e('visitor name', 'watuproplay')?></option>
	</select>
	
	<?php _e('Period:', 'watuproplay');?>
	<select name="period">
		<option value="all"><?php _e('All time', 'watuproplay');?></option>
		<option value="this week" <?php if(!empty($_POST['period']) and $_POST['period'] == 'this week') echo 'selected'?>><?php _e('This week', 'watuproplay');?></option>
		<option value="last week" <?php if(!empty($_POST['period']) and $_POST['period'] == 'last week') echo 'selected'?>><?php _e('Last week', 'watuproplay');?></option>
		<option value="this month" <?php if(!empty($_POST['period']) and $_POST['period'] == 'this month') echo 'selected'?>><?php _e('This month', 'watuproplay');?></option>
		<option value="last month" <?php if(!empty($_POST['period']) and $_POST['period'] == 'last month') echo 'selected'?>><?php _e('Last month', 'watuproplay');?></option>
		<option value="this year" <?php if(!empty($_POST['period']) and $_POST['period'] == 'this year') echo 'selected'?>><?php _e('This year', 'watuproplay');?></option>
		<option value="last year" <?php if(!empty($_POST['period']) and $_POST['period'] == 'last year') echo 'selected'?>><?php _e('Last year', 'watuproplay');?></option>
	</select>
	<input type="submit" value="<?php _e('Generate', 'watuproplay')?>" name="gen_global">
	<?php if(!empty($_POST['gen_global'])):?>
		<input type="text" onclick="this.select();" value='[watuproplay-leaderboard global <?php echo $_POST['segment'].' '.$_POST['num'].
			' '.$_POST['basedon']?> user_criteria="<?php echo $_POST['user_criteria']?>" period="<?php echo $_POST['period']?>"]' size="70">
	<?php endif;?></p>
	</form>
	
	<h3><?php _e('Leaderboards for specific quiz', 'watuproplay')?></h3>	
	
	<?php if(sizeof($exams)):?>
		<form method="post">
		<p><?php _e('Show leaderboard of')?> <select name="quiz_segment">
		<option value="top" <?php if(!empty($_POST['quiz_segment']) and $_POST['quiz_segment']=='top') echo 'selected'?>><?php _e('Top', 'watuproplay')?></option>	
		<option value="bottom" <?php if(!empty($_POST['quiz_segment']) and $_POST['quiz_segment']=='bottom') echo 'selected'?>><?php _e('Bottom', 'watuproplay')?></option>
		</select> <input type="text" name="quiz_num" value="<?php echo empty($_POST['quiz_num'])?10:$_POST['quiz_num']?>"  size="6"> <?php _e('users based on','watuproplay')?>  <select name="quiz_basedon">
			<option value="points" <?php if(!empty($_POST['quiz_basedon']) and $_POST['quiz_basedon']=='points') echo 'selected'?>><?php _e('total points collected', 'watuproplay')?></option>	
			<option value="points_ungrouped" <?php if(!empty($_POST['quiz_basedon']) and $_POST['quiz_basedon']=='points_ungrouped') echo 'selected'?>><?php _e('points, ungrouped', 'watuproplay')?></option>	
			<option value="percent" <?php if(!empty($_POST['quiz_basedon']) and $_POST['quiz_basedon']=='percent') echo 'selected'?>><?php _e('% correct answers', 'watuproplay')?></option>		
			<option value="percent_ungrouped" <?php if(!empty($_POST['quiz_basedon']) and $_POST['quiz_basedon']=='percent_ungrouped') echo 'selected'?>><?php _e('% correct, ungrouped', 'watuproplay')?></option>	
		</select>
		<?php _e('for quiz')?> 
		<select name="quiz_id">
		<?php foreach($exams as $exam):?>
			<option value="<?php echo $exam->ID?>" <?php if(!empty($_POST['quiz_id']) and $_POST['quiz_id']==$exam->ID) echo 'selected'?>><?php echo stripslashes($exam->name);?></option>
		<?php endforeach;?>
		</select>
		
		<?php _e('Period:', 'watuproplay');?>
		<select name="quiz_period">
			<option value="all"><?php _e('All time', 'watuproplay');?></option>
			<option value="this week" <?php if(!empty($_POST['quiz_period']) and $_POST['quiz_period'] == 'this week') echo 'selected'?>><?php _e('This week', 'watuproplay');?></option>
			<option value="last week" <?php if(!empty($_POST['quiz_period']) and $_POST['quiz_period'] == 'last week') echo 'selected'?>><?php _e('Last week', 'watuproplay');?></option>
			<option value="this month" <?php if(!empty($_POST['quiz_period']) and $_POST['quiz_period'] == 'this month') echo 'selected'?>><?php _e('This month', 'watuproplay');?></option>
			<option value="last month" <?php if(!empty($_POST['quiz_period']) and $_POST['quiz_period'] == 'last month') echo 'selected'?>><?php _e('Last month', 'watuproplay');?></option>
			<option value="this year" <?php if(!empty($_POST['quiz_period']) and $_POST['quiz_period'] == 'this year') echo 'selected'?>><?php _e('This year', 'watuproplay');?></option>
			<option value="last year" <?php if(!empty($_POST['quiz_period']) and $_POST['quiz_period'] == 'last year') echo 'selected'?>><?php _e('Last year', 'watuproplay');?></option>
		</select>
		<input type="submit" value="<?php _e('Generate', 'watuproplay')?>" name="gen_quiz">	
		
		<?php if(!empty($_POST['gen_quiz'])):?>
			<input type="text" onclick="this.select();" value='[watuproplay-leaderboard quiz <?php echo $_POST['quiz_segment'].' '.$_POST['quiz_num'].
				' '.$_POST['quiz_basedon'].' '.$_POST['quiz_id']?> period="<?php echo $_POST['quiz_period']?>"]' size="50">
		<?php endif;?></p>
		</form>
		<p><?php _e('<b>Ungrouped?</b> This selection above means that the leaderboard will not calculate total or average points by user, but will order each attempt individually. If the same user took the quiz multiple times, they may be shown multiple times in the leaderboard.', 'watuproplay')?></p>
	<?php else:?>
		<p><a href="admin.php?page=watupro_exams"><?php _e('You need to create some tests first', 'watuproplay')?></a></p>
	<?php endif;?>	
	
	<h3><?php _e('Leaderboards for specific quiz category', 'watuproplay')?></h3>	
	
	<?php if(sizeof($cats)):?>
		<form method="post">
		<p><?php _e('Show leaderboard of')?> <select name="cat_segment">
		<option value="top" <?php if(!empty($_POST['cat_segment']) and $_POST['cat_segment']=='top') echo 'selected'?>><?php _e('Top', 'watuproplay')?></option>	
		<option value="bottom" <?php if(!empty($_POST['cat_segment']) and $_POST['cat_segment']=='bottom') echo 'selected'?>><?php _e('Bottom', 'watuproplay')?></option>
		</select> <input type="text" size="6" name="cat_num" value="<?php echo empty($_POST['cat_num'])?10:$_POST['cat_num']?>"> users based on <select name="cat_basedon">
			<option value="points" <?php if(!empty($_POST['cat_basedon']) and $_POST['cat_basedon']=='points') echo 'selected'?>><?php _e('total points collected', 'watuproplay')?></option>	
			<option value="percent" <?php if(!empty($_POST['cat_basedon']) and $_POST['cat_basedon']=='percent') echo 'selected'?>><?php _e('% correct answers', 'watuproplay')?></option>
			<option  value="tests" <?php if(!empty($_POST['cat_basedon']) and $_POST['cat_basedon']=='tests') echo 'selected'?>><?php _e('number of unique quizzes completed', 'watuproplay')?></option>
		</select>
		<?php _e('for category')?>
		<select name="cat_id">
		<?php foreach($cats as $cat):?>
			<option value="<?php echo $cat->ID?>" <?php if(!empty($_POST['cat_id']) and $_POST['cat_id']==$cat->ID) echo 'selected'?>><?php echo stripslashes($cat->name);?></option>
		<?php endforeach;?>
		</select>		
		
		<?php _e('display by', 'watuproplay');?>
		<select name="cat_user_criteria">
			<option value="user_login" <?php if(empty($_POST['cat_user_criteria']) or $_POST['cat_user_criteria']=='user_login') echo 'selected'?>><?php _e('user login', 'watuproplay')?></option>
			<option value="name" <?php if(!empty($_POST['cat_user_criteria']) and $_POST['cat_user_criteria']=='name') echo 'selected'?>><?php _e('visitor name', 'watuproplay')?></option>
		</select>
		
		<?php _e('Period:', 'watuproplay');?>
		<select name="cat_period">
			<option value="all"><?php _e('All time', 'watuproplay');?></option>
			<option value="this week" <?php if(!empty($_POST['cat_period']) and $_POST['cat_period'] == 'this week') echo 'selected'?>><?php _e('This week', 'watuproplay');?></option>
			<option value="last week" <?php if(!empty($_POST['cat_period']) and $_POST['cat_period'] == 'last week') echo 'selected'?>><?php _e('Last week', 'watuproplay');?></option>
			<option value="this month" <?php if(!empty($_POST['cat_period']) and $_POST['cat_period'] == 'this month') echo 'selected'?>><?php _e('This month', 'watuproplay');?></option>
			<option value="last month" <?php if(!empty($_POST['cat_period']) and $_POST['cat_period'] == 'last month') echo 'selected'?>><?php _e('Last month', 'watuproplay');?></option>
			<option value="this year" <?php if(!empty($_POST['cat_period']) and $_POST['cat_period'] == 'this year') echo 'selected'?>><?php _e('This year', 'watuproplay');?></option>
			<option value="last year" <?php if(!empty($_POST['cat_period']) and $_POST['cat_period'] == 'last year') echo 'selected'?>><?php _e('Last year', 'watuproplay');?></option>
		</select>			
				
		<input type="submit" value="<?php _e('Generate', 'watuproplay')?>" name="gen_cat">
		
		<?php if(!empty($_POST['gen_cat'])):?>
			<input type="text" onclick="this.select();" value='[watuproplay-leaderboard cat <?php echo $_POST['cat_segment'].' '.$_POST['cat_num'].
				' '.$_POST['cat_basedon'].' '.$_POST['cat_id']?> user_criteria="<?php echo $_POST['cat_user_criteria']?>" period="<?php echo $_POST['cat_period']?>"]' size="70">
		<?php endif;?></p></p>
		</form>
	<?php else:?>
		<p><a href="admin.php?page=watupro_cats"><?php _e('You need to create some exam categories first', 'watuproplay')?></a></p>
	<?php endif;?>	
	
	<h3><?php _e('Leaderboards for specific question category', 'watuproplay')?></h3>	
	
	<?php if(sizeof($qcats)):?>
		<form method="post">
		<p><?php _e('Show leaderboard of')?> <select name="qcat_segment">
		<option value="top" <?php if(!empty($_POST['qcat_segment']) and $_POST['qcat_segment']=='top') echo 'selected'?>><?php _e('Top', 'watuproplay')?></option>	
		<option value="bottom" <?php if(!empty($_POST['qcat_segment']) and $_POST['qcat_segment']=='bottom') echo 'selected'?>><?php _e('Bottom', 'watuproplay')?></option>
		</select> <input type="text" size="6" name="qcat_num" value="<?php echo empty($_POST['qcat_num'])?10:$_POST['qcat_num']?>"> users based on <select name="qcat_basedon">
			<option value="points" <?php if(!empty($_POST['qcat_basedon']) and $_POST['qcat_basedon']=='points') echo 'selected'?>><?php _e('total points collected', 'watuproplay')?></option>	
			<option value="percent" <?php if(!empty($_POST['qcat_basedon']) and $_POST['qcat_basedon']=='percent') echo 'selected'?>><?php _e('% correct answers', 'watuproplay')?></option>
		</select>
				<?php _e('for question category')?>
		<select name="qcat_id">
		<?php foreach($qcats as $cat):?>
			<option value="<?php echo $cat->ID?>" <?php if(!empty($_POST['qcat_id']) and $_POST['qcat_id']==$cat->ID) echo 'selected'?>><?php echo stripslashes($cat->name)?></option>
		<?php endforeach;?>
		</select>	
		
		<?php _e('display by', 'watuproplay');?>
		<select name="qcat_user_criteria">
			<option value="user_login" <?php if(empty($_POST['qcat_user_criteria']) or $_POST['qcat_user_criteria']=='user_login') echo 'selected'?>><?php _e('user login', 'watuproplay')?></option>
			<option value="name" <?php if(!empty($_POST['qcat_user_criteria']) and $_POST['qcat_user_criteria']=='name') echo 'selected'?>><?php _e('visitor name', 'watuproplay')?></option>
		</select>
			
		<?php _e('Period:', 'watuproplay');?>
		<select name="qcat_period">
			<option value="all"><?php _e('All time', 'watuproplay');?></option>
			<option value="this week" <?php if(!empty($_POST['qcat_period']) and $_POST['qcat_period'] == 'this week') echo 'selected'?>><?php _e('This week', 'watuproplay');?></option>
			<option value="last week" <?php if(!empty($_POST['qcat_period']) and $_POST['qcat_period'] == 'last week') echo 'selected'?>><?php _e('Last week', 'watuproplay');?></option>
			<option value="this month" <?php if(!empty($_POST['qcat_period']) and $_POST['qcat_period'] == 'this month') echo 'selected'?>><?php _e('This month', 'watuproplay');?></option>
			<option value="last month" <?php if(!empty($_POST['qcat_period']) and $_POST['qcat_period'] == 'last month') echo 'selected'?>><?php _e('Last month', 'watuproplay');?></option>
			<option value="this year" <?php if(!empty($_POST['qcat_period']) and $_POST['qcat_period'] == 'this year') echo 'selected'?>><?php _e('This year', 'watuproplay');?></option>
			<option value="last year" <?php if(!empty($_POST['qcat_period']) and $_POST['qcat_period'] == 'last year') echo 'selected'?>><?php _e('Last year', 'watuproplay');?></option>
		</select>		
				
		<input type="submit" value="<?php _e('Generate', 'watuproplay')?>" name="gen_qcat">
		
		<?php if(!empty($_POST['gen_qcat'])):?>
			<input type="text" onclick="this.select();" value='[watuproplay-leaderboard qcat <?php echo $_POST['qcat_segment'].' '.$_POST['qcat_num'].
				' '.$_POST['qcat_basedon'].' '.$_POST['qcat_id']?> user_criteria="<?php echo $_POST['qcat_user_criteria']?>" period="<?php echo $_POST['qcat_period']?>"]' size="70">
		<?php endif;?></p>
		</form>
	<?php else:?>
		<p><a href="admin.php?page=watupro_question_cats"><?php _e('You need to create some question categories first', 'watuproplay')?></a></p>
	<?php endif;?>	
	<hr>
	
	<h2><?php _e('Shortcodes for Reward Items List', 'watuproplay')?></h2>
	
	<p><?php _e('If you choose to add some reward items that can be redeemed for quiz points you can use the shortcodes shown below to display a listing of the available items', 'watuproplay')?></p>
	
	<p><input type="text" value="[watuproplay-rewards table]" onclick="this.select();" size="25"> <?php _e('To display the available in two-columns table. The table will have CSS class "watuproplay-rewards" and each table row will have CSS class "watuproplay-rewarditem".', 'watuproplay')?> </p>		
	
	<p><input type="text" value="[watuproplay-rewards column]" onclick="this.select();" size="25"> <?php _e('To display the available in one colum. The whole enclosing DIV tag will have CSS class "watuproplay-rewards" and each DIV inside will have CSS class "watuproplay-rewarditem".', 'watuproplay')?> </p>
	
	<hr>
	
	<h2><?php _e('Shortcodes for a bar chart', 'watuproplay')?></h2>
	
	<p><?php _e('This shortcode will generate a bar chart showing the percentage of users who achieved each grade in the quiz, or the percentage of users achieved each possible number of points.', 'watuproplay')?> <br><b><?php _e('When "Currently submitted" is selected for the quiz, the shortcode should only be used in the "Final screen". Otherwise it can be used everywhere.', 'watuproplay')?></b></p>
	
	<form method="post" id="barChartForm">
		<p><?php _e('Chart type:', 'watuproplay')?> <select name="chart_type" onchange="changeBarChartType(this.value);">
			<option value="grades" <?php if(!empty($_POST['chart_type']) and $_POST['chart_type'] == 'grades') echo 'selected'?>><?php _e('By grades, best on top', 'watuproplay')?></option>
			<option value="grades_alpha" <?php if(!empty($_POST['chart_type']) and $_POST['chart_type'] == 'grades_alpha') echo 'selected'?>><?php _e('By grades, alphabetically', 'watuproplay')?></option>
			<option value="points" <?php if(!empty($_POST['chart_type']) and $_POST['chart_type'] == 'points') echo 'selected'?>><?php _e('By points', 'watuproplay')?></option>
			<option value="avg_score" <?php if(!empty($_POST['chart_type']) and $_POST['chart_type'] == 'avg_score') echo 'selected'?>><?php _e('Average score / your score', 'watuproplay')?></option>
		</select>
		&nbsp;
		<?php _e('Text:', 'watuproplay')?> <input type="text" name="chart_text" value="<?php echo empty($_POST['chart_text']) ? __("%d%% were graded '%s'", 'watuproplay') : stripslashes($_POST['chart_text'])?>">
		&nbsp;
		<?php _e('Color:', 'watuproplay')?> <input type="text" name="chart_color" size="6" value="<?php echo empty($_POST['chart_color']) ? 'gray' : $_POST['chart_color']?>">
		&nbsp;
		<?php _e('Bar height:', 'watuproplay')?> <input type="text" name="chart_height" size="4" value="<?php echo empty($_POST['chart_height']) ? '20' : $_POST['chart_height']?>"><?php _e('px;', 'watuproplay');?> 
		&nbsp;
		<?php _e('Max width:', 'watuproplay')?> <input type="text" name="chart_max_width" size="4" value="<?php echo empty($_POST['chart_max_width']) ? '500' : $_POST['chart_max_width']?>"><?php _e('px;', 'watuproplay');?> 
		<?php if(sizeof($exams)): _e('for quiz')?> 
			<select name="quiz_id">
			<option value="0"><?php _e('Currently submitted', 'watuproplay')?></option>	
			<?php foreach($exams as $exam):?>
				<option value="<?php echo $exam->ID?>" <?php if(!empty($_POST['quiz_id']) and $_POST['quiz_id']==$exam->ID) echo 'selected'?>><?php echo $exam->name?></option>
			<?php endforeach;?>
			</select>
		<?php endif;?>
		<input type="submit" value="<?php _e('Generate', 'watuproplay')?>" name="gen_chart">
		
		
		<?php if(!empty($_POST['gen_chart'])):?>
			<div><textarea onclick="this.select();" rows="2" cols="80">[watuproplay-barchart <?php echo $_POST['chart_type'].' "'.stripslashes($_POST['chart_text']).
				'" '.$_POST['chart_color'].' '.$_POST['chart_height'].' '.$_POST['chart_max_width'].' '.$_POST['quiz_id']?>]</textarea></div>
		<?php endif;?></p>
		
		<p id="avgScore" style="display:none;"><?php _e('The "Average score / your score" chart will display two bars showing your % correct answer vs the average % correct answer. You can enter two colors in the "Color" field separated by | to use different colors for both bars.', 'watuproplay')?></p>
	</form>
	
	<hr>
	
	<h2><?php _e('Other shortcodes you can use', 'watuproplay')?></h2>
	
	<p><input type="text" value="[watuproplay-userlevel]" onclick="this.select();"> <?php _e('or', 'watuproplay')?> <input type="text" value="[watuproplay-userlevel x]" onclick="this.select();">  <?php _e('To display the level of the currently logged user. The second code can be used to display the level of another user (replace x with the user ID).', 'watuprolay')?></p>
	<p><input type="text" value="[watuproplay-userbadges]" onclick="this.select();"> <?php _e('or', 'watuproplay')?> <input type="text" value="[watuproplay-userbadges x]" onclick="this.select();">  <?php _e('To display the badges earned from the user.', 'watuprolay')?></p>
	<p><input type="text" value="[watuproplay-userrewards]" onclick="this.select();"> <?php _e('or', 'watuproplay')?> <input type="text" value="[watuproplay-userrewards x]" onclick="this.select();">  <?php _e('To display the redeemed rewards for this user.', 'watuprolay')?></p>
	<p><input type="text" value="[watuproplay-points]" onclick="this.select();"> <?php _e('or', 'watuproplay')?> <input type="text" value="[watuproplay-points x]" onclick="this.select();"> <?php _e('To display the points balance of the user.', 'watuprolay')?></p>
	<p><input type="text" value="[watuproplay-avgpercent]" onclick="this.select();"> <?php _e('or', 'watuproplay')?> <input type="text" value="[watuproplay-avgpercent x]" onclick="this.select();"> <?php _e('To display the average % correct answers from all quiz attempts.', 'watuprolay')?></p>
	<p><input type="text" value="[watuproplay-avgpercent-required]" onclick="this.select();"> <?php _e('or', 'watuproplay')?> <input type="text" value="[watuproplay-avgpercent-required x]" onclick="this.select();"> <?php _e('To display the average % correct answers from all latest attempts on each quiz. This is important because badge and level requirements consider only latest attempt for the user on each quiz.', 'watuprolay')?></p>
	<p><input type="text" size="18" value="[watuproplay-overview]" onclick="this.select();">  <?php _e('To display the user overview page of the current logged in user, configurable below.', 'watuproplay')?></p>
	
	<h2><?php _e('User Overwiew Page', 'watuproplay')?></h2>
	
	<form method="post">
	<p><input type="checkbox" name="enable_overview" value="1" <?php if($enable_overview) echo 'checked'?> onclick="this.checked ? jQuery('#overviewOptions').show() : jQuery('#overviewOptions').hide();"> <?php _e('Enable user overview page.', 'watuproplay')?></p>
	
	<p><?php _e('This page allows you to display a current user overview of user level, badge, redeemed rewards, and points balance inside the WatuPRO dashboard.<br> <b>If you do not show a dashboard you can expose the page on the front-end using the shortcode</b>', 'watuproplay')?> <input type="text" size="18" value="[watuproplay-overview]" onclick="this.select();"></p>
	
	<div id="overviewOptions" style="display:<?php echo $enable_overview ? 'block' : 'none';?>">		
			<p><?php _e('Overview page title:','watuproplay')?> <input type="text" name="overview_title" value="<?php echo stripslashes($overview_title)?>" size="60"></p>
			
			<p><?php _e('Overview page contents:', 'watuproplay')?> <?php wp_editor(stripslashes($overview_design), 'overview_design')?></p>
			
			<p><?php _e('You can use all the shortcodes available to design this page. Most useful here would be [watuproplay-userlevel], [watuproplay-userbadges], [watuproplay-points] and [watuproplay-userrewards].','watuproplay')?></p>
			<p><?php printf(__('The shortcode %s acepts argument %s to show the level contents / description. When used, the title is placed in a h3 tag (but you can pass the %s atribute to change this) before the description.', 'watupro'), '[watuproplay-userlevel]', 'show_desc=1', 'tag' );?></p>		
	</div>
	
	<p><input type="submit" name="save_overview" value="<?php _e('Save User Overview Settings','watuproplay')?>"></p>
	</form>	
</div>

<script type="text/javascript" >
function changeBarChartType(val) {
	if(val == 'avg_score') {
		jQuery('#avgScore').show();
		jQuery('#barChartForm input[name=chart_text]').val("<?php _e('Average score: %d%%|Your score: %d%%', 'watuproplay')?>");
	}
	else {
		jQuery('#avgScore').hide();
		jQuery('#barChartForm input[name=chart_text]').val("<?php _e("%d%% were graded '%s'", 'watuproplay')?>");
	}
}
</script>