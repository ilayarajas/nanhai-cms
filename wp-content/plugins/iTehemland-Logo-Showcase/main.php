<?php
/*
Plugin Name: iThemeland Logo Showcase
Plugin URI: http://ithemelandco.com/Plugins/it_logo_showcase/
Description: This Plugin Ideal For Show Logos, Partners, Sponsers And Clients.
Version: 1.1
Author: iThemeland
Author URI: http://ithemelandco.com/
Text Domain: it_logo_showcase_textdomain
Domain Path: /languages/
*/
/*
Version 1.1:
- Add item animation
- fix isotope filters
- fix full view ajax
*/


if(!class_exists('it_logo_showcase_class')) {

    define('__Custom_Post__', 'it_cp_logo');
    define('__Custom_Post_Taxonomy__', 'it_logo_category');

    define('__IT_LOGO_SHOWCASE_ROOT_DIR__', dirname(__FILE__)); //use for include

    //USE IN ENQUEUE AND IMAGE
    define('__IT_LOGO_SHOWCASE_CSS_URL__', plugins_url('assets/css/', __FILE__));
    define('__IT_LOGO_SHOWCASE_JS_URL__', plugins_url('assets/js/', __FILE__));
    define('__IT_LOGO_SHOWCASE_JS_PLUGIN_URL__', plugins_url('assets/plugins/', __FILE__));
    define ('__IT_LOGO_SHOWCASE_URL__', plugins_url('', __FILE__));

    //PERFIX
    define ('__IT_LOGO_SHOWCASE_FIELDS_PERFIX__', 'it_logo_field_');

    //TEXT DOMAIN FOR MULTI LANGUAGE
    define ('__IT_LOGO_SHOWCASE_TEXTDOMAIN__', 'it_logo_showcase_textdomain');

    // add localize script & style functions file
    //include('includes/localize_inline_func.php');

    // add custom post
    include('class/custompost.php');

    // add custom field for metabox
    include('class/customefields.php');

    // declare plugin class

    class it_logo_showcase_class
    {

        function __construct()
        {
            // ajax action
            include('includes/actions.php');

            // shortcode Gen Page
            add_action('admin_menu', array($this, 'it_logo_showcase_shortcode_page_add'));

            // Enquqe Script and css files
            add_action('admin_enqueue_scripts', array($this, 'it_logo_showcase_backend_enqueue'));
            add_action('wp_head', array($this, 'it_logo_showcase_frontend_enqueue'));

            //add_action('wp_head', array($this, 'it_frontend_enqueue'));

            // Shortcode Builder
            add_shortcode('it_logo_showcase', array($this, 'it_logo_showcase_shortcode'));

        }


        function it_logo_showcase_shortcode_page_add()
        {
            $menu_slug = 'edit.php?post_type=' . __Custom_Post__;
            $submenu_page_title = 'Shortcode Generator';
            $submenu_title = 'Shortcode Generator';
            $capability = 'manage_options';
            $submenu_slug = 'logo_showcase_shortcode';
            $submenu_function = array($this, 'logo_showcase_shortcode_page');
            add_submenu_page($menu_slug, $submenu_page_title, $submenu_title, $capability, $submenu_slug, $submenu_function);
        }

        function logo_showcase_shortcode_page()
        {
            include('backend/admin-page.php');
        }

        function it_logo_showcase_backend_enqueue()
        {
            include("includes/admin-embed.php");
        }

        function it_logo_showcase_frontend_enqueue()
        {
            include("includes/frontend-embed.php");
        }

        function it_logo_showcase_shortcode($atts, $content = null)
        {
            return it_preview_shortcode($atts, $content = null);
        }

    }//end class

    // show preview shortcode
    if(!function_exists('it_preview_shortcode')) {
        function it_preview_shortcode($atts, $content = null)
        {
            extract(shortcode_atts(array(
                'category' => 'all',
                'count' => -1,
                'orderby' => 'menu_order',
                'order' => 'DESC',
                'layout' => 'grid',
                'col_desktop' => 4,
                'col_tablet' => 3,
                'col_mobile' => 2,
                // style
                'margin_top' => 0,
                'margin_right' => 0,
                'margin_bottom' => 0,
                'margin_left' => 0,
                'padding_top' => 0,
                'padding_right' => 0,
                'padding_bottom' => 0,
                'padding_left' => 0,
                'background_color' => 'transparent',
                // shadow setting
                'shadow' => 'none',
                'shadow_v' => 0,
                'shadow_h' => 0,
                'shadow_blur' => 0,
                'shadow_spread' => 0,
                'shadow_color' => '',
                // border setting
                'border_style' => 'none',
                'border_width_top' => 0,
                'border_width_right' => 0,
                'border_width_bottom' => 0,
                'border_width_left' => 0,
                'border_radius_top_right' => 0,
                'border_radius_top_left' => 0,
                'border_radius_bottom_right' => 0,
                'border_radius_bottom_left' => 0,
                'border_color' => '#eee',
                // hover border setting
                'hover_background_color' => 'transparent',
                'hover_border_style' => 'none',
                'hover_border_width_top' => 0,
                'hover_border_width_right' => 0,
                'hover_border_width_bottom' => 0,
                'hover_border_width_left' => 0,
                'hover_border_radius_top_right' => 0,
                'hover_border_radius_top_left' => 0,
                'hover_border_radius_bottom_right' => 0,
                'hover_border_radius_bottom_left' => 0,
                'hover_border_color' => '#eee',
                // hover shadow setting
                'hover_shadow' => 'none',
                'hover_shadow_v' => 0,
                'hover_shadow_h' => 0,
                'hover_shadow_blur' => 0,
                'hover_shadow_spread' => 0,
                'hover_shadow_color' => '',
                //
                'item_animation' => 'it-no-animation',
				'custom_class' => '',
                // hover effect
                'effect' => 'none',
                // quick view setting
                'tooltip' => 'false',
                'quick_view_type' => 'none',
                'view_type' => 'popup',
                // customize show details for full view
                'title' => 'true',
                'contact' => 'true',
                'social' => 'true',
                'full_desc' => 'true',
                'gallery' => 'true',
                'video' => 'true',
                // isotope
                'filter_button_position' => 'left',
                'filter_button_skin' => 'light',
                // Carousel Setting
                'laptop_item' => 4,
                'desktop_item' => 6,
                'tablet_item' => 3,
                'mobile_item' => 2,
                'skin' => 'light',
                'autoplay' => 'true',
                'autoplay_speed' => 3000,
                'slideSpeed' => 1000,
                'paginationSpeed' => 3000,
                'rewindSpeed' => 1000,
                'navigation' => 'true',
                'pause' => 'true',
                'pagination' => 'true'
            ), $atts));
            $style = array(
                // padding & margin
                'padding-top' => $padding_top,
                'padding-right' => $padding_right,
                'padding-bottom' => $padding_bottom,
                'padding-left' => $padding_left,
                'margin-top' => $margin_top,
                'margin-right' => $margin_right,
                'margin-bottom' => $margin_bottom,
                'margin-left' => $margin_left,
                'background-color' => $background_color,
                'hover-background-color' => $hover_background_color,
                //shadow
                'shadow' => $shadow,
                'shadow-v' => $shadow_v,
                'shadow-h' => $shadow_h,
                'shadow-blur' => $shadow_blur,
                'shadow-spread' => $shadow_spread,
                'shadow-color' => $shadow_color,
                //border
                'border-width-top' => $border_width_top,
                'border-width-right' => $border_width_right,
                'border-width-bottom' => $border_width_bottom,
                'border-width-left' => $border_width_left,
                'border-style' => $border_style,
                'border-radius-top-right' => $border_radius_top_right,
                'border-radius-top-left' => $border_radius_top_left,
                'border-radius-bottom-right' => $border_radius_bottom_right,
                'border-radius-bottom-left' => $border_radius_bottom_left,
                'border-color' => $border_color,
                //hover
                'hover-border-width-top' => $hover_border_width_top,
                'hover-border-width-right' => $hover_border_width_right,
                'hover-border-width-bottom' => $hover_border_width_bottom,
                'hover-border-width-left' => $hover_border_width_left,
                'hover-border-style' => $hover_border_style,
                'hover-border-radius-top-right' => $hover_border_radius_top_right,
                'hover-border-radius-top-left' => $hover_border_radius_top_left,
                'hover-border-radius-bottom-right' => $hover_border_radius_bottom_right,
                'hover-border-radius-bottom-left' => $hover_border_radius_bottom_left,
                'hover-border-color' => $hover_border_color,

                //shadow
                'hover-shadow' => $hover_shadow,
                'hover-shadow-v' => $hover_shadow_v,
                'hover-shadow-h' => $hover_shadow_h,
                'hover-shadow-blur' => $hover_shadow_blur,
                'hover-shadow-spread' => $hover_shadow_spread,
                'hover-shadow-color' => $hover_shadow_color,
                //
                'effect' => $effect
            );
            $rand_id = rand(1000, 9999);
            include("includes/custom_style.php");
            include("includes/shortcode_view.php");
            add_action('admin_print_scripts', 'ajax_scripts');
            if(!function_exists('ajax_scripts')) {
                function ajax_scripts()
                {
                    global $script_outputs;
                    echo $script_outputs;
                }
            }
            it_logo_showcase_custom_css($style, $rand_id);
			return $html . $script_outputs;
            
        }
    }

    // widget class
    include 'class/Logo_showcase_widget.php';
    //
    new it_logo_showcase_class;
}
?>
