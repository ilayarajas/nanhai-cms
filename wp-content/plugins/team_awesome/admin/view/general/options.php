<?php
global $plugin_page;
$options = TEAM_AWESOME_ADMIN::get_options();
$fontsArray = TEAM_AWESOME_ADMIN::get_google_fonts();
?>

<div class="admin-wrapper">

    <div class="admin-section">

        <div class="admin-section-header">
            <h2><?php _e("General Settings", "team-awesome"); ?></h2>
            <p><?php _e("Team Awesome General Settings", "team-awesome"); ?></p>
        </div>

        <div class="admin-section-body">

            <form id="team-awesome-form-save" action="?page=<?php echo $plugin_page; ?>" method="POST">
               
                <div class="section">    

                    <!--boxwidth-->
                    <!--boxheight-->
                    <!--boxspacing-->
                    <div class="opt">
                        <span><?php _e("Person Box Dimensions", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <input required class="short" name="boxwidth" type="text" value="<?php echo $options["boxwidth"]; ?>" />
                        <input required class="short" name="boxheight" type="text" value="<?php echo $options["boxheight"]; ?>" />
                        <input required class="short" name="boxspacing" type="text" value="<?php echo $options["boxspacing"]; ?>" />
                        <p>(<?php _e("Width (px), Height (px), Spacing (px)", "team-awesome"); ?>)</p>
                    </div>

                    <!--title-->
                    <div class="opt">
                        <span><?php _e("Plugin Title", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Optional", "team-awesome"); ?>)</span><br />
                        <input name="title" type="text" value="<?php echo $options["title"]; ?>" />
                        <p>(<?php _e("Plugin Title", "team-awesome"); ?>)</p>
                    </div>

                    <!--cats-->
                    <div class="opt">
                        <span><?php _e("Use Categories", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Optional", "team-awesome"); ?>)</span><br />
                        <select class="short" name="cats" value="<?php echo $options["cats"]; ?>">
                            <option <?php echo $options["cats"] == "disable" ? "selected" : null; ?> value="disable"><?php _e("Disable", "team-awesome"); ?></option>
                            <option <?php echo $options["cats"] == "enable" ? "selected" : null; ?> value="enable"><?php _e("Enable", "team-awesome"); ?></option>
                        </select>
                        <select class="short" name="catsalign" value="<?php echo $options["catsalign"]; ?>">
                            <option <?php echo $options["catsalign"] == "center" ? "selected" : null; ?> value="center"><?php _e("Align Center", "team-awesome"); ?></option>
                            <option <?php echo $options["catsalign"] == "left" ? "selected" : null; ?> value="left"><?php _e("Align Left", "team-awesome"); ?></option>
                            <option <?php echo $options["catsalign"] == "right" ? "selected" : null; ?> value="right"><?php _e("Align Right", "team-awesome"); ?></option>
                        </select>
                        <p>(<?php _e("Enable / Disable Plugin Categories, Alignment", "team-awesome"); ?>)</p>
                    </div>


                    <!--slider-->
                    <div class="opt">
                        <span><?php _e("Use Slider", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Optional", "team-awesome"); ?>)</span><br />
                        <select class="short" name="slider" value="<?php echo $options["slider"]; ?>">
                            <option <?php echo $options["slider"] == "disable" ? "selected" : null; ?> value="disable"><?php _e("Disable", "team-awesome"); ?></option>
                            <option <?php echo $options["slider"] == "enable" ? "selected" : null; ?> value="enable"><?php _e("Enable", "team-awesome"); ?></option>
                        </select>
                        <input class="short" name="perpage" type="text" value="<?php echo $options["perpage"]; ?>" />
                        <p>(<?php _e("Enable / Disable Plugin Slider, Slides per page", "team-awesome"); ?>)</p>
                    </div>


                    <!--pop-->
                    <div class="opt">
                        <span><?php _e("Popup", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Optional", "team-awesome"); ?>)</span><br />
                        <select class="short" name="pop" value="<?php echo $options["pop"]; ?>">
                            <option <?php echo $options["pop"] == "disable" ? "selected" : null; ?> value="disable"><?php _e("Disable", "team-awesome"); ?></option>
                            <option <?php echo $options["pop"] == "enable" ? "selected" : null; ?> value="enable"><?php _e("Enable", "team-awesome"); ?></option>
                        </select>
                        <input class="short" name="popwidth" type="text" value="<?php echo $options["popwidth"]; ?>" />
                        <input class="short" name="popheight" type="text" value="<?php echo $options["popheight"]; ?>" />
                        <p>(<?php _e("Enable / Disable Plugin Popup, Width (px), Height (px)", "team-awesome"); ?>)</p>
                    </div>

                    <!--avatar-->
                    <!--avatarbc-->
                    <!--avatarb-->
                    <div class="opt">
                        <span><?php _e("Avatar", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Optional", "team-awesome"); ?>)</span><br />
                        <select class="short" name="avatar" value="<?php echo $options["avatar"]; ?>">
                            <option <?php echo $options["avatar"] == "circle" ? "selected" : null; ?> value="circle"><?php _e("Circle", "team-awesome"); ?></option>
                            <option <?php echo $options["avatar"] == "box" ? "selected" : null; ?> value="box"><?php _e("Square", "team-awesome"); ?></option>
                            <option <?php echo $options["avatar"] == "hex" ? "selected" : null; ?> value="hex"><?php _e("Hexagon", "team-awesome"); ?></option>
                        </select>
                        
                        <select class="short" name="avatarb" value="<?php echo $options["avatarb"]; ?>">
                            
                            <option <?php echo $options["avatarb"] == "off" ? "selected" : null; ?> value="off"><?php _e("Off", "team-awesome"); ?></option>
                            <option <?php echo $options["avatarb"] == "on" ? "selected" : null; ?> value="on"><?php _e("On", "team-awesome"); ?></option>
                        </select>
                        
                        <input name="avatarbc" type="text" class="short team-awesome-color-picker" value="<?php echo $options["avatarbc"]; ?>" />
                        <p>(<?php _e("Avatar Style,  Show/Hide Border, Border Color", "team-awesome"); ?>)</p>
                    </div>
                    
                    <!--socanim-->
                    <div class="opt">
                        <span><?php _e("Social Buttons Animation", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <select required name="socanim" value="<?php echo $options["socanim"]; ?>">
                            <option <?php echo $options["socanim"] == "ta-anim-grow" ? "selected" : null; ?> value="ta-anim-grow"><?php _e("Grow", "team-awesome"); ?></option>
                            <option <?php echo $options["socanim"] == "ta-anim-pulse" ? "selected" : null; ?> value="ta-anim-pulse"><?php _e("Pulse", "team-awesome"); ?></option>
                            <option <?php echo $options["socanim"] == "ta-anim-wobble-hor" ? "selected" : null; ?> value="ta-anim-wobble-hor"><?php _e("Wobble Horizontal", "team-awesome"); ?></option>
                            <option <?php echo $options["socanim"] == "ta-anim-wobble-ver" ? "selected" : null; ?> value="ta-anim-wobble-ver"><?php _e("Wobble Vertical", "team-awesome"); ?></option>
                            <option <?php echo $options["socanim"] == "ta-anim-wobble-br" ? "selected" : null; ?> value="ta-anim-wobble-br"><?php _e("Wobble Bottom Right", "team-awesome"); ?></option>
                            <option <?php echo $options["socanim"] == "ta-anim-wobble-tr" ? "selected" : null; ?> value="ta-anim-wobble-tr"><?php _e("Wobble Top Right", "team-awesome"); ?></option>
                            <option <?php echo $options["socanim"] == "ta-anim-wobble-t" ? "selected" : null; ?> value="ta-anim-wobble-t"><?php _e("Wobble Top", "team-awesome"); ?></option>
                            <option <?php echo $options["socanim"] == "ta-anim-wobble-b" ? "selected" : null; ?> value="ta-anim-wobble-b"><?php _e("Wobble Bottom", "team-awesome"); ?></option>
                            <option <?php echo $options["socanim"] == "ta-anim-wobble-c" ? "selected" : null; ?> value="ta-anim-wobble-c"><?php _e("Wobble Center", "team-awesome"); ?></option>
                            <option <?php echo $options["socanim"] == "ta-anim-buzz" ? "selected" : null; ?> value="ta-anim-buzz"><?php _e("Buzz", "team-awesome"); ?></option>
                            <option <?php echo $options["socanim"] == "random" ? "selected" : null; ?> value="random"><?php _e("Random Animation", "team-awesome"); ?></option>
                        </select>
                        <p>(<?php _e("Social buttons animation on mouse hover", "team-awesome"); ?>)</p>
                    </div>
                    

                    <!--shortdesc-->
                    <div class="opt">
                        <span><?php _e("Short Description", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Optional", "team-awesome"); ?>)</span><br />
                        <select name="shortdesc" value="<?php echo $options["cats"]; ?>">
                            <option <?php echo $options["shortdesc"] == "hide" ? "selected" : null; ?> value="hide"><?php _e("Hide", "team-awesome"); ?></option>
                            <option <?php echo $options["shortdesc"] == "show" ? "selected" : null; ?> value="show"><?php _e("Show", "team-awesome"); ?></option>
                        </select>
                        <p>(<?php _e("Short Description", "team-awesome"); ?>)</p>
                    </div>





                    <!--possible separate section--> 






                    <!--bgcolor-->
                    <!--bgimage-->
                    <!--parallax-->
                    <div class="opt">
                        <div class="remove-bg"><i class="fa fa-times"></i></div>
                        <span><?php _e("Plugin Background", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Optional", "team-awesome"); ?>)</span><br />
                        <input name="bgcolor" type="text" class="short team-awesome-color-picker" value="<?php echo $options["bgcolor"]; ?>" />
                        <input class="short" name="bgimage" type="text" value="<?php echo $options["bgimage"]; ?>" />
                        <select class="short" name="parallax" value="<?php echo $options["cats"]; ?>">
                            <option <?php echo $options["parallax"] == "disable" ? "selected" : null; ?> value="disable"><?php _e("Normal", "team-awesome"); ?></option>
                            <option <?php echo $options["parallax"] == "enable" ? "selected" : null; ?> value="enable"><?php _e("Parallax", "team-awesome"); ?></option>
                        </select>
                        <p>(<?php _e("Color, Image, Parallax Effect", "team-awesome"); ?>)</p>
                    </div>


                    <!--titlefont-->
                    <!--titlesize-->
                    <!--titlecolor-->
                    <div class="opt">
                        <span><?php _e("Plugin Title Font", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <select required class="short" name="titlefont">
                            <?php foreach ($fontsArray as $key => $font) : ?>
                                <option value="<?php echo $key; ?>" <?php if ($options["titlefont"] == $font) echo 'selected="selected"'; ?>><?php echo $font; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select required name="titlesize" class="short">
                            <?php
                            for ($i = 10; $i <= 99; $i++) {
                                ?>
                                <option <?php echo $options["titlesize"] == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i . "px"; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input required class="short" name="titleline" type="text" value="<?php echo $options["titleline"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="titlecolor" type="text" value="<?php echo $options["titlecolor"]; ?>" />
                        <p>(<?php _e("Font-Family, Font-Size, Line-Height, Color", "team-awesome"); ?>)</p>
                    </div>

                    <!--catfont-->
                    <!--catsize-->
                    <!--catcolor-->
                    <div class="opt">
                        <span><?php _e("Categories Font", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <select required class="short" name="catfont">
                            <?php foreach ($fontsArray as $key => $font) : ?>
                                <option value="<?php echo $key; ?>" <?php if ($options["catfont"] == $font) echo 'selected="selected"'; ?>><?php echo $font; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select required name="catsize" class="short">
                            <?php
                            for ($i = 10; $i <= 99; $i++) {
                                ?>
                                <option <?php echo $options["catsize"] == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i . "px"; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input required class="short" name="catline" type="text" value="<?php echo $options["catline"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="catcolor" type="text" value="<?php echo $options["catcolor"]; ?>" />
                        <p>(<?php _e("Font-Family, Font-Size, Line-Height, Color", "team-awesome"); ?>)</p>
                    </div>
                    
                    <div class="opt">
                        <span><?php _e("Active Category Color", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Optional!", "team-awesome"); ?>)</span><br />
                        <input class="short team-awesome-color-picker" name="catactivecolor" type="text" value="<?php echo $options["catactivecolor"]; ?>" />
                        <p>(<?php _e("Active Category Color", "team-awesome"); ?>)</p>
                    </div>

                    <!--pnamefont-->
                    <!--pnamesize-->
                    <!--pnamecolor-->
                    <div class="opt">
                        <span><?php _e("Person Name Font", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <select required class="short" name="pnamefont">
                            <?php foreach ($fontsArray as $key => $font) : ?>
                                <option value="<?php echo $key; ?>" <?php if ($options["pnamefont"] == $font) echo 'selected="selected"'; ?>><?php echo $font; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select required name="pnamesize" class="short">
                            <?php
                            for ($i = 10; $i <= 99; $i++) {
                                ?>
                                <option <?php echo $options["pnamesize"] == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i . "px"; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input required class="short" name="pnameline" type="text" value="<?php echo $options["pnameline"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="pnamecolor" type="text" value="<?php echo $options["pnamecolor"]; ?>" />
                        <p>(<?php _e("Font-Family, Font-Size, Line-Height, Color", "team-awesome"); ?>)</p>
                    </div>
                    
                    <div class="opt">
                        <span><?php _e("Divider", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Optional!", "team-awesome"); ?>)</span><br />
                        <select class="short" name="divider" value="<?php echo $options["divider"]; ?>">
                            <option <?php echo $options["divider"] == "disable" ? "selected" : null; ?> value="disable"><?php _e("Disable", "team-awesome"); ?></option>
                            <option <?php echo $options["divider"] == "enable" ? "selected" : null; ?> value="enable"><?php _e("Enable", "team-awesome"); ?></option>
                        </select>
                        <input required class="short" name="dividert" type="text" value="<?php echo $options["dividert"]; ?>" />
                        <input required class="short" name="dividerw" type="text" value="<?php echo $options["dividerw"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="dividerc" type="text" value="<?php echo $options["dividerc"]; ?>" />
                        <p>(<?php _e("Enable/Disable, Thichkness (px), Width (px), Color", "team-awesome"); ?>)</p>
                    </div>

                    <!--pposfont-->
                    <!--ppossize-->
                    <!--pposcolor-->
                    <div class="opt">
                        <span><?php _e("Person Occupation Font", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <select required class="short" name="pposfont">
                            <?php foreach ($fontsArray as $key => $font) : ?>
                                <option value="<?php echo $key; ?>" <?php if ($options["pposfont"] == $font) echo 'selected="selected"'; ?>><?php echo $font; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select required class="short" name="ppossize" class="short">
                            <?php
                            for ($i = 10; $i <= 99; $i++) {
                                ?>
                                <option <?php echo $options["ppossize"] == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i . "px"; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input required class="short" name="pposline" type="text" value="<?php echo $options["pposline"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="pposcolor" type="text" value="<?php echo $options["pposcolor"]; ?>" />
                        <p>(<?php _e("Font-Family, Font-Size, Line-Height, Color", "team-awesome"); ?>)</p>
                    </div>

                    <!--pshortdescfont-->
                    <!--pshortdescsize-->
                    <!--pshortdesccolor-->
                    <div class="opt">
                        <span><?php _e("Person Short Description Font", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <select required class="short" name="pshortdescfont">
                            <?php foreach ($fontsArray as $key => $font) : ?>
                                <option value="<?php echo $key; ?>" <?php if ($options["pshortdescfont"] == $font) echo 'selected="selected"'; ?>><?php echo $font; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select required class="short" name="pshortdescsize" class="short">
                            <?php
                            for ($i = 10; $i <= 99; $i++) {
                                ?>
                                <option <?php echo $options["pshortdescsize"] == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i . "px"; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input required class="short" name="pshortdescline" type="text" value="<?php echo $options["pshortdescline"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="pshortdesccolor" type="text" value="<?php echo $options["pshortdesccolor"]; ?>" />
                        <p>(<?php _e("Font-Family, Font-Size, Line-Height, Color", "team-awesome"); ?>)</p>
                    </div>

                    <!--pagibg-->
                    <!--pagiico-->
                    <!--pagibr-->
                    <div class="opt">
                        <span><?php _e("Slider Pagination Buttons", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <input required class="short team-awesome-color-picker" name="pagibg" type="text" value="<?php echo $options["pagibg"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="pagiico" type="text" value="<?php echo $options["pagiico"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="pagibr" type="text" value="<?php echo $options["pagibr"]; ?>" />
                        <p>(<?php _e("Background, Icon Color, Border Color", "team-awesome"); ?>)</p>
                    </div>

                    <!--pagibghover-->
                    <!--pagiicohover-->
                    <!--pagibrhover-->
                    <div class="opt">
                        <span><?php _e("Slider Pagination Buttons HOVER", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <input required class="short team-awesome-color-picker" name="pagibghover" type="text" value="<?php echo $options["pagibghover"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="pagiicohover" type="text" value="<?php echo $options["pagiicohover"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="pagibrhover" type="text" value="<?php echo $options["pagibrhover"]; ?>" />
                        <p>(<?php _e("Background, Icon Color, Border Color", "team-awesome"); ?>)</p>
                    </div>


                    <!--skillstitlefont-->
                    <!--skillstitlesize-->
                    <!--skillstitlecolor-->
                    <div class="opt">
                        <span><?php _e("Skills Title Font", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <select required class="short" name="skillstitlefont">
                            <?php foreach ($fontsArray as $key => $font) : ?>
                                <option value="<?php echo $key; ?>" <?php if ($options["skillstitlefont"] == $font) echo 'selected="selected"'; ?>><?php echo $font; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select required class="short" name="skillstitlesize" class="short">
                            <?php
                            for ($i = 10; $i <= 99; $i++) {
                                ?>
                                <option <?php echo $options["skillstitlesize"] == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i . "px"; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input required class="short" name="skillstitleline" type="text" value="<?php echo $options["skillstitleline"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="skillstitlecolor" type="text" value="<?php echo $options["skillstitlecolor"]; ?>" />
                        <p>(<?php _e("Font-Family, Font-Size, Line-Height, Color", "team-awesome"); ?>)</p>
                    </div>


                    <!--skillsprctfont-->
                    <!--skillsprctsize-->
                    <!--skillsprctcolor-->
                    <div class="opt">
                        <span><?php _e("Skills Percents Font", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <select required class="short" name="skillsprctfont">
                            <?php foreach ($fontsArray as $key => $font) : ?>
                                <option value="<?php echo $key; ?>" <?php if ($options["skillsprctfont"] == $font) echo 'selected="selected"'; ?>><?php echo $font; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select required class="short" name="skillsprctsize" class="short">
                            <?php
                            for ($i = 10; $i <= 99; $i++) {
                                ?>
                                <option <?php echo $options["skillsprctsize"] == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i . "px"; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input required class="short" name="skillsprctline" type="text" value="<?php echo $options["skillsprctline"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="skillsprctcolor" type="text" value="<?php echo $options["skillsprctcolor"]; ?>" />
                        <p>(<?php _e("Font-Family, Font-Size, Line-Height, Color", "team-awesome"); ?>)</p>
                    </div>

                    <!--skillsbarbg-->
                    <!--skillsbarfill-->

                    <div class="opt">
                        <span><?php _e("Skills Bar Background", "team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Required!", "team-awesome"); ?>)</span><br />
                        <input required class="short team-awesome-color-picker" name="skillsbarbg" type="text" value="<?php echo $options["skillsbarbg"]; ?>" />
                        <input required class="short team-awesome-color-picker" name="skillsbarfill" type="text" value="<?php echo $options["skillsbarfill"]; ?>" />
                        <p>(<?php _e("Bar Color, Fill Color", "team-awesome"); ?>)</p>
                    </div>


                </div>

                <button><?php _e("Save Settings", "team-awesome"); ?></button>
                <input type="hidden" name="task" value="save" />
            </form>

            <form method="POST">
                <button><?php _e("Reset Defaults", "team-awesome"); ?></button>
                <input type="hidden" name="task" value="reset" />
            </form>

            <button onclick="window.location.href = '?page=<?php echo $plugin_page; ?>';"><?php _e("Cancel", "team-awesome"); ?></button>

        </div>
    </div>
</div>