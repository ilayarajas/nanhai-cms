<?php
// levels controller
class WatuPROPlayLevels {
	static function manage() {
		global $wpdb;
		$_level = new WatuPROPlayLevel();
		
		// select exam categories
		$cats = $wpdb->get_results("SELECT * FROM ".WATUPRO_CATS." ORDER BY name");
		$_POST['atype']='level';
			
		$action = empty($_GET['action']) ? 'list' : $_GET['action'];
		switch($action) {
			case 'add':
				if(!empty($_POST['ok'])) {
					$_level -> add($_POST);
					watuproplay_redirect('admin.php?page=watuproplay_levels');
				}
				
				// select question categories
				$qcats = $wpdb->get_results("SELECT * FROM ".WATUPRO_QCATS." ORDER BY name");
				require(WATUPROPLAY_PATH."/views/level.html.php");
			break;
			
			case 'edit':
				if(!empty($_POST['del'])) {
					$_level -> delete($_GET['id']);
					watuproplay_redirect('admin.php?page=watuproplay_levels');
				}			
			
				if(!empty($_POST['ok'])) {
					$_level->save($_POST, $_GET['id']);
					watuproplay_redirect('admin.php?page=watuproplay_levels');
				}
				
				// select this level
				$level = $wpdb -> get_row( $wpdb->prepare("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE id=%d", $_GET['id']));
				
				// select question categories
				$qcats = $wpdb->get_results("SELECT * FROM ".WATUPRO_QCATS." ORDER BY name");
				require(WATUPROPLAY_PATH."/views/level.html.php");
			break;	
			
			case 'users':
				// view users with a given level
				$level = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE id=%d", $_GET['id']));
				$offset = empty($_GET['offset']) ? 0 : intval($_GET['offset']);
				$limit = 50;
				$args = array(
					'meta_key'     => 'watuproplay_user_level',
					'meta_value'   => intval($_GET['id']),			
					'offset' => $offset,
					'number' => $limit,
					'count_total'=> true		
				);
				$user_query = new WP_User_Query( $args );
				$users = $user_query->get_results();
				$cnt_users = $user_query->get_total();
				
				include(WATUPROPLAY_PATH."/views/level-users.html.php");
			break;		
			
			case 'list':
				// select currently available levels
				$levels = $wpdb -> get_results("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE atype='level' ORDER BY rank");	
				
				// save the email notice settings
				if(!empty($_POST['save_admin_level_email'])) {
					update_option('watuproplay_admin_level_email', $_POST['admin_level_email']);
					update_option('watuproplay_admin_level_subject', $_POST['admin_level_subject']);
					update_option('watuproplay_admin_level_message', $_POST['admin_level_message']);
				}		
				
				if(!empty($_POST['save_user_level_email'])) {
					update_option('watuproplay_user_level_email', $_POST['user_level_email']);
					update_option('watuproplay_user_level_subject', $_POST['user_level_subject']);
					update_option('watuproplay_user_level_message', $_POST['user_level_message']);
				}					
			
				require(WATUPROPLAY_PATH."/views/levels.html.php");
			default:
			break;
		}
	} // end manage()
	
	// displays options to restrict quiz to user levels
	static function restrict_by_level_form($play_levels = null) {
		global $wpdb;
		
		// select levels
		$levels = $wpdb -> get_results("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE atype='level' ORDER BY rank");
		
		if(count($levels)) include(WATUPROPLAY_PATH . "/views/restrict-by-level.html.php");
	} // end quiz_form_login_required
	
	// can user access this exam? (restrictions by level)
	static function can_access($exam) {
		global $wpdb, $user_ID;
		
		// unserialize advanced settings to get levels
		$advanced_settings = unserialize(stripslashes($exam->advanced_settings));
		
		// if the quiz is restricted to levels, compare to user's
		if(!empty($advanced_settings['play_levels'])) {
			// get user level
			$level_id = get_user_meta($user_ID, 'watuproplay_user_level', true);
			
			if(!in_array($level_id, $advanced_settings['play_levels'])) {
				$levels_str = '';
				$levels = $wpdb -> get_results("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE atype='level' ORDER BY rank");
				
				foreach($levels as $cnt => $level) {
					if(in_array($level->id, $advanced_settings['play_levels'])) {
						if($cnt) $levels_str .= ', ';
						$levels_str .= stripslashes($level->name);
					} 
				}				
				
				printf(__('This quiz is accessible only to users with the following levels: <b>%s</b>', 'watuproplay'), $levels_str);
				WatuPRO::$output_sent = true;
				return false;
			}
		}
		
		return true;
	} // end can_access
	
	// display field to edit user level
	static function edit_profile($user) {
		global $wpdb;
		
		if(!current_user_can(WATUPRO_MANAGE_CAPS)) return false;
		
		$levels = $wpdb->get_results("SELECT * FROM ".WATUPROPLAY_LEVELS. " WHERE atype='level' ORDER BY rank");
		
		$user_level = get_user_meta($user->ID, "watuproplay_user_level", true);
		?>		
		<table class="form-table">
	    <tr>
	      <th><label for="phone"><?php _e("Watu PRO User Level", 'watuproplay'); ?></label></th>
	      <td>
	      	<select name="watuproplay_level">
	      	<option>-------------------</option>
	      	<?php foreach($levels as $level):
	      	if($level->id == $user_level) $selected="selected";
	      	else $selected="";?>
	      		<option value="<?php echo $level->id?>" <?php echo $selected;?>><?php echo stripslashes($level->name)?></option>
	      	<?php endforeach;?>
	      	</select> 
	    </td>
	    </tr>
	   </table> 
		<?php 
	} // end edit_profile
	
	// save the change to the user level
	static function save_edit_profile($user_id) {
		if(!current_user_can(WATUPRO_MANAGE_CAPS)) return false;
		
		 update_user_meta( $user_id, 'watuproplay_user_level', @$_POST['watuproplay_level'] );
		 
		 return true;		 
	} // end save_edit_profile
}