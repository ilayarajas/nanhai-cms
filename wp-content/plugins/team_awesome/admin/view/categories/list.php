<?php global $plugin_page; ?>
<div class="admin-wrapper">
    <div class="admin-section">
        <div class="admin-section-header">
            <h2><?php _e("Category List","team-awesome");?></h2>
            <p><?php _e("Create / Edit existing Category","team-awesome");?></p>
        </div>
        <div class="admin-section-body fullwidth">
            <button onclick="window.location.href = '?page=<?php echo $plugin_page; ?>&view=addedit';"><?php _e("Create New Category","team-awesome");?></button>
            <form method="POST">
                <button class="categories-reorder"><?php _e("Update Category Order","team-awesome");?></button>
            </form>
            <ul class="list no-touch">
                <?php $categories = TEAM_AWESOME_ADMIN::get_categories(); ?>
                <?php foreach ($categories as $category): ?>
                
                <li id="category-<?php echo $category->id; ?>">
                    <p><?php echo $category->title;?><span>(ID#<?php echo $category->id;?> <?php _e("Order","team-awesome"); ?>)</span></span></p>
                    <button onclick="window.location.href = '?page=<?php echo $plugin_page; ?>&view=list&task=delete&catid=<?php echo $category->id; ?>'"><?php _e("Delete","team-awesome");?></button>
                    <button onclick="window.location.href = '?page=<?php echo $plugin_page; ?>&view=addedit&catid=<?php echo $category->id;?>';"><?php _e("Edit","team-awesome");?></button>
                    <input autocomplete="off" type="text" name="order" value="<?php echo $category->ordering; ?>" />
                </li>
                <?php endforeach; ?>
            </ul>

            <ul class="pagination">
                <?php
                TEAM_AWESOME_ADMIN::categories_pagination();
                ?>
            </ul>

        </div>
    </div>
</div>