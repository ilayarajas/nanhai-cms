<?php

class TEAM_AWESOME_ADMIN {

    static $loaded_includes = false;
    static $google_fonts = false;
    static $cat_per_page = 10;
    static $mem_per_page = 10;

    public static function load_includes() {
        if (self::$loaded_includes === true) {
            return;
        }
        
        wp_enqueue_script("jquery");
        wp_enqueue_media();
        wp_enqueue_style("font-awesome", "http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css");
        wp_enqueue_style("team-awesome", plugins_url("/admin/css/admin.css", TEAM_AWESOME__FILE__));
        wp_enqueue_style("team-awesome-cp-css", plugins_url("admin/helpers/colorpicker/css/colorpicker.css", TEAM_AWESOME__FILE__));
        wp_enqueue_script("team-awesome-cp", plugins_url("/admin/helpers/colorpicker/js/colorpicker.js", TEAM_AWESOME__FILE__), array("jquery"));
        wp_enqueue_script("team-awesome-js", plugins_url("/admin/js/admin.js", TEAM_AWESOME__FILE__), array("jquery"));

        self::$loaded_includes = true;
    }

    public static function get_google_fonts() {
        if (!self::$google_fonts) {
            $file = file_get_contents(dirname(TEAM_AWESOME__FILE__) . DS . "admin" . DS . "fonts" . DS . "google.fonts.json");
            $json = json_decode($file);
            $fonts = $json->items;
            self::$google_fonts = array();

            // Add default
            self::$google_fonts["default"] = __("Use Default", "team-awesome");

            foreach ($fonts as $font) {
                $key = $font->family;
                self::$google_fonts[$key] = $key;

                $variants = $font->variants;

                foreach ($variants as $var) {
                    if ($var != "regular") {
                        $key = $font->family . ":" . $var;
                        self::$google_fonts[$key] = $key;
                    }
                }
            }
        }

        return self::$google_fonts;
    }

    public static function set_defaults() {

        $defaults = array(
            "boxwidth" => "320",
            "boxheight" => "480",
            "boxspacing" => "40",
            "popwidth" => "415",
            "popheight" => "700",
            "title" => "Team Awesome",
            "cats" => "enable",
            "catsalign" => "center",
            "slider" => "enable",
            "perpage" => "3",
            "pop" => "enable",
            "avatar" => "circle",
            "avatarbc" => "#ffffff",
            "avatarb" => "on",
            "socanim" => "random",
            "shortdesc" => "show",
            "bgcolor" => "none",
            "bgimage" => "",
            "parallax" => "disable",
            "titlefont" => "Questrial",
            "titlesize" => "24",
            "titleline" => "1",
            "titlecolor" => "#202020",
            "catfont" => "Lato",
            "catsize" => "10",
            "catline" => "2.5",
            "catcolor" => "#747474",
            "catactivecolor" => "#ff2a00",
            "pnamefont" => "Roboto",
            "pnamesize" => "16",
            "pnameline" => "1",
            "pnamecolor" => "#202020",
            "pposfont" => "Lato",
            "ppossize" => "14",
            "pposline" => "1",
            "pposcolor" => "#747474",
            "pshortdescfont" => "Lato",
            "pshortdescsize" => "18",
            "pshortdescline" => "2",
            "pshortdesccolor" => "#656565",
            "pagibg" => "none",
            "pagiico" => "#bfbfbf",
            "pagibr" => "#bfbfbf",
            "pagibghover" => "#ffffff",
            "pagiicohover" => "#ee4444",
            "pagibrhover" => "#ee4444",
            "skillstitlefont" => "Lato",
            "skillstitlesize" => "14",
            "skillstitleline" => "1",
            "skillstitlecolor" => "#747474",
            "skillsprctfont" => "Lato",
            "skillsprctsize" => "14",
            "skillsprctline" => "1",
            "skillsprctcolor" => "#ababab",
            "skillsbarbg" => "#dfdfdf",
            "skillsbarfill" => "#202020",
            "divider" => "enable",
            "dividert" => "2",
            "dividerw" => "120",
            "dividerc" => "#ff2a00"
            
        );

        update_option("team-awesome", $defaults);
    }

    public static function get_options() {

        $options = get_option("team-awesome");
        
        return $options;
    }

    public static function reset_options() {
        self::set_defaults();
    }

    public static function get_var($var, $escape_html = false, $escape_sql = false) {

        if (!$var) {
            return;
        }

        $value = isset($_REQUEST[$var]) ? $_REQUEST[$var] : "";

        // escape html
        if ($escape_html) {
            $value = htmlspecialchars($value);
        }

        // escape mysql
        if ($escape_sql) {
            $value = mysql_real_escape_string($value);
        }

        return $value;
    }

    public static function save_options() {

        $options = get_option("team-awesome");
        $save = array();

        foreach ($options as $key => $value) {
            $newval = self::get_var($key);
            $save[$key] = isset($newval) ? $newval : $value;
        }

        update_option("team-awesome", $save);
    }
    
    public static function sortByOrder($a, $b) {
        return $a->ordering - $b->ordering;
    }

    public static function get_categories($nopages = false) {

        $categories = get_option("team-awesome-categories", array());

        usort($categories, array('TEAM_AWESOME_ADMIN','sortByOrder'));
        
        if ($nopages) {
            return stripslashes_deep($categories);
        }
        
        $perpage = self::$cat_per_page;
        $cpage   = (int) self::get_var("paged") - 1;
        $offset  = $perpage * $cpage;
        
        if ($offset < 0) {
            $offset = 0;
        }
        
        $categories = stripslashes_deep($categories);
        
        return array_slice($categories, $offset, $perpage);

    }

    public static function get_category() {

        $id = self::get_var("catid");
        
        if (!$id) return false;

        $categories = get_option("team-awesome-categories", array());

        foreach ($categories as $key => $cat) {
            if ($cat->id == $id) {
                return stripslashes_deep($cat);
            }
        }
    }

    public static function create_category() {

        $title  = self::get_var("title", true);
        $update = self::get_var("update");
        
        // update expects integer
        $update = (int) $update;
        
        $categories = get_option("team-awesome-categories", array());

        if ($update) {
            
            $id = $update;

            foreach ($categories as $key => $cat) {
                if ($cat->id == $id) {
                    $categories[$key]->title = $title;
                }
            }
        } else {
            $id = 1;

            if (!empty($categories)) {
                $id = end($categories);
                $id = $id->id + 1;
            }

            $categories[] = (object) array(
                        "id"       => $id,
                        "title"    => $title,
                        "ordering" => "0"
            );
        }


        update_option("team-awesome-categories", $categories);
    }

    public static function delete_category() {

        $id = self::get_var("catid");

        $categories = get_option("team-awesome-categories", array());

        foreach ($categories as $key => $cat) {
            if ($cat->id == $id) {
                unset($categories[$key]);
            }
        }
        
        // update new category list
        update_option("team-awesome-categories", $categories);
        
        
        // delete category from member->catid
        $members = get_option("team-awesome-members", array());
        
        $update = false; 
        
        foreach ($members as $key => $member) {
            
            $cats = $member->catid;
            
            if (($key = array_search($id, $cats)) !== false) {
                unset($cats[$key]);
                $update = true;
            }
            
            $members[$key]->catid = $cats;
            
        }
        
        // update if changes occured
        if ($update) {
            update_option("team-awesome-members", $members);
        }
        
    }

    public static function order_categories() {

        $order = self::get_var("order");

        if (empty($order) || !is_array($order)) {
            return;
        }

        $categories = get_option("team-awesome-categories", array());

        foreach ($order as $k) {
            $k = explode(":", $k);
            $p = (int) $k[0];
            $o = (int) $k[1];

            foreach ($categories as $key => $category) {
                if ($category->id == $p) {
                    $categories[$key]->ordering = $o;
                }
            }
        }

        update_option("team-awesome-categories", $categories);
    }
    
    /* members below */
    
    public static function get_members($nopages = false) {
        
        $members = get_option("team-awesome-members", array());
        
        if (empty($members)) {
            return $members;
        }

        usort($members, array('TEAM_AWESOME_ADMIN','sortByOrder'));
        
        if ($nopages) {
            return stripslashes_deep($members);
        }
        
        $perpage = self::$mem_per_page;
        $cpage   = (int) self::get_var("paged") - 1;
        $offset  = $perpage * $cpage;
        
        if ($offset < 0) {
            $offset = 0;
        }
        
        $members = stripslashes_deep($members);
        
        return array_slice($members, $offset, $perpage);
        
    }

    public static function get_member() {

        $id = self::get_var("id");
        
        if (!$id) {
            return false;
        }

        $members = get_option("team-awesome-members", array());

        foreach ($members as $key => $member) {
            if ($member->id == $id) {
                return stripslashes_deep($member);
            }
        }
    }

    public static function create_member() {

        $title       = self::get_var("title", true);
        $catid       = self::get_var("catid"); // ARRAY
        $avatar      = self::get_var("avatar", true);
        $vidavatar   = self::get_var("vidavatar", true);
        $occupation  = self::get_var("occupation", true);
        $short       = self::get_var("short", true);
        $long        = self::get_var("long", true);
        
        $skills      = self::get_var("skills"); // ARRAY
        $social      = self::get_var("social"); // ARRAY
        
        
        // cat must be array even empty
        if (!is_array($catid)) {
            $catid = array();
        }
        
        // filter avatar src
        $avatar = esc_url($avatar);
        
        
        // filter vidavatar src
        $vidavatar = esc_url($vidavatar);
        
        // filter cat ids
        foreach($catid as $k => $cat) {
            $catid[$k] = (int)$cat;
        }
        
        // filter skills
        foreach($skills as $k => $skill) {
            if ($skill["title"] != "") {
                $skills[$k]["title"] = htmlspecialchars($skill["title"]);
                $skills[$k]["percents"] = $skill["percents"];
            }
        }
        
        // filter social
        foreach($social as $k => $soc) {
            $social[$k]["icon"] = htmlspecialchars($soc["icon"]);
            $social[$k]["link"] = ($soc["link"]); // esc_url strips illegal skype links
        }

        $update = self::get_var("update");

        $members = get_option("team-awesome-members", array());

        if ($update) {
            $id = $update;

            foreach ($members as $key => $member) {
                if ($member->id == $id) {
                    $members[$key]->title      = $title;
                    $members[$key]->catid      = $catid;
                    $members[$key]->avatar     = $avatar;
                    $members[$key]->vidavatar  = $vidavatar;
                    $members[$key]->occupation = $occupation;
                    $members[$key]->short      = $short;
                    $members[$key]->long       = $long;
                    $members[$key]->skills     = $skills;
                    $members[$key]->social     = $social;
                    
                }
            }
        } else {
            $id = 1;

            if (!empty($members)) {
                $id = end($members);
                $id = $id->id + 1;
            }

            $members[] = (object) array(
                "id"         => $id,
                "title"      => $title,
                "catid"      => $catid,
                "avatar"     => $avatar,
                "vidavatar"  => $vidavatar,
                "occupation" => $occupation,
                "short"      => $short,
                "long"       => $long,
                "skills"     => $skills,
                "social"     => $social,
                
                "ordering"   => "0"
            );
        }


        update_option("team-awesome-members", $members);
    }

    public static function delete_member() {

        $id = self::get_var("id");
        
        $id = (int) $id;
        
        if (!$id) {
            return;
        }
        
        $members = get_option("team-awesome-members", array());

        foreach ($members as $key => $member) {
            if ($member->id == $id) {
                unset($members[$key]);
            }
        }
        
        update_option("team-awesome-members", $members);
    }

    public static function order_members() {

        $order = self::get_var("order");

        if (empty($order) || !is_array($order)) {
            return;
        }

        $members = get_option("team-awesome-members", array());

        foreach ($order as $k) {
            $k = explode(":", $k);
            $p = (int) $k[0];
            $o = (int) $k[1];

            foreach ($members as $key => $member) {
                if ($member->id == $p) {
                    $members[$key]->ordering = $o;
                }
            }
        }

        update_option("team-awesome-members", $members);
    }
    
    public static function categories_pagination() {
        global $plugin_page;
        
        $cats  = get_option("team-awesome-categories", array());
        $cats  = count($cats);
        $cpp   = self::$cat_per_page;
        $pages = ceil($cats / $cpp);
        
        $cpage = (int) TEAM_AWESOME_ADMIN::get_var("paged");
        
        if (!$cpage) {
            $cpage = 1;
        }
        
        if ($pages <= 1) {
            return;
        }
        
        for ($i = 1; $i <= $pages; $i++) :
            $kl = $cpage == $i ? "active" : "";
            $href = "?page=$plugin_page&paged=$i";
            ?>
            <li class="<?php echo $kl; ?>"><a href="<?php echo $href; ?>"><?php echo $i; ?></a></li>
            <?php
        endfor;
    }
    
    public static function members_pagination() {
        global $plugin_page;
        
        $members  = get_option("team-awesome-members", array());
        $members  = count($members);
        $mpp   = self::$mem_per_page;
        $pages = ceil($members / $mpp);
        
        $cpage = (int) TEAM_AWESOME_ADMIN::get_var("paged");
        
        if (!$cpage) {
            $cpage = 1;
        }
        
        if ($pages <= 1) {
            return;
        }

        for ($i = 1; $i <= $pages; $i++) :
            $kl = $cpage == $i ? "active" : "";
            $href = "?page=$plugin_page&paged=$i";
            ?>
            <li class="<?php echo $kl; ?>"><a href="<?php echo $href; ?>"><?php echo $i; ?></a></li>
            <?php
        endfor;
    }

}
