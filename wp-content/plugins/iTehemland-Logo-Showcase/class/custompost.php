<?php
	if ( ! function_exists( 'it_logo_showcase_register_custom_post' ) )
	{

		add_action('init', 'it_logo_showcase_register_custom_post');
		function it_logo_showcase_register_custom_post() {
			$args = array(
				'description' => __('Project Name',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
				'show_ui' => true,
				'show_in_menu' => true,
				'show_in_nav_menus' => true,
				'exclude_from_search' => true,
				'menu_icon' => 'dashicons-schedule',
				'labels' => array(
					'name'=> __('Logos',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'singular_name' => __('Logo',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
                    'all_items' => __('All Logos',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'add_new' => __('Add Logo',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'add_new_item' => __('Add Logo',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'edit' => __('Edit Logo',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'edit_item' => __('Edit Logo',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'new-item' => 'New Logo',
					'view' => __('View Logo',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'view_item' => __('View Logo',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'search_items' => __('Search Logo',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'not_found' => __('No Logo Found',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'not_found_in_trash' => __('No Logo Found in Trash',__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
					'parent' => __('Parent Logo',__IT_LOGO_SHOWCASE_TEXTDOMAIN__)
				),
				'public' => true,
                'publicly_queriable' => true,
				'capability_type' => 'post',
				'hierarchical' => false,
				'rewrite' => false,
				'supports' => array('title','thumbnail' ,'page-attributes'),
				'has_archive' => false,
			);
		
			register_post_type(  __Custom_Post__  , $args );
		}
		
		
		add_action( 'init', 'it_create_taxonomy' );
		function it_create_taxonomy() {
			$labels = array(
			  'name'                       => __('Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__ ),
			  'singular_name'              => __('Category', __IT_LOGO_SHOWCASE_TEXTDOMAIN__ ),
			  'search_items'               => __( 'Search Categories', __IT_LOGO_SHOWCASE_TEXTDOMAIN__ ),
			  'popular_items'              => __( 'Popular Categories' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			  'all_items'                  => __( 'All Categories' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			  'parent_item'                => null,
			  'parent_item_colon'          => null,
			  'edit_item'                  => __( 'Edit Category' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			  'update_item'                => __( 'Update Category' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			  'add_new_item'               => __( 'Add New Category' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			  'new_item_name'              => __( 'New Category Name' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			  'separate_items_with_commas' => __( 'Separate Categories with commas' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			  'add_or_remove_items'        => __( 'Add or remove Categories' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			  'choose_from_most_used'      => __( 'Choose from the most used Categories' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			  'not_found'                  => __( 'No Categories found.' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			  'menu_name'                  => __( 'Categories' , __IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			 );
		
			 $args = array(
			 	'hierarchical' => true,
				'labels' => $labels,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => array('slug' => 'category' )
			 );
		
			register_taxonomy( __Custom_Post_Taxonomy__, __Custom_Post__ , $args );
		}
		
	}

    // add thumbnail and order columns in custom post edit page
    add_filter( 'manage_posts_columns', 'it_logo_showcase_columns_head' );
    add_action( 'manage_posts_custom_column', 'it_logo_showcase_columns_content', 10, 2 );
    function it_logo_showcase_columns_head($defaults)
    {
        global $post;
        if (isset($_GET['post_type']) && ($_GET['post_type'] == __Custom_Post__ )) {
            $new = array();
            if(!isset($_GET['order']) || $_GET['order']=='desc' )
                $order = 'asc';
            else if( $_GET['order']=='asc' )
                $order = 'desc';

            foreach($defaults as $key => $title) {
                if($key == 'title')
                    $new['featured_image'] = __("Image" ,__IT_LOGO_SHOWCASE_TEXTDOMAIN__);
                if ($key=='date') // Put the Thumbnail column before the Author column
                    $new[__IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'order'] = '<a href="edit.php?post_type='.__Custom_Post__.'&orderby=menu_order&order='.$order.'">'.__("Order" ,__IT_LOGO_SHOWCASE_TEXTDOMAIN__).'</a>';
                $new[$key] = $title;
            }
            return $new;
        }
        return $defaults;
    }

    // SHOW THE FEATURED IMAGE in admin

    function it_logo_showcase_columns_content($column_name, $post_ID)
    {
        global $post;
        if ($post->post_type == __Custom_Post__) {

            if ($column_name == __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'order') {
                echo $post->menu_order; //note that it won't update the value in the database
            }


            if ($column_name == 'featured_image') {

                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post_ID), 'thumbnail');

                if ($image != false) {
                    echo get_the_post_thumbnail(
                        $post_ID, array(
                        80,
                        80
                    ));
                }
            }
        }
    }
?>