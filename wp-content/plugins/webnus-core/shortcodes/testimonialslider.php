<?php
function webnus_testimonial_slider_shortcode( $attributes, $content = null ) {
	extract( shortcode_atts( array(
		'type'			=> 'mono',
		'process_item'	=> '',
		), $attributes) );

	// process_item loop
	$process_item		= (array) vc_param_group_parse_atts( $process_item );
	$process_item_data	= array();
	foreach ( $process_item as $data ) :
		$new_line 						= $data;
		$new_line['process_name']					= isset( $data['process_name'] ) ? $data['process_name'] : '';
		$new_line['process_img']					= isset( $data['process_img'] ) ? $data['process_img'] : '';
		$new_line['process_bgimg']					= isset( $data['process_bgimg'] ) ? $data['process_bgimg'] : '';
		$new_line['process_subtitle']				= isset( $data['process_subtitle'] ) ? $data['process_subtitle'] : '';
		$new_line['process_testimonial_content']	= isset( $data['process_testimonial_content'] ) ? $data['process_testimonial_content'] : '';
		$new_line['process_first_social']			= isset( $data['process_first_social'] ) ? $data['process_first_social'] : '';
		$new_line['process_first_url']				= isset( $data['process_first_url'] ) ? $data['process_first_url'] : '';
		$new_line['process_second_social']			= isset( $data['process_second_social'] ) ? $data['process_second_social'] : '';
		$new_line['process_second_url']				= isset( $data['process_second_url'] ) ? $data['process_second_url'] : '';
		$new_line['process_third_social']			= isset( $data['process_third_social'] ) ? $data['process_third_social'] : '';
		$new_line['process_third_url']				= isset( $data['process_third_url'] ) ? $data['process_third_url'] : '';
		$new_line['process_fourth_social']			= isset( $data['process_fourth_social'] ) ? $data['process_fourth_social'] : '';
		$new_line['process_fourth_url']				= isset( $data['process_fourth_url'] ) ? $data['process_fourth_url'] : '';
		$new_line['line_flag']						= isset( $data['line_flag'] ) ? $data['line_flag'] : '';
		$process_item_data[]						= $new_line;
	endforeach;

	$out = '
	<div class="testimonials-slider-w ts-'.$type.'">
		<ul id="testimonials-slider" class="slides w-crsl">';
			foreach ( $process_item_data as $line ) :
				if( is_numeric( $line['process_img'] ) )
					$line['process_img'] = wp_get_attachment_url( $line['process_img'] );

				if( is_numeric( $line['process_bgimg'] ) )
					$line['process_bgimg'] = wp_get_attachment_url( $line['process_bgimg'] );
				
				$socialvar = '';
				if( $line['process_first_url'] || $line['process_second_url'] ||  $line['process_third_url'] || $line['process_fourth_url'] ) :
					$socialvar .= '<div class="social-testimonial"><ul>';

				if( !empty( $line['process_first_url'] ) )
					$socialvar .= '<li class="first-social"><a href="'. esc_url($line['process_first_url']) .'"><i class="fa-'. $line['process_first_social'] .'"></i></a></li>';

				if(!empty($line['process_second_url']))
					$socialvar .= '<li class="second-social"><a href="'. esc_url($line['process_second_url']) .'"><i class="fa-'. $line['process_second_social'] .'"></i></a></li>';

				if(!empty($line['process_third_url']))
					$socialvar .= '<li class="third-social"><a href="'. esc_url($line['process_third_url']) .'"><i class="fa-'.  $line['process_third_social'] .'"></i></a></li>';

				if(!empty($line['process_fourth_url']))
					$socialvar .= '<li class="fourth-social"><a href="'. esc_url($line['process_fourth_url']) .'"><i class="fa-'. $line['process_fourth_social'] .'"></i></a></li>';

				$socialvar .= '</ul></div>';
				endif;
				if ( $type == 'octa' ) :
					$out .=
				'<li>
				<div class="testimonial '.$type.'" style="background: url('. $line['process_bgimg'] .') center center; ">
					<div class="testimonial-content">
						<img src="'. $line['process_img'] .'" alt="">
						<h5>'.$line['process_name'].'</h5>
						<h6>'.$line['process_subtitle'].'</h6>	
						<p>'.$line['process_testimonial_content'].'</p>
						' . $socialvar . '
					</div>
				</div>
			</li>';

			elseif ( $type == 'nona' ) :
				$out .=
			'<li>
			<div class="testimonial '.$type.'">
				<div class="testimonial-content clearfix">
					<div class="col-md-2"><img src="'. $line['process_img'] .'" alt=""></div>
					<div class="col-md-10"><p>'.$line['process_testimonial_content'].'</p></div>
				</div>
				<h5>'.$line['process_name'].', <span>'.$line['process_subtitle'].'</span> </h5>
				' . $socialvar . '
			</div>
		</li>';

		elseif ( $type == 'deca' ) :
					$out .=
				'<li>
				<div class="testimonial '.$type.'" style="background: url('. $line['process_bgimg'] .') center center; ">
						<div class="col-sm-4 '.$type.'-leftsection">
							<img src="'. $line['process_img'] .'" alt="">
							<div class="details">
								<span class="colorb"></span>
								<h5>'.$line['process_name'].'</h5>
								<h6>'.$line['process_subtitle'].'</h6>
							</div>
						</div>
						<div class="col-sm-8 '.$type.'-rightsection">
							<p>'.$line['process_testimonial_content'].'</p>
						' . $socialvar . '
						</div>
				</div>
			</li>';
		else :
		$imgvar = '';
		if( !empty( $line['process_img'] ) )
			$imgvar = '<img src="' . $line['process_img'] . '	" alt="' . $line['process_name'] . '">';
		$out .= '<li>
			<div class="testimonial">		  
				<div class="testimonial-content">		 
					<h4><q>' . $line['process_testimonial_content'] . '</q></h4>			
					<div class="testimonial-arrow"></div>			  
				</div>			  
				<div class="testimonial-brand">
					<div class="w-square">' . $imgvar . '</div>
					<h5><strong>' . $line['process_name'] . '</strong><br><em>' . $line['process_subtitle'] . '</em></h5>
					' . $socialvar . '
				</div>
			</div>
		</li>';
		endif;
			endforeach;
			$out .= '
		</ul>
	</div>';

	return $out;

}

add_shortcode( 'testimonial_slider', 'webnus_testimonial_slider_shortcode' );