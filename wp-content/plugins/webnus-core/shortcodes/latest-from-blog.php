<?php
 function michigan_webnus_latestfromblog( $attributes, $content = null ) {
extract(shortcode_atts(	array(
	'type'=>'one',
	'category'=>'',
	'author'=>'',
), $attributes));
	ob_start();

	$post_format = get_post_format(get_the_ID());
	$content = get_the_content();

	$orderby ='';
	// orderby query args
	switch ( $orderby ) :
		case 'comment_count':
			$orderby = '&orderby=comment_count&order=DESC';
		break;

		case 'view_count':
			$orderby = '&meta_key=michigan_webnus_views&orderby=meta_value_num&order=DESC';
		break;

		case 'social_score':
			if ( class_exists( 'SocialMetricsTracker' ) ) {
				$orderby ='&post_type=post&meta_key=socialcount_total&orderby=meta_value_num&order=DESC';
			}
		break;

		default:
			$orderby = '';
		break;
	endswitch;
	
$main_container = ($type == 'eight' || $type == 'nine' || $type == 'ten')  ? 'row' : 'container' ;
?>

<div class="<?php echo $main_container; ?> latestposts-<?php echo $type ?>">
<?php
	if ($type=='one'){
			$query = new WP_Query('posts_per_page=2&category_name='.$category.$orderby.'');
			while ($query -> have_posts()) : $query -> the_post();
?>
	<div class="col-md-6 col-sm-6"><article class="latest-b"><figure class="latest-img"><?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_latest_img' ) );   ?></figure><div class="latest-content"><h6 class="latest-b-cat"><?php the_category(', '); ?></h6><h3 class="latest-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3><p class="latest-author"><?php the_author_posts_link(); ?> / <?php the_time('F d, Y'); ?></p><p class="latest-excerpt"><?php echo michigan_webnus_excerpt(36); ?></p></div></article></div>
<?php
	endwhile;
	}elseif ($type=='two'){
			$i = 0;  
			$query = new WP_Query('posts_per_page=5&category_name='.$category.$orderby.'');
			while ($query -> have_posts()) : $query -> the_post(); 
      		if( $i == 0 ) {
      		?>
      		<div class="col-md-7">
				<article class="blog-post clearfix ">
					<figure class="pad-r20">
								<?php
								  $image = get_the_image( array( 'meta_key' => array( 'Thumbnail', 'Thumbnail' ), 'size' => 'latestfromblog' ,'echo'=>false) );
								  if( !empty($image) ) 
									echo $image;
								  else 
									echo '<img src="'.get_template_directory_uri() . '/images/featured.jpg" />';
								?>
					</figure>
					<div class="entry-content">
					<div class="blgt1-top-sec">
					<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<h6 class="blog-cat"><?php the_category(', ') ?></h6><h6 class="blog-date"><i class="fa-clock-o"></i><?php the_time('F d, Y') ?></h6>
					</div>
						 <?php  
							if( 'quote' == $post_format  ) echo '<blockquote>';
							echo '<p class="blog-detail">';
							echo michigan_webnus_excerpt(45);
							echo '... <br><br><a class="readmore" href="' . get_permalink($query->ID) . '">' . esc_html(michigan_webnus_options::michigan_webnus_blog_readmore_text()) . '</a>';
							echo '</p>';
							if( 'quote' == $post_format  ) echo '</blockquote>';
						?>
					</div>
				</article>
			</div><div class="col-md-5">
		<?php  }else{ ?>
		
      	<article class="blog-line clearfix">
          	<a href="<?php the_permalink(); ?>" class="img-hover"><?php  
				$image = get_the_image( array( 'meta_key' => array( 'Thumbnail', 'Thumbnail' ), 'size' => 'michigan_webnus_blog2_img' ,'echo'=>false, 'link_to_post' => false,) ); 
				if( !empty($image) ) 
					echo $image;
				else 
					echo '<img src="'.get_template_directory_uri() . '/images/featured_140x110.jpg" />';	
          	?></a>
			<p class="blog-cat"><?php the_category(', '); ?></p><h4><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h4><p><?php echo get_the_time('F d, Y'); ?> 	/<strong><?php esc_html_e('by', 'michigan') ?></strong> <?php echo get_the_author(); ?>
        </article>
		
      <?php
		}
		$i++; 
		endwhile;
		?>
		</div>
		<?php
	}elseif ($type=='three'){
	$query = new WP_Query('posts_per_page=3&category_name='.$category.$orderby.'');
	while ($query -> have_posts()) : $query -> the_post();
?>
	<div class="col-md-4 col-sm-4"><article class="latest-b2"><figure class="latest-b2-img"><?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_blog2_img' ) );   ?></figure><div class="latest-b2-cont"><h6 class="latest-b2-cat"><?php the_category(', '); ?></h6><h3 class="latest-b2-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3><p><?php echo michigan_webnus_excerpt(17); ?></p><div class="latest-b2-metad2"><i class="fa-comment-o"></i><span><?php echo get_comments_number() ?></span> / <span class="latest-b2-date"><?php the_author_posts_link(); ?> / <?php echo get_the_date('F d, Y');?></span></div></div></article></div>
<?php
	endwhile;
	}elseif ($type=='four'){
	$query = new WP_Query('posts_per_page=2&category_name='.$category.$orderby.'');
	while ($query -> have_posts()) : $query -> the_post();
?>	
	<div class="col-md-6"><article class="latest-b2"> <div class="col-md-3"> <h6 class="blog-date"><span><?php the_time('d') ?> </span><?php the_time('M Y') ?> </h6> <div class="au-avatar"><?php echo get_avatar( get_the_author_meta( 'user_email' ), 90 ); ?></div> <h6 class="blog-author"><strong><?php esc_html_e('Written by','michigan'); ?></strong><br> <?php the_author_posts_link(); ?> </h6> <h6 class="latest-b2-cat"><?php the_category(', '); ?></h6> </div><div class="col-md-9"> <figure class="latest-b2-img"><?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_latest_img' ) );   ?></figure> <div class="latest-b2-cont"><h3 class="latest-b2-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3> </div> </div><hr class="vertical-space"></article></div>
<?php
	endwhile;
	}elseif ($type=='five'){
			$query = new WP_Query('posts_per_page=6&category_name='.$category.$orderby.'');
			while ($query -> have_posts()) : $query -> the_post();
?>
	 <div class="col-md-6 col-lg-4"><article class="latest-b2">
	  <figure class="latest-b2-img"><?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_blog2_img' ) );   ?></figure>
	  <div class="latest-b2-cont">
	  <h6 class="latest-b2-cat"><?php the_category(', '); ?></h6> 
	  <h3 class="latest-b2-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	  <h5 class="latest-b2-date"><?php the_author_posts_link(); ?> / <?php echo get_the_date('F d, Y');?></h5>
	  </div></article></div>
<?php
	endwhile;
	} elseif ($type=='six') {
			$query = new WP_Query('posts_per_page=4&category_name='.$category.$orderby.'');
			while ($query -> have_posts()) : $query -> the_post();
?>
	<div class="col-md-3 col-sm-6"><article class="latest-b">
	  <figure class="latest-img"><?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_blog2_img' ) );   ?></figure>
		<div class="latest-content">
		<p class="latest-date"><?php the_time('F d, Y'); ?></p>
		<h3 class="latest-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<p class="latest-author"><strong><?php esc_html_e('by','michigan') ?></strong> <?php the_author_posts_link(); ?></p>  
		</div> 
      </article></div>
<?php
	endwhile;
	} elseif ( $type == 'seven' ) {
		$wpbp = new WP_Query('posts_per_page=3&category_name='.$category.$orderby.'');
		if ($wpbp->have_posts()) : while ($wpbp->have_posts()) : $wpbp->the_post(); ?>
		<div class="col-md-4 col-sm-4"><article class="latest-b">
		<figure class="latest-img"><?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_blog3_img' ) );   ?></figure>
		  	<div class="wrap-date-icons">
			    <h3 class="latest-date">
			    	<span class="latest-date-month"><?php the_time('M') ?></span>
			    	<span class="latest-date-day"><?php the_time('d') ?></span>
			    	<span class="latest-date-year"><?php the_time('Y') ?></span>
			    </h3>
			    <div class="latest-icons">
			    	<p>
			    		<span><i class="fa-eye"></i></span>
			    	</p>
			    	<p>
			            <span><?php echo michigan_webnus_getViews(get_the_ID()); ?></span>		
				    </p>
			    </div>
			</div>
			<div class="latest-content">
			    <h6 class="latest-cat"><?php the_category(', '); ?></h6>
				<h3 class="latest-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<h6 class="latest-author"><strong><?php esc_html_e('by','michigan') ?></strong> <?php the_author_posts_link(); ?></h6>  
			</div> 
	    </article></div> <?php

		endwhile; endif;
	} elseif ($type == 'eight'){
			$query = new WP_Query('posts_per_page=3&category_name='.$category.$orderby.'');
			while ($query -> have_posts()) : $query -> the_post();
			?>
			<div class="col-md-4 col-sm-4">
				<article class="latest-b8">
					<figure class="latest-b8-img">
						<?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_latest_img' ) );   ?>
					</figure>
					<div class="latest-b8-cont">
						<h6 class="latest-b8-cat"><?php the_category(' '); ?></h6>
						<h3 class="latest-b8-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<p>
							<?php echo michigan_webnus_excerpt(10); ?>
						</p>

					</div>
					<div class="latest-b8-metad8">
							<h6 class="blog-views"><i class="fa-eye"></i><span class="colorb"><?php echo michigan_webnus_getViews(get_the_ID()); ?></span> </h6>
							<h6 class="blog-comments"><i class="fa fa-comment" aria-hidden="true"></i><a class="hcolorf" href="<?php the_permalink(); ?>#comments"> <?php comments_number( '0', '1', '%'); ?></a></h6>
							<div class="blog-date"><a href="<?php the_permalink(); ?>">
								<p class="latest-b8-month"><?php echo get_the_time('M'); ?></p>
								<p class="latest-b8-day"><?php echo get_the_time('d'); ?></p>
								<p class="latest-b8-year"><?php echo get_the_time('Y'); ?></p></a></div>
					</div>
				</article>
			</div>
			<?php
			endwhile;
		} elseif ($type == 'nine'){
			$query = new WP_Query('posts_per_page=3&category_name='.$category.$author.'');
			while ($query -> have_posts()) : $query -> the_post();
			?>
			<div class="col-md-4 col-sm-4">
				<article class="latest-b9">
					<div class="latest-b9-cont">
						<h6 class="latest-b9-tagline"><span><?php esc_html_e( 'news', 'michigan' )?></span><i class="fa fa-newspaper-o" aria-hidden="true"></i></h6>
						<h3 class="latest-b9-title"><a class="hcolorf" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

					</div>
					<div class="latest-b9-metad9">
								<span class="latest-b9-author"><?php esc_html_e( 'by ', 'michigan' ); the_author_posts_link(); ?></span>
								<span class="latest-b9-date"><?php echo get_the_date(); ?></span>
								<span class="latest-b9-time"><?php echo get_the_time(); ?></span>
					</div>
					<div class="entry-footer">
						<a class="colorb" href="<?php the_permalink();?>"><?php esc_html_e( 'read more', 'michigan' ) ?></a>
					</div>
				</article>
			</div>
			<?php
			endwhile;
		} elseif ($type == 'ten'){
			$query = new WP_Query('posts_per_page=2&category_name='.$category.$orderby.'');
			while ($query -> have_posts()) : $query -> the_post();
			?>
			<div class="col-md-6 col-sm-6">

				<article class="latest-b10" style="background: url(<?php echo the_post_thumbnail_url ( $query->ID, 'michigan_webnus_latest_img' ); ?> ); ">
					<div class="latest-b10-cont">
						<h6 class="latest-b10-tagline"><span><?php esc_html_e( 'news', 'michigan' )?></span><i class="fa fa-newspaper-o" aria-hidden="true"></i></h6>
						<h3 class="latest-b10-title"><a class="hcolorf" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

					</div>
					<div class="latest-b10-metad9">
								<span class="latest-b10-author"><?php esc_html_e( 'by ', 'michigan' ); the_author_posts_link(); ?></span>
								<span class="latest-b10-date"><?php the_date( $query->ID ); ?></span>
								<span class="latest-b10-time"><?php the_time( $query->ID ); ?></span>
					</div>
				</article>
			</div>
			<?php
			endwhile;
		} elseif ($type=='eleven') {
		$query = new WP_Query('posts_per_page=4&category_name='.$category.'');
		while ($query -> have_posts()) : $query -> the_post(); ?>
			<div class="col-md-6">
				<article class="latest-b11">
					<figure class="latest-b11-img">
						<?php get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_square_img' ) ); ?>
					</figure>
					<div class="latest-b11-content">
						<div class="latest-b11-meta">
							<span class="date"><?php echo get_the_date(''); ?></span>
						</div>
						<h3 class="latest-b11-title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<p><?php echo michigan_webnus_excerpt(14); ?></p>
						<a class="readmore colorf" href="<?php the_permalink(); ?>"><?php esc_html_e( 'Continue Reading', 'michigan' ) ?></a>
					</div>
				</article>
			</div>
		<?php endwhile; }
?>
</div>
<?php 
	$out = ob_get_contents();
	ob_end_clean();	
	wp_reset_postdata();
	return $out;
 }
 add_shortcode('latestfromblog', 'michigan_webnus_latestfromblog');
?>