<?php global $plugin_page; ?>
<div class="admin-wrapper">
    <div class="admin-section">
        <div class="admin-section-header">
            <h2><?php _e("Add/Edit Member","team-awesome");?></h2>
            <p><?php _e("Add new or edit existing team member","team-awesome");?></p>
        </div>

        <div class="admin-section-body">
            <form id="team-awesome-form-save" action="?page=<?php echo $plugin_page; ?>&view=list" method="POST">
                <?php
                /*
                 *  $member - Member Object Array ($_GET["id"])
                 *  if $member - update table;
                 *  else - create new member;
                 * 
                 */
                $member     = TEAM_AWESOME_ADMIN::get_member();
                ?>

                <div class="opt">
                    <span><?php _e("Member Title","team-awesome");?></span>
                    <span class="caption-note">(<?php _e("Required!","team-awesome");?>)</span><br />
                    <input required type="text" name="title" value="<?php echo $member ? $member->title : "" ?>"/>
                    <p>(<?php _e("Person name","team-awesome");?>)</p>
                </div>
                
                <div class="opt">
                    <span><?php _e("Member Category","team-awesome");?></span>
                    <span class="caption-note">(<?php _e("Optional!","team-awesome");?>)</span><br />
                    <select name="catid[]" multiple>
                        <?php $categories = TEAM_AWESOME_ADMIN::get_categories(TRUE); ?>
                        <?php foreach ($categories as $category): ?>
                            <option <?php echo $member && (in_array($category->id, $member->catid)) ? "selected" : ""; ?> value="<?php echo $category->id; ?>"><?php echo $category->title; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <p>(<?php _e("Select member category. Hold Ctrl key to select multiple categories.","team-awesome");?>)</p>
                </div>
                
                <div class="opt">
                    <span><?php _e("Member Image","team-awesome");?></span>
                    <span class="caption-note">(<?php _e("Required!","team-awesome");?>)</span><br />
                    <input required type="text" name="avatar" value="<?php echo $member ? $member->avatar : "" ?>"/>
                    <p>(<?php _e("Avatar photo","team-awesome");?>)</p>
                </div>
                
                <div class="opt">
                    <span><?php _e("Member Video","team-awesome");?></span>
                    <span class="caption-note">(<?php _e("Optional.","team-awesome");?>)</span><br />
                    <input type="text" name="vidavatar" value="<?php echo $member ? $member->vidavatar : "" ?>"/>
                    <p>(<?php _e("Avatar video (muted loop)","team-awesome");?>)</p>
                </div>
                
                <div class="opt">
                    <span><?php _e("Member Occupation","team-awesome");?></span>
                    <span class="caption-note">(<?php _e("Required!","team-awesome");?>)</span><br />
                    <input required type="text" name="occupation" value="<?php echo $member ? $member->occupation : "" ?>"/>
                    <p>(<?php _e("Occupation/Position","team-awesome");?>)</p>
                </div>
                
                <div class="opt">
                    <span><?php _e("Member Short Description","team-awesome");?></span>
                    <span class="caption-note">(<?php _e("Optional!","team-awesome");?>)</span><br />
                    <textarea name="short"><?php echo $member ? $member->short : "" ?></textarea>
                    <p>(<?php _e("Short description about the person","team-awesome");?>)</p>
                </div>
                
                <div class="opt">
                    <span><?php _e("Member Long Description","team-awesome");?></span>
                    <span class="caption-note">(<?php _e("Optional!","team-awesome");?>)</span><br />
                    <textarea name="long"><?php echo $member ? $member->long : "" ?></textarea>
                    <p>(<?php _e("Popup text description about the person","team-awesome");?>)</p>
                </div>

                <div class="opt">
                    <span><?php _e("Member Skills", "team-awesome"); ?></span>
                    <span class="caption-note">(<?php _e("Optional!", "team-awesome"); ?>)</span><br />

                    <?php
                    if ($member && $member->skills) {

                        foreach ($member->skills as $k => $skill) :
                            ?>

                            <div class="opt-skill">
                                <div class="remove-skill"><i class="fa fa-times"></i></div>
                                <input class="short-skill-title" type="text" name="skills[<?php echo $k; ?>][title]" value="<?php echo $skill["title"]; ?>" />
                                <select class="short" type="text" name="skills[<?php echo $k; ?>][percents]">
                                    <?php
                                    for ($i = 0; $i <= 100; $i++) :
                                        ?>
                                        <option <?php echo $skill["percents"] == $i ? "selected" : ""; ?> value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
                                        <?php
                                    endfor;
                                    ?>
                                </select>
                                
                                <input class="short team-awesome-color-picker" type="text" name="skills[<?php echo $k; ?>][color]" value="<?php echo isset($skill["color"]) ? $skill["color"] : ""; ?>" />
                                <p>(<?php _e("Skill Title, Percents, Fill Color", "team-awesome"); ?>)</p>
                            </div>

                            <?php
                        endforeach;
                    } else {
                        ?>
                        <div class="opt-skill">
                            <input class="short-skill-title" type="text" name="skills[0][title]" value="" />
                            <select class="short" type="text" name="skills[0][percents]">
                                <?php
                                for ($i = 0; $i <= 100; $i++) :
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
                                    <?php
                                endfor;
                                ?>
                            </select>
                            <input class="short team-awesome-color-picker" type="text" name="skills[0][color]" value="" />
                            <p>(<?php _e("Skill Bar Title, Percents, Fill Color", "team-awesome"); ?>)</p>
                        </div>
                        <?php
                    }
                    ?>

                    <button id="moreskills">Add more skills</button>
                </div>
                
                <?php
                    for ($i = 0; $i < 8; $i++) :
                ?>
                
                    <div class="opt">
                        <span><?php _e("Social Button ","team-awesome"); ?></span>
                        <span class="caption-note">(<?php _e("Optional!","team-awesome");?>)</span><br />
                        <select class="short" name="social[<?php echo $i; ?>][icon]">
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-behance" ? "selected" : ""; ?> value="fa-behance">Behance</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-deviantart" ? "selected" : ""; ?> value="fa-deviantart">Deviantart</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-dribbble" ? "selected" : ""; ?> value="fa-dribbble">Dribbble</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-facebook" ? "selected" : ""; ?> value="fa-facebook">Facebook</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-flickr" ? "selected" : ""; ?> value="fa-flickr">Flickr</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-github" ? "selected" : ""; ?> value="fa-github">Github</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-google-plus" ? "selected" : ""; ?> value="fa-google-plus">Google+</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-lastfm" ? "selected" : ""; ?> value="fa-lastfm">Last.fm</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-linkedin" ? "selected" : ""; ?> value="fa-linkedin">Linkedin</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-reddit" ? "selected" : ""; ?> value="fa-reddit">Reddit</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-soundcloud" ? "selected" : ""; ?> value="fa-soundcloud">Soundcloud</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-twitch" ? "selected" : ""; ?> value="fa-twitch">Twitch</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-tumblr" ? "selected" : ""; ?> value="fa-tumblr">Tumblr</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-twitter" ? "selected" : ""; ?> value="fa-twitter">Twitter</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-vimeo" ? "selected" : ""; ?> value="fa-vimeo">Vimeo</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-youtube" ? "selected" : ""; ?> value="fa-youtube">YouTube</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-instagram" ? "selected" : ""; ?> value="fa-instagram">Instagram</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-pinterest" ? "selected" : ""; ?> value="fa-pinterest">Pinterest</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-skype" ? "selected" : ""; ?> value="fa-skype">Skype</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-envelope" ? "selected" : ""; ?> value="fa-envelope">E-Mail</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-phone" ? "selected" : ""; ?> value="fa-phone">Phone</option>
                            <option <?php echo $member && $member->social[$i]["icon"] == "fa-link" ? "selected" : ""; ?> value="fa-link">URL Link</option>
                        </select>
                        <input class="semishort" type="text" name="social[<?php echo $i; ?>][link]" value="<?php echo $member ? $member->social[$i]["link"] : "" ?>"/>
                        <p>(<?php _e("Social Icon and Link","team-awesome");?>)</p>
                    </div>
                
                <?php endfor; ?>

                <input type="hidden" name="task" value="create" />
                <?php if ($member) : ?><input type="hidden" name="update" value="<?php echo $member->id; ?>" /><?php endif; ?>
                <?php if ($member) : ?><button><?php _e("Update Member","team-awesome");?></button><?php else: ?><button><?php _e("Save Member","team-awesome");?></button><?php endif; ?>

            </form>
            <button onclick="window.location.href = '?page=<?php echo $plugin_page; ?>&view=list';"><?php _e("Cancel","team-awesome");?></button>
        </div>
    </div>
</div>
