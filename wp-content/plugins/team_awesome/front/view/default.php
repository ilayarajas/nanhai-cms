<!-- blur filter --> 
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="ta-svg-filters">
    <defs>
        <filter id="ta-blur">
            <feGaussianBlur in="SourceGraphic" stdDeviation="0,0" />
        </filter>

        <filter id="ta-blur-vert">
            <feGaussianBlur in="SourceGraphic" stdDeviation="0,0" />
        </filter>
    </defs>
</svg>

<!-- actual html --> 
<div id="<?php echo $options["id"]; ?>" class="team-awesome <?php echo $options["parallax"] == "enable" ? "ta-parallax" : ""; ?>">
    <div class="team-awesome-container">

        <?php if ($options["title"] != "") : ?>
            <h4><?php echo htmlspecialchars($options["title"]); ?></h4>
        <?php endif; ?>

        <!--Menu Filter--> 
        <ul class="team-awesome-menu">
            <li data-cat="0" class="active"><?php _e("ALL STAFF", "team-awesome"); ?></li>
            <?php
            $cats = TEAM_AWESOME::get_categories();
            foreach ($cats as $cat) :
                ?>
                <li data-cat="<?php echo $cat->id; ?>"><?php echo $cat->title; ?></li>
                <?php
            endforeach;
            ?>
        </ul>

        <!--Slider--> 
        <div class="team-awesome-slider" data-perpage="<?php echo $options["perpage"]; ?>">  

            <?php
            $json = array();
            $members = TEAM_AWESOME::get_members();
            $i = 0;
            ?>

            <?php foreach ($members as $member) : ?>

                <?php
                $cats = implode("-", $member->catid);
                $json[$i]["cat"] = $cats;
                $view = $options["avatar"];
                ?>

                <?php ob_start(); ?>

                <div class="team-awesome-member-box" data-width="<?php echo (int) $options["boxwidth"]; ?>" data-cat="<?php echo $cats; ?>">

                    <!--Load avatar--> 
                    <?php include("default-{$view}.php"); ?>

                    <div class="team-awesome-member-about">
                        <p class="team-awesome-member-about-title"><?php echo $member->title; ?></p>
                        <?php if ($options["divider"] == "enable") : ?>
                            <span class="team-awesome-member-about-dash"></span>
                        <?php endif; ?>
                        <p class="team-awesome-member-about-occupation"><?php echo $member->occupation; ?></p>
                        <?php if ($options["shortdesc"] == "show") : ?>
                            <p class="team-awesome-member-about-short"><?php echo $member->short; ?></p>
                        <?php endif; ?>
                    </div>

                    <?php if ($options["pop"] == "enable") : ?>

                        <div class="team-awesome-member-pop" data-width="<?php echo (int) $options["popwidth"]; ?>" data-maxHeight="<?php echo (int) $options["popheight"]; ?>">
                            
                            <div class="team-awesome-member-pop-top-gradient"></div>
                            <div class="team-awesome-member-pop-bottom-gradient"></div>
                            
                            <div class="team-awesome-member-pop-scroll">
                                <div class="team-awesome-member-pop-inner">

                                    <div class="team-awesome-member-pop-image ta-pop-image-<?php echo $view; ?>" style="background-image: url('<?php echo $member->avatar; ?>')">
                                        <?php if ($view == "hex") : ?>
                                            <div class="ta-pop-image-hexTop"></div>
                                            <div class="ta-pop-image-hexBottom"></div>
                                        <?php endif; ?>
                                    </div>
                                    <p class="team-awesome-member-pop-title"><?php echo $member->title; ?></p>
                                    <p class="team-awesome-member-pop-occupation"><?php echo $member->occupation; ?></p>
                                    <p class="team-awesome-member-pop-long">
                                        <?php echo nl2br($member->long); ?>
                                    </p>

                                    <div class="team-awesome-member-pop-stats">

                                        <?php $skills = $member->skills; ?>
                                        <?php foreach ($skills as $skill) : ?>
                                            <?php if ($skill["percents"] || $skill["title"] != "") : ?>
                                                <div class="team-awesome-member-pop-percents-box">
                                                    <p class="team-awesome-member-pop-percents-box-title"><?php echo htmlspecialchars($skill["title"]); ?></p>
                                                    <p class="team-awesome-member-pop-percents-box-aftertitle">0%</p>
                                                    <div class="team-awesome-member-pop-percents-outer">
                                                        <div class="team-awesome-member-pop-percents-inner" data-fill="<?php echo $skill["percents"]; ?>%" style="background:<?php echo $skill["color"]; ?>;"></div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endif; ?>
                </div>

                <?php
                $json[$i]["html"] = ob_get_clean();
                $i++;
                ?>

            <?php endforeach; ?>


        </div>

        <div class="team-awesome-slider-control">
            <div class="team-awesome-slider-control-left"><span><i class="fa fa-chevron-left"></i></span></div>
            <div class="team-awesome-slider-control-right"><span><i class="fa fa-chevron-right"></i></span></div>
        </div>

    </div>
</div>

