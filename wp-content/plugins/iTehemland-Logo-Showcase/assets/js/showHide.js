jQuery(function($) {
    var curWindowWidth = $(window).width();
    $(window).resize(function () {
        curWindowWidth = $(window).width();
    });
    $(".show-inline").click(function () {
        var showElem = $(this);
        var itemNum = showElem.attr('data-item-number');
        var id = showElem.attr("data-id");
        var desktop = showElem.parent().attr('data-desktop');
        var rand_id = showElem.parent().attr('data-rand-id');
        var tablet = showElem.parent().attr('data-tablet');
        var mobile = showElem.parent().attr('data-mobile');
        var itemCount = showElem.parent().find('.show-inline').length;
        var curRow;
        var showPlace;
        if (curWindowWidth > 1200) {
            curRow = Math.ceil(itemNum / desktop);
            showPlace = curRow * desktop;
        }
        else if (curWindowWidth > 1024) {
            curRow = Math.ceil(itemNum / desktop);
            showPlace = curRow * desktop;
        }
        else if (curWindowWidth > 720) {
            curRow = Math.ceil(itemNum / tablet);
            showPlace = curRow * tablet;
        }
        else if (curWindowWidth <= 720) {
            curRow = Math.ceil(itemNum / mobile);
            showPlace = curRow * mobile;
        }
        if (showPlace > itemCount)
            showPlace = itemCount;
        showPlace -= 1;
        var data = {
            action: "it_quick_view_get",
            id: id,
            rand_id: args[rand_id].rand_id,
            title: args[rand_id].title,
            contact: args[rand_id].contact,
            social: args[rand_id].social,
            full_desc: args[rand_id].full_desc,
            gallery: args[rand_id].gallery,
            nonce : ajax_object.nonce
        };
        // ajax request
        if (showElem.parent().find('#show-inline-box').length) {
            showElem.parent().find(".ls-ajax-loading").fadeIn();
            showElem.parent().find('#show-inline-box .ls-showdetail').slideUp('slow', function () {
                $(this).parent().remove();
                $.ajax({
                    url: ajax_object.ajaxurl,
                    data: data,
                    type: 'GET',
                    dataType: "html",
                    success: function (data) {
                        showElem.parent().find(".show-inline:eq(" + showPlace + ")").after(data);
                    },
                    complete: function (xhr, status) {
                        showElem.parent().find(".ls-ajax-loading").fadeOut();
                        showElem.parent().find('#show-inline-box .ls-showdetail').slideDown();
                        $(".ls-full-content").mCustomScrollbar({
                            theme: "dark-thick"
                        });
                    }
                });
            });
        }else {
            showElem.parent().find(".ls-ajax-loading").fadeIn();
            $.ajax({
                url: ajax_object.ajaxurl,
                data: data,
                type: 'GET',
                dataType: "html",
                success: function (data) {
                    showElem.parent().find(".show-inline:eq(" + showPlace + ")").after(data);
                },
                complete: function (xhr, status) {
                    showElem.parent().find(".ls-ajax-loading").fadeOut();
                    showElem.parent().find('#show-inline-box .ls-showdetail').slideDown();
                    $(".ls-full-content").mCustomScrollbar({
                        theme: "dark-thick"
                    });
                }
            });
        }
    });
    // show detail when layout is carousel
    $(".show-inline-car").click(function () {
        var showElem = $(this);
        var id = showElem.attr("data-id");
        var rand_id = showElem.closest('.ls-logo-cnt').attr('data-rand-id');
        var data = {
            action: "it_quick_view_get",
            id: id,
            rand_id: args[rand_id].rand_id,
            title: args[rand_id].title,
            contact: args[rand_id].contact,
            social: args[rand_id].social,
            full_desc: args[rand_id].full_desc,
            gallery: args[rand_id].gallery,
            nonce : ajax_object.nonce
        };
        // ajax request
        if (showElem.closest('.ls-logo-cnt').find('#show-inline-box').length) {
            showElem.closest('.ls-logo-cnt').find('.ls-ajax-loading').fadeIn();
            showElem.closest('.ls-logo-cnt').find('#show-inline-box .ls-showdetail').slideUp('slow', function () {
                $(this).parent().remove();
                $.ajax({
                    url: ajax_object.ajaxurl,
                    data: data,
                    type: 'GET',
                    dataType: "html",
                    success: function (data) {
                        showElem.closest('.ls-logo-cnt').find(".owl-wrapper-outer").append(data);
                    },
                    complete: function (xhr, status) {
                        showElem.closest('.ls-logo-cnt').find(".ls-ajax-loading").fadeOut();
                        showElem.closest('.ls-logo-cnt').find('#show-inline-box .ls-showdetail').slideDown();
                        $(".ls-full-content").mCustomScrollbar({
                            theme: "dark-thick"
                        });
                    }
                });
            });
        }else {
            showElem.closest('.ls-logo-cnt').find(".ls-ajax-loading").fadeIn();
            $.ajax({
                url: ajax_object.ajaxurl,
                data: data,
                type: 'GET',
                dataType: "html",
                success: function (data) {
                    showElem.closest('.ls-logo-cnt').find(".owl-wrapper-outer").append(data);
                },
                complete: function (xhr, status) {
                    showElem.closest('.ls-logo-cnt').find(".ls-ajax-loading").fadeOut();
                    showElem.closest('.ls-logo-cnt').find('#show-inline-box .ls-showdetail').slideDown();
                    $(".ls-full-content").mCustomScrollbar({
                        theme: "dark-thick"
                    });
                }
            });
        }
    });
    //
    $(".ls-showdetail-close").live("click", function () {
        $(this).parent('.ls-showdetail').slideUp('slow', function () {
            $(this).parent().remove();
        });
    });
    $('.it-modal').click(function() {
        $(".remodal .ls-ajax-loading").fadeIn();
        $(".inner-modal").html('<div class="ls-grid-m-12 ls-grid-t-12 ls-grid-d-12" id="show-inline-box"  ></div>');
        var id = $(this).attr("data-id");
        var rand_id = $(this).closest('.ls-logo-cnt').attr('data-rand-id');
        var data = {
            action: "it_quick_view_get",
            id: id,
            rand_id: args[rand_id].rand_id,
            title: args[rand_id].title,
            contact: args[rand_id].contact,
            social: args[rand_id].social,
            full_desc: args[rand_id].full_desc,
            gallery: args[rand_id].gallery,
            nonce : ajax_object.nonce
        };
        $.ajax({
            url: ajax_object.ajaxurl,
            data: data,
            type: 'GET',
            dataType: "html",
            success: function (data) {
                $(".inner-modal").html(data);
                $('#show-inline-box .ls-showdetail').slideDown();
                $(".ls-full-content").mCustomScrollbar({
                    theme: "dark-thick"
                });
            },
            complete: function(xhr ,status){
                $(".remodal .ls-ajax-loading").fadeOut();
            }
        });
    });
});