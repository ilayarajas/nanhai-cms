<?php

$task = TEAM_AWESOME_ADMIN::get_var("task");

switch (strtolower($task)) :
    
    case "save" :
        $return = TEAM_AWESOME_ADMIN::save_options();
        if (is_wp_error($return)) {
            echo "<p class='error'>" . $return->get_error_message() . "</p>";
        }
        break;
        
    case "reset" :
        $return = TEAM_AWESOME_ADMIN::reset_options();
        if (is_wp_error($return)) {
            echo "<p class='error'>" . $return->get_error_message() . "</p>";
        }
        break;
        
endswitch;

require (dirname(__FILE__) . DS . "options.php");