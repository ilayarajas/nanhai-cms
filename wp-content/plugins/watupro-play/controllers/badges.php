<?php
// badges controller
class WatuPROPlayBadges {
	static function manage() {
		global $wpdb;
		$_level = new WatuPROPlayLevel();
		
		// select exam categories
		$cats = $wpdb->get_results("SELECT * FROM ".WATUPRO_CATS." ORDER BY name");
		$_POST['atype']='badge';
		
		// mycred badges?
		if(class_exists('myCRED_Badge_Module')) {
			$args = array(
				'posts_per_page'   => -1,
				'post_type'        => 'mycred_badge',
				'post_status'      => 'publish',				 
			);
			$mycred_badges = get_posts($args);			
		}
			
		$action = empty($_GET['action']) ? 'list' : $_GET['action'];
		switch($action) {
			case 'add':
				if(!empty($_POST['ok'])) {
					$_level -> add($_POST);
					watuproplay_redirect('admin.php?page=watuproplay_badges');
				}
				$qcats = $wpdb->get_results("SELECT * FROM ".WATUPRO_QCATS." ORDER BY name");
				require(WATUPROPLAY_PATH."/views/badge.html.php");
			break;
			
			case 'edit':
				if(!empty($_POST['del'])) {
					$_level -> delete($_GET['id']);
					watuproplay_redirect('admin.php?page=watuproplay_badges');
				}			
			
				if(!empty($_POST['ok'])) {
					$_level->save($_POST, $_GET['id']);
					watuproplay_redirect('admin.php?page=watuproplay_badges');
				}
				
				// select this badge
				$badge = $wpdb -> get_row( $wpdb->prepare("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE id=%d", $_GET['id']));
				
				// select question categories
				$qcats = $wpdb->get_results("SELECT * FROM ".WATUPRO_QCATS." ORDER BY name");
				require(WATUPROPLAY_PATH."/views/badge.html.php");
			break;			
			
			case 'list':
				// select currently available badges
				$badges = $wpdb -> get_results("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE atype='badge' ORDER BY name");			
				
				// save the email notice settings
				if(!empty($_POST['save_admin_badge_email'])) {
					update_option('watuproplay_admin_badge_email', @$_POST['admin_badge_email']);
					update_option('watuproplay_admin_badge_subject', $_POST['admin_badge_subject']);
					update_option('watuproplay_admin_badge_message', $_POST['admin_badge_message']);
				}		
				
				if(!empty($_POST['save_user_badge_email'])) {
					update_option('watuproplay_user_badge_email', @$_POST['user_badge_email']);
					update_option('watuproplay_user_badge_subject', $_POST['user_badge_subject']);
					update_option('watuproplay_user_badge_message', $_POST['user_badge_message']);
				}			
			
				// select users who earned badges
				$users = $wpdb->get_results("SELECT tU.ID, tM.meta_value as badges FROM {$wpdb->users} tU
					JOIN {$wpdb->usermeta} tM ON tU.ID = tM.user_id AND tM.meta_key = 'watuproplay_user_badges'
					WHERE tM.meta_value!='' AND tM.meta_value != '||'");
				
				// for each badge find number of users
				foreach($badges as $cnt => $badge) {
					$uids = array();
					foreach($users as $user) {
						if(strstr($user->badges, "|".$badge->id."|") and !in_array($user->ID, $uids)) $uids[] = $user->ID;
					}					
					
					$badges[$cnt]->num_users = sizeof($uids);
				}	
					 
				require(WATUPROPLAY_PATH."/views/badges.html.php");
			default:
			break;
		}
	} // end manage()
	
	// get users who earned a badge. When available show also which taking exactly earned it
	static function users_earned() {
		global $wpdb;
		
		$badge = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE id=%d", $_GET['id']));
		
		if(!empty($_GET['del']) and check_admin_referer('watuproplay_userbadges')) {			
			$user_badges = get_user_meta($_GET['user_id'], 'watuproplay_user_badges', true);
			$badge_ids = explode('|', $user_badges);
			foreach($badge_ids as $cnt=>$id) {
				if($id == $badge->id) unset($badge_ids[$cnt]);
			}
			$badge_ids = array_filter($badge_ids);
			$user_badges = '|' . implode('|', $badge_ids) . '|';
			update_user_meta($_GET['user_id'], 'watuproplay_user_badges', $user_badges);
		}
		
		// select users who earned this badge
		$users = $wpdb->get_results("SELECT tU.*, tM.meta_value as badges FROM {$wpdb->users} tU
				JOIN {$wpdb->usermeta} tM ON tU.ID = tM.user_id AND tM.meta_key = 'watuproplay_user_badges'
				WHERE tM.meta_value LIKE '%|".$badge->id."|%'");
				
		// select history items order by DESC to match to users
		// so we know the taking record when available
		$history_items = $wpdb->get_results($wpdb->prepare("SELECT tH.timestamp as timestamp, tH.user_id as user_id,
		tH.taking_id as taking_id, tE.name as exam_name, tE.ID as exam_id
		FROM ".WATUPROPLAY_HISTORY." tH LEFT JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.ID = tH.taking_id  	
		LEFT JOIN ".WATUPRO_EXAMS." tE ON tE.ID = tT.exam_id
		WHERE tH.action = 'assigned badge' AND tH.item_id=%d ORDER BY tH.id DESC", $badge->id));
		
		// match to users
		foreach($users as $cnt=>$user) {
			foreach($history_items as $item) {
				if($item->user_id == $user->ID) {
					$users[$cnt]->assigned_badge_timestamp = $item->timestamp;
					$users[$cnt]->assigned_badge_taking_id = $item->taking_id;
					$users[$cnt]->assigned_badge_exam_name = $item->exam_name;
					$users[$cnt]->assigned_badge_exam_id = $item->exam_id;
					break; // we need only the 1st / most recent
				}
			}
		}	// end foreach user
		// print_r($users);
		$dateformat = get_option('date_format');
		
		require(WATUPROPLAY_PATH."/views/badge-users.html.php");
	}
}