﻿=== Modal Window - create a simple popup & insert any content ===
Contributors: Wpcalc
Donate link: https://wow-estore.com/downloads/wow-modal-windows-pro/
Tags: animated popup, auto close, delayed popup, image popup, lightbox, modal plugin, modal popup, modal window, modal windows, onclick popup, popup, popup plugin, popup window, popups, unlimited popups
Requires at least: 3.2
Tested up to: 4.6
Stable tag: 2.1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Create popups. Insert any content. Trigger on anything. Place anywhere with a shortcode.

== Description ==
Create modal windows and insert any kind of content.

= Demo =
[Demo](http://wow-demo.com/wow-modal-windows/)

[youtube https://www.youtube.com/watch?v=Yk7XYub_d0Q]

= Main features =
* Create any kind of modal windows
* Insert any content via the built-in editor
* Trigger the modal windows on anything
* Show all the time or only once
* Place them anywhere with a shortcode
* Create up to 3 modal windows

= Pro version =
Achieve even better results with the PRO version:

* Unlimited amount of modal windows
* Powerful positioning
* Create not only popups, but also flyout panels
* Powerful styling, including image backgrounds
* Powerful placing: In addition to shortcodes, place the popup to the whole website, only posts, only pages, only certain pages or only certain posts, all posts except specified ones or all pages except specified ones.
* Show modal windows via the fixed buttons with your custom text. Buttons can be placed on the left, right, top or bottom of the screen.
* And more...

[Pro version demo](http://wow-demo.com/wow-modal-windows-pro/)

[Buy Pro version](https://wow-estore.com/downloads/wow-modal-windows-pro/)

= Trigger the modal window on: = 
* Click on a link or button with an ID
* Click on a link or button with an #anchor-link
* Opening the page (auto-show)
* Scrolling the page
* On exit intent (When the user tries to navigate away from the page - moves the cursor away from your page and tries to close or switch tabs)
* Enable closing the modal window on overlay & via the esc button

= Enable timing: = 
* Set the delay with which the modal window appears

= Show modal window: = 
* All the time
* Only once
* Show again after the number of days you specify

= Insert any content: = 
* HTML Text
* Banner Ads
* Image or Image Gallery
* Video or Video Gallery
* Audio or Audio Gallery
* Pdf viewer
* Iframe Content
* Forms
* SlideShare Content
* and other media
 
= Modal windows can be used for: = 
* Capturing emails
* Advertisements
* Contact forms
* Disclaimer contents
* Notifications
* Alert messages
* Product description
* Work portfolio
* Product images
* Google maps
* Notices
* Other information

= Use with our other popular plugins to maximize your results =
* Wow Side Menus - add fixed side buttons to your website & lead users where you want (Unique plugin) [Free version](https://wordpress.org/plugins/mwp-side-menu/) | [Pro version](https://wow-estore.com/downloads/wow-side-menus-pro/)
* Wow Countdowns - create countdowns with randomizer & triggers (Unique plugin) [Free version](https://wordpress.org/plugins/mwp-countdown/) | [Pro version](https://wow-estore.com/downloads/wow-countdowns-pro/)
* Wow Forms - create simple forms [Free version](https://wordpress.org/plugins/mwp-forms/) | [Pro version](https://wow-estore.com/downloads/wow-forms-pro/)
* Wow Herd Effects - create a queue effect on your website (Unique plugin) [Free version](https://wordpress.org/plugins/mwp-herd-effect/) | [Pro version](https://wow-estore.com/downloads/wow-herd-effects-pro/)
* Wow Viral Signups - run basic or limited optin campaigns, with viral sharing (Unique plugin) [Free version](https://wordpress.org/plugins/mwp-viral-signup/) | [Pro version](https://wow-estore.com/downloads/wow-viral-signups-pro/)
* Wow Skype Buttons - add a skype button to your website [Free version](https://wordpress.org/plugins/mwp-skype/) | [Pro version](https://wow-estore.com/downloads/wow-skype-buttons-pro/)
* Wow Marketing-WP Suite - all of the above plugins in a single ultimate marketing plugin [Free version](https://wordpress.org/plugins/marketing-wp/) | [Pro version](https://wow-estore.com/downloads/wow-marketing-wp-suite-pro/)

= Translations =
* English
* Russian

= Support =
Search for answers and ask your questions at [our support center](https://wow-affect.com/support/)

== Installation ==

* Upload the folder to the plugins directory `/wp-content/plugins/` or upload the plugin via the wp-admin
* Activate «Modal Windows» in the “Plugins” section.

== Screenshots ==
1. Overview
2. Create modal windows & insert any content
3. Insert a form (together with Wow Forms plugin)
4. Create exit intent popups. Retain users on your website or at least offer the abandoning users something valuable to capture their emails.
5. Setup any widgets. Insert any other plugins' shortcodes.
6. Create a phone call request widget
7. Use for showing ads
8. Modal window setup example
9. Features
10. Pro version features


== Changelog ==

= 2.1.2 = 
* Fixed script (click on link)

= 2.1.1 = 
* Fixed include modal windows

= 2.1 = 
* Fixed show modal window


= 2.0 = 
* Add new options
* Fixed code
* Change style

= 1.3 = 
* Fixed code
* Change style

= 1.2.1 = 
* Edited contacts

= 1.2 = 
* Fixed display a modal window


= 1.1 = 
* Fixed display a modal window
* Add option closing modal window


= 1.0 = 
* Initial release