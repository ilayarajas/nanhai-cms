<?php
class WatuPROPlayUser {
	// assigns proper levels and badges
	// called on do_action when exam is completed or taking is edited
	function update_meta($taking_id) {
		global $wpdb;
		
		// select taking
		$taking = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".WATUPRO_TAKEN_EXAMS." WHERE ID=%d", $taking_id));
		$uid = $taking->user_id;
		if(empty($uid)) return false;
		
		// get stats		
		$userstats = $this->stats($uid);
		
		// select all exams in the system
		$exams = $wpdb->get_results("SELECT ID, cat_id FROM ".WATUPRO_EXAMS." ORDER BY ID");
		
		$_level = new WatuPROPlayLevel();
				
		$_level->assign_level($uid, $userstats, $exams, $taking_id);		
		$_level->assign_badges($uid, $userstats, $exams, $taking_id);
		
		// now update points balance
		$points = $userstats['points'];
		$spent_points = $wpdb->get_var($wpdb->prepare("SELECT SUM(points) FROM ".WATUPROPLAY_REDEEMED."
			WHERE user_id=%d", $uid));
		$points_balance = $points - $spent_points;
		update_user_meta($uid, 'watuproplay-points', $points_balance);	
	}
	
	// gets stats for the user - total points, % correct answers etc 
	function stats($uid) {
		global $wpdb;
		
		// select all takings 
		$takings = $wpdb -> get_results( $wpdb->prepare("SELECT * FROM ".WATUPRO_TAKEN_EXAMS." 
			WHERE user_id=%d AND in_progress=0 ORDER BY ID DESC", $uid) );
			
		// select all points & is_correct from the answers of these takings
		$tids = array(0);
		foreach($takings as $taking) $tids[] = $taking->ID;
		$answers = $wpdb->get_results("SELECT tA.points as points, tA.is_correct as is_correct, tQ.is_survey as is_survey, 
			tA.taking_id as taking_id, tQ.cat_id as cat_id, tC.parent_id as parent_cat_id
			FROM ".WATUPRO_STUDENT_ANSWERS." tA LEFT JOIN ".WATUPRO_QUESTIONS." tQ ON tQ.ID = tA.question_id
			LEFT JOIN ".WATUPRO_QCATS." tC ON tC.ID = tQ.cat_id
			WHERE tA.taking_id IN (".implode(', ', $tids).")");
				
		// calculate stats - we'll use only the latest taking on each exam
		$points = $latest_points = $percent = $num_exams = 0;
		$taken_exam_ids = $final_takings = $qcats = array();
		
		foreach($takings as $taking) {
			$points += $taking->points;
			
			// add category points
			foreach($answers as $answer) {
				if($answer->taking_id != $taking->ID or !$answer->cat_id) continue;
				if(!isset($qcats[$answer->cat_id])) $qcats[$answer->cat_id] = array("num_answers"=>0, "num_correct"=>0, "points"=>0, "percent"=>0, 'latest_points'=>0);
				$qcats[$answer->cat_id]['points'] += $answer->points; 
				
				// add also to parent cat if any
				if($answer->parent_cat_id) {
					if(!isset($qcats[$answer->parent_cat_id])) $qcats[$answer->parent_cat_id] = array("num_answers"=>0, "num_correct"=>0, "points"=>0, "percent"=>0, 'latest_points'=>0);
					$qcats[$answer->parent_cat_id]['points'] += $answer->points; 
				}
			}

			// get only the latest (in this case first)
			if(in_array($taking->exam_id, $taken_exam_ids)) continue;
			$taken_exam_ids[] = $taking->exam_id;
			$latest_points += $taking->points;
			
			// add category % correct. We have to repeat the $answers loop here because we use only latest attempt on each quiz
			foreach($answers as $answer) {
				if($answer->taking_id != $taking->ID or !$answer->cat_id or $answer->is_survey) continue;
				$qcats[$answer->cat_id]['num_answers']++;
				$qcats[$answer->cat_id]['num_correct'] += intval($answer->is_correct);
				
				// add also to parent cat if any
				if($answer->parent_cat_id) {
					$qcats[$answer->parent_cat_id]['num_answers']++;
					$qcats[$answer->parent_cat_id]['num_correct'] += intval($answer->is_correct);
				}
			}
			
			// now again add category points, this time for latest attempt only
			foreach($answers as $answer) {
				if($answer->taking_id != $taking->ID or !$answer->cat_id) continue;
				if(!isset($qcats[$answer->cat_id])) $qcats[$answer->cat_id] = array("num_answers"=>0, "num_correct"=>0, "points"=>0, "percent"=>0, 'latest_points'=>0);
				$qcats[$answer->cat_id]['latest_points'] += $answer->points; 
				
				// add also to parent cat if any
				if($answer->parent_cat_id) {
					if(!isset($qcats[$answer->parent_cat_id])) $qcats[$answer->parent_cat_id] = array("num_answers"=>0, "num_correct"=>0, "points"=>0, "percent"=>0, 'latest_points'=>0);
					$qcats[$answer->parent_cat_id]['latest_points'] += $answer->points; 
				}
			}
			
			$num_exams ++;
			$final_takings[] = $taking;
			$percent += $taking->percent_correct;			
		}	
		
		// calculate average % correct and dump the data
		if($num_exams) $percent = round($percent / $num_exams);
		else $percent = 0;
		
		// calculate % correct on question categories
		foreach($qcats as $id=>$qcat) {
			if(empty($qcat['num_answers'])) continue; // percent is already initialized as 0
			$percent = round(100 * $qcat['num_correct'] / $qcat['num_answers']);
			$qcats[$id]['percent'] = $percent;
		}
		
		// allow other plugins to add or extract points
		$points = apply_filters('watuproplay-adjust-points', $points);
		
		$stats = array('points'=>$points, 'percent' => $percent, 'num_exams' => $num_exams, 
			'completed_exam_ids'=>$taken_exam_ids, 'takings'=>$final_takings, "qcats"=>$qcats, 'latest_points'=>$latest_points);
		
		return $stats;	
	}
	
	// displays the overview page if enabled
	function overview($uid = 0, $shortcode_call = false) {
		global $user_ID;
		if(empty($uid)) $uid = $user_ID;
		
		if(empty($uid)) return sprintf(__('Please <a href="%s">log in</a> to see your overview.', 'watuproplay'), wp_login_url(get_permalink()));
		
		// get page title & design
		$title = get_option('watuproplay_overview_title');
		$design = get_option('watuproplay_overview_design');
		$design = apply_filters('watupro_content', $design);
		
		// when called as shortcode, just return the design
		if($shortcode_call) return $design;
		
		include(WATUPROPLAY_PATH."/views/user-overview.html.php");
	}
	
	// cleanup all user data - points, levels, badges...
	function cleanup($uid) {
		global $wpdb;
		
		if(get_option('watupro_del_play_data') != 'yes') return false;
		
		update_user_meta($uid, 'watuproplay-points', 0);	
		update_user_meta($uid, 'watuproplay_user_level', 0);
		update_user_meta($uid, 'watuproplay_user_badges', '');
		
		$wpdb->query($wpdb->prepare("DELETE FROM " . WATUPROPLAY_HISTORY . " WHERE user_id=%d", $uid));
		
		$wpdb->query($wpdb->prepare("DELETE FROM " . WATUPROPLAY_REDEEMED . " WHERE user_id=%d", $uid));
	}
}