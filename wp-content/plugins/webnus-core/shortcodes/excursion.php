<?php
function michigan_excursion( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'category'		=> '',
		'count'			=> '8',
		'navigation'	=> '',
	), $atts));

	ob_start();

	// Query
	$navigation = $navigation ? get_query_var( is_front_page() ? 'page' : 'paged' ) : '1';
	$excursion_args = array(
		'post_type'			=> 'excursion',
		'posts_per_page'	=> $count,
		'category_name'		=> $category,
		'paged'				=> $navigation,
	);
	$excursion_query = new WP_Query( $excursion_args );

	// Render
	if( $excursion_query->have_posts() ) : ?>
		<div class="row">
			<div class="excursions">
				<?php while( $excursion_query->have_posts() ) : $excursion_query->the_post(); ?>
					<article class="col-sm-3">
						<div class="excursion-item">
							<header class="excursion-header">
								<?php echo get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_courses_img', 'echo' => false ) ); ?>
								<div class="title-meta-sec">
									<?php
										the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
							
										// meta datas
										$excursion_meta = rwmb_meta( 'michigan_excursion_location_meta' );

										if( $excursion_meta )
											echo '<span class="entry-location">' . esc_html( $excursion_meta ) . '</span>';
							
										echo '<span class="entry-time">' . get_the_time( 'm/d/Y - H:i A' ) . '</span>';
									?>
								</div>
							</header>
							<div class="excursion-content">
								<?php the_excerpt(); ?>
								<a class="more-info" href="<?php the_permalink(); ?>"><?php esc_html_e( 'More info', 'michigan' ); ?></a>
							</div>
						</div>
					</article>
				<?php endwhile; ?>
			</div>
		</div>
		<?php if ( $navigation != 1 ) : ?>
			<div class="pagination">
				<div class="row">
					<div class="col-sm-12">
						<?php
							if( function_exists( 'wp_pagenavi' ) )
								wp_pagenavi( array( 'query' => $excursion_query ) );
						?>
					</div>
					<hr class="vertical-space2">
				</div>
			</div>
		<?php endif;
	endif;

	wp_reset_postdata();


	// Output
	$out = ob_get_contents();
	ob_end_clean();
	return $out;
}

add_shortcode( 'excursion', 'michigan_excursion' );