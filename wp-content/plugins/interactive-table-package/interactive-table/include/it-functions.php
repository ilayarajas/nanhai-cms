<?php
// A callback function to add a custom field to our "it_cell_category" taxonomy  
function cell_category_taxonomy_custom_fields($tag) {  
   // Check for existing taxonomy meta for the term we're editing  
	$t_id = $tag->term_id; // Get the ID of the term we're editing  
	$term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check  
?>  
  
<tr class="form-field">  
	<th scope="row" valign="top">  
		<label for="cell_cat_order"><?php _e('Order'); ?></label>  
	</th>  
	<td>  
		<input type="text" name="term_meta[cell_cat_order]" id="term_meta[cell_cat_order]" size="25" style="width:60%;" value="<?php echo $term_meta['cell_cat_order'] ? $term_meta['cell_cat_order'] : ''; ?>"><br />  
		<span class="description"><?php _e('Cell Category Order.'); ?></span>  
	</td>  
</tr>  

<tr class="form-field">  
	<th scope="row" valign="top">  
		<label for="cell_cat_color"><?php _e('Color'); ?></label>  
	</th>  
	<td>  
		<input type="text" name="term_meta[cell_cat_color]" id="term_meta[cell_cat_color]" size="25" class="color-picker" value="<?php echo $term_meta['cell_cat_color'] ? $term_meta['cell_cat_color'] : ''; ?>"><br />  
		<span class="description"><?php _e('Cell Category Color.'); ?></span>  
	</td>  
</tr>  
  
<?php  
}

function add_custom_tax_field_oncreate( $term ){
	echo "<div class='form-field term-order-wrap'>";
	echo "<label for='term_meta[cell_cat_order]'>Order</label>";
	echo "<input id='term_meta[cell_cat_order]' value='' size='10' type='text' name='term_meta[cell_cat_order]'/>";
	echo '<p class="description">Cell Category Order.</p>';
	echo "<div>";
	
	echo "<div class='form-field term-color-wrap'>";
	echo "<label for='term_meta[cell_cat_color]'>Color</label>";
	// Get the custom fields based on the $presenter term ID  
	/*$cell_cat_custom_fields = get_option( "taxonomy_term_$term->term_id" );
	$cell_cat_custom_fields['cell_cat_color'];*/
	echo '<input type="text" id="term_meta[cell_cat_color]" name="term_meta[cell_cat_color]" class="color-picker" value="" />';
	echo '<p class="description">Cell Category Color.</p>';
	echo '<br />';
	echo "<div>";
}
// Add the fields to the "it_cell_category" taxonomy, using our callback function
add_action( 'it_cell_category_add_form_fields', 'add_custom_tax_field_oncreate' );
add_action( 'it_cell_category_edit_form_fields', 'cell_category_taxonomy_custom_fields', 10, 2 );  

// A callback function to save our extra taxonomy field(s)  
function save_taxonomy_custom_fields( $term_id ) {  
    if ( isset( $_POST['term_meta'] ) ) {  
        $t_id = $term_id;  
        $term_meta = get_option( "taxonomy_term_$t_id" );  
        $cat_keys = array_keys( $_POST['term_meta'] );  
            foreach ( $cat_keys as $key ){  
            if ( isset( $_POST['term_meta'][$key] ) ){  
                $term_meta[$key] = $_POST['term_meta'][$key];  
            }  
        }  
        //save the option array  
        update_option( "taxonomy_term_$t_id", $term_meta );  
    }  
}  
// Save the changes made on the "it_cell_category" taxonomy, using our callback function
add_action( 'create_it_cell_category', 'save_taxonomy_custom_fields' );
add_action( 'edited_it_cell_category', 'save_taxonomy_custom_fields', 10, 2 );


function add_it_cell_category_columns($columns){
    $description = $columns['description'];
    unset($columns['description']);
    $slug = $columns['slug'];
    unset($columns['slug']);
    $posts = $columns['posts'];
    unset($columns['posts']);		
	$columns['term_id'] = 'ID';	
    $columns['description'] = $description;		
	//$columns['slug'] = $slug;	
	$columns['posts'] = $posts;	
    return $columns;
}
add_filter('manage_edit-it_cell_category_columns', 'add_it_cell_category_columns');

function add_it_cell_category_column_content($content, $column_name, $term_id){
    $term= get_term($term_id, 'it_cell_category');
    switch ($column_name) {
        case 'term_id':
            $content = $term_id;
            break;
        default:
            break;
    }
    return $content;
}
add_filter('manage_it_cell_category_custom_column', 'add_it_cell_category_column_content',10,3);