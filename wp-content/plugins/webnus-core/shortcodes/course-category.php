<?php
function michigan_course_category( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'type'			=> '1',
		'category'		=> '',
		'border_bottom'	=> true,
	), $atts));

	$category = get_term_by( 'slug', $category, 'course_cat' );

	
	// render
	$out = '';
	if ( $category ) :
		if(function_exists('tax_icons_output_term_icon')){
			$cat_icon = tax_icons_output_term_icon( $category->term_id )? tax_icons_output_term_icon( $category->term_id ):'';
		}else{
			$cat_icon = '';
		}
		switch ( $type ) :
			case '1' :
				$course_text = ( $category->count > 1 ) ? esc_html__( 'Books', 'michigan' ) : esc_html__( 'Book', 'michigan' );
$course_num = preg_split('/^Level\s+/', $category->name);
//var_dump($course_num);
				$out = '
				<div class="course-category-box c' . $course_num[1] . '">
					<a href="' . esc_url( get_category_link( $category ) ) . '" title="' . esc_attr( sprintf( __( '%s category', 'michigan' ), $category->name ) ) . '">
						<div class="ccb-content colorf">
							' . $cat_icon . '
							<span class="category-name">' . esc_html( $category->name ) . '</span>
						</div>
						<div class="ccb-hover-content colorb">
							<span class="category-count">' . esc_html( $category->count ) . '</span>
							<span>' . $course_text . '</span>
						</div>
					</a>
				</div>';
			break;

			case '2' :
				$border_bottom = $border_bottom ? '' : esc_attr( ' no-border' );
				$out = '
				<div class="course-category-box2' . $border_bottom . '">
					<a href="' . esc_url( get_category_link( $category ) ) . '" title="' . esc_attr( sprintf( __( '%s category', 'michigan' ), $category->name ) ) . '">
						<span class="colorf">' . $cat_icon . '</span>
						<span class="category-name">' . esc_html( $category->name ) . '</span>
					</a>
				</div>';
			break;
		endswitch;
	else :
		switch ( $type ) :
			case '1' :
				$out = '
				<div class="course-category-box">
					<a class="hcolorf" href="' . esc_url( get_site_url() ) . '/courses/" title="' . esc_attr( 'View all Categories', 'michigan' ). '">
						<div class="ccb-content colorf">
							<i class="fa-book"></i>
							<span class="category-name">' . esc_html__( 'All Books', 'michigan' ) . '</span>
						</div>
						<div class="ccb-hover-content colorb">
							<span>' . esc_html__( 'All Books', 'michigan' ) . '</span>
						</div>
					</a>
				</div>';
			break;

			case '2' :
				$border_bottom = $border_bottom ? '' : esc_attr( ' no-border' );
				$out = '
				<div class="course-category-box2' . $border_bottom . '">
					<a class="hcolorf" href="' . esc_url( get_site_url() ) . '/courses/" title="' . esc_attr( 'View all Categories', 'michigan' ). '">
						<span class="colorf"><i class="fa-book"></i></span>
						<span class="category-name">' . esc_html( 'All Books', 'michigan' ) . '</span>
					</a>
				</div>';
			break;
		endswitch;
	endif;

	return $out;
}

add_shortcode( 'course-category', 'michigan_course_category' );
