<?php

function michigan_webnus_onethird( $attributes, $content = null ) {

	extract(shortcode_atts(array(
 	'last'  => null,
 	), $attributes));
	
	$out = '<div class="col-md-4">';
	$out .= do_shortcode($content);
	$out .= '</div>';
			
	return $out;
}
 add_shortcode('one_third', 'michigan_webnus_onethird');
 
 
function michigan_webnus_onehalf( $attributes, $content = null ) {

	extract(shortcode_atts(array(
 	'last'  => null,
 	), $attributes));
	
	$out = '<div class="col-md-6">';
	$out .= do_shortcode($content);
	$out .= '</div>';
			
	return $out;
}
 add_shortcode('one_half', 'michigan_webnus_onehalf');

 
 
function michigan_webnus_twothird( $attributes, $content = null ) {

	extract(shortcode_atts(array(
 	'last'  => null,
 	), $attributes));
	
	$out = '<div class="col-md-8">';
	$out .= do_shortcode($content);
	$out .= '</div>';
			
	return $out;
}
 add_shortcode('two_third', 'michigan_webnus_twothird');
 
 
 
 
function michigan_webnus_onefourth( $attributes, $content = null ) {

	extract(shortcode_atts(array(
 	'last'  => null,
 	), $attributes));
	
	$out = '<div class="col-md-3">';
	$out .= do_shortcode($content);
	$out .= '</div>';
			
	return $out;
}
 add_shortcode('one_fourth', 'michigan_webnus_onefourth');
 
 
 
function michigan_webnus_onesixth( $attributes, $content = null ) {

	extract(shortcode_atts(array(
 	'last'  => null,
 	), $attributes));
	
	$out = '<div class="col-md-2">';
	$out .= do_shortcode($content);
	$out .= '</div>';
			
	return $out;
}
 add_shortcode('one_sixth', 'michigan_webnus_onesixth');
 
 function michigan_webnus_onetwelfth( $attributes, $content = null ) {

	extract(shortcode_atts(array(
 	'last'  => null,
 	), $attributes));
	
	$out = '<div class="col-md-1">';
	$out .= do_shortcode($content);
	$out .= '</div>';
			
	return $out;
}
 add_shortcode('one_twelfth', 'michigan_webnus_onetwelfth');
 
?>