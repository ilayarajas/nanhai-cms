<?php 
class WatuPROPlayCharts {
	static function exam_by_points($quiz_id, $text, $color, $height, $step) {
		global $wpdb;
		
		// select exam
		$exam = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".WATUPRO_EXAMS." WHERE ID=%d", $quiz_id));
		
		// select all completed takings
		$takings = $wpdb->get_results($wpdb->prepare("SELECT points FROM ".WATUPRO_TAKEN_EXAMS." 
			WHERE exam_id=%d AND in_progress=0", $exam->ID));
			
		$num_takings = 0;
		$points_arr = array(); // this array will hold points(keys) and no. users who got that many points (vals)
		
		foreach($takings as $taking) {
			$num_takings++;
			$points = round($taking->points); // round them
			if(isset($points_arr[$points])) $points_arr[$points]++;
			else $points_arr[$points] = 1;
		}	
		
		// now recalculate points values in %
		foreach($points_arr as $key=>$val) {
			// avoid division by zero error
			if($num_takings == 0) {
				$points_arr[$key] = 0;
				continue;
			} 
			
			// now calculate
			$percent = round( 100 * $val / $num_takings);
			$points_arr[$key] = $percent;  
			if($percent == 0) unset($points_arr[$key]);
		}
		
		ksort($points_arr);
		
		include(WATUPROPLAY_PATH."/views/chart-exam-by-points.html.php");
	} // end exam_by_points
	
	static function exam_by_grades($quiz_id, $text, $color, $height, $step, $ob = 'points DESC') {
		global $wpdb;
		
		// select exam
		$exam = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".WATUPRO_EXAMS." WHERE ID=%d", $quiz_id));
		
		// select all completed takings
		$takings = $wpdb->get_results($wpdb->prepare("SELECT tT.ID as ID, tT.points as points, 
			tG.gtitle as grade_title, tG.ID as grade_id, tG.gfrom as gfrom 
			FROM ".WATUPRO_TAKEN_EXAMS." tT JOIN ".WATUPRO_GRADES." tG ON tG.ID = tT.grade_id
			WHERE tT.exam_id=%d AND tT.in_progress=0 ORDER BY $ob", $exam->ID));
			
		$num_takings = 0;
		$grades_arr = array(); // this array will hold grade_ids(keys) and no. users who got that grade (vals)
		foreach($takings as $taking) {
			$num_takings++;			
			if(isset($grades_arr[$taking->grade_title])) $grades_arr[$taking->grade_title]++;
			else $grades_arr[$taking->grade_title] = 1;
		}	
		
		// now recalculate grade values in %
		foreach($grades_arr as $key=>$val) {
			// avoid division by zero error
			if($num_takings == 0) {
				$grades_arr[$key] = 0;
				continue;
			} 
			
			// now calculate
			$percent = round( 100 * $val / $num_takings);
			$grades_arr[$key] = $percent;  
			if($percent == 0) unset($grades_arr[$key]);
		}
		
		// this may be used if we decide to add order by highest % users
		// ksort($grades_arr);
		
		// for now it makes no sense to create another view. So let's use the same and assign the var
		$points_arr = $grades_arr;		
		include(WATUPROPLAY_PATH."/views/chart-exam-by-points.html.php");
	}  // end exam_by_grades
	
	// average score / your score
	static function exam_by_avg_score($quiz_id, $text, $color, $height, $step) {
		global $wpdb, $user_ID;
		
		if(empty($_POST['quiz_id']) and empty($user_ID)) {
			echo __('This chart is only for logged in users.', 'watuproplay');
			return true;
		}
		
		// now we either work with current taking ID or latest taking ID for the user
		if(empty($_POST['quiz_id'])) {
			$taking_id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM ".WATUPRO_TAKEN_EXAMS."
				WHERE user_id=%d AND exam_id=%d AND in_progress=0
				ORDER BY ID DESC LIMIT 1", $user_ID, $quiz_id)); 
		}
		else $taking_id = $_POST['watupro_current_taking_id'];
		
		// select % correct answers
		$your_score = $wpdb->get_var($wpdb->prepare("SELECT percent_correct FROM ".WATUPRO_TAKEN_EXAMS."
			 WHERE ID=%d", $taking_id));
		
		// select avg percent
		$avg_score = $wpdb->get_var($wpdb->prepare("SELECT AVG(percent_correct) FROM ".WATUPRO_TAKEN_EXAMS."
			WHERE exam_id=%d AND in_progress=0", $quiz_id));
		
		// texts
		$texts = explode('|', $text);
		if(empty($texts[0])) $texts[0] = __('Your score:', 'watuproplay');
		if(empty($texts[1])) $texts[1] = __('Average score:', 'watuproplay');
		
		// colors	
		$colors = explode('|', $color);	
		if(empty($colors[0])) $colors[0] = 'gray';
		if(empty($colors[1])) $colors[1] = 'blue';
			
		include(WATUPROPLAY_PATH."/views/chart-exam-by-avg.html.php");
	}
}