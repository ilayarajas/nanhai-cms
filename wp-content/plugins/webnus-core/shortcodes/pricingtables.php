<?php
function michigan_webnus_pricing_tables( $attributes, $content = null ) {
	extract(shortcode_atts(array(
		'type'			=> '1',
		'title'			=> '1',
		'price'			=> '10',
		'currency'		=> '$',
		'period'		=> 'Month',
		'pt_popular'	=> '',
		'link_url'		=> '',
		'link_text'		=> '',
		'features'		=> '',
		'featured'		=> '',
	), $attributes));
	
	// features loop
	$features_out	= '';
	$features_data	= array();
	$featured		= (!empty($featured)) ? ' featured' : '' ;
	$features		= (array) vc_param_group_parse_atts( $features );

	foreach ( $features as $data ) :
		$new_line 					= $data;
		$new_line['feature_item']	= isset($data['feature_item']) ? $data['feature_item'] : '';
		$features_data[] 			= $new_line;
	endforeach;

	foreach ( $features_data as $line ) :
		$features_out .= '<li class="feature-item">' . esc_html( $line['feature_item'] ) . '</li>';
	endforeach;
	$colorf = ($type == 3)? 'colorf' : '';
	$colorb = ($type == 3)? 'colorb' : '';
	$title = $title ? '<h5 class="plan-title '.$colorf.'">' . esc_html( $title ) . ' </h5>' : '' ;
	$pt_popular = $pt_popular ? '<span> '. esc_html( $pt_popular ) .' </span>' : '' ;
	$currency = $currency ? '<span>'.esc_html($currency).'</span>' : '' ;
	$period = $period ? '<small>/' . esc_html($period) . '</small>' : '' ;
	$features_out = $features_out ? '<ul class="features">' .$features_out. ' </ul>' : '' ;

	$out ='
		<div class="w-pricing-table' . $type . ' hcolorf ' . $featured . '">
			<div class="price-header">
				' . $title  . ' 
			</div> 
			<div class="ptcontent">
				'. $pt_popular .'
				<h6 class="plan-price">' .$currency. $price  . $period . '</h6>' .$features_out. '
			<div class="price-footer"> 
				<a class="readmore '.$colorb.'" href="' . esc_url($link_url) . '" class="button">' . $link_text . '</a>
			</div>
			</div>
		</div> <!-- end pricing-table -->';

	return $out;
}

add_shortcode('pricing-tables', 'michigan_webnus_pricing_tables');