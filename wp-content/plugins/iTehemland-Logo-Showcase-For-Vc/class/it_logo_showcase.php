<?php
if(!class_exists('it_vc_logo_showcase'))
{
	class it_vc_logo_showcase
	{	
		
		public $layout,$desktop_column,$tablet_column,$mobile_column,$skin,$autoplay,$autoplay_speed,$pause,$laptop_item,$desktop_item,$tablet_item,$mobile_item,$pagination,$navigation,$background_color,$border_style,$border_width_top,$border_width_right,$border_width_bottom,$border_width_left,$border_color,$border_radius_top_right,$border_radius_top_left,$border_radius_bottom_right,$border_radius_bottom_left,$padding_top,$padding_right,$padding_bottom,$padding_left,$margin_top,$margin_right,$margin_bottom,$margin_left,$shadow,$shadow_h,$shadow_v,$shadow_blur,$shadow_spread,$shadow_color,$item_animation,$custom_class,$hover_background_color,$effect,$hover_border_style,$hover_border_width_top,$hover_border_width_right,$hover_border_width_bottom,$hover_border_width_left,$hover_border_color,$hover_border_radius_top_right,$hover_border_radius_top_left,$hover_border_radius_bottom_right,$hover_border_radius_bottom_left,$hover_shadow,$hover_shadow_h,$hover_shadow_v,$hover_shadow_blur,$hover_shadow_spread,$hover_shadow_color,$tooltip,$quick_view_type,$view_type,$title,$contact,$social,$full_desc,$gallery,$video,$rand_id,$script_outputs,$css_classes,$modal,$tooltip_class,$show_class,$logo_effect,$i,$shortdesc,$full_args;
		
		function __construct()
		{
			add_action('vc_before_init',array($this,'createShortcodes'));
			add_shortcode( 'it_vc_logo_showcase', array( $this, 'renderShortcode' ) );
			add_shortcode( 'it_vc_logo_showcase_item', array( $this, 'renderitemShortcode' ) );
		}
		// Shortcode function
		function createShortcodes()
		{
			include( 'class_it_logo_showcase.php' );

			if(function_exists('vc_map'))
			{
				// Parent container
				vc_map( array(
					"name" => __( 'Logo Showcase', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
					"base" => "it_vc_logo_showcase",
					"icon" => __IT_VC_LOGO_SHOWCASE_ROOT_VC_URL__ .'/img/icon.png',
					"as_parent" => array( 'only' => 'it_vc_logo_showcase_item' ),
					"js_view" => 'VcColumnView',
					"content_element" => true,
					'is_container' => true,
					'container_not_allowed' => false,
					"category" => __('iThemelandco Addons', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
					"params" => array(
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("<div class='it-main-heading'><span>Layout Setting</span></div>Layout",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("Choose your Layout",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "layout",
							"value" => array(
								__("list" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"list",
								__("grid" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"grid",
								__("carousel" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"carousel",
								),
							"description" => __( 'Layout', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN   ),
							'group' => "Layout"
						),
						/////**grid layout****/////
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Desktop Column",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "desktop_column",
							"value" => array(
								__("1 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"1",
								__("2 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"2",
								__("3 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"3",
								__("4 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"4",
								__("6 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"6",
								__("12 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"12",	
								),
							"description" => __( 'Desktop Column', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN   ),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'grid')
										),
							'group' => "Layout"
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Tablet Column",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "tablet_column",
							"value" => array(
								__("1 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"1",
								__("2 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"2",
								__("3 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"3",
								__("4 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"4",
								__("6 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"6",
								__("12 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"12",	
								),
							"description" => __( 'Tablet Column', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN   ),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'grid')
										),
							'group' => "Layout"
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Mobile Column",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "mobile_column",
							"value" => array(
								__("1 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"1",
								__("2 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"2",
								__("3 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"3",
								__("4 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"4",
								__("6 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"6",
								__("12 Column" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"12",	
								),
							"description" => __( 'Mobile Column', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN   ),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'grid')
										),
							'group' => "Layout"
						),
						/////**carousel layout****/////
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("<div class='it-main-heading'><span>Carousel Setting</span></div>Carousel Skin",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("Choose your Skin",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "skin",
							"value" => array(
								__("Light" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"light",
								__("Dark" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"dark",
								__("Red" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"red",
								__("Orange" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"orange",
								__("Blue" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"blue",
								__("Green" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"green",
								),
							"description" => __( 'Carousel Skin', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN   ),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'carousel')
										),

							'group' => "Layout"
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Auto Play",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("Auto Play",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "autoplay",
							"value" => array(
								__("false" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"false",
								__("true" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"true",
								),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'carousel')
										),

							'group' => "Layout"
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Auto Play Speed", IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "autoplay_speed",
							"value" =>'3000',
							"description" =>  __("Auto Play Speed", IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							"dependency" => array(
											'element' => 'autoplay',
											'value' => array( 'true')
										),

							'group' => "Layout",
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Stop On Hover",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("Stop On Hover",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "pause",
							"value" => array(
								__("false" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"false",
								__("true" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"true",
								),
							"dependency" => array(
											'element' => 'autoplay',
											'value' => array( 'true')
										),

							'group' => "Layout"
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("<div class='it-main-heading'><span>Carousel Items per view</span></div>Laptop",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "laptop_item",
							"value" =>'4',
							"description" =>  __("Laptop", IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'carousel')
										),

							'group' => "Layout",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Desktop",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "desktop_item",
							"value" =>'4',
							"description" =>  __("Desktop", IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'carousel')
										),

							'group' => "Layout",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Tablet",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "tablet_item",
							"value" =>'3',
							"description" =>  __("Tablet", IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'carousel')
										),

							'group' => "Layout",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Mobile",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "mobile_item",
							"value" =>'2',
							"description" =>  __("Mobile", IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'carousel')
										),

							'group' => "Layout",
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Show Pagination",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("Show Pagination",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "pagination",
							"value" => array(
								__("true" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"true",
								__("false" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"false",
								),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'carousel')
										),

							'group' => "Layout"
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Show Controls",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("Show Controls",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "navigation",
							"value" => array(
								__("true" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"true",
								__("false" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"false",
								),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'carousel')
										),

							'group' => "Layout"
						),
						/////**style tab****/////
						array(
						   "type" => "colorpicker",
						   "class" => "",
							"heading" => __("<div class='it-main-heading'><span>Style Setting</span></div>Item Background Color",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "background_color",
						   "value" => '',
						   "description" => __("Leave blank to ignore",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   'group' => "Styles"
						  ),
						/////**borders****/////
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Border Style",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "border_style",
							"value" => array(
								__("None" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"none",
								__("Solid" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"solid",
								__("Dashed" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"dashed",
								__("Dotted" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"dotted",
										),
							"description" => __( 'Border Style', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN   ),
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("<div><span>Border Width :</span></div></br>Top",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "border_width_top",
							"value" =>'1',
							"dependency" => array(
											'element' => 'border_style',
											'value' => array( 'solid','dashed','dotted')
										),

							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Right",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "border_width_right",
							"value" =>'1',
							"dependency" => array(
											'element' => 'border_style',
											'value' => array( 'solid','dashed','dotted')
										),

							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Bottom",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "border_width_bottom",
							"value" =>'1',
							"dependency" => array(
											'element' => 'border_style',
											'value' => array( 'solid','dashed','dotted')
										),

							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Left",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "border_width_left",
							"value" =>'1',
							"dependency" => array(
											'element' => 'border_style',
											'value' => array( 'solid','dashed','dotted')
										),

							'group' => "Styles",
						),
						array(
						   "type" => "colorpicker",
						   "class" => "",
						   "heading" => __("Border Color",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "border_color",
						   "value" => '',
						   "description" => __("Leave blank to ignore",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"dependency" => array(
											'element' => 'border_style',
											'value' => array( 'solid','dashed','dotted')
										),
						   'group' => "Styles"
						  ),
						////****border radius****////
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("<div><span>Border Radius :</span></div></br>Top-Right",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "border_radius_top_right",
							"value" =>'0',
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Tpo-left",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "border_radius_top_left",
							"value" =>'0',
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Bottom-Right",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "border_radius_bottom_right",
							"value" =>'0',
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Bottom-Left",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "border_radius_bottom_left",
							"value" =>'0',
							'group' => "Styles",
						),
						////****padding****////
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("<div><span>Padding :</span></div></br>Top",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "padding_top",
							"value" =>'0',
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Right",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "padding_right",
							"value" =>'0',
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Bottom",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "padding_bottom",
							"value" =>'0',
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Left",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "padding_left",
							"value" =>'0',
							'group' => "Styles",
						),
						////****margin****////
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("<div><span>Margin :</span></div></br>Top",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "margin_top",
							"value" =>'0',
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'list','grid')
										),
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Right",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "margin_right",
							"value" =>'0',
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Bottom",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "margin_bottom",
							"value" =>'0',
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'list','grid')
										),
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Left",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "margin_left",
							"value" =>'0',
							'group' => "Styles",
						),
						////****Shadow Setting****////
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("<div class='it-main-heading'><span>Shadow Setting</span></div>Box Shadow",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("Box Shadow",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "shadow",
							"value" => array(
								__("None" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"none",
								__("Outset Shadow" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"outset",
								__("Inset Shadow" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"inset",
								),
							'group' => "Styles"
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Horizontal",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "shadow_h",
							"value" =>'0',
							"dependency" => array(
											'element' => 'shadow',
											'value' => array( 'outset','inset')
										),
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Vertical",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "shadow_v",
							"value" =>'0',
							"dependency" => array(
											'element' => 'shadow',
											'value' => array( 'outset','inset')
										),
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Blur",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "shadow_blur",
							"value" =>'0',
							"dependency" => array(
											'element' => 'shadow',
											'value' => array( 'outset','inset')
										),
							'group' => "Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Spread",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "shadow_spread",
							"value" =>'0',
							"dependency" => array(
											'element' => 'shadow',
											'value' => array( 'outset','inset')
										),
							'group' => "Styles",
						),
						array(
						   "type" => "colorpicker",
						   "class" => "",
						   "heading" => __("Color",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "shadow_color",
						   "value" => '',
						   "description" => __("Leave blank to ignore",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"dependency" => array(
											'element' => 'shadow',
											'value' => array( 'outset','inset')
										),
						   'group' => "Styles"
						  ),
						/////*****item animation*****//////
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Item Animation",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "item_animation",
							"value" => array(
								__("None" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"it-no-animation",
								__("bounce" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"bounce",
								__("flash" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"flash",	
								__("pulse" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"pulse",											
								__("rubberBand" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"rubberBand",	
								__("shake" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"shake",	
								__("swing" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"swing",	
								__("tada" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"tada",	
								__("wobble" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"wobble",	
								__("jello" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"jello",	
								__("bounceIn" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"bounceIn",	
								__("bounceInDown" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"bounceInDown",	
								__("bounceInLeft" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"bounceInLeft",	
								__("bounceInRight" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"bounceInRight",	
								__("bounceInUp" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"bounceInUp",	
								__("fade" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"fade",	
								__("fadeInDown" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"fadeInDown",	
								__("fadeInDownBig" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"fadeInDownBig",	
								__("fadeInLeft" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"fadeInLeft",	
								__("fadeInLeftBig" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"fadeInLeftBig",	
								__("fadeInRight" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"fadeInRight",	
								__("fadeInRightBig" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"fadeInRightBig",	
								__("fadeInUp" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"fadeInUp",	
								__("fadeInUpBig" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"fadeInUpBig",	
								__("flipInX" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"flipInX",	
								__("flipInY" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"flipInY",	
								__("lightSpeedIn" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"lightSpeedIn",	
								__("rotateIn" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"rotateIn",	
								__("rotateInDownLeft" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"rotateInDownLeft",	
								__("rotateInDownRight" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"rotateInDownRight",	
								__("rotateInUpLeft" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"rotateInUpLeft",	
								__("rotateInUpRight" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"rotateInUpRight",	
								__("slideInDown" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"slideInDown",	
								__("slideInLeft" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"slideInLeft",	
								__("slideInRight" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"slideInRight",	
								__("slideInUp" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"slideInUp",	
								__("zoomIn" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"zoomIn",	
								__("zoomInDown" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"zoomInDown",	
								__("zoomInLeft" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"zoomInLeft",	
								__("zoomInRight" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"zoomInRight",	
								__("zoomInUp" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"zoomInUp",	
								__("rollIn" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"rollIn",	
								),
							"description" => __( 'Item Animation', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN   ),
							'group' => "Styles"
						),
						array(
								"type" => "textfield",
								"heading" => __( 'Custom Class', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "custom_class",
								'group' => "Styles",
						),
						/////**Hover Styles Tab****/////
						array(
						   "type" => "colorpicker",
						   "class" => "",
							"heading" => __("<div class='it-main-heading'><span>Hover Setting</span></div>Item Background Color",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "hover_background_color",
						   "value" => '',
						   "description" => __("Leave blank to ignore",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   'group' => "Hover Styles"
						  ),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Effect on hover",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("Effect on hover",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "effect",
							"value" => array(
								__("None" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"none",
								__("Opacity" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"ls-opacity",
								__("Grey Scale" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"ls-greyscale",
								__("Zoom In" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"ls-zoomin",
								__("Zoom Out" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"ls-zoomout",
								),
							'group' => "Hover Styles"
						),
						/////**hover borders****/////
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Border Style",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_border_style",
							"value" => array(
								__("None" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"none",
								__("Solid" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"solid",
								__("Dashed" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"dashed",
								__("Dotted" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"dotted",
										),
							"description" => __( 'Border Style', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN   ),
							'group' => "Hover Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("<div><span>Hover Border Width :</span></div></br>Top",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_border_width_top",
							"value" =>'0',
							"dependency" => array(
											'element' => 'hover_border_style',
											'value' => array( 'solid','dashed','dotted')
										),

							'group' => "Hover Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Right",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_border_width_right",
							"value" =>'0',
							"dependency" => array(
											'element' => 'hover_border_style',
											'value' => array( 'solid','dashed','dotted')
										),

							'group' => "Hover Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Bottom",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_border_width_bottom",
							"value" =>'0',
							"dependency" => array(
											'element' => 'hover_border_style',
											'value' => array( 'solid','dashed','dotted')
										),

							'group' => "Hover Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Left",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_border_width_left",
							"value" =>'0',
							"dependency" => array(
											'element' => 'hover_border_style',
											'value' => array( 'solid','dashed','dotted')
										),

							'group' => "Hover Styles",
						),
						array(
						   "type" => "colorpicker",
						   "class" => "",
						   "heading" => __("Border Color",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "hover_border_color",
						   "value" => '',
						   "description" => __("Leave blank to ignore",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"dependency" => array(
											'element' => 'hover_border_style',
											'value' => array( 'solid','dashed','dotted')
										),
						   'group' => "Hover Styles"
						  ),
						////****اhover border radius****////
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("<div><span>Hover Border Radius :</span></div></br>Top-Right",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_border_radius_top_right",
							"value" =>'0',
							'group' => "Hover Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Tpo-left",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_border_radius_top_left",
							"value" =>'0',
							'group' => "Hover Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Bottom-Right",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_border_radius_bottom_right",
							"value" =>'0',
							'group' => "Hover Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Bottom-Left",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_border_radius_bottom_left",
							"value" =>'0',
							'group' => "Hover Styles",
						),
						////****hover Shadow Setting****////
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("<div class='it-main-heading'><span>Hover Shadow Setting</span></div>Box Shadow",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("Box Shadow",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_shadow",
							"value" => array(
								__("None" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"none",
								__("Outset Shadow" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"outset",
								__("Inset Shadow" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"inset",
								),
							'group' => "Hover Styles"
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Horizontal",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_shadow_h",
							"value" =>'0',
							"dependency" => array(
											'element' => 'hover_shadow',
											'value' => array( 'outset','inset')
										),
							'group' => "Hover Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Vertical",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_shadow_v",
							"value" =>'0',
							"dependency" => array(
											'element' => 'hover_shadow',
											'value' => array( 'outset','inset')
										),
							'group' => "Hover Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Blur",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_shadow_blur",
							"value" =>'0',
							"dependency" => array(
											'element' => 'hover_shadow',
											'value' => array( 'outset','inset')
										),
							'group' => "Hover Styles",
						),
						array(
							"type" => "it_number",
							"class" => "",
							"heading" => __("Spread",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "hover_shadow_spread",
							"value" =>'0',
							"dependency" => array(
											'element' => 'hover_shadow',
											'value' => array( 'outset','inset')
										),
							'group' => "Hover Styles",
						),
						array(
						   "type" => "colorpicker",
						   "class" => "",
						   "heading" => __("Color",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "hover_shadow_color",
						   "value" => '',
						   "description" => __("Leave blank to ignore",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"dependency" => array(
											'element' => 'hover_shadow',
											'value' => array( 'outset','inset')
										),
						   'group' => "Hover Styles"
						  ),
						/////**View Details Tab****/////
						array(
						   "type" => "checkbox",
						   "class" => "",
							"heading" => __("<div class='it-main-heading'><span>Quick View Setting</span></div>Tooltip",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "tooltip",
						   "value" => array(
							""=>"true"          
						   ),
							"dependency" => array(
											'element' => 'layout',
											'value' => array( 'grid','carousel')
										),
						   'group' => "View Details"
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Quick View Type",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("Quick View Type",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "quick_view_type",
							"value" => array(
								__("None" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"none",
								__("Full View" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"full",
								),
							'group' => "View Details"
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("View Type",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"description" => __("View Type",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
							"param_name" => "view_type",
							"value" => array(
								__("Pop Up" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"popup",
								__("Inline" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"inline",
								),
							"dependency" => array(
											'element' => 'quick_view_type',
											'value' => array( 'full')
										),
							'group' => "View Details"
						),
						array(
						   "type" => "checkbox",
						   "class" => "",
							"heading" => __("Hide Title",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "title",
						   "value" => array(
							""=>"false"          
						   ),
							"dependency" => array(
											'element' => 'quick_view_type',
											'value' => array( 'full')
										),
						   'group' => "View Details"
						),
						array(
						   "type" => "checkbox",
						   "class" => "",
							"heading" => __("Hide Contact Info",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "contact",
						   "value" => array(
							""=>"false"          
						   ),
							"dependency" => array(
											'element' => 'quick_view_type',
											'value' => array( 'full')
										),
						   'group' => "View Details"
						),
						array(
						   "type" => "checkbox",
						   "class" => "",
							"heading" => __("Hide Social Links",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "social",
						   "value" => array(
							""=>"false"          
						   ),
							"dependency" => array(
											'element' => 'quick_view_type',
											'value' => array( 'full')
										),
						   'group' => "View Details"
						),
						array(
						   "type" => "checkbox",
						   "class" => "",
							"heading" => __("Hide Full Description",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "full_desc",
						   "value" => array(
							""=>"false"          
						   ),
							"dependency" => array(
											'element' => 'quick_view_type',
											'value' => array( 'full')
										),
						   'group' => "View Details"
						),
						array(
						   "type" => "checkbox",
						   "class" => "",
							"heading" => __("Hide Gallery Images",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "gallery",
						   "value" => array(
							""=>"false"          
						   ),
							"dependency" => array(
											'element' => 'quick_view_type',
											'value' => array( 'full')
										),
						   'group' => "View Details"
						),
						array(
						   "type" => "checkbox",
						   "class" => "",
							"heading" => __("Hide Video",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
						   "param_name" => "video",
						   "value" => array(
							""=>"false"          
						   ),
							"dependency" => array(
											'element' => 'quick_view_type',
											'value' => array( 'full')
										),
						   'group' => "View Details"
						),
						
					)
					));
				// Nested Element
				vc_map( array(
						"name" => __( 'Logo Showcase Item', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
						"base" => "it_vc_logo_showcase_item",
						"icon" => __IT_VC_LOGO_SHOWCASE_ROOT_VC_URL__ .'/img/icon.png',
						"as_child" => array( 'only' => 'it_vc_logo_showcase' ),
						"content_element" => true,
						'container_not_allowed' => false,
						"params" => array(
							array(
								"type" => "textfield",
								"heading" => __( 'Title', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "title",
								"description" => __( 'Title', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							),
							array(
								"type" => "attach_image",
								"class" => "",
								"heading" => __("Logo Image",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "image",
								"description" => __( 'Set featured image', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN   ),
							),
							array(
								"type" => "textarea",
								"heading" => __("<div class='it-main-heading'><span>General</span></div>Short Description",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
								"param_name" => "shortdesc",
								"description" => __( 'Enter Short Desc here ... ', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							),
							array(
								"type" => "textfield",
								"heading" => __( 'Link Url', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "link_url",
								"description" => __( 'http://www.example.com/', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							),
							array(
								"type" => "dropdown",
								"heading" => __("Link Target",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
								"description" => __("Choose Target",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
								"param_name" => "link_target",
								"value" => array(
									__("_blank" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"_blank",
									__("_self" ,  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  )=>"_self",
									),
							),
							array(
								"type" => "textarea_html",
								"heading" => __("<div class='it-main-heading'><span>Quick view details</span></div>Full Description",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
								"param_name" => "content",
								"description" => '',
							),
							array(
								"type" => "textfield",
								"heading" => __( 'Website', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "website",
								"description" => __( 'http://www.example.com/', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							),
							array(
								"type" => "textfield",
								"heading" => __( 'Email', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "email",
								"description" => __( 'example@domain.com', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							),
							array(
								"type" => "textfield",
								"heading" => __( 'Tell', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "tell",
								"description" => __( '+1-2112345678', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							),
							array(
								"type" => "attach_images",
								"class" => "",
								"heading" => __("<div class='it-main-heading'><span>Gallery options</span></div>Full Description",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
								"heading" => __("Images",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "gallery_image",
								"description" => __( 'Choose you images for gallery', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN   ),
							),
							array(
								"type" => "textfield",
								"heading" => __("<div class='it-main-heading'><span>Social Links</span></div>Facebook",  IT_VC_LOGO_SHOWCASE_TEXTDOMAIN  ),
								"param_name" => "facebook",
								"description" => __( 'http://www.facebook.com/', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							),
							array(
								"type" => "textfield",
								"heading" => __( 'Twitter', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "twitter",
								"description" => __( 'http://www.twitter.com/', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							),
							array(
								"type" => "textfield",
								"heading" => __( 'Instagram', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "instagram",
								"description" => __( 'http://www.instagram.com/', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							),
							array(
								"type" => "textfield",
								"heading" => __( 'Google+', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "google_plus",
								"description" => __( 'http://www.google.com/', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
							),
							array(
								"type" => "textfield",
								"heading" => __( 'Linked In', IT_VC_LOGO_SHOWCASE_TEXTDOMAIN ),
								"param_name" => "linked_in",
								"description" => '',
							),

							)
						));	
			}
		}
		// Shortcode handler function for  Parent
		function renderShortcode($atts, $content = null)
		{
			$layout=
			$desktop_column=
			$tablet_column=
			$mobile_column=
			$skin=
			$autoplay=
			$autoplay_speed=
			$pause=
			$laptop_item=
			$desktop_item=
			$tablet_item=
			$mobile_item=
			$pagination=
			$navigation=
			$background_color=
			$border_style=
			$border_width_top=
			$border_width_right=
			$border_width_bottom=
			$border_width_left=
			$border_color=
			$border_radius_top_right=
			$border_radius_top_left=
			$border_radius_bottom_right=
			$border_radius_bottom_left=
			$padding_top=
			$padding_right=
			$padding_bottom=
			$padding_left=
			$margin_top=
			$margin_right=
			$margin_bottom=
			$margin_left=
			$shadow=
			$shadow_h=
			$shadow_v=
			$shadow_blur=
			$shadow_spread=
			$shadow_color=
			$item_animation=
			$custom_class=
			$hover_background_color=
			$effect=
			$hover_border_style=
			$hover_border_width_top=
			$hover_border_width_right=
			$hover_border_width_bottom=
			$hover_border_width_left=
			$hover_border_color=
			$hover_border_radius_top_right=
			$hover_border_radius_top_left=
			$hover_border_radius_bottom_right=
			$hover_border_radius_bottom_left=
			$hover_shadow=
			$hover_shadow_h=
			$hover_shadow_v=
			$hover_shadow_blur=
			$hover_shadow_spread=
			$hover_shadow_color=
			$tooltip=
			$quick_view_type=
			$view_type=
			$title=
			$contact=
			$social=
			$full_desc=
			$gallery=
			$video=
			$output='';
			
			extract(shortcode_atts( array(
				'layout'=>'list',
				'desktop_column'=>'1',
				'tablet_column'=>'1',
				'mobile_column'=>'1',
				'skin'=>'light',
				'autoplay'=>'false',
				'autoplay_speed'=>'3000',
				'pause'=>'false',
				'laptop_item'=>'4',
				'desktop_item'=>'4',
				'tablet_item'=>'3',
				'mobile_item'=>'2',
				'pagination'=>'true',
				'navigation'=>'true',
				'background_color'=>'',
				'border_style'=>'none',
				'border_width_top'=>'0',
				'border_width_right'=>'0',
				'border_width_bottom'=>'0',
				'border_width_left'=>'0',
				'border_color'=>'#eee',
				'border_radius_top_right'=>'0',
				'border_radius_top_left'=>'0',
				'border_radius_bottom_right'=>'0',
				'border_radius_bottom_left'=>'0',
				'padding_top'=>'0',
				'padding_right'=>'0',
				'padding_bottom'=>'0',
				'padding_left'=>'0',
				'margin_top'=>'0',
				'margin_right'=>'0',
				'margin_bottom'=>'0',
				'margin_left'=>'0',
				'shadow'=>'none',
				'shadow_h'=>'0',
				'shadow_v'=>'0',
				'shadow_blur'=>'0',
				'shadow_spread'=>'0',
				'shadow_color'=>'#999',
				'item_animation'=>'it-no-animation',
				'custom_class'=>'',
				'hover_background_color'=>'',
				'effect'=>'none',
				'hover_border_style'=>'none',
				'hover_border_width_top'=>'0',
				'hover_border_width_right'=>'0',
				'hover_border_width_bottom'=>'0',
				'hover_border_width_left'=>'0',
				'hover_border_color'=>'#eee',
				'hover_border_radius_top_right'=>'0',
				'hover_border_radius_top_left'=>'0',
				'hover_border_radius_bottom_right'=>'0',
				'hover_border_radius_bottom_left'=>'0',
				'hover_shadow'=>'none',
				'hover_shadow_h'=>'0',
				'hover_shadow_v'=>'0',
				'hover_shadow_blur'=>'0',
				'hover_shadow_spread'=>'0',
				'hover_shadow_color'=>'#999',
				'tooltip'=>'false',
				'quick_view_type'=>'none',
				'view_type'=>'popup',
				'title'=>'true',
				'contact'=>'true',
				'social'=>'true',
				'full_desc'=>'true',
				'gallery'=>'true',
				'video'=>'true',
				),$atts));

			$circle = "";
			$this->layout=$layout;
			$this->desktop_column=$desktop_column;
			$this->tablet_column=$tablet_column;
			$this->mobile_column=$mobile_column;
			$this->skin=$skin;
			$this->autoplay=$autoplay;
			$this->autoplay_speed=$autoplay_speed;
			$this->pause=$pause;
			$this->laptop_item=$laptop_item;
			$this->desktop_item=$desktop_item;
			$this->tablet_item=$tablet_item;
			$this->mobile_item=$mobile_item;
			$this->pagination=$pagination;
			$this->navigation=$navigation;
			$this->background_color=$background_color;
			$this->border_style=$border_style;
			$this->border_width_top=$border_width_top;
			$this->border_width_right=$border_width_right;
			$this->border_width_bottom=$border_width_bottom;
			$this->border_width_left=$border_width_left;
			$this->border_color=$border_color;
			$this->border_radius_top_right=$border_radius_top_right;
			$this->border_radius_top_left=$border_radius_top_left;
			$this->border_radius_bottom_right=$border_radius_bottom_right;
			$this->border_radius_bottom_left=$border_radius_bottom_left;
			$this->padding_top=$padding_top;
			$this->padding_right=$padding_right;
			$this->padding_bottom=$padding_bottom;
			$this->padding_left=$padding_left;
			$this->margin_top=$margin_top;
			$this->margin_right=$margin_right;
			$this->margin_bottom=$margin_bottom;
			$this->margin_left=$margin_left;
			$this->shadow=$shadow;
			$this->shadow_h=$shadow_h;
			$this->shadow_v=$shadow_v;
			$this->shadow_blur=$shadow_blur;
			$this->shadow_spread=$shadow_spread;
			$this->shadow_color=$shadow_color;
			$this->item_animation=$item_animation;
			$this->custom_class=$custom_class;
			$this->hover_background_color=$hover_background_color;
			$this->effect=$effect;
			$this->hover_border_style=$hover_border_style;
			$this->hover_border_width_top=$hover_border_width_top;
			$this->hover_border_width_right=$hover_border_width_right;
			$this->hover_border_width_bottom=$hover_border_width_bottom;
			$this->hover_border_width_left=$hover_border_width_left;
			$this->hover_border_color=$hover_border_color;
			$this->hover_border_radius_top_right=$hover_border_radius_top_right;
			$this->hover_border_radius_top_left=$hover_border_radius_top_left;
			$this->hover_border_radius_bottom_right=$hover_border_radius_bottom_right;
			$this->hover_border_radius_bottom_left=$hover_border_radius_bottom_left;
			$this->hover_shadow=$hover_shadow;
			$this->hover_shadow_h=$hover_shadow_h;
			$this->hover_shadow_v=$hover_shadow_v;
			$this->hover_shadow_blur=$hover_shadow_blur;
			$this->hover_shadow_spread=$hover_shadow_spread;
			$this->hover_shadow_color=$hover_shadow_color;
			$this->tooltip=$tooltip;
			$this->quick_view_type=$quick_view_type;
			$this->view_type=$view_type;
			$this->title=$title;
			$this->contact=$contact;
			$this->social=$social;
			$this->full_desc=$full_desc;
			$this->gallery=$gallery;
			$this->video=$video;
			$this->i=0;
			$this->rand_id=rand(0,1000);
			$modal='';
			// grid class number
			$desk_num = intval(12 / $desktop_column);
			$tablet_num = intval(12 / $tablet_column);
			$mobile_num = intval(12 / $mobile_column);
						
			$this->script_outputs='<script type="text/javascript">';
			// output
			$this->css_classes = '';
			$this->modal ='';
			$this->tooltip_class = '';
			$this->show_class ='';
			$this->logo_effect='pl-animate'.$this->rand_id.' '.$item_animation;
			
			// assign value to variables
			if($this->layout != 'list')
				$this->css_classes .= 'ls-item-wrap-'.$this->rand_id.' ls-grid-d-' . $desk_num . ' ls-grid-tl-' . $desk_num . ' ls-grid-m-' . $mobile_num . ' ls-grid-t-' . $tablet_num . ' ls-grid-' . $tablet_num . ' ';
		
			// begin html tags
			$output ='<div class="ls-logo-cnt ls-gred-layout '.$custom_class.'" data-rand-id="'.$this->rand_id.'" data-desktop="'.$desktop_column.'" data-tablet="'.$tablet_column.'" data-mobile="'.$mobile_column.'">
					<div class="ls-ajax-loading" style="display:none;">
						<div class="ls-ajax-loading-back"></div>
						<img src="'.plugin_dir_url_it_vc_logo_showcase.'/css/ajax-loader.gif" />
					</div>';
			
			switch ($this->layout) {
				case 'grid':
					if ($this->quick_view_type == 'full' && $this->view_type == 'inline') {
						$this->css_classes .= 'show-inline ';
					}
					break;
				case 'list':
					$output ='<div class="ls-logo-cnt ls-list-layout  '.$custom_class.'" data-rand-id="'.$this->rand_id.'" data-desktop="1" data-tablet="1" data-mobile="1" >';
					if ($this->quick_view_type == 'full' && $this->view_type == 'inline') {
						$this->show_class = 'show-inline';
					}
					break;
				case 'carousel':
					/////OWL CAROUSEL//////
					wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel-css');
					wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel-th');
					wp_enqueue_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel');
					
					$output .= '<div id="'.__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__.'owl-box_'.$this->rand_id.'" class="owl-carousel owl-theme '.$skin.'">';
					if ($this->quick_view_type == 'full' && $this->view_type == 'inline') {
						$this->show_class = 'show-inline-car ';
					}
					break;
				default:
					break;
			}
			
			// quick view
			if($this->quick_view_type == 'full') {
				// Full View
				wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'lightbox-me-css');
				wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'scroller-css');
				
				wp_enqueue_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'lightbox-me');
				wp_enqueue_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'showHide');
				wp_enqueue_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'scroller');
				// ajax post args
				$this->full_args = array(
					'rand_id' => $this->rand_id,
					'title' => $title,
					'contact' => $contact,
					'social' => $social,
					'full_desc' => $full_desc,
					'gallery' => $gallery
				);
				//die(print_r($args));
				
				//
				
				if ($this->view_type == 'popup') {
					wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal-css');
					wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal-theme-css');
					wp_enqueue_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal');
					$modal = '<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
								<div class="ls-ajax-loading" style="display:none;">
									<div class="ls-ajax-loading-back"></div>
									<img src="'.plugin_dir_url_it_vc_logo_showcase.'/css/ajax-loader.gif" />
								</div>
								<div class="inner-modal">
								</div>
						</div>';
					$this->modal = 'data-remodal-target="modal"';
					$this->css_classes .= 'it-modal ';
				}
			}
			if($tooltip == 'true') {
				$this->tooltip_class = 'tooltip ';
			}
		
    		// logo loop
			$output .= do_shortcode($content);
	
			
	
			
			$this->script_outputs .= 'if(!args)   var args = [];
								args["'.$this->rand_id.'"] = ' . json_encode($this->full_args) . ';';
			//die(print_r($this->full_args));				
			// end html tags
			switch ($this->layout) {
				case 'carousel':
					if ($autoplay == 'true')
						$autoplay = $autoplay_speed;
					$this->script_outputs .= '
									jQuery(function($) {
										$("#'.__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__.'owl-box_'.$this->rand_id.'").owlCarousel({
											autoPlay: ' . $autoplay . ', //Set AutoPlay to 3 seconds
											items : ' . $laptop_item . ',
											itemsDesktop : [1199,' . $desktop_item . '],
											itemsTablet: [768,' . $tablet_item . '],
											itemsMobile : [479,' . $mobile_item . '],
											navigation : ' . $navigation . ',
											pagination : ' . $pagination . ',
											rewindSpeed : 1000,
											paginationSpeed : 3000,
											slideSpeed : 1000,
											stopOnHover : '.$pause.',
											navigationText: [
												"<i class=\'fa fa-angle-left\'></i>",
												"<i class=\'fa fa-angle-right\' ></i>"
											]
										});
									});
								';
					break;
				default:
					break;
			}
			if($tooltip == 'true') {
				$this->script_outputs .= '
						jQuery(function($){
							$(".tooltip").tipsy({
								gravity : $.fn.tipsy.autoNS ,
								fade : true
							});
						});';
			}
			if ($this->logo_effect!= 'it-no-animation')	{
				$this->script_outputs .='
					jQuery(document).ready(function() {
						wow'.$this->rand_id.' = new WOW(
						  {
							boxClass:     "pl-animate'.$this->rand_id.'",
							animateClass: "visible animated",
							offset:100,
						  }
						);
						wow'.$this->rand_id.'.init();
					});';
			}
			$this->script_outputs .= '</script>';
			$output .= '</div>'.$modal;
			$output.=$this->script_outputs;
			$this->it_logo_custom_css();
			return $output;	
		}
		// Shortcode handler function for  Nested
		function renderitemShortcode($atts, $content = null)
		{
			$title=
			$image=
			$shortdesc=
			$link_url=
			$link_target=
			$full_description=
			$website=
			$email=
			$tell=
			$gallery_image=
			$facebook=
			$twitter=
			$instagram=
			$google_plus=
			$linked_in=
			$output='';
			
			extract(shortcode_atts( array(
				'title'=>'',
				'image'=>'',
				'shortdesc'=>'',
				'link_url'=>'',
				'link_target'=>'_blank',
				'full_description'=>'',
				'website'=>'',
				'email'=>'',
				'tell'=>'',
				'gallery_image'=>'',
				'facebook'=>'',
				'twitter'=>'',
				'instagram'=>'',
				'google_plus'=>'',
				'linked_in'=>'',
				),$atts));
	  		//$content = wpb_js_remove_wpautop($content, true);
			//$ali=strip_shortcodes($content);  
			//die(print_r($atts));
			
			//$arr=array("items"=>);
			//die(do_shortcode($content));
			$atts['full_description']=($content);
			if($this->quick_view_type == 'full') {
			$this->full_args=array_merge($this->full_args,array($this->i=>$atts));
			}
			
			$this->shortdesc=$shortdesc;
			
			$post_id=$this->i.$this->rand_id;
			$image_url = wp_get_attachment_image_src( $image, 'full');
			$image_url = $image_url[0];
			
			$tooltip_str='';
			if($this->tooltip == 'true') {
				$tooltip_str = 'title = "'.$title.'"';
			}

			// link event
			$link_detail = '';
			$more_btn ='';
			if ($this->quick_view_type == 'none' && $link_url!='') {
				$link_detail = '<a href="' . $link_url . '" target="' . $link_target . '" >
									<img  src="' . $image_url . '" class="' . $this->effect . '" >
								</a>';
			}
			else{
				if ($this->quick_view_type != 'none' ) {
					$more_btn = '<div class="ls-more-btn">'.__('More Details','IT_VC_LOGO_SHOWCASE_TEXTDOMAIN').'</div>';
				}
				$link_detail = '<img  src="' . $image_url . '" class="' . $this->effect . '" >';
			}
			//
        	switch ($this->layout) {
            case 'grid':
                $output .= '<div class="' . $this->css_classes . ' " data-item-number="'.$this->i.'" '.$this->modal.' data-id="' . $post_id . '">
                            <div class="ls-gred-item ls-gred-item-'.$this->rand_id.' '.$this->logo_effect.' '.$this->tooltip_class.'" ' . $tooltip_str . ' >
                                '.$link_detail.'
                            </div>
                          </div>';
                break;
            case 'list':
                    $output .= '<div class="it-modal ls-item-wrap-'.$this->rand_id.' ls-grid-m-12 ls-grid-t-12 ls-grid-d-12 '.$this->show_class.'" data-item-number="'.$this->i.'" data-id="' . $post_id . '" '.$this->modal.'>
                                <div class="ls-list-item ls-gred-item-'.$this->rand_id.'  '.$this->logo_effect.' ">
                                    <div class="ls-grid-m-12 ls-grid-t-4 ls-grid-m-3 ls-list-img">
                                        '.$link_detail.'
                                    </div>
                                    <div class="ls-grid-m-12 ls-grid-t-8 ls-grid-m-9">
                                        <div class="ls-list-title">'.$title.'</div>
                                        <div class="ls-list-desc">'.$shortdesc.'</div>
                                        '.$more_btn.'
                                    </div>
                                </div>
                            </div>';
                break;
            case 'carousel':
                // Echo Logos
                $output .= '<div class="item it-modal ls-item-wrap-'.$this->rand_id.' '.$this->show_class.'" '.$this->modal.' data-item-number="'.$this->i.'" data-id="' . $post_id . '">
                            <div class="ls-gred-item ls-gred-item-'.$this->rand_id.' '.$this->tooltip_class.'"' . $tooltip_str . ' >
                                '.$link_detail.'
                            </div>
                          </div>';
                break;

            default:
                break;
        }
			$this->i++;
			
			return $output;	
		}
		// function for  Custom CSS
		function it_logo_custom_css()  {
			///die($this->hlink);
			$inline_css='';
        //$style = '<style>';
        $r = (int)$this->margin_right;
        $l = (int)$this->margin_left;
		
		$style='';
		$style = '	
        .ls-list-layout #show-inline-box.ls-inline-box-' . $this->rand_id . '{
            padding:0 ' . $r . 'px 0 ' . $l . 'px !important;
        }
        .ls-list-layout #show-inline-box.ls-inline-box-' . $this->rand_id . ' .ls-showdetail{
            border-color :' . $this->border_color . ' !important;
        }

        .ls-gred-layout #show-inline-box.ls-inline-box-' . $this->rand_id . '{
            padding:0 ' . $r . 'px 0 ' . $l . 'px !important;
        }
        .ls-gred-layout #show-inline-box.ls-inline-box-' . $this->rand_id . ' .ls-showdetail{
            border-color :' . $this->border_color . ' !important;
        }

        .ls-item-wrap-' . $this->rand_id . '{
            padding-right:' . $r . 'px !important;
            padding-left :' . $l . 'px !important;
        }
		.ls-gred-item-' . $this->rand_id . '{
            background-color: ' . $this->background_color . ' !important;
		    padding:' . $this->padding_top . 'px ' . $this->padding_right . 'px ' . $this->padding_bottom . 'px ' . $this->padding_left . 'px !important;
		    margin:' . $this->margin_top . 'px 0 ' . $this->margin_bottom . 'px 0 !important;
			border-style :' . $this->border_style . ' !important;
			border-color :' . $this->border_color . ' !important;
			border-width :' . $this->border_width_top . 'px ' . $this->border_width_right . 'px ' . $this->border_width_bottom . 'px ' . $this->border_width_left . 'px !important;
			-webkit-border-radius: ' . $this->border_radius_top_left . 'px ' . $this->border_radius_top_right . 'px ' . $this->border_radius_bottom_right . 'px ' . $this->border_radius_bottom_left . 'px !important;
			-moz-border-radius: ' . $this->border_radius_top_left . 'px ' . $this->border_radius_top_right . 'px ' . $this->border_radius_bottom_right . 'px ' . $this->border_radius_bottom_left . 'px !important;
			border-radius: ' . $this->border_radius_top_left . 'px ' . $this->border_radius_top_right . 'px ' . $this->border_radius_bottom_right . 'px ' . $this->border_radius_bottom_left . 'px !important;';
        if ($this->shadow == 'inset')
            $style .= 'box-shadow: ' . $this->shadow_h . 'px ' . $this->shadow_v . 'px ' . $this->shadow_blur . 'px ' . $this->shadow_spread . 'px ' . $this->shadow_color . ' ' . $this->shadow . ' !important;';
        else if ($this->shadow == 'outset')
            $style .= 'box-shadow: ' . $this->shadow_h . 'px ' . $this->shadow_v . 'px ' . $this->shadow_blur . 'px ' . $this->shadow_spread . 'px ' . $this->shadow_color . ' !important;';
        if ($this->effect == 'opacity')
            $style .= 'opacity:0.6 !important;';
        $style .= '
		}
		.ls-gred-item-' . $this->rand_id . ':hover{
		    background-color: ' . $this->hover_background_color . ' !important;
		    border-style :' . $this->hover_border_style . ' !important;
			border-color :' . $this->hover_border_color . ' !important;
			border-width :' . $this->hover_border_width_top . 'px ' . $this->hover_border_width_right . 'px ' . $this->hover_border_width_bottom . 'px ' . $this->hover_border_width_left . 'px !important;
			-webkit-border-radius: ' . $this->hover_border_radius_top_left . 'px ' . $this->hover_border_radius_top_right . 'px ' . $this->hover_border_radius_bottom_right . 'px ' . $this->hover_border_radius_bottom_left . 'px !important;
			-moz-border-radius: ' . $this->hover_border_radius_top_left . 'px ' . $this->hover_border_radius_top_right . 'px ' . $this->hover_border_radius_bottom_right . 'px ' . $this->hover_border_radius_bottom_left . 'px !important;
			border-radius: ' . $this->hover_border_radius_top_left . 'px ' . $this->hover_border_radius_top_right . 'px ' . $this->hover_border_radius_bottom_right . 'px ' . $this->hover_border_radius_bottom_left . 'px !important;';
        if ($this->hover_shadow == 'inset')
            $style .= 'box-shadow: ' . $this->hover_shadow_h . 'px ' . $this->hover_shadow_v . 'px ' . $this->hover_shadow_blur . 'px ' . $this->hover_shadow_spread . 'px ' . $this->hover_shadow_color . ' ' . $this->hover_shadow . ' !important;';
        else if ($this->hover_shadow == 'outset')
            $style .= 'box-shadow: ' . $this->hover_shadow_h . 'px ' . $this->hover_shadow_v . 'px ' . $this->hover_shadow_blur . 'px ' . $this->hover_shadow_spread . 'px ' . $this->hover_shadow_color . ' !important;';
        $style .= '}';
        //$style .= '</style>';
        //echo $style;






			wp_enqueue_style('it-custom-css', __IT_VC_LOGO_SHOWCASE_ROOT_VC_URL__ . '/css/custom-css.css', array() , null); 
			wp_add_inline_style( 'it-custom-css', $style );
		}

	}
}
$GLOBALS['it_vc_logo_showcase'] = new it_vc_logo_showcase;
?>