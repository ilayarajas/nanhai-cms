(function ($) {

    "use strict";

    function team_awesome_admin() {
        this.init = function () {
            bindControls();
        };
        
        var bindControls = function () {
            
            $("#team-awesome-form-save").on("submit", function (e) {

                $('input,textarea,select').removeClass("invalid");

                var valid = true;

                $('input,textarea,select').filter('[required]').each(function () {
                    if (!$(this).val() || $.trim($(this).val()) == "") {
                        $(this).addClass("invalid");
                        valid = false;
                    }
                });

                if (valid !== true) {
                    alert("Please fill all required fields first!");
                }

                return valid;
            });
            
            $(".remove-skill").on("click", function(e){
                e.preventDefault();
                
                if ($(".opt-skill").length > 1) {
                    $(this).parents('.opt-skill').remove();
                } else {
                    $(this).parents('.opt-skill').children().val("");
                }
                
            });
            
            $(".remove-bg").on("click", function(e){
                e.preventDefault();
                
                $(this).parents('.opt').find("input").val("");
                
            });
            
            $("#moreskills").on("click", function(e){
                e.preventDefault();
                
                var skill = $(".opt-skill:last").clone(true);
                
                skill.find("select, input").each(function () {
                    this.name = this.name.replace(/\[(\d+)\]/, function (str, p1) {
                        return '[' + (parseInt(p1, 10) + 1) + ']';
                    });
                });
                
                
                skill.children().val("");
                
                $(this).before(skill);
            });
            
            $("input[name='vidavatar']").on("click", function () {
                
                var th = this;
                
                var media_frame = wp.media({
                    button: {text: "Use as Avatar"},
                    library: {type: 'video'},
                    frame: 'select',
                    title: "Select Member Video Avatar",
                    multiple: false
                });

                media_frame.open();

                media_frame.on('select', function () {
                    var attachment = media_frame.state().get('selection').first().toJSON();
                    
                    var url = attachment.url;
                    
                    $(th).val(url);
                    

                });
                
            });
            
            
            $("input[name='avatar']").on("click", function () {
                
                var th = this;
                
                var media_frame = wp.media({
                    button: {text: "Use as Avatar"},
                    library: {type: 'image'},
                    frame: 'select',
                    title: "Select Member Avatar",
                    multiple: false
                });

                media_frame.open();

                media_frame.on('select', function () {
                    var attachment = media_frame.state().get('selection').first().toJSON();
                    
                    var url = attachment.url;
                    
                    $(th).val(url);
                    

                });
                
            });
            
            $("input[name='bgimage']").on("click", function () {
                
                var th = this;
                
                var media_frame = wp.media({
                    button: {text: "Use as Background"},
                    library: {type: 'image'},
                    frame: 'select',
                    title: "Select Plugin Background",
                    multiple: false
                });

                media_frame.open();

                media_frame.on('select', function () {
                    var attachment = media_frame.state().get('selection').first().toJSON();
                    
                    var url = attachment.url;
                    
                    $(th).val(url);
                    

                });
                
            });
            
            
            
            
            
            
            
            
            
            
            $(".categories-reorder").on("click", function (e) {
                var th = this;

                $(th).siblings().remove();

                $(".list").children().each(function() {
                    var id = $(this).attr("id").replace("category-", "");
                    var num = $(this).find("input[type=text]").val();
                    $(th).after('<input type="hidden" name="order[]" value="' + id + ":" + num + '" />');
                });

                $(th).after('<input type="hidden" name="task" value="updateOrder" />');
            });
            
            
            
            $(".members-reorder").on("click", function (e) {
                var th = this;

                $(th).siblings().remove();

                $(".list").children().each(function() {
                    var id = $(this).attr("id").replace("member-", "");
                    var num = $(this).find("input[type=text]").val();
                    $(th).after('<input type="hidden" name="order[]" value="' + id + ":" + num + '" />');
                });

                $(th).after('<input type="hidden" name="task" value="updateOrder" />');
            });
            
           
            $('.team-awesome-color-picker').each(function () {
                var th = this;

                var color = $(th).val();

                $(th).css({
                    backgroundColor: color
                });

                $(th).ColorPicker({
                    color: color,
                    onShow: function (colpkr) {
                        $(colpkr).show();
                        return false;
                    },
                    onHide: function (colpkr) {
                        $(colpkr).hide();
                        return false;
                    },
                    onChange: function (hsb, hex, rgb) {
                        $(th).val("#" + hex);

                        $(th).css({
                            backgroundColor: $(th).val()
                        });
                    }
                });
            });
        };
    }

    $(document).ready(function () {
        new team_awesome_admin().init();
    });

})(jQuery);