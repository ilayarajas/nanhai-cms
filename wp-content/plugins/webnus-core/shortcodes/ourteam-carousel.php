<?php
function michigan_webnus_ourteam_carousel ( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'carousel_item'	=> '',
		'item_carousle'	=> '3',
	), $atts ));

	// Fetch Carousle Item Loop Variables
	$carousel_item = (array) vc_param_group_parse_atts( $carousel_item );
	$carousel_item_data = array();

	foreach ( $carousel_item as $data ) {
		$new_line 							= $data;
		$new_line['team_title'] 			= isset( $new_line['team_title'] )				? $new_line['team_title']: '';
		$new_line['team_subtitle'] 			= isset( $new_line['team_subtitle'] ) 			? $new_line['team_subtitle']: '';
		$new_line['team_content'] 			= isset( $new_line['team_content'] )			? $new_line['team_content']: '';
		$new_line['team_image'] 			= isset( $new_line['team_image'] )				? $new_line['team_image']: '';
		$new_line['team_first_social'] 		= isset( $new_line['team_first_social'] )		? $new_line['team_first_social']: '';
		$new_line['team_first_social_url'] 	= isset( $new_line['team_first_social_url'] )	? $new_line['team_first_social_url']: '';
		$new_line['team_second_social'] 	= isset( $new_line['team_second_social'] )		? $new_line['team_second_social']: '';
		$new_line['team_second_social_url'] = isset( $new_line['team_second_social_url'] )	? $new_line['team_second_social_url']: '';
		$new_line['team_third_social'] 		= isset( $new_line['team_third_social'] )		? $new_line['team_third_social']: '';
		$new_line['team_third_social_url'] 	= isset( $new_line['team_third_social_url'] )	? $new_line['team_third_social_url']: '';
		$new_line['team_fourth_social'] 	= isset( $new_line['team_fourth_social'] )		? $new_line['team_fourth_social']: '';
		$new_line['team_fourth_social_url'] = isset( $new_line['team_fourth_social_url'] )	? $new_line['team_fourth_social_url']: '';
		$carousel_item_data[]				= $new_line;
	}

	// Render
	$out = '
		<div class="row">
			<div class="container">
				<div class="our-team-carousel-wrap owl-carousel owl-theme" data-items="' . $item_carousle . '" >';
					foreach ( $carousel_item_data as $line ) :

					$line['team_image'] 		= is_numeric( $line['team_image'] ) ? wp_get_attachment_url( $line['team_image'] ) : $line['team_image'];
					$line['team_image'] 		= $line['team_image'] 			? '<img src="' . $line['team_image'] . '" alt="' . $line['team_title'] . '">' : '' ;
					$line['team_title'] 		= $line['team_title'] 			? '<h2>' . $line['team_title'] . '</h2>' : '' ;
					$line['team_subtitle'] 		= $line['team_subtitle'] 		? '<h5>' . $line['team_subtitle'] . '</h5>' : '' ;
					$line['team_content'] 		= $line['team_content'] 		? '<p>' . $line['team_content'] . '</p>' : '' ;
					$line['team_first_social'] 	= $line['team_first_social'] 	? '<a href="'. $new_line['team_first_social_url'] .'"><i class="fa-'. $line['team_first_social'] .'"></i></a>' : '' ;
					$line['team_second_social'] = $line['team_second_social'] 	? '<a href="'. $new_line['team_second_social_url'] .'"><i class="fa-'. $line['team_second_social'] .'"></i></a>' : '' ;
					$line['team_third_social'] 	= $line['team_third_social'] 	? '<a href="'. $new_line['team_third_social_url'] .'"><i class="fa-'. $line['team_third_social'] .'"></i></a>' : '' ;
					$line['team_fourth_social'] = $line['team_fourth_social'] 	? '<a href="'. $new_line['team_fourth_social_url'] .'"><i class="fa-'. $line['team_fourth_social'] .'"></i></a>' : '' ;


					$out .='
					<div class="our-team-carousel our-team6">
						' . $line['team_image'] . '
						<div class="tdetail">
							' . $line['team_title'] . $line['team_subtitle'] . $line['team_content'] . '
							<div  class="social-team">
								'. $line['team_first_social'] . $line['team_second_social'] . $line['team_third_social'] . $line['team_fourth_social'] . '
							</div>
						</div>
					</div>';
					endforeach;
	$out .='
		</div>
			</div>
				</div>';

	return $out;

}
	add_shortcode( 'our_team_carousel','michigan_webnus_ourteam_carousel' );