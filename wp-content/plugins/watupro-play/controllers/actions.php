<?php
// handles the add_action calls
class WatuPROPlayActions {
	static function init() {
		// to update user awards
		$_user = new WatuPROPlayUser();
		add_action('watupro_completed_exam', array($_user, 'update_meta'));
		add_action('watupro_completed_exam_edited', array($_user, 'update_meta'));
		add_action('watupro_deleted_taking', array($_user, 'update_meta'));		
		add_action('watupro_user_menu', array(__CLASS__, 'user_menu'));		
		add_action('watupro-view-payment-options', array('WatuPROPlayRewards', 'payment_options'));
		add_action('watupro_deleted_user_data', array($_user, 'cleanup'));		
		add_action('watupro_quiz_form_login_required', array('WatuPROPlayLevels', 'restrict_by_level_form'));
		add_action( 'edit_user_profile', array('WatuPROPlayLevels', 'edit_profile') );
		add_action( 'edit_user_profile_update', array('WatuPROPlayLevels', 'save_edit_profile') );
	}
	
	// adds configurable user menu page if required
	static function user_menu() {		
		if(get_option('watuproplay_enable_overview')) {
			$student_caps = current_user_can(WATUPRO_MANAGE_CAPS) ? WATUPRO_MANAGE_CAPS:'read'; 
			$title = get_option('watuproplay_overview_title');
			$_user = new WatuPROPlayUser();		
			add_submenu_page('my_watupro_exams', $title, $title, $student_caps, 'watuproplay_my_overview', array($_user, 'overview'));
		}
	}
}