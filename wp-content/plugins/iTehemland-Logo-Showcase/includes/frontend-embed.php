<?php
	//CSS
	//Enqueue You Css Here
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'custom-css', __IT_LOGO_SHOWCASE_CSS_URL__ . 'custom_css.css', array() , null);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'public-css', __IT_LOGO_SHOWCASE_CSS_URL__. 'public.css', array() , null);
	
	/////tooltip//////
	wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'tooltip/src/stylesheets/tipsy.css');
	
	//FONTAWESOME STYLE //font-awesome-css
    wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'font-awesome-ccc', __IT_LOGO_SHOWCASE_CSS_URL__ . 'font-awesome.css', true);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'font-awesome-ccc');
	
	///////// grid responsive ///////////
    wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'responsive-grid', __IT_LOGO_SHOWCASE_CSS_URL__ . 'responsive-grid.css', true);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'responsive-grid');
	
	/////COLOR PICKKER//////
    wp_enqueue_style('wp-color-picker');
	
	
	/////OWL CAROUSEL//////
	wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'owlCarousel/owl-carousel/owl.carousel.css', true);
	wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel-th', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'owlCarousel/owl-carousel/owl.theme.css', true);
	
	
	// Full View
	wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'lightbox-me-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'lightbox/css/lightbox.css', true);
	wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'lightbox-me-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'lightbox/css/lightbox.css', true);
	wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'scroller-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'scroller/jquery.mCustomScrollbar.css', true);
	
	//Modal
	wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'modal/dist/remodal.css', true);
	wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal-theme-css', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'modal/dist/remodal-default-theme.css', true);
	
	///////////////////Animation.css//////////////////////
	wp_register_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'animate', __IT_LOGO_SHOWCASE_CSS_URL__ . 'animate.css', true);
    wp_enqueue_style(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'animate');
	
	
	//JS
	/////JS ENQUEUE////////////
    wp_enqueue_script('jquery');

	/////tooltip//////
	wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'tooltip/src/javascripts/jquery.tipsy.js');
	 
	//////////////////CUSTOM JS//////////////////////////
    wp_register_script( __IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'ajaxHandle', __IT_LOGO_SHOWCASE_JS_URL__.'front-end/custom-js.js', array('jquery'), false, true );
    wp_enqueue_script( __IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'ajaxHandle' );
    wp_localize_script( __IT_LOGO_SHOWCASE_FIELDS_PERFIX__.'ajaxHandle', 'ajax_object',
        array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce( 'it_logoshowcase_nonce' ) ) );

    /////COLOR PICKKER////////////
    wp_enqueue_script('wp-color-picker');
	
	/////OWL CAROUSEL//////
	wp_register_script( __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'owlCarousel/owl-carousel/owl.carousel.min.js', array('jquery'), false, true );
	
	/////Isotope//////
	wp_register_script( __IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'isotope', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'isotope/isotope.pkgd.min.js', array('jquery'), false, true );	
	
	/////Full View//////
	wp_register_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'lightbox-me', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'lightbox/js/lightbox.min.js');
	wp_register_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'showHide', __IT_LOGO_SHOWCASE_JS_URL__ . 'showHide.js');
	wp_register_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'scroller', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'scroller/jquery.mCustomScrollbar.concat.min.js');
	
	////Modal////
	wp_register_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal', __IT_LOGO_SHOWCASE_JS_PLUGIN_URL__ . 'modal/dist/remodal.min.js');
	
	//////WOW/////////////
	wp_register_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'wow', __IT_LOGO_SHOWCASE_JS_URL__ . 'front-end/wow.js', array('jquery'), false, true );
	wp_enqueue_script(__IT_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'wow');

?>