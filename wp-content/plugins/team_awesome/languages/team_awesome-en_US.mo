��    m      �  �   �      @	  	   A	     K	     a	  $   p	     �	     �	     �	  
   �	     �	     �	  -   �	     
     
  $   7
     \
     r
     w
     ~
     �
     �
     �
     �
     �
     �
       $   "     G     N     V     ^     c  -   j  6   �  /   �  2   �  *   2     ]     n     s     {     �     �     �     �     �     �     �     �               $     8     <     ?  	   H  	   R     \     b     k     �     �     �     �     �     �     �       '   
     2     8  	   I     S     b     p     |  D   �     �  "   �       %   	  !   /     Q     g     |     �     �     �     �     �  '        -     4     R     d     t     �     �     �     �  
   �  %   �     �               )  
   ;     F     W  �  g  	             #  $   2     W     i     y  
   �     �     �  -   �     �     �  $   �          4     9     @     P     ^     m     t     �     �     �  $   �     	                     %  -   ,  6   Z  /   �  2   �  *   �          0     5     =     B     R     _     w     �     �     �     �     �     �     �     �     �       	   
  	             $     -     C     T     k     �     �     �     �     �  '   �     �     �  	             $     2     >  D   L     �  "   �     �  %   �  !   �          )     >     P     j     �     �     �  '   �     �     �          &     6     L     Z     o     ~  
   �  %   �     �     �     �     �  
   �               [      ,   %                     P                 j   I   >   N   '       =   h   	   k       d           ]   B   O   (   6   S      D                   $   @   L       f      J       0   V          
   !   "           X       G              E              C   1   ^         W       2   m       a          :   8      *       -   ;   g   i   3   <                     Q                   Y   l   c   9   )   /   T   5                       R   \   H      _              U   e   .   M   &   Z   K      7       F   A       #       b   4   ?          `           +    ALL STAFF Active Category Color Add New Member Add new or edit existing team member Add/Edit Category Add/Edit Member Align Center Align Left Align Right Avatar Avatar Style,  Show/Hide Border, Border Color Avatar photo Avatar video (muted loop) Background, Icon Color, Border Color Bar Color, Fill Color Buzz Cancel Categories Font Category List Category Title Circle Color, Image, Parallax Effect Create / Edit existing Category Create / Edit existing Member Create New Category Create new or edit existing category Delete Disable Divider Edit Enable Enable / Disable Plugin Categories, Alignment Enable / Disable Plugin Popup, Width (px), Height (px) Enable / Disable Plugin Slider, Slides per page Enable/Disable, Thichkness (px), Width (px), Color Font-Family, Font-Size, Line-Height, Color General Settings Grow Hexagon Hide Member Category Member Image Member Long Description Member Occupation Member Short Description Member Skills Member Title Member Video Name of the category Normal Occupation/Position Off On Optional Optional! Optional. Order Parallax Person Box Dimensions Person Name Font Person Occupation Font Person Short Description Font Person name Plugin Background Plugin Title Plugin Title Font Popup Popup text description about the person Pulse Random Animation Required! Reset Defaults Save Category Save Member Save Settings Select member category. Hold Ctrl key to select multiple categories. Short Description Short description about the person Show Skill Bar Title, Percents, Fill Color Skill Title, Percents, Fill Color Skills Bar Background Skills Percents Font Skills Title Font Slider Pagination Buttons Slider Pagination Buttons HOVER Social Button  Social Buttons Animation Social Icon and Link Social buttons animation on mouse hover Square Team Awesome General Settings Team Members List Update Category Update Category Order Update Member Update Members Order Use Categories Use Default Use Slider Width (px), Height (px), Spacing (px) Wobble Bottom Wobble Bottom Right Wobble Center Wobble Horizontal Wobble Top Wobble Top Right Wobble Vertical Project-Id-Version: Team Awesome
POT-Creation-Date: 2016-02-22 11:28+0200
PO-Revision-Date: 2016-02-22 11:28+0200
Last-Translator: 
Language-Team: Stachethemes
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _e;__
X-Poedit-SearchPath-0: ..
 ALL STAFF Active Category Color Add New Member Add new or edit existing team member Add/Edit Category Add/Edit Member Align Center Align Left Align Right Avatar Avatar Style,  Show/Hide Border, Border Color Avatar photo Avatar video (muted loop) Background, Icon Color, Border Color Bar Color, Fill Color Buzz Cancel Categories Font Category List Category Title Circle Color, Image, Parallax Effect Create / Edit existing Category Create / Edit existing Member Create New Category Create new or edit existing category Delete Disable Divider Edit Enable Enable / Disable Plugin Categories, Alignment Enable / Disable Plugin Popup, Width (px), Height (px) Enable / Disable Plugin Slider, Slides per page Enable/Disable, Thichkness (px), Width (px), Color Font-Family, Font-Size, Line-Height, Color General Settings Grow Hexagon Hide Member Category Member Image Member Long Description Member Occupation Member Short Description Member Skills Member Title Member Video Name of the category Normal Occupation/Position Off On Optional Optional! Optional. Order Parallax Person Box Dimensions Person Name Font Person Occupation Font Person Short Description Font Person name Plugin Background Plugin Title Plugin Title Font Popup Popup text description about the person Pulse Random Animation Required! Reset Defaults Save Category Save Member Save Settings Select member category. Hold Ctrl key to select multiple categories. Short Description Short description about the person Show Skill Bar Title, Percents, Fill Color Skill Title, Percents, Fill Color Skills Bar Background Skills Percents Font Skills Title Font Slider Pagination Buttons Slider Pagination Buttons HOVER Social Button  Social Buttons Animation Social Icon and Link Social buttons animation on mouse hover Square Team Awesome General Settings Team Members List Update Category Update Category Order Update Member Update Members Order Use Categories Use Default Use Slider Width (px), Height (px), Spacing (px) Wobble Bottom Wobble Bottom Right Wobble Center Wobble Horizontal Wobble Top Wobble Top Right Wobble Vertical 