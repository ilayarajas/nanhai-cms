<?php
/*
Plugin Name: iThemeland Logo Showcase for VC
Plugin URI: http://ithemelandco.com/Plugins/Logo-Showcase-For-VC/
Description: This Plugin Ideal For Show Logos, Partners, Sponsers And Clients.
Author: iThemelandco
Version: 1.0
Author URI: http://ithemelandco.com/
Text Domain: it_vc_logo_showcase
 */
 
define('plugin_dir_url_it_vc_logo_showcase', plugin_dir_url( __FILE__ ));
define ('__IT_VC_LOGO_SHOWCASE_ROOT_VC_URL__',plugins_url('', __FILE__));
define( '__IT_VC_LOGO_SHOWCASE_ROOT_VC__', dirname(__FILE__));

define ('IT_VC_LOGO_SHOWCASE_TEXTDOMAIN','it_vc_logo_showcase');
//PERFIX
define ('__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__', 'it-logo-showcase' );

 

class it_vc_logo_showcase_plugin  {
	public function __construct() 
	{
		// ajax action
		include('class/actions.php');

		
		//Add Shortcode Post And Page
		add_filter('init', array( $this,'it_vc_logo_showcase_shortcodes_add_scripts'));
		
		//Add And Remove Param
		add_action( 'init', array( $this, 'integrateWithVC' ) );
		
		add_action('admin_enqueue_scripts',array($this,'it_vc_logo_showcase_admin_scripts'));
		// IMAGE SIZE
		add_image_size('it_hor_image', '760', '500', true);
		add_image_size('it_ver_image', '470', '630', true);
		add_image_size('it_rec_image', '500', '500', true);

		
		$this->includes();
	}
	

	
	//Add And Remove Param
	public function integrateWithVC() {
		// Check if Visual Composer is installed
		if ( ! defined( 'WPB_VC_VERSION' ) ) {
			return;
		}
		if(function_exists('vc_add_shortcode_param')){
				vc_add_shortcode_param('it_number' , array($this, 'it_number_settings_field' ) );
			}else if(function_exists('add_shortcode_param'))
			{
				add_shortcode_param('it_number' , array($this, 'it_number_settings_field' ) );
			}
	}
	
	//Number Param 
	function it_number_settings_field($settings, $value){
		$dependency = '';
		$param_name = isset($settings['param_name']) ? $settings['param_name'] : '';
		$min = isset($settings['min']) ? $settings['min'] : '';
		$max = isset($settings['max']) ? $settings['max'] : '';
		$suffix = isset($settings['suffix']) ? $settings['suffix'] : '';		   
		$output = '<input name="'.$settings['param_name']
				.'" class="wpb_vc_param_value wpb-textinput '
				.$settings['param_name'].' '.$settings['type'].' '.$settings['class'].'" id="'
				.$settings['param_name'].'" type="number" min="'.$min.'" max="'.$max.'" value="'.$value.'" ' . $dependency . 'style="max-width:100px; margin-right: 10px;" />'.$suffix;
				
		return $output;
	}
		
	function it_vc_logo_showcase_admin_scripts()
		{
			
			wp_enqueue_style('it-backend-style', plugin_dir_url_it_vc_logo_showcase .'/css/back-end/custom_css_back.css');
			
		}	
		
		
		
	public function includes()	{

		require_once( 'class/it_logo_showcase.php');
		
	}
	
	function it_vc_logo_showcase_shortcodes_add_scripts() {
		if(!is_admin()) {
			//CSS
			//Enqueue You Css Here
			wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__.'public-css', plugin_dir_url_it_vc_logo_showcase.'/css/public.css', array() , null);
			
			/////tooltip//////
			wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip-css', plugin_dir_url_it_vc_logo_showcase.'/plugins/tooltip/src/stylesheets/tipsy.css');
			
			//FONTAWESOME STYLE //font-awesome-css
			wp_register_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'font-awesome-ccc', plugin_dir_url_it_vc_logo_showcase.'/css/font-awesome.css', true);
			wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'font-awesome-ccc');
			
			///////// grid responsive ///////////
			wp_register_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'responsive-grid', plugin_dir_url_it_vc_logo_showcase.'/css/responsive-grid.css', true);
			wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'responsive-grid');
					
			/////OWL CAROUSEL//////
			wp_register_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel-css', plugin_dir_url_it_vc_logo_showcase.'/plugins/owlCarousel/owl-carousel/owl.carousel.css', true);
			wp_register_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel-th', plugin_dir_url_it_vc_logo_showcase.'/plugins/owlCarousel/owl-carousel/owl.theme.css', true);
			
			
			// Full View
			wp_register_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'lightbox-me-css', plugin_dir_url_it_vc_logo_showcase.'/plugins/lightbox/css/lightbox.css', true);
			wp_register_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'lightbox-me-css', plugin_dir_url_it_vc_logo_showcase.'/plugins/lightbox/css/lightbox.css', true);
			wp_register_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'scroller-css', plugin_dir_url_it_vc_logo_showcase.'/plugins/scroller/jquery.mCustomScrollbar.css', true);
			
			//Modal
			wp_register_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal-css', plugin_dir_url_it_vc_logo_showcase.'/plugins/modal/dist/remodal.css', true);
			wp_register_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal-theme-css', plugin_dir_url_it_vc_logo_showcase.'/plugins/modal/dist/remodal-default-theme.css', true);
			
			
			///////////////////Animation.css//////////////////////
			wp_register_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'animate', plugin_dir_url_it_vc_logo_showcase.'/css/animate.css', true);
			wp_enqueue_style(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'animate');
							
					
			/*SCRIPTS*/
			wp_enqueue_script('jquery');
			
			
			/////tooltip//////
			wp_enqueue_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'tooltip', plugin_dir_url_it_vc_logo_showcase.'/plugins/tooltip/src/javascripts/jquery.tipsy.js');
			 
			//////////////////CUSTOM JS//////////////////////////
			wp_register_script( __IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__.'ajaxHandle', plugin_dir_url_it_vc_logo_showcase.'js/front-end/custom-js.js', array('jquery'), false, true );
			wp_enqueue_script( __IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__.'ajaxHandle' );
			wp_localize_script( __IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__.'ajaxHandle', 'ajax_object',
				array(
					'ajaxurl' => admin_url( 'admin-ajax.php' ),
					'nonce' => wp_create_nonce( 'it_logoshowcase_nonce' ) ) );
					
			/////OWL CAROUSEL//////
			wp_register_script( __IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'owl-carousel', plugin_dir_url_it_vc_logo_showcase.'/plugins/owlCarousel/owl-carousel/owl.carousel.min.js', array('jquery'), false, true );
			
			/////Full View//////
			wp_register_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'lightbox-me', plugin_dir_url_it_vc_logo_showcase.'/plugins/lightbox/js/lightbox.min.js');
			wp_register_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'showHide', plugin_dir_url_it_vc_logo_showcase.'js/showHide.js');
			wp_register_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'scroller', plugin_dir_url_it_vc_logo_showcase.'/plugins/scroller/jquery.mCustomScrollbar.concat.min.js');
			
			////Modal////
			wp_register_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'modal', plugin_dir_url_it_vc_logo_showcase.'/plugins/modal/dist/remodal.min.js');
			
		
			//////WOW/////////////
			wp_register_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'wow', plugin_dir_url_it_vc_logo_showcase.'js/front-end/wow.js', array('jquery'), false, true );
			wp_enqueue_script(__IT_VC_LOGO_SHOWCASE_FIELDS_PERFIX__ . 'wow');
					

			
		}else
		{
			
			
		}
	}
	
}
new it_vc_logo_showcase_plugin();
?>