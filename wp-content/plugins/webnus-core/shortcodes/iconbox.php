<?php
function michigan_webnus_iconbox( $attributes, $content = null ) {
	extract(shortcode_atts(array(
		"type"					=>'',
		'icon_subtitle'			=>'',
		'icon_title'			=>'',
		'icon_link_url'			=>'',
		'icon_link_text'		=>'',
		"icon_name"				=>'',
		"iconbox_content"		=>'',
		"icon_size"				=>'',
		"icon_color"			=>'',
		"title_color"			=>'',
		"content_color"			=>'',
		"link_color"			=>'',
		"link_target"			=>'',		
		"icon_image"			=>'',
		"background_color_t18"	=>'',
		"background_color"		=>'',
		"align"					=>'',
		"border"				=>'',
		"bgcolor"				=>'',
		"border_color"			=>'',
		"iconbgcolor"			=>'',
	), $attributes));
	ob_start();
	
	$type = ($type==0) ? '' : $type ; //clear type 0
	$iconbox_style = ($icon_color)? ' style="color: ' . $icon_color . '"' : '' ;
	$target = ($link_target)?'target="_blank" ':'';
	$link_style = !empty($link_color)?' style="color:'.$link_color.'"':'';
	$link_start = $link_end = $readmore = '';
	$border_color = ($border_color)? 'border-color:'.$border_color.'':'';
	$align_center = ($type==20) ? 'aligncenter' : '';
	if($icon_link_url){
		$link_start = '<a '.$target.'href="' . $icon_link_url . '">';
		$link_end = '</a>';
		$readmore = ($icon_link_text)?'<a '.$target.$link_style.' class="magicmore" href="'.$icon_link_url.'">'.$icon_link_text.'</a>':'';
	}
	$title_style = !empty($title_color)?' style="color:'.$title_color.'"':'';
	$content_style = !empty($content_color)?' style="color:'.$content_color.'"':'';
	$color_skin = ($type==20) ? 'colorb ' : '' ;
	echo '<article class="icon-box' . $type .' '.$color_skin .' '. $align_center .' '. $background_color_t18 . ' ' . $align . ' ' . str_replace(',', ' ', $border) . '" style="background:' . $bgcolor . ';'.$border_color.'">';
		if ( $type == 18 || $type == 20 )
			echo "<span class=\"shape\"></span>";

	if(!empty($icon_name) && $icon_name != 'none'){
		if ( $type == 21 ){
			echo '<div class="iconbox-leftsection">';
				echo $link_start.do_shortcode( "[icon name='$icon_name' size='$icon_size' color='$icon_color' bgcolor='$background_color']" ).$link_end;
			echo '</div>';
		} else
			echo $link_start.do_shortcode( "[icon name='$icon_name' size='$icon_size' color='$icon_color' bgcolor='$background_color']" ).$link_end;
	}elseif($icon_image){
		if(is_numeric($icon_image)){
			$icon_image = wp_get_attachment_url( $icon_image );
		}
		echo $link_start.'<img src="'.$icon_image.'" />'.$link_end;
	}
	echo ($type==17)? '<div class="content-s">' : '';
	$icon_subtitle = $icon_subtitle ? '<h6'.$title_style.'>'.$icon_subtitle.'</h6>' : '' ;
	if ( $type == 21 ){
		echo '<div class="iconbox-rightsection"><h6'.$title_style.'>'.$icon_subtitle.'</h6><h4'.$title_style.'>'.$icon_title.'</h4><p'.$content_style.'>'.$iconbox_content .'</p>'.$readmore.'</div>';
	} else
		echo $icon_subtitle . '<h4'.$title_style.'>'.$icon_title.'</h4><p'.$content_style.'>'.$iconbox_content .'</p>'.$readmore.'';
	echo ($type==17)? '</div></article>' : '</article>';

$out = ob_get_contents();
ob_end_clean();
$out = str_replace('<p></p>','',$out);
	return $out;
 }
 add_shortcode('iconbox', 'michigan_webnus_iconbox');
?>