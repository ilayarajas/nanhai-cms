<?php
	///////////////////METABOXES//////////////////////////////////

	// change thumbnail box from side on top
	add_action( 'do_meta_boxes', 'it_logo_image_box' );
	function it_logo_image_box() {
		remove_meta_box( 'postimagediv', __Custom_Post__, 'side' );
		add_meta_box( 'postimagediv',
			__( 'Logo Image' ,__IT_LOGO_SHOWCASE_TEXTDOMAIN__),
			'post_thumbnail_meta_box',
			__Custom_Post__,
			'normal',
			'high' );
	}

	// Custom fields
	if(!function_exists('it_logo_showcase_add_metabox')){
		function it_logo_showcase_add_metabox() {
			add_meta_box(  
				'it_logo_showcase_metaboxname', // $id
				__('Logo Details',__IT_LOGO_SHOWCASE_TEXTDOMAIN__), // $title
				'it_logo_showcase_metaboxname', // $callback
				__Custom_Post__, // $page
				'normal', // $context  
				'default');
				
		}  
		add_action('add_meta_boxes', 'it_logo_showcase_add_metabox');
	}
	///////////////////END METABOXES//////////////////////////////////
	
	
	///////////////////LIST OF FILDS VARIABLES//////////////////////////////////
		
	include  'customfields-fields-variables.php';
	
	///////////////////END LIST OF FIELDS VARIABLES/////////////////////////////

	
	/////////////////SHOW CUSTOM FIELD////////////////////////
	
	include  'customfields-fields-functions.php';	
	
	/////////////////END SHOW CUSTOM FIELD////////////////////
	
	
?>