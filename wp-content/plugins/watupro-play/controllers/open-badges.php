<?php
// Open badges documentation URLs:
// https://github.com/mozilla/openbadges/wiki/Assertions
// https://github.com/mozilla/openbadges/wiki/Issuer-API
class WatuPROPlayOpenBadges {
	static function settings() {
		if(!empty($_POST['ok'])) {
			update_option("watuproplay_openbadges_org", array(
				"name" => $_POST['name'],
				"image" => $_POST['image'],
				"url" => $_POST['url'],
				"description" => $_POST['description'],
				"email" => $_POST['email']
			));
		}		
		
		$org = get_option('watuproplay_openbadges_org');
		
		include(WATUPROPLAY_PATH."/views/open-badges.html.php");
	}
	
	// assertion URLs	
	static function template_redirect() {
		// url like ?wtp_open_badge=2.3&open-badge.json
		// this is the assertion, i.e. evidence of awarded badge
		if(!empty($_GET['wtp_open_badge'])) {
			self :: assert_badge($_GET['wtp_open_badge']);
		}
		
		// url like ?wtp_open_badge_obj=2.3&badge-def.json
		// this is the badhe class
		if(!empty($_GET['wtp_open_badge_obj'])) {
			self :: badge_class($_GET['wtp_open_badge_obj']);
		}
		
		// issuer organization info
		// URL is always ?wtp_issuer=1&badge-issuer.json
		if(!empty($_GET['wtp_issuer'])) {
			self :: issuer();
		}

		// a page with URL to accept badges		
		if(!empty($_GET['wtp_accept_badges'])) {
			self :: accept_badges($_GET['uid']);
		}
	}
	
	// output badge assertion
	// $id is actually a string user_id.badge_id
	static function assert_badge($complex_id) {
		global $wpdb;
		
		$_level = new WatuPROPlayLevel();
		
		list($uid, $id) = explode(".", $complex_id);
		$badges = $_level->get_user_badges($uid);
		
		if(!in_array($id, $badges)) self::output_json(array($complex_id => __('No such badge','watuproplay')));	
		
		$badge = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE id=%d", $id));
		if(empty($badge->id)) self::output_json(array($complex_id => __('Badge no longer available','watuproplay')));
		
		// get history record for the timestamp
		$history = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".WATUPROPLAY_HISTORY." WHERE
			user_id=%d AND item_id=%d AND action='assigned badge' ORDER BY id DESC LIMIT 1",
			$uid, $id));
		if(empty($history->id)) self::output_json(array($complex_id => __('Badge no longer available','watuproplay')));
		
		// construct recipient object
		$user = get_userdata($uid);
		$salt = md5(serialize($user));
		$recipitent = array("type"=>"email",
		"hashed"=> true, 
		"identity" => 'sha256$' . hash('sha256', $user->user_email . $salt),
		"salt" => $salt);
		
		// construct verification object
		$verify = array("type" => "hosted",
			"url"=>site_url("?wtp_open_badge=".$complex_id."&open-badge.json"));
		
		$json_badge = array(
			"uid"=>$complex_id,
			"recipient" => $recipitent,
			"badge" => site_url('?wtp_open_badge_obj='.$badge->id.'&badge-def.json'),
			"verify" => $verify,
			"issuedOn" => date("Y-m-d", $history->timestamp) /* This should be changed to actual time of issuing the badge! */
		);	
		
		self::output_json($json_badge);
	}
	
	// outputs a badge class
	static function badge_class($id) {
		global $wpdb;
		
		$badge = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE id=%d", $id));
		if(empty($badge->id)) self::output_json(array($complex_id => __('No such badge','watuproplay')));	
		
		$json_badge = array(
			"name" => stripslashes($badge->name),
			"description" => stripslashes($badge->name),
			"image" => $badge->badge_graphic,
			"criteria" => $badge->badge_criteria_url,
			"issuer" => site_url("?wtp_issuer=1&badge-issuer.json")
		);
		
		self::output_json($json_badge);
	}
	
	// outputs issuer organization ifno
	static function issuer() {
		$org = get_option('watuproplay_openbadges_org');		
		$org['description'] = stripslashes($org['description']);
		self::output_json($org);
	}
	
	// output badge output
	static function output_json($atts) {
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		
		$output = json_encode($atts); 
		
		// remove unwanted backslashes and then output
		$output = str_replace('\/','/',$output);
		echo $output;
		exit;
	}
	
	// outputs the page to accept badges 
	static function accept_badges($uid) {
		global $wpdb, $user_ID;
		if(!is_user_logged_in()) die(__("You need to <a href='".site_url('wp-login.php')."'>login</a> first.", 'watuproplay'));
		
		if($user_ID != $uid) die(__("You can only accept your own badges.", 'watuproplay'));
		
		// now get my badges
		$_badge = new WatuPROPlayLevel();
		$badge_ids = $_badge->get_user_badges($uid);

		// intersect with badges passed in the URL
		$badge_ids = array_intersect($badge_ids, explode(".", $_GET['bids']));		
		
		if(empty($badge_ids)) $badge_ids = array(0);
		// now select only these that are open badges
		$open_badges = $wpdb->get_results("SELECT * FROM ".WATUPROPLAY_LEVELS." WHERE atype='badge' AND id IN (".implode(',', $badge_ids).")"); ?>
		<html><head>
		<?php wp_head();?>
		<script src="https://beta.openbadges.org/issuer.js"></script>
		</head>
		<body style="padding:100px;">
		<?php
		// output body and link		
		$onclick = "OpenBadges.issue([";
		foreach($open_badges as $cnt=>$badge) {			
			if($cnt>0) $onclick .= ", ";
			$onclick .= "'".site_url("?wtp_open_badge=".$uid.".".$badge->id."&open-badge.json")."'";
		}
		$onclick .= "], function(errors, successes) {	 
			window.location='".site_url()."';
			// console.log(successes);
		})";
		
		echo "<p>".sprintf(__('You have been issued Mozilla open badges. <a href="#" onclick="%s;return false;">Click here</a> to add them to your <a href="https://backpack.openbadges.org/" target="_blank">Backpack</a>.', 'watuproplay'), $onclick)."</p>";
		?>
		</body>
		</html>
		<?php exit;
	}
}