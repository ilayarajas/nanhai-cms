<div class="wrap">
	<h1><?php printf(__('Users with level %s', 'watuproplay'), stripslashes($level->name));?></h1>
	
	<p><a href="admin.php?page=watuproplay_levels"><?php _e('Back to levels', 'watuproplay');?></a></p>
	
	<?php if($cnt_users):?>
		<table class="widefat">
			<tr><th><?php _e('Username', 'watuproplay');?></th><th><?php _e('Full name', 'watuproplay');?></th>
				<th><?php _e('Email', 'watuproplay')?></th><th><?php _e('Quizzes', 'watuproplay')?></th><th><?php _e('Edit', 'watuproplay');?></th></tr>
			<?php foreach($users as $user):
				$first_name = get_user_meta($user->ID, 'first_name', true);
    			$last_name = get_user_meta($user->ID, 'last_name', true);
    			$class = ('alternate' == @$class) ? '' : 'alternate';?>
    			<tr class="<?php echo $class?>">
    				<td><?php echo $user->user_login;?></td>
    				<td><?php echo $first_name ? $first_name .' '. $last_name : _('N/a', 'watuproplay');?></td>
    				<td><?php echo $user->user_email?></td>
    				<td><a href="admin.php?page=my_watupro_exams&user_id=<?php echo $user->ID?>" target="_blank"><?php _e('Quizzes', 'watuproplay');?></a></td>
    				<td><a href="user-edit.php?user_id=<?php echo $user->ID?>&wp_http_referer=<?php echo urlencode('admin.php?page=watuproplay_levels&action=users&id='.$level->ID)?>" target="_blank"><?php _e('Edit', 'watuproplay');?></a></td>
    			</tr>
			<?php endforeach;?>	
		</table>
		<p align="center">
			<?php if($offset > 0):?>
				<a href="admin.php?page=watuproplay_levels&action=users&id=<?php echo $level->id?>&offset=<?php echo $offset-$limit?>"><?php _e('previous page', 'watuproplay');?></a>
			<?php endif;?>
			<?php if($offset + $limit < $cnt_users):?>
				<a href="admin.php?page=watuproplay_levels&action=users&id=<?php echo $level->id?>&offset=<?php echo $offset+$limit?>"><?php _e('next page', 'watuproplay');?></a>
			<?php endif;?>
		</p>
	<?php else:?>
		<p><?php _e('There are no users with this level yet.', 'watuproplay');?></p>
	<?php endif;?>
</div>