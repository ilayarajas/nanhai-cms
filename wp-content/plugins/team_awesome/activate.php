<?php

$tainstall = get_option("team-awesome-install", false);

if (!$tainstall) {
    // Set default general settings values
    TEAM_AWESOME_ADMIN::reset_options();
    update_option("team-awesome-install", true);
}