<style>
	.postbox .form-table th { padding:10px }
	table.widefat { border:0 }
</style>
<script>
	jQuery(document).ready(function($) { 	
		$("#plt_settings_form").submit(function(){
			if($("input[name='_categories\[\]']:checked").length == 0){
				alert('Please select at least one category.')
				return false;
			}
		});
	} ); 
</script>

<div class="wrap">
	<h2>Post List Table <?php echo _e('Settings')?></h2><br />
	
	<?php if (!empty($message)): ?>
		<div id="message" class="updated"><p><?php echo $message ?></p></div>
	<?php endif;?>
	
	<div id="poststuff">
		<form id="plt_settings_form" method="post" action="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=plt_admin_main&noheader=true');?>">
			<input type="hidden" name="table_id" value="<?php echo $table_id;?>" />
			
			<div class="postbox">
				<h3 class="hndle ui-sortable-handle">
					<span><div class="dashicons dashicons-admin-generic"></div> <?php echo _e('General Settings')?></span>
				</h3>
				<div class="inside">
					<table class="form-table">
					<tbody>					 
						<tr valign="top">
							<th scope="row">
								<label><?php echo _e('Table Name')?></label>
							</th>
							<td>
								<input type="text" name="_table[name]" value="<?php echo $settings->name ?>"  />
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label><?php echo _e('Show Filter')?></label>
							</th>
							<td>
								<input id="filter_checkbox" type="checkbox" name="_table[filter]" <?php if (!empty($settings->filter) && $settings->filter) echo 'checked="checked"' ?>  />
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label><?php echo _e('Filter Position')?></label>
							</th>
							<td>
								<select name="_table[filter_position]">
									<option value="head" <?php if (!empty($settings->filter_position) && $settings->filter_position == 'head') echo 'selected="selected"' ?>>Top</option>
									<option value="foot" <?php if (!empty($settings->filter_position) && $settings->filter_position == 'foot') echo 'selected="selected"' ?>>Bottom</option>
								</select>
							</td>
							<td>
								
							</td>
						</tr>
						
						
						<tr valign="top">
							<th scope="row">
								<label><?php echo _e('Order by Field')?></label>
							</th>
							<td>
								<?php if(empty($settings->order_field)) $settings->order_field = 'Date'; ?>
								<select name="_table[order_field]">
									<?php foreach($settings->fields as $s): ?>
									<option value="<?php echo $s->field_name ?>" <?php if (!empty($settings->order_field) && $settings->order_field == $s->field_name) echo 'selected="selected"' ?>><?php echo $s->field_name ?></option>
									<?php endforeach; ?>
								</select>
							</td>
							<td>
								
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label><?php echo _e('Order Direction')?></label>
							</th>
							<td>
								<?php if(empty($settings->order_direction)) $settings->order_direction = 'DESC'; ?>
								<select name="_table[order_direction]">
									<option value="asc" <?php if (!empty($settings->order_direction) && $settings->order_direction == 'asc')  echo 'selected="selected"' ?>><?php echo _e('Ascending')?></option>
									<option value="desc" <?php if (!empty($settings->order_direction) && $settings->order_direction == 'desc') echo 'selected="selected"' ?>><?php echo _e('Descending')?></option>
								</select>
							</td>
							<td>
								
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label><?php echo _e('Page Length')?></label>
							</th>
							<td>
								<select name="_table[page_length]">
									<option value="10" <?php if (!empty($settings->page_length) && $settings->page_length == '10') echo 'selected="selected"' ?>>10</option>
									<option value="25" <?php if (!empty($settings->page_length) && $settings->page_length == '25') echo 'selected="selected"' ?>>25</option>
									<option value="50" <?php if (!empty($settings->page_length) && $settings->page_length == '50') echo 'selected="selected"' ?>>50</option>
									<option value="100" <?php if (!empty($settings->page_length) && $settings->page_length == '100') echo 'selected="selected"' ?>>100</option>
								</select>
							</td>
							<td>
								
							</td>
						</tr>
							   

						<tr>
							<td colspan="2">
								<input type="submit" name="submitStep1" id="submitStep1" class="button-primary" value="<?php echo _e('Save')?>">
							</td>
						</tr>
						

					</table>
				</div>
			</div>
			
			<div class="postbox">
				<h3 class="hndle ui-sortable-handle">
					<span><div class="dashicons dashicons-feedback"></div> <?php echo _e('Field Settings')?></span>
				</h3>
				<div class="inside">
			
					<table class="wp-list-table widefat fixed posts">
						<thead>
						<tr>
						<th class="manage-column"><?php echo _e('Field Name') ?></th>
						<th><?php echo _e('Field Label') ?></th>
						<th><?php echo _e('Visible') ?></th>
						<th><?php echo _e('Link') ?></th>
						</tr>  
						</thead>
						<tbody>
							 
						<?php foreach($settings->fields as $field_name => $s): ?>
						<tr valign="top">
						<input type="hidden" name="_fields[<?php echo $field_name?>][id]" value="<?php echo $s->id ?>" />
						<th scope="row">
							<label><?php echo _e($field_name)?></label>
						</th>
						<td>
							<input type="text" name="_fields[<?php echo $field_name?>][field_label]" value="<?php if (!empty($s->field_label)) echo $s->field_label ?>" />
						</td>
						<td>
							<input type="checkbox" name="_fields[<?php echo $field_name?>][visible]" <?php if (!empty($s->visible) && $s->visible) echo 'checked="checked"' ?> />
						</td>
						<td>
							<?php if($field_name != 'Excerpt' && $field_name != 'Date'):?>
							<input type="checkbox" name="_fields[<?php echo $field_name?>][link]" <?php if (!empty($s->link) && $s->link) echo 'checked="checked"' ?> />
							<?php endif; ?>
						</td>
						</tr>
						<?php endforeach; ?>
							   

						<tr>
						<td colspan="2">
							<input type="submit" name="submitStep1" id="submitStep1" class="button-primary" value="<?php echo _e('Save')?>">
						</td>
						</tr>
						</tbody>

					</table>
				</div>
			</div>
			
			<!-- Start CATEGORIES ==================================================================== -->
			<div class="postbox">
				<h3 class="hndle ui-sortable-handle">
					<span><div class="dashicons dashicons-category"></div> <?php echo _e('Choose Categories')?></span>
				</h3>
				<div class="inside">
					<table class="wp-list-table widefat fixed posts">
						<thead>
							<tr>
								<th scope="col"><?php _e('Category'); ?></th>
								<th scope="col"><?php _e('Include in table'); ?></th>
							</tr>
						</thead>
						<tbody id="the-list">
						<?php
						$odd = 0;
						foreach( $categories as $cat ) {
							?>
							<tr<?php if ( $odd == 1 ) { echo ' class="alternate"'; $odd = 0; } else { $odd = 1; } ?>>
								<th scope="row"><?php echo $cat->cat_name; //. ' (' . $cat->cat_ID . ')'; ?></th>
								<td><input type="checkbox" name="_categories[]" value="<?php echo $cat->cat_ID ?>"  <?php if ( in_array($cat->cat_ID, $selected_categories ) ) { echo 'checked="true" '; } ?> /></td>
							</tr>			
						<?php } ?>
							<tr>
							<td colspan="2">
								<input type="submit" name="submitStep1" id="submitStep1" class="button-primary" value="<?php echo _e('Save')?>">

							</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- End CATEGORIES ====================================================================== -->

		</form>
	</div>
</div>
