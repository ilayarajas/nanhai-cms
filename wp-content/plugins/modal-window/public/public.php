<?php
if ( ! defined( 'ABSPATH' ) ) exit; 
function show_modalsimple_window($atts) {
    extract(shortcode_atts(array('id' => ""), $atts));	
    global $wpdb;
	$table_modal = $wpdb->prefix . "modalsimple";
    $sSQL = "select * from $table_modal WHERE id=$id";
    $arrresult = $wpdb->get_results($sSQL); 
    if (count($arrresult) > 0) {
        foreach ($arrresult as $key => $val) {
			include( 'partials/public.php' );
			wp_enqueue_script( 'wow-modalsimple-script-'.$val->id, plugins_url().'/'.WOW_MODALSIMPLE_PLUGIN_BASENAME. '/asset/wowscript-'.$val->id.'.js', array( 'jquery' ) );					
			wp_enqueue_style( 'wow-modalsimple-style-'.$val->id, plugins_url().'/'.WOW_MODALSIMPLE_PLUGIN_BASENAME. '/asset/wowstyle-'.$val->id.'.css');				
						
			if ($val->use_cookies == "yes"){
				wp_enqueue_script( 'cookie-modal-window', plugin_dir_url( __FILE__ ) . 'js/jquery.cookie.js', array( 'jquery' ) );
			}			
        }
    } else {		
		echo "<p><strong>No Records</strong></p/><p/>";        
    }  
	
	return;
}
add_shortcode('modalsimple', 'show_modalsimple_window');


function wow_show_modalsimple_window($atts) {
    extract(shortcode_atts(array('id' => ""), $atts));	
    global $wpdb;
	$table_modal = $wpdb->prefix . "modalsimple";
    $sSQL = "select * from $table_modal WHERE id=$id";
    $arrresult = $wpdb->get_results($sSQL); 
    if (count($arrresult) > 0) {
        foreach ($arrresult as $key => $val) {
			include( 'partials/public.php' );
			wp_enqueue_script( 'wow-modalsimple-script-'.$val->id, plugins_url().'/'.WOW_MODALSIMPLE_PLUGIN_BASENAME. '/asset/wowscript-'.$val->id.'.js', array( 'jquery' ) );					
			wp_enqueue_style( 'wow-modalsimple-style-'.$val->id, plugins_url().'/'.WOW_MODALSIMPLE_PLUGIN_BASENAME. '/asset/wowstyle-'.$val->id.'.css');				
						
			if ($val->use_cookies == "yes"){
				wp_enqueue_script( 'cookie-modal-window', plugin_dir_url( __FILE__ ) . 'js/jquery.cookie.js', array( 'jquery' ) );
			}			
        }
    } else {		
		echo "<p><strong>No Records</strong></p/><p/>";        
    }  
	
	return;
}
add_shortcode('Wow-Modal-Windows', 'wow_show_modalsimple_window');