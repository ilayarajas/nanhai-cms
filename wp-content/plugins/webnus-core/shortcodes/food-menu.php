<?php
function  michigan_webnus_food_menu_shortcode( $attributes, $content ) {
	extract( shortcode_atts( array(
		'title'				=> '',
		'food_menu_item'	=> '',
		'arrow_none'		=> '',
	), $attributes ) );

	// food_menu_item loop
	$food_menu_item		= (array) vc_param_group_parse_atts( $food_menu_item );
	$food_menu_item_data	= array();
	foreach ( $food_menu_item as $data ) :
		$new_line 						= $data;
		$new_line['food_name']			= isset( $data['food_name'] ) ? $data['food_name'] : '';
		$new_line['icon_fontawesome']	= isset( $data['icon_fontawesome'] ) ? $data['icon_fontawesome'] : 'fa fa-adjust';
		$new_line['icon_image']			= isset( $data['icon_image'] ) ? $data['icon_image'] : '';
		$food_menu_item_data[]			= $new_line;
	endforeach;

	// render
	$out = '<div class="food-menu-wrap">
				<h2 class="food-menu-title">' . esc_html( $title ) . '</h2>
				<div class="food-menu-owl-carousel owl-carousel owl-theme">';
					foreach ( $food_menu_item_data as $line ) :
						// Enqueue needed icon font
						vc_icon_element_fonts_enqueue( 'fontawesome' );
						$icon = '';
						if ( ! empty( $line['icon_image'] ) ) :
							$icon = is_numeric( $line['icon_image'] ) ? '<img src="' . esc_attr( wp_get_attachment_url( $line['icon_image'] ) ) . '" alt="' . esc_attr( 'food item' ) . '">' : '<img src="' . esc_attr( $line['icon_image'] ) . '" alt="' . esc_attr( 'food item' ) . '">';
						elseif( $line['icon_fontawesome'] ) :
							$icon = '<i class="' . esc_attr( $line['icon_fontawesome'] ) . '"></i>';
						endif;

						$out .= '
							<div class="food-menu-item">
								' . $icon . '
								<p>' . $line['food_name'] . '</p>
							</div>';
					endforeach;
				$out .= '</div>'; // end .food-menu-owl-carousel
			// pagination
			$out .= '
				<div class="food-menu-navigation ' . $arrow_none . '">
					<a class="btn next">next<i class="fa-angle-right"></i></a>
					<a class="btn prev"><i class="fa-angle-left"></i>previous</a>
				</div>';
	$out .= '</div>';

	return $out;
}

add_shortcode( 'food_menu', 'michigan_webnus_food_menu_shortcode' );