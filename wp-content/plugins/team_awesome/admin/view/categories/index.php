<?php

$view = TEAM_AWESOME_ADMIN::get_var("view");
$task = TEAM_AWESOME_ADMIN::get_var("task");

switch (strtolower($task)) :
    case "create" : 
        $return = TEAM_AWESOME_ADMIN::create_category();
        if (is_wp_error($return))
        echo "<p class='error'>".$return->get_error_message()."</p>";
    break;
    
    case "delete" : 
        $return = TEAM_AWESOME_ADMIN::delete_category();
        if (is_wp_error($return))
        echo "<p class='error'>".$return->get_error_message()."</p>";
    break;
    
    case "updateorder" : 
        $return = TEAM_AWESOME_ADMIN::order_categories();
        if (is_wp_error($return))
        echo "<p class='error'>".$return->get_error_message()."</p>";
    break;
endswitch;


switch (strtolower($view)) :
    case "addedit" : 
        require (dirname(__FILE__) . DS . "addedit.php");
    break;

    case "list" : 
    default: 
        require (dirname(__FILE__) . DS . "list.php");
endswitch;