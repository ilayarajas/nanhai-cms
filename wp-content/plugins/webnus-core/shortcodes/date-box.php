<?php
function michigan_webnus_datebox( $attributes, $content = null ) {
extract(shortcode_atts(	array(
	'title'		=>	'',
	'des'		=>	'',
	'day' 		=>	'',
	'month'		=>	'',
), $attributes));
	ob_start();
	echo '<div class="date-box">
			<article class="ln-item">
			<div class="ln-date"><div class="ln-day">'.$day.'</div><div class="ln-month">'.$month.'</div></div>
			<div class="ln-content">
				<h3>'.$title.'</h3>
				<p class="ln-des">'.$des.'</p>
			</div>
			</article>
		</div>';
	$out = ob_get_contents();
	ob_end_clean();	
	return $out;
 }
 add_shortcode('datebox', 'michigan_webnus_datebox');
?>