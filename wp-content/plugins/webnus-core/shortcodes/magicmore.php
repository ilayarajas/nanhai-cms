<?php

 function michigan_webnus_magicmore( $attributes, $content = null ) {
 	
	extract(shortcode_atts(array(
	
	"title" =>'',
	"link" =>'#',	
	), $attributes));

 return '<a href="'.$link.'" class="magicmore">'.$title. '</a>'; 
	

 }
 add_shortcode('magicmore', 'michigan_webnus_magicmore');
?>