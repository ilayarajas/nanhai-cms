<?php
 function michigan_webnus_agoal( $attributes, $content = null ) {
extract(shortcode_atts(	array(
	'post'=>'',
), $attributes));
	ob_start();		
	$args = array(
		'post_type' => 'goal',
		'posts_per_page' => 1,
		'p'	=> $post,
	);
	$query = new WP_Query($args);	
?>
<div class="container goals single-goal">
<?php	while ($query -> have_posts()) : $query -> the_post();	
		$post_id = get_the_ID();
		$cats = get_the_terms( $post_id , 'goal_category' );
		if(is_array($cats)){
			$goal_category = array();
			foreach($cats as $cat){
				$goal_category[] = $cat->slug;
			}
		}else $goal_category=array();
		$cats = get_the_terms($post_id, 'goal_category' );
		$cats_slug_str = '';
		if ($cats && ! is_wp_error($cats)) :
			$cat_slugs_arr = array();
		foreach ($cats as $cat) {
			$cat_slugs_arr[] = '<a href="'. get_term_link($cat, 'goal_category') .'">' . $cat->name . '</a>';
		}
		$cats_slug_str = implode( ", ", $cat_slugs_arr);
		endif;
	
		$category = ($cats_slug_str)?esc_html__('Category: ','michigan') . $cats_slug_str:'';
		$date = get_the_time('F d, Y');
		$permalink = get_the_permalink();
		$image = get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_courses_img','echo'=>false, ) );
		$image2 = get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_blog2_img','echo'=>false, ) );	
		$title = '<h4><a class="goal-title" href="'.$permalink.'">'.get_the_title().'</a></h4>';
		$content ='<p>'.michigan_webnus_excerpt(64).'</p>';
		$view = '<div class="goal_view"><i class="fa-eye"></i>'.michigan_webnus_getViews($post_id).'</div>';
		$progressbar = $goal_days = $goal_donate = '';
		$percentage = 0;
		$received = rwmb_meta( 'michigan_goal_amount_received_meta' ) ? rwmb_meta( 'michigan_goal_amount_received_meta' ) : 0;
		$amount = rwmb_meta( 'michigan_goal_amount_meta' ) ? rwmb_meta( 'michigan_goal_amount_meta' ) : 0 ; 
		$end = rwmb_meta( 'michigan_goal_end_meta' ) ? rwmb_meta( 'michigan_goal_end_meta' ) : '' ; 
		$currency = esc_html(michigan_webnus_options::michigan_webnus_currency());
		if($amount) {
			$percentage = ($received/$amount)*100;
			$percentage = round($percentage);
			$out= $currency.$received.esc_html__(' Raised of ','michigan').$currency.$amount;
			$progressbar = do_shortcode('[vc_pie value="'.$percentage.'" color="custom"  custom_color="#d0ae5e" el_class="single-goal-pie" units="%"]');
		}
		$now = date('Y-m-d 23:59:59');
		$now = strtotime($now);
		$end_date = $end.' 23:59:59';
		$your_date = strtotime($end_date);
		$datediff = $your_date - $now;
		$days_left = floor($datediff/(60*60*24)); 
		$date_msg = '';
		if($days_left==0) {$date_msg = '1';}
		elseif($days_left<0) {$date_msg = 'No';}
		else {$date_msg = $days_left+'1'.'';}
		$goal_days = ($percentage<100)?'<span>'.$date_msg.'</span> '.esc_html__('Days left to achieve target','michigan'):esc_html__('Thank You','michigan');
		echo '<article class="container"><div class="goal-content row">';
		echo '<div class="goal-progress col-md-4 col-sm-4">'.$progressbar.'</div>';
		echo '<div class="col-md-1 col-sm-1"></div>';
		echo '<div class="goal-details col-md-7 col-sm-7">'.$title.$content;
		if($days_left>=0 && $percentage<100 && michigan_webnus_options::michigan_webnus_donate_form()){
			echo michigan_webnus_modal_donate();
			echo '<span class="goal-raised">'.$out.'</span>';
		}else{
			echo '<p class="goal-completed">'.esc_html__('Has been completed','michigan').'</p>';
		}	
		echo '<span class="goal-days">'.$goal_days.'</span></div></div></article>';
	endwhile;
		$out = ob_get_contents();
		ob_end_clean();	
		wp_reset_postdata();
		return $out;
	}
 add_shortcode('agoal', 'michigan_webnus_agoal');
?>