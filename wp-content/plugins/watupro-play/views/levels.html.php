<div class="wrap">
	<h1><?php _e('Manage Levels', 'watuproplay')?></h1>
	
	<div class="watupro-alert">
	<p><?php _e('Levels can be assigned to your users upon achieving a number of points, % correct answers, completing all tests from a category etc. For example you can create levels like "Newbie", "Advanced user", "Wizard" etc to encourage the participation in your site.', 'watuproplay')?></p>
	
	<p><?php _e("Each user can have only one level at a time (i.e. they can't be Expert and Beginner at the same time for example). The user will always be assigned the highest level they qualify for.", 'watupro')?></p>
	</div>
	
	<p><a href="admin.php?page=watuproplay_levels&action=add"><?php _e('Click here to define a new level', 'watupro')?></a></p>
	
	<?php if(sizeof($levels)):?>
		<table class="widefat">
			<tr><th><?php _e('Rank', 'watuproplay')?></th><th><?php _e('Level', 'watuproplay')?></th><th><?php _e('Shortcode *', 'watuproplay')?></th>
			<th><?php _e('View users', 'watuproplay')?></th><th><?php _e('Edit', 'watuproplay')?></th></tr>
			<?php foreach($levels as $level):
				$class = ('alternate' == @$class) ? '' : 'alternate';?>
				<tr class="<?php echo $class?>"><td><?php echo $level->rank?></td><td><?php echo $level->name?></td><td><input type="text" onclick="this.select();" value="[watuproplay-list level <?php echo $level->id?>]"></td>
				<td><a href="admin.php?page=watuproplay_levels&action=users&id=<?php echo $level->id?>"><?php _e('View users', 'watuproplay');?></a></td>
				<td><a href="admin.php?page=watuproplay_levels&action=edit&id=<?php echo $level->id?>"><?php _e('Edit', 'watuproplay')?></a></td></tr>
			<?php endforeach;?>
		</table>
		
		<p class="watuproplay-help"><?php _e('* The shortcode shown in the table can be used to display all users with this level. For the moment it will only list usernames one per line. More advanced configurations are coming soon.', 'watuproplay')?></p>
		
		<p>&nbsp;</p>
		<h2><?php _e('Manage Notifications for Levels', 'watuproplay')?></h2>
		<div class="wp-admin watupro-padded postbox">
			<form method="post" class="watupro">
				<h3><?php _e('Email message sent to you', 'watuproplay')?></h3>
				<p><input type="checkbox" name="admin_level_email" value="1" <?php if(get_option('watuproplay_admin_level_email')) echo 'checked'?>> <?php _e('Email me when user reaches a new level', 'watuproplay')?> </p>
								
				<p><label><?php _e('Email subject:', 'watuproplay')?></label> <input type="text" name="admin_level_subject" value="<?php echo get_option('watuproplay_admin_level_subject')?>" size="60"></p>
				
				<p><label><?php _e('Email contents:', 'watuproplay')?></label> <?php wp_editor(stripslashes(get_option('watuproplay_admin_level_message')), 'admin_level_message')?></p>
				
				<p class="watupro-help"><?php _e('You can use the following variables:', 'watuproplay')?> {{user-name}}, {{user-email}}, {{user-id}}, {{old-level}}, {{new-level}}</p>
				
				<p align="center"><input type="submit" value="<?php _e('Save Notification Email', 'watuproplay')?>"></p>
				<input type="hidden" name="save_admin_level_email" value="1">
			</form>	
			
			<p>&nbsp;</p>
			
			<form method="post" class="watupro">
				<h3><?php _e('Email message sent to user', 'watuproplay')?></h3>
				<p><input type="checkbox" name="user_level_email" value="1" <?php if(get_option('watuproplay_user_level_email')) echo 'checked'?>> <?php _e('Email user when they reach a new level', 'watuproplay')?> </p>
								
				<p><label><?php _e('Email subject:', 'watuproplay')?></label> <input type="text" name="user_level_subject" value="<?php echo get_option('watuproplay_user_level_subject')?>" size="60"></p>
				
				<p><label><?php _e('Email contents:', 'watuproplay')?></label> <?php wp_editor(stripslashes(get_option('watuproplay_user_level_message')), 'user_level_message')?></p>
				
				<p class="watupro-help"><?php _e('You can use the following variables:', 'watuproplay')?> {{user-name}}, {{user-email}}, {{user-id}}, {{old-level}}, {{new-level}}</p>
				
				<p align="center"><input type="submit" value="<?php _e('Save Notification Email', 'watuproplay')?>"></p>
				<input type="hidden" name="save_user_level_email" value="1">
			</form>	
		</div>	
	<?php else:?>
		<p><?php _e('You have not yet defined any levels', 'watuproplay')?></p>	
	<?php endif;?>
	
</div>