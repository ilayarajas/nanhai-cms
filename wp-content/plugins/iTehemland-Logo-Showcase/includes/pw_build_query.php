<?php
	$query='';
    $args = array(
			'post_type'  => __Custom_Post__,
			'posts_per_page'=> $count,
            'orderby' => $orderby,
            'order' => $order
    );
    // select category
    $tax_terms='';
    if($category != 'all') {
        $tax_terms = explode(',' ,$category);
        $args['tax_query'] = array(array('taxonomy' => __Custom_Post_Taxonomy__, 'field' => 'slug', 'terms' => $tax_terms));
    }
	// The Query
    $query = new WP_Query( $args );

?>