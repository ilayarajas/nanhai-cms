<?php
 function michigan_webnus_goals( $attributes, $content = null ) {
extract(shortcode_atts(	array(
	'type'=>'grid',
	'category'=>'',
	'count'=>'6',
	'page'=>'',
	'sort'=>'',
	'icon'=>'',
), $attributes));
	ob_start();		
	$view =($sort=='view')?"'&orderby=meta_value_num&meta_key=michigan_webnus_views":"";
	$paged = ( is_front_page() ) ? 'page' : 'paged' ;
	$pages = ($page)?'&paged='.get_query_var($paged):'&paged=1';
	$query = new WP_Query('post_type=goal&posts_per_page='.$count.'&category_name='.$category.$pages.$view);
?>
<div class="container goals goals-<?php echo $type ?>">
<?php
	$col = ($count<5)? 12/$count:4;
	$row = 12/$col;
	$rcount= 1 ;
	while ($query -> have_posts()) : $query -> the_post();	
		$post_id = get_the_ID();
		$cats = get_the_terms( $post_id , 'goal_category' );
		if(is_array($cats)){
			$goal_category = array();
			foreach($cats as $cat){
				$goal_category[] = $cat->slug;
			}
		}else $goal_category=array();
		$cats = get_the_terms($post_id, 'goal_category' );
		$cats_slug_str = '';
		if ($cats && ! is_wp_error($cats)) :
			$cat_slugs_arr = array();
		foreach ($cats as $cat) {
			$cat_slugs_arr[] = '<a href="'. get_term_link($cat, 'goal_category') .'">' . $cat->name . '</a>';
		}
		$cats_slug_str = implode( ", ", $cat_slugs_arr);
		endif;
		
	
		$category = ($cats_slug_str)?esc_html__('Category: ','michigan') . $cats_slug_str:'';
		$date = get_the_time('F d, Y');
		$permalink = get_the_permalink();
		$image = get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_courses_img','echo'=>false, ) );
		$image2 = get_the_image( array( 'meta_key' => array( 'thumbnail', 'thumbnail' ), 'size' => 'michigan_webnus_blog2_img','echo'=>false, ) );	
		$title = '<h4><a class="goal-title" href="'.$permalink.'">'.get_the_title().'</a></h4>';
		$content ='<p>'.michigan_webnus_excerpt(16).'</p>';
		$view = '<div class="goal_view"><i class="fa-eye"></i>'.michigan_webnus_getViews($post_id).'</div>';
		$progressbar = $goal_days = $goal_donate = '';
		$percentage = 0;
		$received = rwmb_meta( 'michigan_goal_amount_received_meta' ) ? rwmb_meta( 'michigan_goal_amount_received_meta' ) : 0;
		$amount = rwmb_meta( 'michigan_goal_amount_meta' ); 
		$end = rwmb_meta( 'michigan_goal_end_meta' );
		$currency = esc_html(michigan_webnus_options::michigan_webnus_currency());
		if($amount) {
			$percentage = ($received/$amount)*100;
			$percentage = round($percentage);
			$out=$percentage.'% '.esc_html__('DONATED OF ','michigan').$currency.$amount;
			$progressbar = do_shortcode('[vc_progress_bar values="'.$percentage.'|'.$out.'" bgcolor="custom" options="striped,animated" custombgcolor="#d0ae5e"]');
		}
		$now = date('Y-m-d 23:59:59');
		$now = strtotime($now);
		$end_date = $end.' 23:59:59';
		$your_date = strtotime($end_date);
		$datediff = $your_date - $now;
		$days_left = floor($datediff/(60*60*24)); 
		$date_msg = '';
		if($days_left==0) {$date_msg = '1';}
		elseif($days_left<0) {$date_msg = 'No';}
		else {$date_msg = $days_left+'1'.'';}
		$goal_days = ($percentage<100)?'<span>'.$date_msg.'</span> '.esc_html__('Days left to achieve target','michigan'):esc_html__('Thank You','michigan');
		if ($type=='grid'){
			echo ($rcount == 1)?'<div class="row">':'';		
			echo '<div class="col-md-'.$col.' col-sm-'.$col.'"><article>'.$image;
			echo '<div class="goal-content">'.$title.$content;
			echo '<div class="goal-meta">'.$progressbar.'<p class="goal-days">'.$goal_days.'</p>';
			if($days_left>=0 && $percentage<100 && michigan_webnus_options::michigan_webnus_donate_form()){
				echo michigan_webnus_modal_donate();
			}else{
				echo '<p class="goal-completed">'.esc_html__('Has been completed','michigan').'</p>';
			}	
			echo '</div></article></div>';
			if($rcount == $row){
				echo '</div>';
				$rcount = 0;
			}
			$rcount++;
		}
		elseif ($type=='list'){
			echo '<article id="post-'.$post_id.'"><div class="row"><div class="col-md-4">';
			echo ($image)?'<figure class="goal-img">'.$image2.'</figure>':'';
			echo '</div><div class="col-md-8"><div class="goal-content">'.$title.'<div class="postmetadata">';
			?>
			<ul class="goal-metadata">
			<li class="goal-date"> <i class="fa-calendar-o"></i><span><?php the_time('F d, Y') ?></span> </li>
			<li class="goal-comments"> <i class="fa-folder"></i><span><?php the_terms($post_id, 'goal_category', '',' | ','' ); ?></span> </li>
			<li class="goal-comments"> <i class="fa-comments"></i><span><?php comments_number(); ?></span> </li>
			<li  class="goal-views"> <i class="fa-eye"></i><span><?php echo michigan_webnus_getViews($post_id); ?></span><?php esc_html_e(' Views','michigan');?></li>
			</ul>
			</div>
			<?php echo $content.'<div class="goal-meta">'.$progressbar;
			if($days_left>=0 && $percentage<100 && michigan_webnus_options::michigan_webnus_donate_form()){
				echo michigan_webnus_modal_donate();
			}else{
				echo '<p class="goal-completed">'.esc_html__('Has been completed','michigan').'</p>';
			}				
			
			?>	
			<div class="goal-sharing">
				<div class="goal-social">
				<a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" target="blank"><i class="goal-sharing-icon fa-facebook"></i></a>
				<a class="google" href="https://plusone.google.com/_/+1/confirm?hl=en-US&amp;url=<?php the_permalink(); ?>" target="_blank"><i class="goal-sharing-icon fa-google-plus"></i></a>
				<a class="twitter" href="https://twitter.com/intent/tweet?original_referer=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>&amp;tw_p=tweetbutton&amp;url=<?php the_permalink(); ?><?php echo isset( $twitter_user ) ? '&amp;via='.$twitter_user : ''; ?>" target="_blank"><i class="goal-sharing-icon fa-twitter"></i></a>
				</div>
			</div>
			<?php
			echo '</div></div></article>';
		}
	endwhile;
	echo(($type=='grid')&&($rcount !=1))?'</div>':'';
	echo "</div>";
		
if($page){ ?>
	<section class="container aligncenter">
        <?php 
			if(function_exists('wp_pagenavi')) {
				wp_pagenavi( array( 'query' => $query ) );
			}
	    ?>
        <hr class="vertical-space2">
    </section>  
	<?php }
		$out = ob_get_contents();
		ob_end_clean();	
		wp_reset_postdata();
		return $out;
	}
 add_shortcode('goals', 'michigan_webnus_goals');
?>