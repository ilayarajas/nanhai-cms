<div class="wrap">
	<h1><?php _e('Open Badges Issuer Settings', 'watuproplay');?></h1>
	
	<p><b><?php _e('These settings are required by WatuPRO Play Plugin in order to issue Mozilla Open Badges. Please do complete the required settings below.', 'watuproplay')?></b></p>
	
	<form method="post" class="watupro" onsubmit="return validateOpenBadges(this);">
		<p><label><b><?php _e('Organization name:', 'watuproplay')?></b></label> <input type="text" name="name" value="<?php echo @$org['name']?>" size="30"></p>
		<p><label><b><?php _e('Organization URL:', 'watuproplay')?></b></label> <input type="text" name="url" value="<?php echo @$org['url']?>" size="30"></p>
		<p><label><?php _e('Description:', 'watuproplay')?></label> <?php wp_editor(@stripslashes($org['description']), 'description');?></p>
		<p><label><?php _e('Logo url:', 'watuproplay')?></label> <input type="text" name="image" value="<?php echo @$org['image']?>" size="30"></p>
		<p><label><?php _e('Email:', 'watuproplay')?></label> <input type="text" name="email" value="<?php echo @$org['email']?>" size="30"></p>
		
		<p align="center"><input type="submit" value="<?php _e('Save Settings', 'watupro')?>"></p>
		<input type="hidden" name="ok" value="1">
	</form>
</div>

<script type="text/javascript" >
function validateOpenBadges(frm) {
	if(frm.name.value == '') {
		alert("<?php _e('Please enter organization name','watuproplay');?>");
		frm.name.focus();
		return false;
	}
	
	if(frm.url.value == '') {
		alert("<?php _e('Please enter organization URL','watuproplay');?>");
		frm.url.focus();
		return false;
	}
}
</script>