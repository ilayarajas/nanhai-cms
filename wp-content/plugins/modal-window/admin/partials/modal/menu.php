<?php
global $wpdb;
$table_wow = $wpdb->prefix . "modalsimple";
$info = (isset($_REQUEST["info"])) ? $_REQUEST["info"] : '';
if ($info == "saved") {
    echo "<div class='updated' id='message'><p><strong>".__("Record Added", "wow-marketings")."</strong>.</p></div>";
}
if ($info == "update") {
    echo "<div class='updated' id='message'><p><strong>".__("Record Updated", "wow-marketings")."</strong>.</p></div>";
}
if ($info == "del") {
    $delid = $_GET["did"];
    $wpdb->query("delete from " . $table_wow . " where id=" . $delid);
    echo "<div class='updated' id='message'><p><strong>".__("Record Deleted", "wow-marketings").".</strong>.</p></div>";
}
$resultat = $wpdb->get_results("SELECT * FROM " . $table_wow . " order by id asc");
$count = count($resultat);
?>
<div class="wow">
<h1><?php esc_attr_e("Modal Window", "wow-marketings") ?></h1>
<ul class="wow-admin-menu">
<li><a href='admin.php?page=wow-modalsimple'><?php esc_attr_e("List", "wow-marketings") ?></a></li>
<li>
	<?php if($count<3){?>
	<a href='admin.php?page=wow-modalsimple&wow=add' ><?php esc_attr_e("Add new", "wow-marketings") ?></a>
	<?php } ?>
	</li>
<li><a href='https://wow-estore.com/downloads/wow-modal-windows-pro/' target="_blank"><b><?php esc_attr_e("Get Pro version", "wow-marketings") ?></b></a></li>
<li><a href='admin.php?page=wow-modalsimple&wow=discount'><?php esc_attr_e("Get Discount", "wow-marketings") ?></a></li>
</ul>
<div class="wow_admin_signup">
<div class="wow-admin-col">
<div class="wow-admin-col-6">
<h2>GET DISCOUNTS & FREEBIES:</h2>
<div id="mc_embed_signup">
<form action="//wow-company.us14.list-manage.com/subscribe/post?u=126700b62c4c1e00f43226607&amp;id=d97a92dc65" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div class="mc-field-group">
<input type="email" value=""  placeholder="Enter your email here.." name="EMAIL" class="required email" id="mce-EMAIL">
<input class="wow_admin_green" name="subscribe" id="mc-embedded-subscribe" type="submit" value="Sign Up Now!">


</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>   
    <div class="clear"></div>
</form>


</div>
<!--End mc_embed_signup-->

</div>
<div class="wow-admin-col-6">
<h2>KEEP UP WITH THE INDUSTRY:</h2>
<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fwowaffect%2F&tabs&width=500&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=365329313856232" width="500" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
</div>
</div>

</div>