jQuery(document).ready(function($) { 
// Initialization ------------------------------------------------------------------------------
	var table = $("#post-list-table").dataTable({
		dom: 'Blfrtip',
		responsive: 'true',
		colReorder: 'true',
		buttons: [ 'colvis' ],
		order: [[ from_php.order_field, from_php.order_direction ]],
		pageLength: from_php.page_length
	}) 
	
// Filter --------------------------------------------------------------------------------------
	$(".plt_filter_input, .plt_filter_select").on( 'keyup change', function () { 		// Filter - search for text boxes
		var column_index = $(this).parent().index();
		var search_value = $(this).val();
		table.api().columns(column_index).search(search_value).draw();
	} );
	table.on( 'column-visibility.dt', function (  ) { 					// Hide/show filter elements on resize
		var api = table.api();
		responsiveFilter(api)
	} );	
	
	$("<button id='reset_btn' style='margin-left:10px'>Reset Table</button>").appendTo("div.dataTables_filter");
	$("#reset_btn").on('click', function(){
		document.location = from_php.reset_url;
	} );
} ); 

// Hide/show filter elements on table init ----------------------------------------------------
	jQuery(document).on( 'init.dt', function ( e, settings ) {
		var api = new jQuery.fn.dataTable.Api( settings );
		responsiveFilter(api)
	} );	

// Hidng filters for the hidden columns --------------------------------------------------------
	function responsiveFilter(api){
		api.columns().eq(0).each( function ( index ) {
			if(api.column( index ).visible()) jQuery(".filter_row th:eq("+index+")").show();
			else jQuery(".filter_row th:eq("+index+")").hide();
	} );
}