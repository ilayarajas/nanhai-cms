<?php
/*
	Plugin Name: 	Post List Table
	Plugin URI: 	http://gza.dev.ge
	Description: 	This plugin displays Wordpress posts list in a table with searching, sorting and filtering capabilities
	Version: 	1.2.0
	Author: 	GZLab
	Author URI: 	http://gza.dev.ge
*/

// don't load directly
if (!function_exists('is_admin')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

define( 'POST_LIST_TABLE_VERSION', '1.2.0' );
define( 'POST_LIST_TABLE_RELEASE_DATE', date_i18n( 'F j, Y', '1397937230' ) );
define( 'POST_LIST_TABLE_DIR', plugin_dir_path( __FILE__ ) );
define( 'POST_LIST_TABLE_URL', plugin_dir_url( __FILE__ ) );

if (!class_exists("Post_List_Table")) :

class Post_List_Table {
	var $settings, $options_page;
	
	function __construct() {
		global $wpdb;
		
		$wpdb->plt_tables = $wpdb->prefix . 'plt_tables';
		$wpdb->plt_tables_fields = $wpdb->prefix . 'plt_tables_fields';
				
		add_action('init', array($this,'init') );
		add_action('admin_menu', array($this,'admin_menu') );
		
		register_activation_hook( __FILE__, array($this,'activate') );
		register_deactivation_hook( __FILE__, array($this,'deactivate') );
		
		add_shortcode( 'post_list_table', array( 'Post_List_Table', 'table' ) );
	}

	function activate(){
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "
		CREATE TABLE {$wpdb->plt_tables} (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  name varchar(255) NOT NULL,
		  filter tinyint(1) NOT NULL,
		  filter_position enum('head','foot') NOT NULL,
		  page_length int(11) NOT NULL,
		  order_field varchar(100) NOT NULL,
		  order_direction varchar(5) NOT NULL,
		  PRIMARY KEY  (id)
		) $charset_collate;

		CREATE TABLE {$wpdb->plt_tables_fields} (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  table_id int(11) NOT NULL,
		  field_name varchar(50) NOT NULL,
		  field_label varchar(250) NOT NULL,
		  visible tinyint(1) NOT NULL,
		  link tinyint(1) NOT NULL,
		  PRIMARY KEY  (id)
		) $charset_collate;		
		";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
// Creating PLT initial settings - will be REMOVED from 2.0
		$wpdb->insert( $wpdb->plt_tables, array( 'name' => 'My First Table', 'filter' => '1', 'filter_position' => 'head' ));
		$wpdb->insert( $wpdb->plt_tables_fields, array( 'table_id' => '1', 'field_name' => 'Image', 'field_label' => 'Image', 'visible' => 1, 'link' => 1 ));
		$wpdb->insert( $wpdb->plt_tables_fields, array( 'table_id' => '1', 'field_name' => 'Title', 'field_label' => 'Title', 'visible' => 1, 'link' => 1 ));
		$wpdb->insert( $wpdb->plt_tables_fields, array( 'table_id' => '1', 'field_name' => 'Excerpt', 'field_label' => 'Excerpt', 'visible' => 0, 'link' => 0 ));
		$wpdb->insert( $wpdb->plt_tables_fields, array( 'table_id' => '1', 'field_name' => 'Categories', 'field_label' => 'Categories', 'visible' => 1, 'link' => 1 ));
		$wpdb->insert( $wpdb->plt_tables_fields, array( 'table_id' => '1', 'field_name' => 'Tags', 'field_label' => 'Tags', 'visible' => 1, 'link' => 1 ));
		$wpdb->insert( $wpdb->plt_tables_fields, array( 'table_id' => '1', 'field_name' => 'Date', 'field_label' => 'Date', 'visible' => 1, 'link' => 0 ));
	}
	
	function deactivate(){		
		if ( !current_user_can( 'install_plugins' ) )
		wp_die( 'You do not have permission to run this script.' );
	}

	function init() {} 
	
	function load_datatable(){
		global $settings;

		$columns_count = 0;
		foreach($settings->fields as $s){
			if($s->visible) $columns_count++;
		}

// START - GIA
		$plt_url = plugin_dir_url( __FILE__ );

		wp_enqueue_style( 'datatables.css', $plt_url . 'DataTables/datatables.min.css' );
		wp_enqueue_script( 'datatables.js', $plt_url . 'DataTables/datatables.min.js' );
		wp_enqueue_style( 'colreorder.css', $plt_url . 'DataTables/ColReorder-1.2.0/css/colReorder.dataTables.min.css' );
		wp_enqueue_script( 'colreorder.js', $plt_url . 'DataTables/ColReorder-1.2.0/js/dataTables.colReorder.min.js' );

		wp_enqueue_style( 'buttons.css', $plt_url . 'DataTables/Buttons-1.0.3/css/buttons.dataTables.min.css' );
		wp_enqueue_script( 'buttons.js', $plt_url . 'DataTables/Buttons-1.0.3/js/dataTables.buttons.min.js' );
		wp_enqueue_script( 'colvis.js', $plt_url . 'DataTables/Buttons-1.0.3/js/buttons.colVis.min.js' );

		wp_enqueue_style( 'post-list-table.css', $plt_url . 'post-list-table.css' );
		wp_enqueue_script( 'post-list-table.js', $plt_url . 'post-list-table.js' );				
// END - GIA

		//Find index of the field
		$order_field_index = $columns_count-1;
		$i=0;
		foreach($settings->fields as $f){
			if($f->visible){
				if($f->field_name == $settings->order_field){
					$order_field_index = $i;
					break;
				}
				$i++;
			}
		}

		wp_localize_script( 'post-list-table.js', 'from_php', array(
			'reset_url' => remove_query_arg( array('tag', 'cat') ), 
			'columns_count' => $columns_count, 
			'page_length' => ( isset($settings->page_length) ? $settings->page_length : 10 ),
			'order_field' => $order_field_index,
			'order_direction' => $settings->order_direction,
		));
	}

	function admin_menu() {
		add_menu_page( 'Post List Table Settings', 'Post List Table', 'manage_options', 'plt_admin_main', array($this, 'admin_main'), 'dashicons-grid-view' );
	}


	function admin_main(){
		global $wpdb;

		if(isset($_POST['_table']) && isset($_POST['_fields'])){
			//print_r($_POST); exit;

			//General settings for table
			$table_id = $_POST['table_id'];
			$wpdb->update( $wpdb->plt_tables, array(
				'name' 			=>	$_POST['_table']['name'],
				'filter'		=>	( isset($_POST['_table']['filter']) && $_POST['_table']['filter']=='on' ? 1 : 0 ),
				'filter_position' 	=>	$_POST['_table']['filter_position'],
				'page_length' 		=>	$_POST['_table']['page_length'],
				'order_field' 		=>	$_POST['_table']['order_field'],
				'order_direction' 	=>	$_POST['_table']['order_direction'],
				
			), array("id" => $table_id) );

			//Field settings
			foreach($_POST['_fields'] as $field=>$values){
				$field_settings = array(
					'field_label' 	=> $values['field_label'],
					'visible' 		=> ( isset($values['visible']) && $values['visible']=='on' ? 1 : 0 ),
					'link' 			=> ( isset($values['link']) && $values['link']=='on' ? 1 : 0 ),
				);

				$wpdb->update( $wpdb->plt_tables_fields, $field_settings, array("id" => $values['id']) );
			}
			
			//Category settings
			if(isset($_POST['_categories'])) update_option('gzlab_plt_categories', $_POST['_categories']);
			
			wp_redirect(get_admin_url(get_current_blog_id(), 'admin.php?page=plt_admin_main&msg=success'));
		}else{
			//if (isset($_GET['noheader'])) require_once(ABSPATH . 'wp-admin/admin-header.php'); 

			$message = '';
			if(isset($_GET['msg'])){
				if($_GET['msg'] == 'success') $message = __('Settings updated successfully.');
			}

			$table_id 	= 1; 					//Temp
			$settings 	= $this->get_table_settings(1); 
			$categories = get_categories(array('hide_empty' => 0,'order' => 'ASC'));
			$selected_categories = get_option( 'gzlab_plt_categories' ); 
			if(!$selected_categories){
				foreach($categories as $c){
					$selected_categories[]= $c->cat_ID;
				}
			}
			
			require_once 	'plt-admin.php';
		}
		exit;
	}


	function get_table_settings($table_id){
		global $wpdb;
		$table_settings = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->plt_tables} WHERE id = '%s'",$table_id));
		
		$table_fields = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->plt_tables_fields} WHERE table_id = '%s'",$table_id));
		$table_settings->fields = array();
		foreach($table_fields as $field){
			$table_settings->fields[$field->field_name] = $field;
		}
		return $table_settings;
	}


	function table($str, $print_info=TRUE) {
		global $wpdb, $post, $settings;

		$selected_categories = get_option( 'gzlab_plt_categories' );
		$filter = array('nopaging' => true, 'orderby' => 'date');
		
		if(isset($_GET['cat'])) $filter['category']= (int)$_GET['cat'];
		elseif($selected_categories)	$filter['category']= implode(',',$selected_categories);
		
		if(isset($_GET['tag'])) $filter['tax_query']= array(array('taxonomy' => 'post_tag', 'field' => 'slug', 'terms' => $_GET['tag']));
		$posts_array = get_posts($filter); 			//print_r($posts_array);
		
		$settings = self::get_table_settings(1);
		self::load_datatable();
		
		$table_columns = '';
		foreach($settings->fields as $s){
			$css_class = '';
			if($s->field_name == 'Excerpt') $css_class = 'class="plt_excerpt_td"';
			if($s->visible) $table_columns .= '<th '.$css_class.'>'.$s->field_label.'</th>';
		}

		//This is other filter. Works on client side
		$table_filters = ''; 
		if($settings->filter){			
			foreach($settings->fields as $s){
				if(!$s->visible) continue;
								
				$table_filters .= '<th>';				
				if($s->field_name == 'Image'){
					continue;
				}elseif( $s->field_name == 'Title' || $s->field_name == 'Excerpt' || $s->field_name == 'Date' ){
					$table_filters .= '<input class="plt_filter_input" type="text" />';
				}elseif($s->field_name == 'Categories'){
				
					$filter_categories = array();
					foreach($posts_array as $p){ //Get categories for all the selected posts
						$filter_categories = array_merge($filter_categories, wp_get_post_categories( $p->ID ));
					}
					$filter_categories = array_unique($filter_categories);
					
					$filter_categories_final = array();
					$filter_categories_final_obj = array();
					foreach($filter_categories as $cat){
						if($selected_categories){
							if(in_array($cat,$selected_categories)){	
								$filter_categories_final[]= $cat;
								$filter_categories_final_obj[$cat] = get_category( $cat );
							}
						}else{
							$filter_categories_final[]= $cat;
							$filter_categories_final_obj[$cat] = get_category( $cat );
						}
					}
					
					$generated_dropdown = wp_dropdown_categories( array( 'include' => $filter_categories_final, 'show_option_all' => __('All'), 'hierarchical' => true, 'class' => 'plt_filter_select', 'hide_empty' => false, 'echo'=>false) );
					$generated_dropdown = str_replace("value='0'",'value=""', $generated_dropdown);					
					foreach($filter_categories_final  as $fcf) 
						$generated_dropdown = str_replace('value="'.$fcf.'"','value="'.$filter_categories_final_obj[$fcf]->name.'"', $generated_dropdown);
					$table_filters .= $generated_dropdown;
				}elseif($s->field_name == 'Tags'){
					$filter_tags = array();
					foreach($posts_array as $p){ //Get categories for all the selected posts
						$filter_tags = array_merge_recursive($filter_tags, wp_get_post_tags( $p->ID ));
					}

					//Trick to array_unique the object
					$filter_tags = array_map('json_encode', $filter_tags); 
					$filter_tags = array_unique($filter_tags); 
					$filter_tags = array_map('json_decode', $filter_tags);
					
					$table_filters .= '<select class="plt_filter_select"><option value="">'.__('All').'</option>';	
					foreach($filter_tags as $tag){
						$table_filters .= '<option value="'.$tag->name.'">'.$tag->name.'</option>';
					}
					$table_filters .= '</select>';	
				}
				
				$table_filters .= '</th>';
				/*
				}elseif($s->visible && ( $s->field_name == 'Categories' || $s->field_name == 'Tags')){
					$table_filters .= '<th><select class="plt_filter_select"><option value="">'.__('All').'</option>';						

					if($s->field_name == 'Categories'){
						$filter_categories = array();
						foreach($posts_array as $p){ //Get categories for all the selected posts
							$filter_categories = array_merge($filter_categories, wp_get_post_categories( $p->ID ));
						}

						$filter_categories = array_unique($filter_categories);
						foreach($filter_categories as $cat){
							if($selected_categories){
								if(in_array($cat,$selected_categories)){
									$cat = get_category( $cat );
									$table_filters .= '<option value="'.$cat->name.'">'.$cat->name.'</option>';
								}
							}else{
								$cat = get_category( $cat );
								$table_filters .= '<option value="'.$cat->name.'">'.$cat->name.'</option>';
							}
						}
					}elseif($s->field_name == 'Tags'){
						$filter_tags = array();
						foreach($posts_array as $p){ //Get categories for all the selected posts
							$filter_tags = array_merge_recursive($filter_tags, wp_get_post_tags( $p->ID ));
						}

						//Trick to array_unique the object
						$filter_tags = array_map('json_encode', $filter_tags); 
						$filter_tags = array_unique($filter_tags); 
						$filter_tags = array_map('json_decode', $filter_tags);
						foreach($filter_tags as $tag){
							$table_filters .= '<option value="'.$tag->name.'">'.$tag->name.'</option>';
						}
					}
 
					
					
					$table_filters .= '</select></th>';	
				}
				*/
			}
			if(isset($p)) unset($p); //Clean up
		}

		$fpos = 't'.$settings->filter_position;

		$html = '
			<style type="text/css">
				.plt_img_td {width:100px;}
				.plt_img_td img {max-width:100px; max-height:100px}
				table.dataTable td.plt_excerpt_td {max-width:200px; white-space:normal; }
			</style>
			<table id="post-list-table" class="display compact" cellspacing="0" width="100%"> 
				<thead><tr>'.$table_columns.'</tr></thead>
				<'.$fpos.'><tr class="filter_row">'.$table_filters.'</tr></'.$fpos.'>
				<tbody>';
		$i = 0;
		foreach($posts_array as $p){
			if($p->ID == $post->ID) continue; 			//Exclude the post where the shortcode is inserted
			$row = array();

		// Image ----------------------------------------------------------------------------------
			$row['image'] = '';
			if (has_post_thumbnail( $p->ID ) ){
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $p->ID ) ); 
				if($settings->fields['Image']->link) $row['image'] = '<a href="'.get_permalink($p->ID).'"><img src="'.$image[0].'" /></a>';
				else $row['image'] = '<img src="'.$image[0].'" />';
			}

		// Title & Excerpt --------------------------------------------------------------------------
			$row['title'] = ( $settings->fields['Title']->link ? '<a href="'.get_permalink($p->ID).'">'.$p->post_title.'</a>' : $p->post_title );
			$row['excerpt'] = $p->post_excerpt;

		// Categories -----------------------------------------------------------------------------
			$row['categories'] = '';
			$categories = wp_get_post_categories( $p->ID ); 
			foreach($categories as $cat){
				$cat = get_category( $cat );
				if($selected_categories && !in_array($cat->cat_ID,$selected_categories))  continue;
				
				if($cat->category_parent != 0) $cat_name = '<i>'.$cat->name.'</i>';
				else $cat_name = $cat->name;
				
				if($settings->fields['Categories']->link)
					$row['categories'] .=  '<a href="'.add_query_arg( 'cat', $cat->cat_ID ).'">'.$cat_name.'</a>, ';
				else
					$row['categories'] .=  $cat_name.', ';
			}
			$row['categories'] = rtrim($row['categories'], ', ');
			
		// Tags -----------------------------------------------------------------------------------
			$row['tags'] = '';
			$tags = wp_get_post_tags( $p->ID ); //print_r($tags); exit;
			foreach($tags as $tag){
				if($settings->fields['Tags']->link)
					$row['tags'] .=  '<a href="'.add_query_arg( 'tag', $tag->slug ).'">'.$tag->name.'</a>, ';
				else
					$row['tags'] .=  $tag->name.', ';
			}
			$row['tags'] = rtrim($row['tags'], ', ');

		// Date -----------------------------------------------------------------------------------
			$row['date'] = get_the_date( get_option( 'date_format' ), $p->ID ); // GIA - preserve WP custom data format.

		// Build Table ----------------------------------------------------------------------------
			$html .= '<tr>';			
			if($settings->fields['Image']->visible) $html .= '<td class="plt_img_td">'.$row['image'] .'</td>';
			if($settings->fields['Title']->visible) $html .= '<td>' . $row['title'] . '</td>';
			if($settings->fields['Excerpt']->visible) $html .= '<td class="plt_excerpt_td">' . $row['excerpt'] .'</td>';
			if($settings->fields['Categories']->visible) $html .= '<td>' . $row['categories'] .'</td>';
			if($settings->fields['Tags']->visible) $html .= '<td>' . $row['tags'] .'</td>';
			if($settings->fields['Date']->visible) $html .= '<td>' . $row['date'] .'</td>';
			$html .= '</tr>';
			$i++;
		}

		$html .= '</tbody>
			</table>
		';
		return $html;
	}

} // end class
endif;

// Initializing our plugin object.
global $post_list_table;
	if (class_exists("Post_List_Table") && !$post_list_table) {
		$post_list_table = new Post_List_Table();
	}

?>