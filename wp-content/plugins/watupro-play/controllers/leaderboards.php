<?php
// generates and displays various leaderboards
class WatuPROPlayLeaderBoards {
	// Global leaderboards based on all tests
	static function global_leaderboard($atts) {
		global $wpdb;
		list($segment, $num, $criteria) = $atts;
		$dir = ($segment == 'top') ? 'DESC' : 'ASC';
		
		// logged in users or filled "name" field?
		$user_criteria = empty($atts['user_criteria']) ? 'user_login' : $atts['user_criteria'];
		$time_sql = self :: time_sql($atts);
		
		switch($criteria) {
			case 'points':			
				if($user_criteria == 'user_login') {	
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT SUM(tT.points) as points, tU.user_login as user_login,
						tU.display_name as display_name 
						FROM {$wpdb->users} tU JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.user_id = tU.ID $time_sql
						WHERE tT.in_progress = 0 GROUP BY tU.ID ORDER BY points $dir, tT.ID LIMIT %d", $num), ARRAY_A);
				}
				else {
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT SUM(tT.points) as points, tT.name as user_login,
						tT.name as display_name 
						FROM ".WATUPRO_TAKEN_EXAMS." tT
						WHERE tT.in_progress = 0 AND tT.name != '' $time_sql GROUP BY tT.name 
						ORDER BY points $dir, tT.ID LIMIT %d", $num), ARRAY_A);
				}		
				return self :: prepare_leaderboard($users, array(__('User', 'watuproplay'), __('Points', 'watuproplay')), array('display_name', 'points'));	
			break;
			
			case 'percent':		
				if($user_criteria == 'user_login') {			
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT AVG(tT.percent_correct) as avg_percent, tU.user_login as user_login,
						tU.display_name as display_name 
						FROM {$wpdb->users} tU JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.user_id = tU.ID $time_sql
						WHERE tT.in_progress = 0 GROUP BY tU.ID ORDER BY avg_percent $dir, tT.ID LIMIT %d", $num), ARRAY_A);
				}
				else {
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT AVG(tT.percent_correct) as avg_percent, tT.name as user_login,
						tT.name as display_name 
						FROM  ".WATUPRO_TAKEN_EXAMS." tT 
						WHERE tT.in_progress = 0 AND tT.name != '' $time_sql
						GROUP BY tT.name ORDER BY avg_percent $dir, tT.ID LIMIT %d", $num), ARRAY_A);
				}

				// round and add %
				foreach($users as $cnt=>$user) $users[$cnt]['avg_percent'] = round($user['avg_percent'], 2)."%";						
					
				return self :: prepare_leaderboard($users, array(__('User', 'watuproplay'), __('Avg. % Correct Answers', 'watuproplay')), array('display_name', 'avg_percent'));	
			break;
			
			case 'tests':		
				if($user_criteria == 'user_login') {			
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT COUNT(DISTINCT tT.exam_id) as num_exams, tU.user_login as user_login,
						tU.display_name as display_name 
						FROM {$wpdb->users} tU JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.user_id = tU.ID $time_sql
						WHERE tT.in_progress = 0 GROUP BY tU.ID ORDER BY num_exams $dir, tT.ID LIMIT %d", $num), ARRAY_A);
				}
				else {
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT COUNT(DISTINCT tT.exam_id) as num_exams, tT.name as user_login,
						tT.name as display_name 
						FROM ".WATUPRO_TAKEN_EXAMS." tT 
						WHERE tT.in_progress = 0 AND tT.name != '' $time_sql
						GROUP BY tT.name ORDER BY num_exams $dir, tT.ID LIMIT %d", $num), ARRAY_A);
				}
				return self :: prepare_leaderboard($users, array(__('User', 'watuproplay'), __('Tests Completed', 'watuproplay')), array('display_name', 'num_exams'));	
			break;
		}
	}

	// leaderboards for specific test	
	static function quiz_leaderboard($atts) {
		global $wpdb;
		list($segment, $num, $criteria, $quiz_id) = $atts;
		$dir = ($segment == 'top') ? 'DESC' : 'ASC';
		$time_sql = self :: time_sql($atts);
		
		// select quiz. If quiz asks for contact details we'll use that instead of user login
		$quiz = $wpdb->get_row($wpdb->prepare("SELECT * FROM " . WATUPRO_EXAMS. " WHERE ID=%d", $quiz_id));
		$advanced_settings = unserialize(stripslashes($quiz->advanced_settings));
		if(!empty($advanced_settings['ask_for_contact_details']) and !empty($advanced_settings['contact_fields']['name'])) $contact_mode = true;
		 
		switch($criteria) {
			case 'points':			
				if(empty($contact_mode)) {
				   // based on user login		
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT SUM(tT.points) as points, tU.user_login as user_login,
						tU.display_name as display_name 
						FROM {$wpdb->users} tU JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.user_id = tU.ID
						WHERE tT.in_progress = 0 AND tT.exam_id=%d $time_sql
						GROUP BY tU.ID ORDER BY points $dir, tT.ID LIMIT %d", $quiz_id, $num), ARRAY_A);
				}
				else {
					// based on name
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT SUM(tT.points) as points, tT.name as user_login,
						tT.name as display_name 
						FROM ".WATUPRO_TAKEN_EXAMS." tT
						WHERE tT.in_progress = 0 AND tT.exam_id=%d  AND tT.name != '' $time_sql
						GROUP BY tT.name ORDER BY points $dir, tT.ID LIMIT %d", $quiz_id, $num), ARRAY_A);
				}		
				return self :: prepare_leaderboard($users, array(__('User', 'watuproplay'), __('Points', 'watuproplay')), array('display_name', 'points'));
			break;
			
			case 'percent':
				if(empty($contact_mode)) {
					// based on user login	
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT AVG(tT.percent_correct) as avg_percent, tU.user_login as user_login,
						tU.display_name as display_name 
						FROM {$wpdb->users} tU JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.user_id = tU.ID
						WHERE tT.in_progress = 0 AND tT.exam_id=%d $time_sql
						GROUP BY tU.ID ORDER BY avg_percent $dir, tT.ID LIMIT %d", $quiz_id, $num), ARRAY_A);
				}
				else {
					// based on name
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT AVG(tT.percent_correct) as avg_percent, tT.name as user_login,
						tT.name as display_name
						FROM ".WATUPRO_TAKEN_EXAMS." tT
						WHERE tT.in_progress = 0 AND tT.exam_id=%d  AND tT.name != '' $time_sql
						GROUP BY tT.name ORDER BY avg_percent $dir, tT.ID LIMIT %d", $quiz_id, $num), ARRAY_A);
				}
					
				// round and add %
				foreach($users as $cnt=>$user) $users[$cnt]['avg_percent'] = round($user['avg_percent'], 2)."%";				
				return self :: prepare_leaderboard($users, array(__('User', 'watuproplay'), __('% Correct Answers', 'watuproplay')), array('display_name', 'avg_percent'));
			break;	
			case 'points_ungrouped':
			case 'percent_ungrouped':
				// this is a different leaderboard based on the last taking of each user. Makes most sense if the quiz allows only one taking per user
				// It will also display something like WP-Pro Quiz leaderboard
				$ob = ($criteria == 'points_ungrouped') ? "tT.points" : "tT.percent_correct";
				
				if(empty($contact_mode)) {	
					// based on user login	
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT tU.user_login as user_login, 
						tU.display_name as display_name,	
						tT.points as points, tT.percent_correct as percent_correct, 
						tT.start_time as start_time, tT.end_time as end_time, tT.date as date 
						FROM {$wpdb->users} tU JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.user_id = tU.ID
						WHERE tT.in_progress = 0 AND tT.exam_id=%d $time_sql
						ORDER BY $ob $dir, tT.ID LIMIT %d", $quiz_id, $num), ARRAY_A);
				}
				else {
					// based on name	
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT tT.name as user_login, 
						tT.name as display_name,	
						tT.points as points, tT.percent_correct as percent_correct, 
						tT.start_time as start_time, tT.end_time as end_time, tT.date as date 
						FROM ".WATUPRO_TAKEN_EXAMS." tT 
						WHERE tT.in_progress = 0 AND tT.exam_id=%d AND tT.name != '' $time_sql
						ORDER BY $ob $dir, tT.ID LIMIT %d", $quiz_id, $num), ARRAY_A);
				}
					
				// calculate time spent
				foreach($users as $cnt => $user) {
					$taking = (object)$user;
					$time_spent = WTPRecord :: time_spent_human(WTPRecord :: time_spent($taking));
					$users[$cnt]['time_spent'] = $time_spent;
				}	
				
				return self :: prepare_leaderboard($users, 
					array(__('User', 'watuproplay'), __('Date', 'watuproplay'), __('Points', 'watuproplay'), __('% Correct', 'watuproplay'),  __('Time Spent', 'watuproplay')), 
					array('display_name', 'date', 'points', 'percent_correct', 'time_spent'));
			break;	
		}
	}
	
	// leaderboard per exam category
	static function cat_leaderboard($atts) {
		global $wpdb;
		list($segment, $num, $criteria, $cat_id) = $atts;
		$dir = ($segment == 'top') ? 'DESC' : 'ASC';
		$time_sql = self :: time_sql($atts);		
		
		// logged in users or filled "name" field?
		$user_criteria = empty($atts['user_criteria']) ? 'user_login' : $atts['user_criteria'];
		
		switch($criteria) {
			case 'points':		
				if($user_criteria == 'user_login') {		
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT SUM(tT.points) as points, tU.user_login as user_login,
						tU.display_name as display_name, tE.cat_id as cat_id 
						FROM {$wpdb->users} tU JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.user_id = tU.ID
						JOIN ".WATUPRO_EXAMS." tE ON tE.ID = tT.exam_id
						WHERE tT.in_progress = 0 AND tE.cat_id = %d 
						GROUP BY tU.ID ORDER BY points $dir, tT.ID LIMIT %d", $cat_id, $num), ARRAY_A);
				}
				else {
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT SUM(tT.points) as points, tT.name as user_login,
						tT.name as display_name, tE.cat_id as cat_id 
						FROM ".WATUPRO_TAKEN_EXAMS." tT 
						JOIN ".WATUPRO_EXAMS." tE ON tE.ID = tT.exam_id
						WHERE tT.in_progress = 0 AND tE.cat_id = %d AND tT.name != '' 
						GROUP BY tT.name ORDER BY points $dir, tT.ID LIMIT %d", $cat_id, $num), ARRAY_A);
				}
				return self :: prepare_leaderboard($users, array(__('User', 'watuproplay'), __('Points', 'watuproplay')), array('display_name', 'points'));	
			break;
			
			case 'percent':		
				if($user_criteria == 'user_login') {				
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT AVG(percent_correct) as avg_percent, tU.user_login as user_login, 
						tU.display_name as display_name, tE.cat_id as cat_id 
						FROM {$wpdb->users} tU JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.user_id = tU.ID
						JOIN ".WATUPRO_EXAMS." tE ON tE.ID = tT.exam_id
						WHERE tT.in_progress = 0 AND tE.cat_id = %d 
						GROUP BY tU.ID ORDER BY avg_percent $dir, tT.ID LIMIT %d", $cat_id, $num), ARRAY_A);
				}
				else {
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT AVG(percent_correct) as avg_percent, tT.name as user_login, 
						tT.name as display_name, tE.cat_id as cat_id 
						FROM ".WATUPRO_TAKEN_EXAMS." tT 
						JOIN ".WATUPRO_EXAMS." tE ON tE.ID = tT.exam_id
						WHERE tT.in_progress = 0 AND tE.cat_id = %d AND tT.name!=''
						GROUP BY tT.name ORDER BY avg_percent $dir, tT.ID LIMIT %d", $cat_id, $num), ARRAY_A);
				}
				// round and add %
				foreach($users as $cnt=>$user) $users[$cnt]['avg_percent'] = round($user['avg_percent'], 2)."%";			
				return self :: prepare_leaderboard($users, array(__('User', 'watuproplay'), __('Avg. % Correct Answers', 'watuproplay')), array('display_name', 'avg_percent'));	
			break;
			
			case 'tests':	
				if($user_criteria == 'user_login') {				
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT COUNT(DISTINCT tT.exam_id) as num_exams, tU.user_login as user_login, 
						tU.display_name as display_name, tE.cat_id as cat_id 
						FROM {$wpdb->users} tU JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.user_id = tU.ID
						JOIN ".WATUPRO_EXAMS." tE ON tE.ID = tT.exam_id
						WHERE tT.in_progress = 0 AND tE.cat_id = %d 
						GROUP BY tU.ID ORDER BY num_exams $dir, tT.ID LIMIT %d", $cat_id, $num), ARRAY_A);
				}
				else {
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT COUNT(DISTINCT tT.exam_id) as num_exams, tT.name as user_login, 
						tT.name as display_name, tE.cat_id as cat_id 
						FROM ".WATUPRO_TAKEN_EXAMS." tT 
						JOIN ".WATUPRO_EXAMS." tE ON tE.ID = tT.exam_id
						WHERE tT.in_progress = 0 AND tE.cat_id = %d AND tT.name != '' 
						GROUP BY tT.name ORDER BY num_exams $dir, tT.ID LIMIT %d", $cat_id, $num), ARRAY_A);
				}
				return self :: prepare_leaderboard($users, array(__('User', 'watuproplay'), __('Tests Completed', 'watuproplay')), array('display_name', 'num_exams'));	
			break;
		}
	}
	
	// leaderboard per question category
	static function qcat_leaderboard($atts) {
		global $wpdb;
		list($segment, $num, $criteria, $qcat_id) = $atts;
		$dir = ($segment == 'top') ? 'DESC' : 'ASC';
		$time_sql = self :: time_sql($atts);
		
		// logged in users or filled "name" field?
		$user_criteria = empty($atts['user_criteria']) ? 'user_login' : $atts['user_criteria'];
		
		switch($criteria) {
			case 'points':		
				if($user_criteria == 'user_login') {			
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT SUM(tA.points) as points, tA.user_id as user_id, 
						tU.user_login as user_login, tU.display_name as display_name 
						FROM ".WATUPRO_STUDENT_ANSWERS." tA JOIN {$wpdb->users} tU ON tU.ID = tA.user_id
						JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.ID =tA.taking_id
						JOIN ".WATUPRO_QUESTIONS." tQ ON tQ.ID =tA.question_id					
						WHERE tT.in_progress = 0 AND tQ.cat_id = %d $time_sql
						GROUP BY tU.ID ORDER BY points $dir, tT.ID LIMIT %d", $qcat_id, $num), ARRAY_A);
				}
				else {
					$users = $wpdb -> get_results( $wpdb->prepare("SELECT SUM(tA.points) as points, tA.user_id as user_id, 
						tT.name as user_login, tT.name as display_name 
						FROM ".WATUPRO_STUDENT_ANSWERS." tA 
						JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.ID =tA.taking_id
						JOIN ".WATUPRO_QUESTIONS." tQ ON tQ.ID =tA.question_id					
						WHERE tT.in_progress = 0 AND tQ.cat_id = %d AND tT.name != '' $time_sql
						GROUP BY tT.name ORDER BY points $dir, tT.ID LIMIT %d", $qcat_id, $num), ARRAY_A);
				}
					
				return self :: prepare_leaderboard($users, array(__('User', 'watuproplay'), __('Points', 'watuproplay')), array('display_name', 'points'));	
			break;
			
			case 'percent':				
				// this is more specific case, we have to select all answers first
				if($user_criteria == 'user_login') {			
					$answers = $wpdb -> get_results( $wpdb->prepare("SELECT tA.is_correct as is_correct, 
						tU.user_login as user_login, tU.display_name as display_name,	tA.user_id as user_id  
						FROM ".WATUPRO_STUDENT_ANSWERS." tA JOIN {$wpdb->users} tU ON tU.ID = tA.user_id
						JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.ID =tA.taking_id
						JOIN ".WATUPRO_QUESTIONS." tQ ON tQ.ID =tA.question_id					
						WHERE tT.in_progress = 0 AND tQ.cat_id = %d $time_sql
						ORDER BY tA.ID", $qcat_id));
				}
				else {
					$answers = $wpdb -> get_results( $wpdb->prepare("SELECT tA.is_correct as is_correct, 
						tT.name as user_login, tT.name as display_name,	tT.name as user_id  
						FROM ".WATUPRO_STUDENT_ANSWERS." tA 
						JOIN ".WATUPRO_TAKEN_EXAMS." tT ON tT.ID =tA.taking_id
						JOIN ".WATUPRO_QUESTIONS." tQ ON tQ.ID =tA.question_id					
						WHERE tT.in_progress = 0 AND tQ.cat_id = %d AND tT.name != '' $time_sql
						ORDER BY tA.ID", $qcat_id));
				}		
				$uids = $users = array();			
								
				foreach($answers as $answer) {
					$uid = $answer->user_id;
					if(!in_array($uid, $uids)) {
						$uids[] = $uid;						
						$users[$uid] = array("display_name"=>$answer->display_name, "total_answers"=>0, "correct"=>0, "incorrect"=>0, 'percent' => 0);
					}
					
					$users[$uid]['total_answers']++;
					if($answer->is_correct) $users[$uid]['correct'] ++;
					else $users[$uid]['incorrect'] ++;
				}
				
				// now calculate percentage
				foreach($users as $key => $user) {
					if(empty($user['total_answers'])) continue; // percent remains 0
					
					$users[$key]['percent'] = round( ($user['correct']*100) / $user['total_answers'], 2);
				}
								
				// now sort them
				if($segment == 'top') uasort($users, array(__CLASS__, 'sort_qcat_leaderboard_top'));
				else uasort($users, array(__CLASS__, 'sort_qcat_leaderboard_bottom'));
				
				$users = array_slice($users, 0, $num);
				foreach($users as $cnt=>$user) $users[$cnt]['percent'] = $users[$cnt]['percent']."%";
					
				return self :: prepare_leaderboard($users, array(__('User', 'watuproplay'), __('Avg. % Correct Answers', 'watuproplay')), array('display_name', 'percent'));	
			break;
		}
	}
	
	// small helpers to sort the percentage based qcat leaderboard
	static function sort_qcat_leaderboard_top($a, $b) {
		if($a['percent'] == $b['percent']) return 0;
		return ($a['percent'] > $b['percent']) ? -1 : 1;
	}	
	static function sort_qcat_leaderboard_bottom($a, $b) {
		if($a['percent'] == $b['percent']) return 0;
		return ($a['percent'] < $b['percent']) ? -1 : 1;
	}
	
	// generates and returns the HTML code for the leaderboard
	static function prepare_leaderboard($users, $titles, $fields) {
		$output = "<table class='watupro-leaderboard' cellpadding='5' cellspacing='2'><tr>\n";

		foreach($titles as $title) {
			$output .= "<th>".$title."</th>";
		}
		$output.="</tr>";
		
		$cnt = 0;
		foreach($users as $user) {
			$cnt ++;
			$output .= "<tr>";
			foreach($fields as $fct=>$field) {
				$output .= "<td>";
				if($fct == 0) $output .= $cnt.". ";
				$output .= @$user[$field]."</td>";
			}
			$output .= "</tr>";
		}
		
		$output .= "</table>";
		
		return $output;
	}
	
	// generate time sql when time interval is requested. 
	// Possible options: all time (default) this month, last month, this week, last week, this year, last year
	static function time_sql($atts) {
		global $wpdb;
		if(empty($atts['period']) or $atts['period'] == 'all') return '';
		
		$time_sql == '';
		
		if($atts['period'] == 'this month') {
			$time_sql = $wpdb->prepare(" AND tT.date >= %s AND tT.date <= %s ", date("Y-m").'-01', date("Y-m-d"));
		}
		
		if($atts['period'] == 'last month') {
			$m = intval(date("m"));			
			$y = date("Y");
			if($m == 1) {
				$m = 12;
				$y--;
			}
			else $m--;
			
			$num_days = cal_days_in_month(CAL_GREGORIAN, $m, $y); 
			$time_sql = $wpdb->prepare(" AND tT.date >= %s AND tT.date <= %s ", $y.'-'.sprintf("%02d", $m).'-01', $y.'-'.sprintf("%02d", $m).'-'.sprintf("%02d", $num_days));
		}
		
		if($atts['period'] == 'this week') {
			$monday = date("Y-m-d", strtotime('Monday this week'));
			$time_sql = $wpdb->prepare(" AND tT.date >= %s AND tT.date <= %s ", $monday, date("Y-m-d"));
		}
		
		if($atts['period'] == 'last week') {
			$monday = date("Y-m-d", strtotime('Monday this week') - 7*24*3600);
			$time_sql = $wpdb->prepare(" AND tT.date >= %s AND tT.date <= %s ", $monday, date("Y-m-d", strtotime('last Sunday')));
		}
		
		if($atts['period'] == 'this year') {			
			$time_sql = $wpdb->prepare(" AND tT.date >= %s AND tT.date <= %s ", date("Y").'-01-01', date("Y-m-d"));
		}
		
		if($atts['period'] == 'last year') {			
			$time_sql = $wpdb->prepare(" AND tT.date >= %s AND tT.date <= %s ", (date("Y")-1).'-01-01', (date("Y")-1).'-12-31');
		}
		
		// echo $time_sql;
		return $time_sql;
	}
}