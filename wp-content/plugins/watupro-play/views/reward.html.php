<div class="wrap">
	<h1><?php _e('Add/Edit Reward Item', 'watuproplay')?></h1>
	
	<form method="post" class="watupro" onsubmit="return validateRewardForm(this);">
	<div class="wp-admin watupro-padded postbox">
		<fieldset>
			<legend><h2><?php _e('Item Details', 'watupro')?></h2></legend>
			<p><label><?php _e('Item title:', 'watuproplay')?></label> <input type="text" name="title" value="<?php echo @$reward->title?>" size="30"></p>					
			<p><label><?php _e('Optional description, image etc', 'watuproplay')?></label> <?php wp_editor(stripslashes(@$reward->description), 'description')?></p>
			<p><label><?php _e('Cost in points:', 'watuproplay')?></label> <input type="text" name="points" value="<?php echo @$reward->points?>" size="8"></p>			
			<p><input type="checkbox" name="tangible" value="1" <?php if(!empty($reward->tangible)) echo 'checked'?>> <?php _e('This is a tangible item', 'watuproplay')?></p>
			
			<p><label><?php _e('Available quantity:', 'watuproplay')?></label> <input type="text" name="qty" value="<?php echo @$reward->qty?>" size="8">
			<span class="watupro-help"><?php _e('For unlimited quantity (intangible items) enter -1. When quantity is 0 the item will not be shown for redeeming.', 'watuproplay')?></span></p>
		</fieldset>
		
		
		<p align="center"><input type="submit" value="<?php _e('Save Reward Item', 'watuproplay')?>"> 
		<?php if(!empty($reward->id)):?>
		<input type="button" value="<?php _e('Delete Item', 'watuproplay')?>" onclick="watuproplayDeleteReward(this.form);">
		<input type="hidden" name="del" value="0">
		<?php endif;?></p>
	</div>
	<input type="hidden" name="ok" value="1">
	</form>
</div>

<script type="text/javascript" >
function validateRewardForm(frm) {
	if(frm.title.value=='') {
		alert("<?php _e('Please enter title for this item', 'watuproplay')?>");
		frm.title.focus();
		return false;
	}
	
	if(frm.points.value=='' || isNaN(frm.points.value) || frm.points.value<=0) {
		alert("<?php _e('Please enter positive number for cost in points', 'watuproplay')?>");
		frm.points.focus();
		return false;
	}
	
	return true;
}

function watuproplayDeleteReward(frm) {
	if(confirm("<?php _e('Are you sure?', 'watuproplay')?>")) {
		frm.del.value=1;
		frm.submit();
	}
}
</script>