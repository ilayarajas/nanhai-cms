<?php global $plugin_page; ?>
<div class="admin-wrapper">
    <div class="admin-section">
        <div class="admin-section-header">
            <h2><?php _e("Add/Edit Category","team-awesome");?></h2>
            <p><?php _e("Create new or edit existing category","team-awesome");?></p>
        </div>

        <div class="admin-section-body">
            <form id="team-awesome-form-save" action="?page=<?php echo $plugin_page; ?>&view=list" method="POST">
                <?php
                /*
                 *  $category - Category Object Array ($_GET["catid"])
                 *  if $category - update table;
                 *  else - create new category;
                 * 
                 */
                $category = TEAM_AWESOME_ADMIN::get_category();
                ?>

                <div class="opt">
                    <span><?php _e("Category Title","team-awesome");?></span>
                    <span class="caption-note">(<?php _e("Required!","team-awesome");?>)</span><br />
                    <input required type="text" name="title" value="<?php echo $category ? $category->title : "" ?>"/>
                    <p>(<?php _e("Name of the category","team-awesome");?>)</p>
                </div>

                <input type="hidden" name="task" value="create" />
                <?php if ($category) : ?><input type="hidden" name="update" value="<?php echo $category->id; ?>" /><?php endif; ?>
                <?php if ($category) : ?><button><?php _e("Update Category","team-awesome");?></button><?php else: ?><button><?php _e("Save Category","team-awesome");?></button><?php endif; ?>

            </form>
            <button onclick="window.location.href = '?page=<?php echo $plugin_page; ?>&view=list';"><?php _e("Cancel","team-awesome");?></button>
        </div>
    </div>
</div>
