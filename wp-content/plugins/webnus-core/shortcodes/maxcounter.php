<?php
function michigan_webnus_maxcounter( $attributes, $content = null ) {
	extract(shortcode_atts(array(
		"type"			=> '1',
		"icon"			=> '',
		"color"			=> '',
		"count"			=> '',
		"prefix"		=> '',
		"suffix"		=> '',
		"title"			=> '',
		"border_right"	=> '',
	), $attributes));
	
	switch($type){
		case 1:
		 	$class = 'm-counter';
		break;      
		case 2:     
			$class = 's-counter';			
		break;      
		case 3:     
			$class = 'o-counter';			
		break;      
		case 4:     
			$class = 'w-counter';			
		break;
	}
	
	$w_border =	($border_right)?' w-border-right':'';
	$w_color =	($color)?'style="color:'. $color. '"':'';
	$w_icon =	($icon)?'<i class="icon-counter '. $icon .'" '.$w_color.'></i>':'';
	$w_prefix =	($prefix)?'<span class="pre-counter">'. $prefix .'</span>':'';
	$w_count =	($count)?'<span class="max-count">'. $count .'</span>':'';
	$w_suffix =	($suffix)?'<span class="suf-counter">'. $suffix .'</span>':'';
	$w_title =	($title)?'<h5>'. $title .'</h5>':'';
	
	$out = '<div class="max-counter '.$class . $w_border.'" data-effecttype="counter" data-counter="' . $count . '">'.$w_icon.$w_prefix.$w_count.$w_suffix.$w_title.'</div>';
	return $out;
}
add_shortcode('maxcounter', 'michigan_webnus_maxcounter');		
?>