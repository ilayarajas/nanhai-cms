<p><input type="checkbox" name="accept_paypoints" <?php if($accept_paypoints) echo 'checked';?> value="1" onclick="this.checked ? jQuery('#watuproPayPoints').show() : jQuery('#watuproPayPoints').hide();"> <?php _e('Accept points earned by taking exams', 'watuproplay')?></p>

<div id="watuproPayPoints" style="display:<?php echo $accept_paypoints ? 'block' : 'none';?>">
	<p><label><?php printf(__('Cost of 1 %s in points:', 'watuproplay'), $currency)?></label> <input type="text" name="paypoints_price" value="<?php echo get_option('watupro_paypoints_price')?>" size="6"></p>
	<p><b><?php _e('Design of the payment button.', 'watuproplay')?></b>
	<?php _e('You can use HTML and the following codes:', 'watuproplay')?> {{{points}}} <?php _e('for the price in points,', 'watuproplay')?> {{{button}}} <?php _e('for the payment button itself and', 'watuproplay')?> {{{user-points}}} <?php _e('to display the currently logged user points balance.', 'watuproplay')?></p>
	<p><textarea name="paypoints_button" rows="7" cols="50"><?php echo stripslashes($paypoints_button)?></textarea></p>
	<hr>	
</div>

