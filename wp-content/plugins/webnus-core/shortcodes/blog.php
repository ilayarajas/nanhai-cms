<?php
function michigan_webnus_blog($attributes, $content = null){
	extract(shortcode_atts(array(
	'type'=>'1',
	'category'=>'',
	'author'=>'',
	'count'=>'',
	), $attributes));
	ob_start();
	$p_count = '0';
	$paged = ( is_front_page() ) ? 'page' : 'paged' ;
	$michigan_webnus_last_time = get_the_time(' F Y'); $michigan_webnus_i=1; $michigan_webnus_flag = false; //timeline
	$args = array(
		   'orderby'=>'date',
		   'order'=>'desc',
		   'post_type'=>'post',
		   'paged' => get_query_var($paged),
		   'category_name' => $category,
		   'author_name' => $author,
		   'posts_per_page'=> $count,
	); 	
	$query = new WP_Query($args);
	if ($type == 6){ 
		echo'<section id="main-content-pin"><div class="container"><div id="pin-content">';
	}elseif ($type == 7){ 
		echo'<section id="main-content-timeline"><div class="container"><div id="tline-content">';
	}
	if ($query ->have_posts()) :
		if ($type == 3)
			echo '<div class="row">';
	while ($query -> have_posts()) : $query -> the_post();
	switch($type){
		case 2:
			get_template_part('parts/blogloop','type2');
		break;
		case 3:
			get_template_part('parts/blogloop','type3');
		break;
		case 4:
			if($p_count == '0')
				get_template_part('parts/blogloop');
			else
				get_template_part('parts/blogloop','type2');
			$p_count++;
		break;
		case 5:
			if($p_count == '0'){
				get_template_part('parts/blogloop');
				echo '<div class="row">';
			}else
				get_template_part('parts/blogloop','type3');
			$p_count++;
		break;
		case 6:
			get_template_part('parts/blogloop','masonry');
		break;
		case 7:
			global $post;
			$post_format = get_post_format(get_the_ID());	
			$content = get_the_content();	
			if( !$post_format ) $post_format = 'standard';			
			if(($michigan_webnus_last_time != date(' F Y',strtotime($post->post_date)) ) || $michigan_webnus_i==1){
				$michigan_webnus_last_time = date(' F Y',strtotime($post->post_date));
				echo '<div class="tline-topdate">'.  date(' F Y',strtotime($post->post_date)) .'</div>';
				if( $michigan_webnus_i>1 ) $michigan_webnus_flag = true;
			} ?>
				<article id="post-<?php the_ID(); ?>"  class="tline-box <?php if(($michigan_webnus_i%2)==0 && $michigan_webnus_flag) { $michigan_webnus_flag = false; $michigan_webnus_i++; } elseif( ($michigan_webnus_i%2)==0 ) echo ' rgtline'; ?>"> <span class="tline-row-<?php if(($michigan_webnus_i%2)==0) echo 'r'; else echo 'l'; ?>"></span>
				<div class="tline-author-box">
				<?php echo get_avatar( get_the_author_meta( 'user_email' ), 90 ); ?>
				<h6 class="tline-author">
				<?php the_author_posts_link(); ?>
				</h6>
				<h6 class="tline-date"><?php esc_html_e('Posted at: ','michigan'); ?><?php echo get_the_date('d M Y');?> </h6>
				</div>
				 <?php
				if(  michigan_webnus_options::michigan_webnus_blog_featuredimage_enable() ){
					$meta_video = rwmb_meta( 'michigan_featured_video_meta' );
					if( 'video'  == $post_format || 'audio'  == $post_format){
						$pattern = '\\[' . '(\\[?)' . "(gallery)" . '(?![\\w-])' . '(' . '[^\\]\\/]*' . '(?:' . '\\/(?!\\])' . '[^\\]\\/]*' . ')*?' . ')' . '(?:' . '(\\/)' . '\\]' . '|' . '\\]' . '(?:' . '(' . '[^\\[]*+' . '(?:' . '\\[(?!\\/\\2\\])' . '[^\\[]*+' . ')*+' . ')' . '\\[\\/\\2\\]' . ')?' . ')' . '(\\]?)';
						preg_match('/'.$pattern.'/s', $post->post_content, $matches);
						if( (is_array($matches)) && (isset($matches[3])) && ( ($matches[2] == 'video') || ('audio'  == $post_format)) && (isset($matches[2]))){
							$video = $matches[0];
							echo do_shortcode($video);
							$content = preg_replace('/'.$pattern.'/s', '', $content);
						}else				
						if( (!empty( $meta_video )) ){
							echo do_shortcode($meta_video);
						}
					}else
					if( 'gallery'  == $post_format){			
						$pattern = '\\[' . '(\\[?)' . "(gallery)" . '(?![\\w-])' . '(' . '[^\\]\\/]*' . '(?:' . '\\/(?!\\])' . '[^\\]\\/]*' . ')*?' . ')' . '(?:' . '(\\/)' . '\\]' . '|' . '\\]' . '(?:' . '(' . '[^\\[]*+' . '(?:' . '\\[(?!\\/\\2\\])' . '[^\\[]*+' . ')*+' . ')' . '\\[\\/\\2\\]' . ')?' . ')' . '(\\]?)';
						preg_match('/'.$pattern.'/s', $post->post_content, $matches);
						if( (is_array($matches)) && (isset($matches[3])) && ($matches[2] == 'gallery') && (isset($matches[2])))
						{
							$ids = (shortcode_parse_atts($matches[3]));
							if(is_array($ids) && isset($ids['ids']))
								$ids = $ids['ids'];
							echo do_shortcode('[vc_gallery onclick="link_no" img_size= "full" type="flexslider_fade" interval="3" images="'.$ids.'"  custom_links_target="_self"]');
							$content = preg_replace('/'.$pattern.'/s', '', $content);
						}
					}else
						get_the_image( array( 'meta_key' => array( 'Thumbnail', 'Thumbnail' ), 'size' => 'michigan_webnus_blog2_img' )  ); 
				} ?> <br>
				  <div class="tline-ecxt">
					<?php if(  michigan_webnus_options::michigan_webnus_blog_meta_category_enable() ):?>
					<h6 class="blog-cat-tline"> <?php the_category('- ') ?></h6>
					<?php endif; ?>
			  <?php
				if(michigan_webnus_options::michigan_webnus_blog_posttitle_enable() ) { 
					if( ('aside' != $post_format ) && ('quote' != $post_format)  ) { 	
						if( 'link' == $post_format ) { 
						 preg_match('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i', $content,$matches);
						 $content = preg_replace('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i','', $content,1);
						 $link ='';
						if(isset($matches) && is_array($matches))
						$link = $matches[0];	
					?>
					<h4><a href="<?php echo esc_url($link); ?>"><?php the_title(); ?></a></h4>
				<?php
				}else{
			  ?>
			  <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			  <?php } } } ?>
				  </div><p>
				  <?php 
				  add_filter( 'excerpt_more', '__return_false' );
			  if( 0 == michigan_webnus_options::michigan_webnus_blog_excerptfull_enable()  ){
					if( 'quote' == $post_format  ) echo '<blockquote>';
					echo michigan_webnus_excerpt(36);
					if( 'quote' == $post_format  ) echo '</blockquote>';
				} 
			  else{
					if( 'quote' == $post_format  ) echo '<blockquote>';
					echo apply_filters('the_content',$content);
					if( 'quote' == $post_format  ) echo '</blockquote>';
				}
			?>
				  </p>
				   <?php
					 if(michigan_webnus_options::michigan_webnus_blog_meta_comments_enable()) {
					?>
				  <div class="blog-com-sp"> <?php comments_popup_link(esc_html__('No Comments','michigan') . ' &#187;', esc_html__('1 Comment','michigan').' &#187;', esc_html__('% Comments','michigan').' &#187;'); ?></div>
				  <?php } ?>
			</article>
			<?php $michigan_webnus_i++;
		break;
		default:
			get_template_part('parts/blogloop'); //type 1
		break;
		}
	endwhile;
	if($type == 3 || $type == 5 || $type == 6)
		echo '</div>';
	elseif($type == 7) // for timeline
		echo'<div class="tline-topdate enddte">'. get_the_time(' F Y') .'</div></div></div>';
	endif;

	if(function_exists('wp_pagenavi')) {
		wp_pagenavi( array( 'query' => $query ) );
	}

	$out = ob_get_contents();
	ob_end_clean();	
	wp_reset_postdata();
	return $out;
}
add_shortcode("blog", "michigan_webnus_blog");
?>