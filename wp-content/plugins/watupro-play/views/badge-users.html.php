<div class="wrap">
	<h1><?php printf(__('Users who earned badge "%s"', 'watuproplay'), $badge->name)?></h1>
	
	<p><a href="admin.php?page=watuproplay_badges"><?php _e('Back to badges', 'watuproplay')?></a></p>
	
	<?php if(empty($users)): // in case we came by direct link?>
		<p><?php _e('There are no users to show.', 'watuproplay');?></p>
	<?php endif;?>
	
	<table class="widefat">
		<tr><th><?php _e('User name', 'watuproplay')?></th><th><?php _e('Badge earned on', 'watuproplay')?></th><th><?php _e('Details', 'watuproplay')?></th>
			<th><?php _e('Delete', 'watuproplay');?></th></tr>
		<?php foreach($users as $user):
			$class = ('alternate' == @$class) ? '' : 'alternate';?>
			<tr class="<?php echo $class?>"><td><?php echo $user->display_name?></td><td><?php echo !empty($user->assigned_badge_timestamp)  ? date($dateformat, $user->assigned_badge_timestamp) : __('n/a', 'watuproplay');?></td>
			<td><?php if(empty($user->assigned_badge_taking_id)): _e('n/a', 'watuproplay'); else:?>
				<a href="admin.php?page=watupro_takings&exam_id=<?php echo $user->assigned_badge_exam_id?>&taking_id=<?php echo $user->assigned_badge_taking_id?>" target="_blank"><?php echo $user->assigned_badge_exam_name?></a>
			<?php endif;?></td>
			<td><a href="#" onclick="WatuPROPlayDeleteBadge(<?php echo $user->ID?>);return false;"><?php _e('Delete', 'watuproplay');?></a></td></tr>
		<?php endforeach;?>
	</table>
</div>

<script type="text/javascript" >
function WatuPROPlayDeleteBadge(id) {
	if(confirm("<?php _e('Are you sure?', 'watuproplay')?>")) {
		window.location = "<?php echo html_entity_decode(wp_nonce_url('admin.php?page=watuproplay_badge_users&id='.$badge->id, 'watuproplay_userbadges')); ?>&del=1&user_id=" + id;
	}
}
</script>